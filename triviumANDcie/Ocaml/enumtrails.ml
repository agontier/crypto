open List
type noeud = A of int | B of int | C of int
type tree = Leaf of noeud
	     | OR of noeud*tree list
	     | AND of tree list

let rmax = 352
let eq n1 n2 = match n1 with(*tool functions*)
  | A _ -> begin match n2 with A _ -> true | _ -> false end
  | B _ -> begin match n2 with B _ -> true | _ -> false end
  | C _ -> begin match n2 with C _ -> true | _ -> false end 
let rec isin n l = match l with [] -> false | a::tl -> if eq a n then true else isin n tl
let rec getdiff l = match l with [] -> []
  | a::[] -> begin match a with | A (_) -> [A 0] | B (_) -> [B 0] | C (_) -> [C 0] end
  | a::b::tl -> begin match b with | A (r) | B (r) | C (r) ->
    begin match a with | A (rr) -> A (rr-r) | B (rr) -> B (rr-r) | C (rr) -> C (rr-r) end end::getdiff (b::tl)
let rec islong ch = match ch with [] | _::[] -> false
  | a::b::[] -> begin match b with | A (r) | B (r) | C (r) ->
    begin match a with | A (rr) -> rr-r=111 | B (rr) -> rr-r=93 | C (rr) -> rr-r=84 end end
  | _::tl -> islong tl

(*Build the upsidedown Trivium tree*)
let rec mkt n ch = if isin n ch then cycle n (getdiff (ch@[n])) else let ch = ch@[n] in match n with 
  | A r -> if r < 1 then Leaf (A r) else OR (A r,bouclea r ch mat::transia r ch mkt)
  | B r -> if r < 1 then Leaf (B r) else OR (B r,boucleb r ch mbt::transib r ch mkt)
  | C r -> if r < 1 then Leaf (C r) else OR (C r,bouclec r ch mct::transic r ch mkt)
(*Trivium transitions*)
and transia r ch f = f (C (r-66)) ch:: f (C (r-111)) ch::if islong ch then [] else [AND (x2a r)]
and transib r ch f = f (A (r-66)) ch:: f (A (r- 93)) ch::if islong ch then [] else [AND (x2b r)]
and transic r ch f = f (B (r-69)) ch:: f (B (r- 84)) ch::if islong ch then [] else [AND (x2c r)]
and bouclea r ch f = f (A (r-69)) ch
and boucleb r ch f = f (B (r-78)) ch
and bouclec r ch f = f (C (r-87)) ch
and x2a r = [mkt (C (r-109)) []; mkt (C (r-110)) []]
and x2b r = [mkt (A (r- 91)) []; mkt (A (r- 92)) []]
and x2c r = [mkt (B (r- 82)) []; mkt (B (r- 83)) []]
(*Added rules*)
and getr n ch = match ch with [] -> failwith "getr" | nn::tl -> if eq n nn then match nn with A (r) | B (r) | C (r) -> r else getr n tl
and cycle n ch = let rr = getr n ch in match n with
  | A r -> if r < 1 then Leaf (A r) else OR (A r,cycle (C (r-rr)) ch::[AND (x2a r)])
  | B r -> if r < 1 then Leaf (B r) else OR (B r,cycle (A (r-rr)) ch::[AND (x2b r)])
  | C r -> if r < 1 then Leaf (C r) else OR (C r,cycle (B (r-rr)) ch::[AND (x2c r)])
and mat n ch = match n with
  | A r -> if r < 1 then Leaf (A r) else OR (A r,bouclea r ch mat::transia r ch mat)
  | B r -> if r < 1 then Leaf (B r) else OR (B r,boucleb r ch mat::[AND (x2b r)])
  | C r -> if r < 1 then Leaf (C r) else OR (C r,bouclec r ch mat::transic r ch mat)
and mbt n ch = match n with
  | A r -> if r < 1 then Leaf (A r) else OR (A r,bouclea r ch mbt::transia r ch mbt)
  | B r -> if r < 1 then Leaf (B r) else OR (B r,boucleb r ch mbt::transib r ch mbt)
  | C r -> if r < 1 then Leaf (C r) else OR (C r,bouclec r ch mbt::[AND (x2c r)])
and mct n ch = match n with
  | A r -> if r < 1 then Leaf (A r) else OR (A r,bouclea r ch mct::[AND (x2a r)])
  | B r -> if r < 1 then Leaf (B r) else OR (B r,boucleb r ch mct::transib r ch mct)
  | C r -> if r < 1 then Leaf (C r) else OR (C r,bouclec r ch mct::transic r ch mct)
let maketree n = mkt n []

type form = X of int | T | F | OU of form list | ET of form list
let rec formula t = match t with (*tree to formula*)
  | Leaf n ->  begin match n with
      | A r -> if -r+1>80 then F else X (-r+1)
      | B r -> if -r+1>80 then F else X (-r+1+93)
      | C r -> if -r+1+177<286 then F else T(*X (-r+1+177)*) end
  | AND l -> ET (map formula l)
  | OR (_,l) -> OU (map formula l)

(*formula to string*)
let rec pft f = match f with
  | X r -> "x"^string_of_int r
  | T -> "1"
  | F -> "0"
  | ET l -> begin match l with [X _;X _] -> pft (hd l)^pft (hd (tl l))| _ -> "("^pft (hd l)^pfan (tl l) end
  | OU l -> pft (hd l)^pfor (tl l)
and pfan l = match l with [] -> ")" | a::tl -> ")("^pft a^pfan tl
and pfor l = match l with [] -> ""  | a::tl -> "+" ^pft a^pfor tl

open Printf 
let printformula f s =
  let ss = "formula/trivium"^s^".tex" in
  let fic = open_out ss in 
  let _ = fprintf fic "%s" (pft f) in
  close_out fic

let printvar a = match a with
  | X r -> "x"^string_of_int r | T -> "" | F | _ -> failwith "false printvar"
let rec printmon l = match l with [] -> "" | [T] -> "T" | a::T::[] -> printvar a |  a::tl -> printvar a^printmon tl
let rec printanfaux l = match l with [] -> "" | a::[] -> printmon a | a::tl -> printmon a^"+ "^printanfaux tl
let printanf f s =
  let ss = "formula/ANFtrivium"^s^".tex" in
  let fic = open_out ss in 
  let _ = fprintf fic "%s" (printanfaux f) in
  close_out fic
    
(*formula analysis*)
let rec ist l = match l with [] -> false | a::tl -> begin match a with T -> true | _ -> ist tl end
let rec isf l = match l with [] -> false | a::tl -> begin match a with F -> true | _ -> isf tl end
let rec remt l =match l with [] -> [] | a::tl -> begin match a with T -> remt tl | _ -> a::remt tl end
let rec remf l =match l with [] -> [] | a::tl -> begin match a with F -> remf tl | _ -> a::remf tl end
let rec rempairtf l t = match l with [] -> if t then [T] else [] | a::tl -> begin match a with
    F -> rempairtf tl t | T -> if t then rempairtf tl false else rempairtf (tl) true | _ -> a::rempairtf tl t end
let etf l = if isf l then F else if ist l then match remt l with [] -> T | a::[] -> a | l -> ET l else ET l
let otf l = match rempairtf l false with [] -> F | a::[] -> a | ll -> OU ll
let rec an f = match f with (*removes 0 and 1 in a formula*)
  | X r -> X r
  | T -> T
  | F -> F
  | ET l -> etf (map an l)
  | OU l -> otf (map an l)

(*sub formula*)
let rec arri ary f cpt = match f with (*removes all small monomials*)(*faux aussi?*)
  | X r -> if cpt > ary then X r else F
  | T -> T
  | F -> F
  | ET l -> ET (map (fun x -> arri ary x (cpt*2)) l)
  | OU l -> OU (map (fun x -> arri ary x  cpt   ) l)
let arrity ary f = arri ary f 1

let concat l = (*Concaténation d'un AND de OR en OR de AND*) 
  match l with []-> [] | l1::[]-> l1 | l1::tl -> fold_left (fun l1 l2 -> flatten (map (fun x -> map (fun y -> x@y) l1) l2)) l1 tl 

let rec inl a l = match l with [] -> false | aa::tl -> a=aa or inl a tl
let rec addmon a b = match a with [] -> b | aa::tl -> if inl aa b then addmon tl b else addmon tl (aa::b)
let concat3 l1 l2 = flatten (map (fun x -> map (fun y -> addmon x y) l1) l2)

let rec anf t = match t with
    X n -> [[X n]]
  | T -> [[T]]
  | F -> [[F]]
  | ET l -> begin match l with a::[] -> anf a
      | [l1;l2] -> concat3 (anf l1) (anf l2)
      | _ -> failwith "false anf" end
  | OU l -> flatten (map anf l)

let compare a b = match a with X aa -> begin match b with X bb -> aa-bb | _ -> -300 end | _ -> 300
let size l = length l > 0

  
let t = maketree (A rmax);;
let b = (an (arrity 0 (formula t)));;
let _ = printformula b ("A"^string_of_int rmax^"arrity>0x2long")
let a = anf b;;
let aa = filter size a;;
let aaa = map (fun x -> sort compare x) aa;;
let _ = printanf aaa ("A"^string_of_int rmax^"arrity>0x2long")

(*
let _ = printformula (an (arrity (formula t))) "arrity>77"(*
let _ = printformula (an (remsf (msfiv (an (formulaF t))))) "K1IV0"(*
let _ = printformula (an (formula t)) "IV0"
let _ = printformula (an (formulaIV t)) "IV1"

(*
x94x95x96x97x98x99x100x101x102x103x104x105x106x107x108x109x110x111x112x113x114x115x116x117x118x119x120x121x122x123x124x125x126x127x128x129x130x131x132x133x134x135x136x137x138x139x140x141x142x143x144x145x146x147x148x149x150x151x152x153x154x155x156x157x158x159x160x161x162x163x164x165x166x167x168x169x170x171x172
let a = concat [[[1]];[[2]];[[1];[2];[3]]]
let iv = [1]

let rec inl a l = match l with [] -> false | aa::tl -> a=aa or inl a tl
let rec addmon a b = match a with [] -> b | aa::tl -> if inl aa b then addmon tl b else addmon tl (aa::b)
let rec addtl a l = match l with [] -> [] | b::tl -> (addmon a b)::(addtl a tl)
let rec concat2 l1 l2 = match l1 with [] -> l2 | a::tl -> concat2 tl l2 @ addtl a l2

let rec formulaF t = match t with (*tree to formula*)
  | Leaf n ->  begin match n with
      | A r -> if -r+1>80 then F else X (-r+1)
      | B r -> F
      | C r -> if -r+1+177<286 then F else T end
  | AND l -> ET (map formulaF l)
  | OR (_,l) -> OU (map formulaF l)
let rec iniv r iv = match iv with [] -> false | a::tl -> a==r or iniv r tl
let rec formulaIV t = match t with (*tree to formula with one IV bit true*)
  | Leaf n ->  begin match n with
      | A r -> if -r+1>80 then F else X (-r+1)
      | B r -> if iniv (-r+1) iv then T else F
      | C r -> if -r+1+177<286 then F else T end
  | AND l -> ET (map formulaIV l)
  | OR (_,l) -> OU (map formulaIV l)


type sfiv = XX of bool*int | TT | FF | OUU of bool*sfiv list | ETT of bool*sfiv list
let issf f = match f  with | TT | FF -> false | XX (b,_) | ETT (b,_) | OUU (b,_) -> b
let rec bsf fl = match fl with [] -> false | a::tl -> issf a or bsf tl
let etsf l = ETT (bsf l,l)
let ousf l = OUU (bsf l,l)
let rec msfiv f = match f with
  | X r -> XX (r=1,r)
  | T -> TT
  | F -> FF
  | ET l -> etsf (map msfiv l)
  | OU l -> ousf (map msfiv l)
let rec remsfaux sf mon = match sf with
  | TT -> T
  | FF -> F
  | XX (b,r) -> if b or mon then X r else F
  | ETT (b,l) -> if mon then ET (map remsftrue l) else if b then match l with [] -> F | a::[] -> ET (map remsf l) | a::b::[] -> begin
    match [issf a;issf b] with 
      | [true;true] -> ET (map remsftrue l)
      | [true;false]-> ET ([remsf a;remsftrue b])
      | [false;true]-> ET ([remsftrue a;remsf b])
      | _ -> failwith "false and" end | _ -> failwith "false and0" else F
  | OUU (b,l) -> if b or mon then OU (map (fun x -> remsfaux x mon) l) else F 
and remsf sf = remsfaux sf false
and remsftrue sf = remsfaux sf true




let piv r = if -r+1=34 or -r+1=47 then "0" else "1"
let pfn n = match n with A r -> if -r+1>80 then "0" else "x"^string_of_int (-r+1) | B r -> if -r+1>80 then "0" else piv r | C r -> if -r+1+177<286 then "0" else "x"^string_of_int (-r+1+177)

match n with A r -> if -r+1>80 then 0 else -r+1 | B r -> if -r+1>80 then 0 else -r+1+93 | C r -> if -r+1+177<286 then 0 else -r+1+177

let concat l = (*Concaténation d'un AND de OR en OR de AND*) 
  match l with []-> [] | l1::[]-> l1 | l1::tl -> fold_left (fun l1 l2 -> flatten (map (fun x -> map (fun y -> x@y) l1) l2)) l1 tl 

let a = concat [[[1]];[[2]];[[1];[2];[3]]]

let rec an t = match t with
    Leaf n -> [[tox n]]
  | AND l -> concat (map an l)
  | OR (n,l) -> flatten (map an l)

type form = X of int | O of form list | E of form list

let rec formula t = match t with
    Leaf n -> X (tox n)
  | AND l -> E (map formula l)
  | OR (_,l) -> O (map formula l)

let aaa = an t

(*
let rec last l = match l with []->failwith "empty list" | a::[] -> a | a::tl -> last tl
let rec getmaxtrail t = match t with
    Leaf n -> Leaf n
  | AND l -> AND (map getmaxtrail l)
  | OR (n,l) -> OR (n,[getmaxtrail (last l)])
let rec getmaxtraill t = match t with
    Leaf n -> Leaf n
  | AND l -> AND [getmaxtraill (hd l)]
  | OR (n,l) -> OR (n,[getmaxtraill (last l)])
let rec getmaxtrailr t = match t with
    Leaf n -> Leaf n
  | AND l -> AND [getmaxtrailr (last l)]
  | OR (n,l) -> OR (n,[getmaxtrailr (last l)])

let tr = getmaxtrail t
let trl = getmaxtraill t
let trr = getmaxtrailr t

let rec add l n = match l with []->[[]] | a::tl -> match a with [] -> add tl n | _ -> (n::a)::add tl n

let _ = add [[(A 228)];[];[]] (A 229)

let rec enumtrail a n = match a with
    Leaf nn -> if nn = n then [[nn]] else [[]]
  | AND [] -> [[]]
  | AND l -> [[]](*flatten (map (fun a -> enumtrail a n) l)*)
  | OR (nn,l) -> if n = nn then [[nn]] else add (flatten (map (fun a -> enumtrail a n) l)) nn

let _ = enumtrail t (A (rmax-(109+91+82)))

let rec enumall t a b =
  if a=b then [] else enumtrail t (A (rmax-a)):: enumtrail t (B (rmax-a)):: enumtrail t (C (rmax-a))::enumall t (a+1) b

let rec ispair l = match l with
    a::b::[] -> begin match b with [] -> false | _ ->
      begin match a with [] -> false | _ -> true end end
  | a::[] -> begin match a with [] -> true | _ -> false end
  | [] -> false
  | a::b::tl -> ispair tl
let rec getpairtrails l =  match l with [] -> [] | a::tl -> if ispair a then a::getpairtrails tl else getpairtrails tl
let rec getimpairtrails l =  match l with [] -> [] | a::tl -> if ispair a then getimpairtrails tl else match a with [] | _::[] | _::[]::[] -> getimpairtrails tl | _ -> a::getimpairtrails tl
let rec getmultitrails l =  match l with [] -> [] | a::tl -> begin match a with [] | _::[] | _::[]::[] -> getmultitrails tl | _ -> a::getmultitrails tl end
let rec balais l = match l with [] -> [] | [[]]::tl -> balais tl | a::tl -> a::balais tl
let rec printtrails2 l = match l with [] -> [] | []::[] -> [] | a::tl -> getdiff a::printtrails2 tl
let rec printtrails l = match l with [] -> [] | a::tl -> printtrails2 a::printtrails tl
(*
let trails = printtrails (getpairtrails (balais (enumall t 100 840)));;
let nbtrails = length trails
let middle = nth trails (nbtrails/2) ;;
let trails = printtrails (getimpairtrails (balais (enumall t 100 840)));;
let nbtrails = length trails
let middle = nth trails (nbtrails/2) ;;*)

(*let rec isin a n = match a with
    Leaf nn -> nn = n
  | AND l -> exists (fun a -> isin a n) l
  | OR (nn,l) -> if n = nn then true else exists (fun a -> isin a n) l

let _ = isin t (A 228)

let rec sort = function
    | [] -> []
    | x :: l -> insert x (sort l)
  and insert elem = function
    | [] -> [elem]
    | x :: l -> if elem < x then elem :: x :: l
                else x :: insert elem l;;
*)
