open List
type noeud = A of int | B of int | C of int
type tree = | Leaf of noeud
	    | Node of noeud*tree list

let rec maketree n = match n with
  | A r -> if r < 93
    then Leaf (A r)
    else Node (A r,[maketree (A (r-68)); maketree (C (r-65)); maketree (C (r-110)); maketree (C (r-109)); maketree (C (r-108))])
  | B r -> if r < 84
    then Leaf (B r)
    else Node (B r,[maketree (B (r-77)); maketree (A (r-65)); maketree (A (r-92)); maketree (A (r-91)); maketree (A (r-90))])
  | C r -> if r < 111
    then Leaf (C r)
    else Node (C r,[maketree (C (r-86)); maketree (B (r-68)); maketree (B (r-83)); maketree (B (r-82)); maketree (B (r-81))])
      
let t = maketree (A 800)



let rec isin a n = match a with
    Leaf nn -> nn = n
  | Node (nn,aa::b::c::d::e::[]) -> if n = nn then true else isin aa n || isin b n || isin c n || isin d n || isin e n

let a = isin t (A 228)

let rec add l n = match l with []->[[]] | a::tl -> match a with [] -> add tl n | _ -> (n::a)::add tl n

let a = add [[(A 228)];[];[]] (A 229)

let rec enumtrail a n = match a with
    Leaf nn -> if nn = n then [[nn]] else [[]]
  | Node (nn,aa::b::c::d::e::[]) -> if n = nn then [[nn]] else add (flatten [enumtrail aa n;enumtrail b n;enumtrail c n;enumtrail d n;enumtrail e n]) nn

let a = enumtrail t (A (800-(109+91+82)))

let rec enumall t a b =
  if a=b then [] else enumtrail t (A (800-a))::enumall t (a+1) b
  
let q = enumall t 250 260;;
let w = enumall t 260 270;;
let e = enumall t 270 280;;
let r = enumall t 280 290;;
let s = enumall t 290 300;;
