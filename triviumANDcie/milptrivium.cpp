#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include <set>
#include <iostream>
#include <bitset>
//#include <execution>
#include <mutex>
#include <ctime>
// #include "/Library/gurobi912/mac64/include/gurobi_c++.h"
#include "gurobi_c++.h"
/*
g++ -march=native -O3 -std=c++17 -o milptrivium milptrivium.cpp -I/Library/gurobi912/mac64/include 
-L/Library/gurobi912/mac64/lib -lgurobi_c++ -lgurobi91 
g++ -march=native -O3 -std=c++17 -o milptrivium milptrivium.cpp -I/opt/gurobi902/linux64/include 
-L/opt/gurobi902/linux64/lib -lgurobi_c++ -lgurobi90

g++ -march=native -O3 -std=c++17 -o monpred trivium.cpp -I/opt/gurobi902/linux64/include -L/opt/gurobi902/linux64/lib -lgurobi_c++ -lgurobi90

*/
using namespace std;
struct cmpBitset288 {
	bool operator()(const bitset<288>& a, const bitset<288>& b) const {
		for (int i = 0; i < 288; i++) {
			if (a[i] < b[i])
				return true;
			else if (a[i] > b[i])
				return false;
		}
		return false;
	}
};
void printEq(vector<set<int> > const & v) {
	if (v.empty()) cout << " 0 " << endl;
	else {
		for (auto const & s : v) {
			if (s.empty()) cout << " 1 ";
			else for (auto x : s) 
				cout << "v" << x << ".";
			
			cout << " + ";
		}
		cout << endl;
	}
}

void printSys(map<int, vector<set<int>>> const & sys) {
	for (auto const & p : sys) {
		cout << "v" << p.first << " = ";
		printEq(p.second);
	}
} 

int display_result_anf(vector<int> cube, map<bitset<288>, int, cmpBitset288>& countingBox){
	int i ; 
	auto it2 = countingBox.begin(); 
	while (it2 != countingBox.end()) {
		if (((*it2).second % 2) == 1) {
			cout << ((*it2).second % 2) << " | " << (*it2).second << "\t";
			bitset<288> tmp = (*it2).first;
			for (i = 0; i < 80; i++) if ((tmp[i] == 1)) 
				cout << "k" << (i ) << " ";
			/*for (i = 0; i < 80; i++) if ((tmp[93 + i] == 1)) 
				cout << "v" << (i) << " ";
			// Uncomment for the cube in the superpoly.*/
			for (i = 0; i < 80; i++) if ((tmp[93 + i] == 1 && !binary_search(cube.begin(),cube.end(),i))) 
				cout << "v" << (i) << " ";
			cout << endl;
		}
		it2++;
	}
	return 0 ; 
}

int getVarTrivium(int x) {
	if (x < 288) return x;
	auto xmod = x%288;
	if (xmod == 0 || xmod == 93 || xmod == 177) return x;
	return getVarTrivium(x - 288 - 1);
}

vector<vector<int>> getins(int R){
	vector<int> vcube;  
	vector<int> v0;
	switch (R) {
		case 590:
		vcube = vector<int> ({0,10,20,30,40,50,60,70});
		break;
		case 672:
		vcube = vector<int> ({3, 13, 18, 26, 38, 40, 47, 49, 55, 57, 66, 79});
		for (int i = 0; i < 80; ++i) if (find(vcube.begin(), vcube.end(), i) == vcube.end()) 
			v0.emplace_back(i);
		break;
		case 675:
		vcube = vector<int> ({2, 13, 20, 24, 37, 42, 43, 46, 53, 55, 57, 67});
		for (int i = 0; i < 80; ++i) if (find(vcube.begin(), vcube.end(), i) == vcube.end()) 
			v0.emplace_back(i);
		break;
		case 735:
		vcube = vector<int> ({1,4,8,11,12,13,18,27,35,37,39,46,48,50,51,52,54,56,62,63,65,72,78});
		for (int i = 0; i < 80; ++i) if (find(vcube.begin(), vcube.end(), i) == vcube.end()) 
			v0.emplace_back(i);
		break;
		case 840:
		for (int i = 0; i < 80; ++i) if (i == 33 || i == 46) v0.emplace_back(i);
			else vcube.emplace_back(i);
		break;
		default:{}
	}
	for (auto & x : v0) //ajout des autres constantes dans v0
		x += 93;//recalage de v0
	for (int i = 80; i < 93; ++i) 
		v0.emplace_back(i);
	for (int i = 93 + 80; i < 285; ++i) 
		v0.emplace_back(i);
	sort(v0.begin(), v0.end());
	return {vcube,v0};
}
vector<int> getcube(int R){
	return getins(R)[0];
}
vector<int> getv0(int R){
	return getins(R)[1];
}


map<int, vector<set<int>>> generateTrivium(int R, bool tocut, set<int> cut) {
	int rlim = R*7/10;
	map<int, vector<set<int>>> sys;
	int root = 288*(R+1);
	auto & root_eq = sys[root];
	set<int> waiting;
	if (cut.size()==0){
		vector<int> indexes_root = {65, 92, 161, 176, 242, 287};
		for (auto i : indexes_root) {
			root_eq.emplace_back(set<int> ({getVarTrivium(288*R + i)}));
			waiting.emplace(getVarTrivium(288*R + i));
		}
	}else for (auto i : cut) {
		root_eq.emplace_back(set<int> ({i}));
		waiting.emplace(i);
	}
	while (!waiting.empty()) {
		auto x = *(waiting.rbegin());
		waiting.erase(x);
		if (tocut && x<=288*rlim) continue;
		if (x/288 == 0) continue;
		auto & eq = sys[x];
		switch (x%288) {
			case 0:
			x -= 288;
			eq.emplace_back(set<int>({getVarTrivium(x + 68)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 242)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 287)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 285), getVarTrivium(x + 286)}));
			waiting.emplace(getVarTrivium(x + 242));
			waiting.emplace(getVarTrivium(x + 287));
			waiting.emplace(getVarTrivium(x + 68));
			waiting.emplace(getVarTrivium(x + 285));
			waiting.emplace(getVarTrivium(x + 286));
			break;
			case 93:
			x -= 288 + 93;
			eq.emplace_back(set<int>({getVarTrivium(x + 170)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 65)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 92)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 90), getVarTrivium(x + 91)}));
			waiting.emplace(getVarTrivium(x + 65));
			waiting.emplace(getVarTrivium(x + 92));
			waiting.emplace(getVarTrivium(x + 170));
			waiting.emplace(getVarTrivium(x + 90));
			waiting.emplace(getVarTrivium(x + 91));
			break;
			case 177:
			x -= 288 + 177;
			eq.emplace_back(set<int>({getVarTrivium(x + 263)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 161)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 176)}));
			eq.emplace_back(set<int>({getVarTrivium(x + 174), getVarTrivium(x + 175)}));
			waiting.emplace(getVarTrivium(x + 161));
			waiting.emplace(getVarTrivium(x + 176));
			waiting.emplace(getVarTrivium(x + 263));
			waiting.emplace(getVarTrivium(x + 174));
			waiting.emplace(getVarTrivium(x + 175));
			break;
		}
	}
	return sys;
}

map<int, vector<int>> reverseSys(map<int, vector<set<int>>> const & sys) {
	map<int, vector<int>> res;
	for (auto const & eq : sys) for (auto const & s : eq.second) for (auto x : s) 
		res[x].emplace_back(eq.first);
	return res;
}

bool in(vector<int>& a,int i){
	return binary_search(a.begin(),a.end(),i);
}


int getT(int x, map<int, vector<set<int>>> const & sys, map<int, int> & T, map<int, int> & TT);
int getT(set<int> const & s, map<int, vector<set<int>>> const & sys, map<int, int> & T, map<int, int> & TT);

int getTT(int x, map<int, vector<set<int>>> const & sys, map<int, int> & T, map<int, int> & TT) {
  auto it = TT.find(x);
  if (it != TT.end()) return it->second;
  auto it_eq1 = sys.find(x);
  auto it_eq2 = sys.find(x+288);
  auto & TTx = TT[x];
  if (it_eq1 == sys.end() && it_eq2 == sys.end()) {
    TTx = getT(x, sys, T, TT) + getT(x+288, sys, T, TT);
  }
  else {
    auto const & eq1 = (it_eq1 != sys.end()) ? it_eq1->second : vector<set<int>> (1, set<int> ({x}));
    auto const & eq2 = (it_eq2 != sys.end()) ? it_eq2->second : vector<set<int>> (1, set<int> ({x+288}));
    TTx = 0;
    for (auto const & s1 : eq1) {
      for (auto s2 : eq2) {
        s2.insert(s1.begin(), s1.end());
        int tmp = getT(s2, sys, T, TT);
        if (tmp > TTx) TTx = tmp;
      }
    }
  }
  return TTx;
}

int getT(set<int> const & s, map<int, vector<set<int>>> const & sys, map<int, int> & T, map<int, int> & TT) {
  if (s.size() == 0) return 0;
  if (s.size() == 1) return getT(*s.begin(), sys, T, TT);
  auto ss = s;
  auto x = *ss.begin();
  ss.erase(ss.begin());
  if (ss.count(x+288) == 0) return getT(x, sys, T, TT) + getT(ss, sys, T, TT);
  else {
    ss.erase(x+288);
    return getTT(x, sys, T, TT) + getT(ss, sys, T, TT);
  }
}

int getT(int x, map<int, vector<set<int>>> const & sys, map<int, int> & T, map<int, int> & TT) {
  auto it = T.find(x);
  if (it != T.end()) return it->second;
  auto & Tx = T[x];
  Tx = 0;
  auto it_eq = sys.find(x);
  if (it_eq != sys.end()) {
    for (auto const & s : it_eq->second) {
      int local_best = getT(s, sys, T, TT);
      if (local_best > Tx) Tx = local_best;
    }
  }
  return Tx;
}

map<int,int> arrity(int R){
	auto sys = generateTrivium(R,false,set<int>());
	map<int,int> arr;
	vector<int> cube = getcube(R);
	vector<int> v0 = getv0(R);
	for (auto const & p : sys) if(
		in(v0,*p.second[0].begin())&&
		in(v0,*p.second[1].begin())&&
		in(v0,*p.second[2].begin())&&(
		in(v0,*p.second[3].begin())||in(v0,*p.second[3].rbegin())))//cout<<p.first;
		v0.emplace_back(p.first);
	int n;

    //propagate 0s
    for (auto const & p : sys) {
      bool flag0 = true;
      for (unsigned i = 0; i < 3; ++i) {
        if (!in(v0,*p.second[i].begin())) flag0 = false;
      }
      auto y1 = *p.second[3].begin();
      auto y2 = *p.second[3].rbegin();
      if (!in(v0,y1) && !in(v0,y2)) flag0 = false;
      if (flag0) {
      	if (!in(v0,p.first)){
      	v0.emplace_back(p.first);
      	sort(v0.begin(),v0.end());
    		}
    	}
    }


    	map<int, int> T, TT;
    	for (auto y : v0) T[y] = -100;
    	for (auto y : cube) T[y] = 1;

	cout<<getT(sys.rbegin()->first, sys, T, TT)<<endl;



	for (int i=0;i<288;i++) if (in(cube,i))
		arr[i]=1;
	else
		arr[i]=0;
	for (auto const & n : sys){
		int max = 0;
		for (auto const & s : n.second) if (s.size()==1)
			max = std::max(max,arr[*s.begin()]);
		else if (*s.begin()<288){
			if (!in(v0,*s.begin())&&!in(v0,*s.rbegin()))
				max = std::max(max,arr[*s.begin()]+arr[*s.rbegin()]);
		}else{
			int a4 = *s.begin();
			int a5 = *s.rbegin();
			if (!in(v0,*sys[a4][0].begin()))
				max = std::max(max,arr[*sys[a4][0].begin()]+arr[a5]);
			if (!in(v0,*sys[a4][1].begin()))
				max = std::max(max,arr[*sys[a4][1].begin()]+arr[a5]);
			if (!in(v0,*sys[a4][2].begin())&&!in(v0,*sys[a5][0].begin()))
				max = std::max(max,arr[*sys[a4][2].begin()]+arr[*sys[a5][0].begin()]);
			if (!in(v0,*sys[a4][2].begin())&&!in(v0,*sys[a5][1].begin()))
				max = std::max(max,arr[*sys[a4][2].begin()]+arr[*sys[a5][1].begin()]);
			if (!in(v0,*sys[a4][2].begin())&&!in(v0,*sys[a5][2].begin()))
				max = std::max(max,arr[*sys[a4][2].begin()]+arr[*sys[a5][2].begin()]);
			if (!in(v0,*sys[a5][3].begin())&&!in(v0,*sys[a5][3].rbegin()))
				max = std::max(max,arr[*sys[a5][3].begin()]+arr[*sys[a5][3].rbegin()]);//aller plus loing ici?  
			if (!in(v0,*sys[a4][3].begin())&&!in(v0,*sys[a4][3].rbegin())&&!in(v0,*sys[a5][0].begin()))
				max = std::max(max,arr[*sys[a4][3].begin()]+arr[*sys[a4][3].rbegin()]+arr[*sys[a5][0].begin()]);
			if (!in(v0,*sys[a4][3].begin())&&!in(v0,*sys[a4][3].rbegin())&&!in(v0,*sys[a5][1].begin()))
				max = std::max(max,arr[*sys[a4][3].begin()]+arr[*sys[a4][3].rbegin()]+arr[*sys[a5][1].begin()]);
			if (!in(v0,*sys[a4][3].begin())&&!in(v0,*sys[a4][3].rbegin())&&!in(v0,*sys[a5][2].begin()))
				max = std::max(max,arr[*sys[a4][3].begin()]+arr[*sys[a4][3].rbegin()]+arr[*sys[a5][2].begin()]);
			if (!in(v0,*sys[a4][3].begin())&&!in(v0,*sys[a4][3].rbegin())&&!in(v0,*sys[a5][3].rbegin()))
				max = std::max(max,arr[*sys[a4][3].begin()]+arr[*sys[a4][3].rbegin()]+arr[*sys[a5][3].rbegin()]);
		}
		arr[n.first]=max;
	}
	// for (auto i:arr) cout<<i.second<<endl;
	// cout<<arr[R*288+288]<<endl;
	return arr;
}

void solvecut(int R, set<int> cut, map<bitset<288>, int, cmpBitset288>& countingBox,GRBEnv env){
	// cout<<"Cut:"; for (auto const & b : cut)cout<<b<<' ';cout<<endl;
	auto sys = generateTrivium(R,false,cut);
	auto rsys = reverseSys(sys);
	int Nvar = 0;
	map<pair<int, int>, int> mapVars;
	for (auto const & p : sys) for (auto const & s : p.second) for (auto x : s) 
		mapVars[make_pair(p.first, x)] = Nvar++;

	GRBModel model = GRBModel(env);
	vector<GRBVar> X (Nvar);
	for (unsigned i = 0; i < Nvar; ++i) 
		X[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);

	vector<int> vcube = getcube(R);
	for (auto x : vcube) {//cube ctr
		x += 93;
		GRBLinExpr e = 0;
		if(rsys.find(x)==rsys.end()) return;//cout<<"Missing in cube graph: "<<x<<endl;}
		for (auto y : rsys.at(x)) {
			e += X[mapVars.at(make_pair(y,x))];
		}
		model.addConstr(e >= 1);
	}
	vector<int> v0 = getv0(R);
	for (auto x : v0) if(rsys.find(x)!=rsys.end()) for (auto y : rsys.at(x)) //v0 ctr
		model.addConstr(X[mapVars.at(make_pair(y,x))] == 0);

	for (auto const & p : sys) {
		GRBLinExpr e_out = 0;
		for (auto const & s : p.second) {
			e_out += X[mapVars[make_pair(p.first, *s.begin())]];
			if (s.size() != 1) //brothers
				model.addConstr(X[mapVars[make_pair(p.first, *s.begin())]] == X[mapVars[make_pair(p.first, *s.rbegin())]]);
		}
		if (p.first == sys.rbegin()->first)
			model.addConstr(e_out == cut.size());//cut obligatoire
		else{
			model.addConstr(e_out <= 1);//un soeul fils par noeud

			GRBLinExpr e_in = 0;
			for (auto x : rsys[p.first]) 
				e_in += X[mapVars[make_pair(x, p.first)]];
			model.addConstr(e_in >= e_out);
			model.addConstr(e_in <= rsys[p.first].size()*e_out);

			if (p.first >= 288) {//doubling pattern
				auto x1 = *sys.at(p.first)[2].begin();
				if (x1 >= 288 && !binary_search(v0.begin(), v0.end(), x1)) {
					auto z1 = *sys.at(x1)[3].begin();
					auto z2 = *sys.at(x1)[3].rbegin();
					if (!binary_search(v0.begin(), v0.end(), z1) && !binary_search(v0.begin(), v0.end(), z2)) {
						auto y1 = *sys.at(p.first)[3].begin();
						auto y2 = *sys.at(p.first)[3].rbegin();
						if (y1 >= 288 && y2 >= 288) {
							auto zz1 = *sys.at(y1)[2].begin();
							auto zz2 = *sys.at(y2)[2].begin();
							GRBLinExpr e = 0;
							for (auto const & a : sys.at(p.first)) 
								e += X[mapVars.at(make_pair(p.first,*a.begin()))];
							for (auto const & a : sys.at(x1)) 
								e += X[mapVars.at(make_pair(x1,*a.begin()))];
							for (auto const & a : sys.at(y1)) 
								e += X[mapVars.at(make_pair(y1,*a.begin()))];
							for (auto const & a : sys.at(y2)) 
								e += X[mapVars.at(make_pair(y2,*a.begin()))];
							e -= 2*X[mapVars.at(make_pair(p.first,x1))];
							e -= 2*X[mapVars.at(make_pair(p.first,y1))];
							e -= X[mapVars.at(make_pair(x1,z1))];
							e -= X[mapVars.at(make_pair(y1,zz1))];
							e -= X[mapVars.at(make_pair(y2,zz2))];
							model.addConstr(e >= 0);
						}
					}
				}
			}
		}
	}
	model.read("tune.prm");
	model.optimize();
	int solCount = model.get(GRB_IntAttr_SolCount);
	if (solCount>0)cout<<" YATTAAA "<<solCount<<endl;
	if (solCount >= 2000000000) {
		cerr << "Number of solutions is too large" << endl;
		exit(0);
	}
	for (int i = 0; i < solCount; i++) {
		model.set(GRB_IntParam_SolutionNumber, i);
		bitset<288> tmp;
		for (int x = 0; x < 288; x++) {
			tmp[x] = 0;
			if(rsys.find(x)!=rsys.end()) for (auto y : rsys.at(x)) if (X[mapVars.at(make_pair(y,x))].get(GRB_DoubleAttr_Xn) > 0.5) 
				tmp[x]=1;
		}
		countingBox[tmp]++;
	}
	// display_result_anf(vcube, countingBox);
}

void cutTrivium(int R) {
	auto time0 = time(NULL);
	auto sys = generateTrivium(R,true,set<int>());
	auto rsys = reverseSys(sys);
	int Nvar = 0;
	map<pair<int, int>, int> mapVars;
	for (auto const & p : sys) for (auto const & s : p.second) for (auto x : s) 
		mapVars[make_pair(p.first, x)] = Nvar++;

	GRBEnv env = GRBEnv();
	env.set("LogFile", "mip1.log");
	env.set(GRB_IntParam_PoolSearchMode, 2);
	env.set(GRB_IntParam_PoolSolutions, 2000000000);
	env.set(GRB_DoubleParam_PoolGap, GRB_INFINITY);
	env.set(GRB_IntParam_Threads, 100);
	env.set(GRB_IntParam_OutputFlag, 0);
	env.start();
	GRBModel model = GRBModel(env);
	vector<GRBVar> X (Nvar);
	for (unsigned i = 0; i < Nvar; ++i) 
		X[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);

	for (auto const & p : sys) {
		GRBLinExpr e_out = 0;
		for (auto const & s : p.second) {
			e_out += X[mapVars[make_pair(p.first, *s.begin())]];
			if (s.size() != 1) //brothers
				model.addConstr(X[mapVars[make_pair(p.first, *s.begin())]] == X[mapVars[make_pair(p.first, *s.rbegin())]]);
		}
		if (p.first==sys.rbegin()->first)
			model.addConstr(e_out == 1);//source obligatoire sinon on a la coupe vide
		else model.addConstr(e_out <= 1);//un soeul fils par noeud

		if (p.first != sys.rbegin()->first) {
			GRBLinExpr e_in = 0;
			for (auto x : rsys[p.first]) 
				e_in += X[mapVars[make_pair(x, p.first)]];
			model.addConstr(e_in >= e_out);
			model.addConstr(e_in <= rsys[p.first].size()*e_out);

			auto x1 = *sys.at(p.first)[2].begin();
			if (sys.find(x1) !=sys.end()) {//doubling pattern
				auto z1 = *sys.at(x1)[3].begin();
				auto z2 = *sys.at(x1)[3].rbegin();
				auto y1 = *sys.at(p.first)[3].begin();
				auto y2 = *sys.at(p.first)[3].rbegin();
				auto zz1 = *sys.at(y1)[2].begin();
				auto zz2 = *sys.at(y2)[2].begin();
				GRBLinExpr e = 0;
				for (auto const & a : sys.at(p.first)) 
					e += X[mapVars.at(make_pair(p.first,*a.begin()))];
				for (auto const & a : sys.at(x1)) 
					e += X[mapVars.at(make_pair(x1,*a.begin()))];
				for (auto const & a : sys.at(y1)) 
					e += X[mapVars.at(make_pair(y1,*a.begin()))];
				for (auto const & a : sys.at(y2)) 
					e += X[mapVars.at(make_pair(y2,*a.begin()))];
				e -= 2*X[mapVars.at(make_pair(p.first,x1))];
				e -= 2*X[mapVars.at(make_pair(p.first,y1))];
				e -= X[mapVars.at(make_pair(x1,z1))];
				e -= X[mapVars.at(make_pair(y1,zz1))];
				e -= X[mapVars.at(make_pair(y2,zz2))];
				model.addConstr(e >= 0);
			}
		}
	}
	model.read("tune.prm");
	model.optimize();
	int solCount = model.get(GRB_IntAttr_SolCount);

	if (solCount >= 2000000000) {
		cerr << "Number of solutions is too large" << endl;
		exit(0);
	}
	set<set<int>> cuts;
	int doublies = 0;
	for (int i = 0; i < solCount; i++) {
		model.set(GRB_IntParam_SolutionNumber, i);
		set<int> cut;
		for (auto const & p : sys) for (auto const & ss : p.second) for (auto s : ss) {
			if (sys.find(s)==sys.end() && X[mapVars.at(make_pair(p.first,s))].get(GRB_DoubleAttr_Xn) > 0.5)
				cut.insert(s);
		}
		if (cuts.find(cut)!=cuts.end()){
			cuts.erase(cut);
			doublies+=2;
		}else cuts.insert(cut);
	}
	int arrirem = 0;

	vector<int> cube = getcube(R);
	map<int,int> arr = arrity(R);
	for (auto it = cuts.begin(); it != cuts.end(); ) {
		int sum = 0;
		for (auto i:*it)
			sum += arr[i];
		if (sum<cube.size()){
			it = cuts.erase(it);
			arrirem++;
		}
		else 
			++it;
	}
	auto time1 = time(NULL);
	cout<<(time1-time0)<<'s'<<endl;
	//for (auto a : cuts) {for (auto b : a)cout<<b<<' ';cout<<endl;}
	cout<<" number of cuts-doublies-lowarrity: "<<solCount<<"-"<<doublies<<"-"<<arrirem<<"="<<cuts.size()<<endl;

	map<bitset<288>, int, cmpBitset288> countingBox;
	for (auto const &  cut : cuts)
		solvecut(R,cut,countingBox,env);
	display_result_anf(cube, countingBox);
	auto time2 = time(NULL);
	cout<<(time2-time1)<<'s'<<endl;
}

void milpTrivium(int R) {
	auto sys = generateTrivium(R,false,set<int>());
	auto rsys = reverseSys(sys);
	int Nvar = 0;
	map<pair<int, int>, int> mapVars;
	for (auto const & p : sys) for (auto const & s : p.second) for (auto x : s) 
		mapVars[make_pair(p.first, x)] = Nvar++;

	GRBEnv env = GRBEnv(true);
	env.set("LogFile", "mip1.log");
	env.set(GRB_IntParam_PoolSearchMode, 2);
	env.set(GRB_IntParam_PoolSolutions, 2000000000);
	env.set(GRB_DoubleParam_PoolGap, GRB_INFINITY);
	env.set(GRB_IntParam_Threads, 8);

	env.start();
	GRBModel model = GRBModel(env);
	vector<GRBVar> X (Nvar);
	for (unsigned i = 0; i < Nvar; ++i) 
		X[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);

	vector<int> vcube = getcube(R);
	for (auto x : vcube) {//cube ctr
		x += 93;
		GRBLinExpr e = 0;
		for (auto y : rsys.at(x)) 
			e += X[mapVars.at(make_pair(y,x))];
		model.addConstr(e >= 1);
	}
	vector<int> v0 = getv0(R);
	// for (auto const & p : sys) {
	// 	bool flag0 = true;
	// 	for (unsigned i = 0; i < 3; ++i) if (!binary_search(v0.begin(), v0.end(), *p.second[i].begin())) 
	// 		flag0 = false;
	// 	auto y1 = *p.second[3].begin();
	// 	auto y2 = *p.second[3].rbegin();
	// 	if (!binary_search(v0.begin(), v0.end(), y1) && !binary_search(v0.begin(), v0.end(), y2)) 
	// 		flag0 = false;
	// 	if (flag0) 
	// 		v0.emplace_back(p.first);
	// }
	for (auto x : v0) for (auto y : rsys.at(x)) //v0 ctr
		model.addConstr(X[mapVars.at(make_pair(y,x))] == 0);

	for (auto const & p : sys) {
		GRBLinExpr e_out = 0;
		for (auto const & s : p.second) {
			e_out += X[mapVars[make_pair(p.first, *s.begin())]];
			if (s.size() != 1) //brothers
				model.addConstr(X[mapVars[make_pair(p.first, *s.begin())]] == X[mapVars[make_pair(p.first, *s.rbegin())]]);
		}
		model.addConstr(e_out <= 1);//un soeul fils par noeud

		if (p.first != sys.rbegin()->first) {
			GRBLinExpr e_in = 0;
			for (auto x : rsys[p.first]) 
				e_in += X[mapVars[make_pair(x, p.first)]];
			model.addConstr(e_in >= e_out);
			model.addConstr(e_in <= rsys[p.first].size()*e_out);

			if (p.first >= 288) {//doubling pattern
				auto x1 = *sys.at(p.first)[2].begin();
				if (x1 >= 288 && !binary_search(v0.begin(), v0.end(), x1)) {
					auto z1 = *sys.at(x1)[3].begin();
					auto z2 = *sys.at(x1)[3].rbegin();
					if (!binary_search(v0.begin(), v0.end(), z1) && !binary_search(v0.begin(), v0.end(), z2)) {
						auto y1 = *sys.at(p.first)[3].begin();
						auto y2 = *sys.at(p.first)[3].rbegin();
						if (y1 >= 288 && y2 >= 288) {
							auto zz1 = *sys.at(y1)[2].begin();
							auto zz2 = *sys.at(y2)[2].begin();
							GRBLinExpr e = 0;
							for (auto const & a : sys.at(p.first)) 
								e += X[mapVars.at(make_pair(p.first,*a.begin()))];
							for (auto const & a : sys.at(x1)) 
								e += X[mapVars.at(make_pair(x1,*a.begin()))];
							for (auto const & a : sys.at(y1)) 
								e += X[mapVars.at(make_pair(y1,*a.begin()))];
							for (auto const & a : sys.at(y2)) 
								e += X[mapVars.at(make_pair(y2,*a.begin()))];
							e -= 2*X[mapVars.at(make_pair(p.first,x1))];
							e -= 2*X[mapVars.at(make_pair(p.first,y1))];
							e -= X[mapVars.at(make_pair(x1,z1))];
							e -= X[mapVars.at(make_pair(y1,zz1))];
							e -= X[mapVars.at(make_pair(y2,zz2))];
							model.addConstr(e >= 0);
						}
					}
				}
			}
		}
	}
	model.read("tune.prm");
	model.optimize();

	//affichage du superpoly
	map<bitset<288>, int, cmpBitset288> countingBox;
	int solCount = model.get(GRB_IntAttr_SolCount);
	if (solCount >= 2000000000) {
		cerr << "Number of solutions is too large" << endl;
		exit(0);
	}
	for (int i = 0; i < solCount; i++) {
		model.set(GRB_IntParam_SolutionNumber, i);
		bitset<288> tmp;
		for (int x = 0; x < 288; x++) {
			tmp[x] = 0;
			for (auto y : rsys.at(x)) if (X[mapVars.at(make_pair(y,x))].get(GRB_DoubleAttr_Xn) > 0.5) 
				tmp[x]=1;
		}
		countingBox[tmp]++;
	}
	display_result_anf(vcube, countingBox) ;
}



int main(){//int argc, char const *argv[]) {
	int R = 840;//stoi(argv[1]);
	arrity(R);
	// milpTrivium(R);
	// cutTrivium(R);
	return 0;
}
