#include "main.h"
#include <chrono>
struct cmpBitset288 {
	bool operator()(const bitset<288>& a, const bitset<288>& b) const {
		for (int i = 0; i < 288; i++) {
			if (a[i] < b[i])
				return true;
			else if (a[i] > b[i])
				return false;
		}
		return false;
	}
};

int display_result_anf(vector<int> cube, map<bitset<288>, int, cmpBitset288>& countingBox){

	int i ; 
	auto it2 = countingBox.begin(); 

	while (it2 != countingBox.end()) {
		
		if (((*it2).second % 2) == 1) {
			
			cout << ((*it2).second % 2) << " | " << (*it2).second << "\t";
			bitset<288> tmp = (*it2).first;
			
			for (i = 0; i < 80; i++) {
				if ((tmp[i] == 1)) cout << "k" << (i ) << " ";
			}
			
			/*
			for (i = 0; i < 80; i++) {
				if ((tmp[93 + i] == 1)) cout << "v" << (i) << " ";
			}*/

			// Uncomment for the superpoly.
			
			for (i = 0; i < 80; i++) {
				if ((tmp[93 + i] == 1 && cube[i] != 1)) cout << "v" << (i) << " ";
			}

			//
			
			cout << endl;
		}
		it2++;
	}

	return 0 ; 
	 
}


int triviumThreeEnumuration(vector<int> cube, vector<int> flag, int evalNumRounds, map<bitset<288>, int, cmpBitset288>& countingBox, int threadNumber, int target);

void triviumCoreThree(GRBModel& model, vector<GRBVar>& x, int i1, int i2, int i3, int i4, int i5) {

	GRBVar y1 = model.addVar(0, 1, 0, GRB_BINARY);
	GRBVar y2 = model.addVar(0, 1, 0, GRB_BINARY);
	GRBVar y3 = model.addVar(0, 1, 0, GRB_BINARY);
	GRBVar y4 = model.addVar(0, 1, 0, GRB_BINARY);
	GRBVar y5 = model.addVar(0, 1, 0, GRB_BINARY);

	GRBVar z1 = model.addVar(0, 1, 0, GRB_BINARY);
	GRBVar z2 = model.addVar(0, 1, 0, GRB_BINARY);
	//GRBVar z3 = model.addVar(0, 1, 0, GRB_BINARY);
	//GRBVar z4 = model.addVar(0, 1, 0, GRB_BINARY);

	GRBVar a = model.addVar(0, 1, 0, GRB_BINARY);
	
	model.addConstr(y1 <= x[i1]);
	model.addConstr(z1 <= x[i1]);
	model.addConstr(y1 + z1 >= x[i1]);
	
	model.addConstr(y2 <= x[i2]);
	model.addConstr(z2 <= x[i2]);
	model.addConstr(y2 + z2 >= x[i2]);

	model.addConstr(y3 <= x[i3]);
	model.addConstr(a <= x[i3]);
	model.addConstr(y3 + a >= x[i3]);

	model.addConstr(y4 <= x[i4]);
	model.addConstr(a <= x[i4]);
	model.addConstr(y4 + a >= x[i4]);

	model.addConstr(y5 == x[i5] + a + z1 + z2);

	x[i1] = y1;
	x[i2] = y2;
	x[i3] = y3;
	x[i4] = y4;
	x[i5] = y5;

}


int triviumThreeEnumuration(vector<int> cube, vector<int> flag, int evalNumRounds, map<bitset<288>, int, cmpBitset288>& countingBox, int threadNumber, int target) {

	int i, j ;

	try {
		// Create the environment
		GRBEnv env = GRBEnv();

		// close standard output
		env.set(GRB_IntParam_LogToConsole, 0);
		env.set(GRB_IntParam_Threads, threadNumber);
		
		
		env.set(GRB_IntParam_MIPFocus, GRB_MIPFOCUS_BESTBOUND);	
		env.set(GRB_IntParam_PoolSearchMode, 2);
		env.set(GRB_IntParam_PoolSolutions, 2000000000);
		env.set(GRB_DoubleParam_PoolGap, GRB_INFINITY);
		
		// Create the model
		GRBModel model = GRBModel(env);

		// Create variables
		vector<vector<GRBVar>> s(evalNumRounds + 1, vector<GRBVar>(288));
		
		for (i = 0; i < 288; i++) {
			s[0][i] = model.addVar(0, 1, 0, GRB_BINARY);
		}

		for(i = 0 ; i<288; i++){
			if(flag[i] == 0) model.addConstr(s[0][i] == 0) ;
		}

		//set monom
		for(i = 0; i<80; i++){
			if (i != 11){
				model.addConstr(s[0][i] == 0); 
			} else {
				model.addConstr(s[0][i] == 1); 
			}
		}

		// Set cube 
		
		GRBLinExpr ks_cube = 0 ; 
		for(i = 0; i<80; i++){
			if(cube[i] == 1) model.addConstr(s[0][93+i] == 1) ; 
			if(cube[i] == 0) model.addConstr(s[0][93+i] == 0) ; 
		}
		

		//model.setObjective(ks_cube, GRB_MAXIMIZE);
		

		// Round function
		for (int r = 0; r < evalNumRounds; r++) {
			vector<GRBVar> tmp(288) ; 
			tmp = s[r];
			
			triviumCoreThree(model, tmp, 65, 170, 90, 91, 92);
			triviumCoreThree(model, tmp, 161, 263, 174, 175, 176);
			triviumCoreThree(model, tmp, 242, 68, 285, 286, 287);
			
			for (i = 0; i < 288; i++) {
				s[r + 1][(i + 1) % 288] = tmp[i];
			}
		}

		// Output bit constraint
		if (target == -1) {
			GRBLinExpr ks = 0;
			for (i = 0; i < 288; i++) {
				if ((i == 65) || (i == 92) || (i == 161) || (i == 176) || (i == 242) || (i == 287)) {
					ks += s[evalNumRounds][i];
				}
				else {
					model.addConstr(s[evalNumRounds][i] == 0);
				}
			}
			model.addConstr(ks == 1);
		}
		
		// State bits
		else{
			GRBLinExpr ks = 0;
			for (i = 0; i < 288; i++) ks += s[evalNumRounds][i];
			model.addConstr(ks == 1);
			model.addConstr(s[evalNumRounds][target] == 1) ; 
		}
		
		
		model.optimize();
		
		/*
		int obj = round(model.getObjective().getValue()) ;
		cout << obj <<endl ; 
		*/

		int solCount = model.get(GRB_IntAttr_SolCount);

			// check solution limit
		if (solCount >= 2000000000) {
			cerr << "Number of solutions is too large" << endl;
			exit(0);
		}

		// store the information about solutions
		for (i = 0; i < solCount; i++) {
			model.set(GRB_IntParam_SolutionNumber, i);
			bitset<288> tmp;

			for (j = 0; j < 288; j++) {

				if(flag[j]!= 1){
					if (round(s[0][j].get(GRB_DoubleAttr_Xn)) == 1) tmp[j] = 1;
					else tmp[j] = 0;
				}
			}

			countingBox[tmp]++;
		}
		
		if (model.get(GRB_IntAttr_Status) == GRB_INFEASIBLE) {
			return -1;
		}
		else if ((model.get(GRB_IntAttr_Status) == GRB_OPTIMAL)) {
			int upperBound = round(model.get(GRB_DoubleAttr_ObjVal));
			return upperBound;
		}
		else {
			cout << model.get(GRB_IntAttr_Status) << endl;
			return -2;
		}
	}
	catch (GRBException e) {
		cerr << "Error code = " << e.getErrorCode() << endl;
		cerr << e.getMessage() << endl;
	}
	catch (...) {
		cerr << "Exception during optimization" << endl;
	}

	return -1;
}

int trivium_anf(int evalNumRounds, int threadNumber) {
	int i; 
  	
  	vector<int> cube(80, 0);
	vector<int> flag(288, 0);
  	


	for(i = 0 ; i<80; i++){
		flag[i] = 2 ; 						// Key bits as variables
		flag[93 + i] = 2 ; 					// IV bits as variables
	}

	//flag[93 + 61] = 1 ; 
	//flag[93 + 49] = 1 ;

	flag[285] = 1 ; 
  	flag[286] = 1 ; 
  	flag[287] = 1 ;	

  	cube[0] = 1 ;
  	cube[1] = 1 ; 
	for(i = 0; i<80; i++){
		if (i != 33 || i != 46){
			cube[i] = 1; 
		} else {
			cube[i] = 0; 
		}
	}

	
	// Set flag = 1 for constant 1 
	// Set flag = 0 for constant 0

  	auto start = chrono::steady_clock::now();

	for(i = 0 ; i< 1; i++){
		cout << "Bit | " << i-1 << endl ; 
  		map<bitset<288>, int, cmpBitset288> countingBox;
  		triviumThreeEnumuration(cube, flag, evalNumRounds, countingBox, threadNumber, i-1);	
  		display_result_anf(cube, countingBox) ;
  		cout << endl ; 
  
  	}

  	auto end = chrono::steady_clock::now();
    auto time = chrono::duration<double> ( end - start );
    
  	cout << "Time: " << time.count() << " seconds" << endl;
  
  return 0 ; 	

	

}

  	
 


  





























