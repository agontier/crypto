ch="/Users/agontier/Desktop/crypto/"
global Nb = 840
global copy1=[i for i in 288+1:288+3]
global copy2=[i for i in 288+4:288+6]
global copy3=[i for i in 288+7:288+9]
global tt1 = [66,91,92,93,171]
global tt2 = [162,175,176,177,264]
global tt3 = [243,286,287,288,69]
global tt1r= [67,92,93,94,172]
global tt2r= [163,176,177,178,265]
global tt3r= [244,287,288,1,70]
global ends = 15+9
function find(a,t)
        for i in 1:length(t)
                if t[i]==a
                        return i
                end
        end
        return 0
end                   
function ic(r,i)
        if r == 0
                return i
        elseif i in tt1r
                j = find(i,tt1r)
                return 288+(r-1)*ends+j
        elseif i in tt2r
                j = find(i,tt2r)
                return 288+(r-1)*ends+j+5
        elseif i in tt3r
                j = find(i,tt3r)
                return 288+(r-1)*ends+j+10
        else
                return ic(r-1,i-1)
        end
end             
function fixx(f,x,t)
        if t
	        write(f,string(x," 0\n"))
        else
                write(f,string("-",x," 0\n"))
        end
end
function fixx(f,x,t)
        if t
                for xi in x
	                write(f,string(xi," 0\n"))
                end
        else
                for xi in x
                        write(f,string("-",xi," 0\n"))
                end
        end
end
function copyt(f,a,b,c)
	write(f,string("-",a," ",b," ",c," 0\n"))
	write(f,string(a," -",b," 0\n"))
	write(f,string(a," -",c," 0\n"))
end
function and(f,y,b,c)
	write(f,string("-",b," -",c," ",y," 0\n"))
	write(f,string("-",y," ",b," 0\n"))
	write(f,string("-",y," ",c," 0\n"))
end
function xortriv(f,a,y,d,e,t)
	write(f,string(t," -",a," 0\n"))
	write(f,string(t," -",y," 0\n"))
	write(f,string(t," -",d," 0\n"))
	write(f,string(t," -",e," 0\n"))
	write(f,string(a," ",y," ",d," ",e," -",t," 0\n"))
        write(f,string("-",a," -",y," 0\n"))
        write(f,string("-",a," -",d," 0\n"))
        write(f,string("-",a," -",e," 0\n"))
        write(f,string("-",y," -",d," 0\n"))
        write(f,string("-",y," -",e," 0\n"))
        write(f,string("-",d," -",e," 0\n"))
end
function laststate(f)
        r = Nb
        fixx(f,[r*ends+i for i in 1:288 if i != 66 && i != 93 && i != 162 && i != 177 && i != 243 && i != 288],false)
        write(f,string(r*ends+66," ",r*ends+93," ",r*ends+162," ",r*ends+177," ",r*ends+243," ",r*ends+288," 0\n"))
        write(f,string("-",r*ends+66," -",r*ends+93," 0\n"))
        write(f,string("-",r*ends+66," -",r*ends+162," 0\n"))
        write(f,string("-",r*ends+66," -",r*ends+177," 0\n"))
        write(f,string("-",r*ends+66," -",r*ends+243," 0\n"))
        write(f,string("-",r*ends+66," -",r*ends+288," 0\n"))
        write(f,string("-",r*ends+93," -",r*ends+162," 0\n"))
        write(f,string("-",r*ends+93," -",r*ends+177," 0\n"))
        write(f,string("-",r*ends+93," -",r*ends+243," 0\n"))
        write(f,string("-",r*ends+93," -",r*ends+288," 0\n"))
        write(f,string("-",r*ends+162," -",r*ends+177," 0\n"))
        write(f,string("-",r*ends+162," -",r*ends+243," 0\n"))
        write(f,string("-",r*ends+162," -",r*ends+288," 0\n"))
        write(f,string("-",r*ends+177," -",r*ends+243," 0\n"))
        write(f,string("-",r*ends+177," -",r*ends+288," 0\n"))
        write(f,string("-",r*ends+243," -",r*ends+288," 0\n"))
end
function shift(f,s)
        for x in s
                write(f,string(x," -",x-ends-1," 0\n"))
                write(f,string(x-ends-1," -",x," 0\n"))
        end
end
function f(f,r,t,copy)
        copyt(f,(r-1)*ends+t[1],(r-1)*ends+copy[1],r*ends+t[1]+1)
        copyt(f,(r-1)*ends+t[2],(r-1)*ends+copy[2],r*ends+t[2]+1)
        copyt(f,(r-1)*ends+t[3],(r-1)*ends+copy[2],r*ends+t[3]+1)
        copyt(f,(r-1)*ends+t[5],(r-1)*ends+copy[3],r*ends+t[5]+1)
        xortriv(f,(r-1)*ends+copy[1],(r-1)*ends+copy[2],(r-1)*ends+t[4],(r-1)*ends+copy[3],r*ends+t[6])
end
function generate()
        open(string(ch,"trivium.cnf"),"w") do d
                # constantes à 0
                fixx(d,[i for i in 81:93],false)
                fixx(d,[i for i in 93+81:285],false)
                # cube
                fixx(d,[i for i in 94:93+80 if i!= 93+34 && i!=93+47],true)
                fixx(d,93+34,false)
                fixx(d,93+47,false)
                # Monome clé (test papier 441 p16)
                #fixx(d,[i for i in 1:80 if i!= 12],false)
                #fixx(d,12,true)
                # Last state
                laststate(d)
                
                tt3 = [243, 286, 287, 288, 69, 1] #a+bc+d+e=t
                tt1 = [66, 91, 92, 93, 171, 94]
                tt2 = [162, 175, 176, 177, 264, 178]
                for r in 1:(Nb)
                        f(d,r,tt3,copy3)
                        f(d,r,tt1,copy1)
                        f(d,r,tt2,copy2)
                        shift(d,[r*ends+j for j in 2:288 if !(j-1 in tt1[1:end-1]) && !(j-1 in tt2[1:end-1]) && !(j-1 in tt3[1:end-1]) && !(j in [1,94,178])]) 
                end
        end
end
function solvez3()
        println("\n    Solving with z3\n")
        run(pipeline(`/Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf`, stdout = "/Users/agontier/Desktop/crypto/res.out"))
end
function solveglucose()
        println("\n    Solving with Glucose Parallel\n")
        run(pipeline(`/Users/agontier/Desktop/glucose-syrup-4.1/parallel/glucose-syrup /Users/agontier/Desktop/crypto/trivium.cnf`),wait=true)

end
function printv(v)
        for b in v
                print(if b "1" else "."end)
        end
        println()
end
function getsol(ss)
        t = zeros(Bool,841,288)
        for r in 1:841
                for i in 1:288
                        t[r,i]=ss[(r-1)*ends+i]
                end
        end
        return t
end
function printsol(t)
        for r in 1:100
                println()
                for i in 1:93
                        print(if t[r,i]'1' else '.' end)
                end
                println()
                for i in 94:177
                        print(if t[r,i]'1' else '.' end)
                end
                println()
                for i in 178:288
                        print(if t[r,i]'1' else '.' end)
                end
        end
end
function writesol(t)
        open(string(ch,"sol.txt"),"w") do f
                for i in 1:288
                        write(f,string(if t[1,i]'1'else'0'end))
                end
                for r in 2:841
                        write(f,"\n")
                        for i in 1:288
                                write(f,string(if t[r,i]'1'else'0'end))
                        end
                end
        end
end
function printsoljava(t)
        open(string(ch,"javasol.txt"),"w") do f
                write(f,string("final boolean[][] matrix = {\n"))
                for r in 1:839
                       write(f,string('{'))
                        for i in 1:287
                                write(f,string(t[r,i],','))
                        end
                        write(f,string(t[r,288],"},\n"))
                end
                write(f,string('{'))
                for i in 1:287
                        write(f,string(t[840,i],','))
                end
                write(f,string(t[840,288],"}};"))
        end
end
function interpret()
        open(string(ch,"res.out"),"r") do s
                s = readlines(s)
                if s[1]=="sat"
                        ss=[parse(Int,i)>0 for i in split(s[2][1:end-1]," ")]
                        print("\nK :")
                        printv(ss[1:80])
                        print("\nIV:")
                        printv(ss[94:173])
                        println("\nThere is at least one trail for K at z",Nb," but SAT needs to count now")
                        t = getsol(ss)
                        printsol(t)
                        writesol(t)
                else
                        println("UNSAT there must be an error")
                end
        end
end
function multisolve()
        open(string(ch,"trivium.cnf"),"w") do d
                # constantes à 0
                fixx(d,[i for i in 81:93],false)
                fixx(d,[i for i in 93+81:285],false)
                # cube
                fixx(d,[i for i in 94:93+80 if i!= 93+34 && i!=93+47],true)
                fixx(d,93+34,false)
                fixx(d,93+47,false)
                # Monome clé (test papier 441 p16)
                fixx(d,[i for i in 1:80 if i!= 12],false)
                fixx(d,12,true)
                # Last state
                laststate(d)
                
                tt3 = [243, 286, 287, 288, 69, 1] #a+bc+d+e=t
                tt1 = [66, 91, 92, 93, 171, 94]
                tt2 = [162, 175, 176, 177, 264, 178]
                for r in 1:(Nb)
                        f(d,r,tt3,copy3)
                        f(d,r,tt1,copy1)
                        f(d,r,tt2,copy2)
                        shift(d,[r*ends+j for j in 2:288 if !(j-1 in tt1[1:end-1]) && !(j-1 in tt2[1:end-1]) && !(j-1 in tt3[1:end-1]) && !(j in [1,94,178])]) 
                end
                sat = true
                ssm1=""
                while (sat)
                        solvez3()
                        open(string(ch,"res.out"),"r") do s
                                s = readlines(s)
                                if s[1]=="sat"
                                        ss=s[2]
                                        if length(ssm1)==length(ss)
                                                println(ssm1==ss)
                                        end
                                        ssm1 = ss

                                        for i in split(s[2][1:end-1]," ")
                                                if i[1]=='-'
                                                        write(d,string(i[2:end],' '))
                                                else
                                                        write(d,string('-',i,' '))
                                                end
                                        end
                                        write(d,"0\n")
                                else
                                        sat = false
                                        println("UNSAT there must be an error")
                                end
                        end
                end
        end
end
function main()
        #println("Writing trivium.cnf DIMACS monomial propag model")
        #generate()
        #@time solvez3()
        #interpret()
        [println(r,',',i,'=',ic(r,i)) for i in 1:288, r in 0:5]
        #@time solveglucose()
        #multisolve()
end

main()

#=
/Applications/Julia-1.5.app/Contents/Resources/julia/bin/julia /Users/agontier/Desktop/crypto/triviumSATbrut.jl
/Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf > /Users/agontier/Desktop/crypto/res.out
=#
