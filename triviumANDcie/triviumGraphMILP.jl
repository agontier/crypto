using JuMP, GLPK, MathOptInterface,  LinearAlgebra, Gurobi#, Profile#, MathProgBase, Clp
function sons(n)
        r = n[1]
        i = n[2]
        if i==1 return [[r-69,1],[r-66,3],[r-111,3],[r-110,3]] end
        if i==2 return [[r-78,2],[r-66,1],[r-93,1], [r-92,1]]  end
        if i==3 return [[r-87,3],[r-69,2],[r-84,2], [r-83,2]]  end
end
function allsons(n)
        r = n[1]
        i = n[2]
        if i==1 return [[r-69,1],[r-66,3],[r-111,3],[r-110,3],[r-109,3]] end
        if i==2 return [[r-78,2],[r-66,1],[r-93,1], [r-92,1], [r-91,1]]  end
        if i==3 return [[r-87,3],[r-69,2],[r-84,2], [r-83,2], [r-82,2]]  end
end
global cube = [1, 11, 21, 31, 41, 51, 61, 71]
#global cube = [4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80]
#global cube = [3, 6, 8, 11, 15, 25, 28, 40, 50, 57, 58, 62]
#global cube = [3, 14, 21, 25, 38, 43, 44, 47, 54, 56, 58, 68]
global nb = 590

global sources = [[nb-66,3], [nb-66,1], [nb-69,2], [nb-111,3], [nb-93,1], [nb-84,2]]

env = Gurobi.Env()
m = Model(with_optimizer(Gurobi.Optimizer,
                         PoolSearchMode=2,
                         PoolSolutions=20,# nb de sol max
                         PoolGap = 2000000000,
                         OutputFlag=1, env))
x = Dict{Tuple{Array{Int64,1},Array{Int64,1}},VariableRef}()

# Construction du graphe
todo = [source for source in sources]
found = [source for source in sources]
global arcs=[]
global nodes = []
while length(todo)>0
        p = pop!(todo)
        for s in allsons(p)
                push!(arcs,[p,s])
                if s[1]>0
                        if !(s in found)
                                push!(nodes,s)
                                push!(todo,s)
                                push!(found,s)
                        end
                end
        end
end
function pere(n)
        return [a[1] for a in arcs if a[2]==n]
end
function cardpere(n)
        return length(pere(n))
end

@variable(m,x[arcs],Bin)#les varriables binaires sont les arcs
@variable(m,a[1:93],Bin)#trois registres
@variable(m,b[1:84],Bin)
@variable(m,c[1:111],Bin)

# Depart
@constraint(m,startsource,sum(x[[source,s]] for source in sources for s in sons(source))==1)
# un seul fils
@constraint(m,uniquefils[n in nodes],sum(x[[n,s]] for s in sons(n))<=1)

# Contraintes qui relient les registres au graphe
@constraint(m,pa[r in 1:length(a)],a[r]<=sum(x[[p,[-r+1,1]]] for p in pere([-r+1,1])))
@constraint(m,da[r in 1:length(a),p=pere([-r+1,1])],a[r]>=x[[p,[-r+1,1]]])
@constraint(m,pb[r in 1:length(b)],b[r]<=sum(x[[p,[-r+1,2]]] for p in pere([-r+1,2])))
@constraint(m,db[r in 1:length(b),p=pere([-r+1,2])],b[r]>=x[[p,[-r+1,2]]])
@constraint(m,pc[r in 1:length(c)],c[r]<=sum(x[[p,[-r+1,3]]] for p in pere([-r+1,3])))
@constraint(m,dc[r in 1:length(c),p=pere([-r+1,3])],c[r]>=x[[p,[-r+1,3]]])

# les freres sont egaux
@constraint(m,bros[n in sources],x[[n,sons(n)[end]]]==x[[n,allsons(n)[end]]])
@constraint(m,bro[n in nodes],   x[[n,sons(n)[end]]]==x[[n,allsons(n)[end]]])

# flots
@constraint(m,flot1[n in nodes],
            sum(x[[p,n]] for p in pere(n))>=sum(x[[n,s]] for s in sons(n)))
@constraint(m,flot2[n in nodes],
            sum(x[[p,n]] for p in pere(n))<=sum(x[[n,s]] for s in sons(n))*cardpere(n))

# Cube
@constraint(m,cube1[r in cube], b[r]==1)
#@constraint(m,cube0[r=1:length(b);!(r in cube)],b[r]==0)

# Constantes
@constraint(m,a0[r=81:length(a)],a[r]==0)
@constraint(m,c0[r=1:length(c)-3],c[r]==0)

#@objective(m,Max,sum(a))

if true
optimize!(m)
if termination_status(m)==MOI.OPTIMAL
        #affichage de la derniere sol
        x = round.(Int,value.(m[:x]))
        for i in arcs if x[i]>0 println(i) end end
        [print("x",i) for i in 1:length(a) if value.(m[:a])[i]==1]
        println()
        [print("v",i) for i in 1:length(b) if value.(m[:b])[i]==1]
        println()
        #println(round.(Int,value.(m[:a])))
        #println(round.(Int,value.(m[:b])))
end
end

