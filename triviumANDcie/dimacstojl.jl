nbetu = 25
nbsuj = 26
nbveu = 2
nbchoix = 26
global veu = [[] for i in 1:nbetu]

function ran()
        r = convert(Int,floor(rand()*nbsuj))
#        r = convert(Int,floor((randn()+3)*(nbsuj/6)))
        return if r<1 1 elseif r>nbsuj nbsuj else r end
end

function ranveu()
        r = convert(Int,floor((randn()+3)*(nbveu/6)))
        return if r<1 1 elseif r>nbsuj nbsuj else r end
end

function pretyprint(A)
        for i in A
                println(i)
        end
        println()
end

function makeveu()
        veu = [[] for i in 1:nbetu]
        for i in 1:nbetu
                tmp = []
                for j in 1:5#7+ranveu()
                        rd = ran()
                        while rd in tmp
                                rd = ran()
                        end
                        push!(tmp,rd)
                end
                veu[i]=tmp
        end
        return veu
end

function isconflict(i,j)
        flag = false
        for ii in 1:nbetu
                if i!=ii
                        if length(veu[ii])>=j
                                if veu[i][j] == veu[ii][j] || veu[i][j]==0
                                        flag = true
                                end
                        end
                end
        end
        return flag
end

function nbconflict(i,j)
        nb = 0
        continu = true
        for jj in j:length(veu[i])
                if isconflict(i,jj) && continu
                        nb = nb+1
                else
                        continu = false
                end
        end
        return nb
end

function fixchoix(etu,choix)
        veu[etu] = [veu[etu][choix]]
        for etu2 in 1:nbetu
                if etu != etu2
                        for j in 1:length(veu[etu2])
                                if veu[etu][1] == veu[etu2][j]
                                        veu[etu2][j] = 0
                                end
                        end
                end
        end
end

function getsame(etu,choix)
        sam = []
        for etu2 in 1:nbetu
                if length(veu[etu2]) > 1
                        if length(veu[etu2])>=choix
                                if veu[etu][choix] == veu[etu2][choix]
                                        push!(sam,etu2)
                                end
                        end
                end
        end
        return sam
end

function estok(veu)
        for v in veu
                if length(v)>1
                        return false
                end
        end
        return true
end

function makestat(veu,res)
        nbr = [0 for i in 1:nbetu]
        for etu in 1:nbetu
                if res[etu]==0
                        nbr[etu] = 0
                else
                        nbr[etu] = findfirst(k->k==res[etu],veu[etu])
                end
        end
        for i in 0:nbetu
                print(count(k->k==i,nbr),' ')
        end
        println()
        #println(nbr)
end

                
function test()
        pasok = true
        choix = 1
        
        veuinit = makeveu()
        global veu = deepcopy(veuinit)
        while pasok
                #pretyprint(veu)
                #println(conflicts)
                for etu in 1:nbetu
                        if sum(veu[etu])==0
                                veu[etu]=[0]
                        end
                end
                for etu in 1:nbetu
                        conflicts = [nbconflict(i,choix) for i in 1:nbetu]
                        if conflicts[etu] == 0 && 
                                length(veu[etu]) > 1 &&
                                length(veu[etu]) >= choix &&
                                veu[etu][choix] != 0
                                fixchoix(etu,choix)
                        end
                end
                for etu in 1:nbetu
                        conflicts = [nbconflict(i,choix) for i in 1:nbetu]
                        if conflicts[etu] > 0 && 
                                length(veu[etu]) > 1 &&
                                length(veu[etu]) >= choix &&
                                veu[etu][choix] != 0
                                max = 0
                                sad = etu
                                for etu2 in getsame(etu,choix)
                                        if conflicts[etu2]>max
                                                max = conflicts[etu2]
                                                sad = etu2
                                        end
                                end
                                fixchoix(sad,choix)
                        end
                end
                if estok(veu) || choix > nbchoix + 1
                        pasok = false
                else
                        choix = choix + 1
                end
        end

        #pretyprint(veu)
        #println(conflicts)
        res = [v[1] for v in veu]
        makestat(veuinit,res)
end

for i in 1:100
        test()
end
