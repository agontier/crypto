using JuMP, MathOptInterface,  LinearAlgebra, Gurobi#, Profile#, MathProgBase, GLPK, Clp
function sons(n)
        r = n[1]-128
        i = n[2]
        if n[1]==nb
                return [[r+ 2,1],[r+15,1],[r+36,1],[r+45,1],[r+64,1],[r+73,1],[r+89,1],[r+93,2],
                        [r+13,2],[r+60,2],
                        [r+ 8,2],[r+42,2],[r+94,2]] end
        if i==1 return [[r+ 0,1],[r+ 2,1],[r+15,1],[r+26,1],[r+36,1],[r+45,1],[r+56,1],[r+64,1],[r+73,1],[r+89,1],[r+91,1],[r+96,1],
                        [r+ 3,1],[r+11,1],[r+17,1],[r+27,1],[r+40,1],[r+61,1],[r+68,1],
                        [r+22,1],[r+70,1],[r+88,1],
                        [r+ 0,2],[r+93,2],
                        [r+13,2],[r+60,2],
                        [r+ 8,2],[r+42,2],[r+94,2]] end
        if i==2 return [[r+ 0,2],[r+ 7,2],[r+28,2],[r+70,2],[r+81,2],[r+93,2],[r+96,2],
                        [r+ 0,1],[r+ 2,1],[r+15,1],[r+26,1],[r+36,1],[r+45,1],[r+56,1],[r+64,1],[r+73,1],[r+89,1],[r+96,1],
                        [r+13,2],[r+60,2],
                        [r+ 8,2],[r+42,2],[r+94,2]] end
end
function allsons(n)
        r = n[1]-128
        return vcat(sons(n),foldl(vcat,[bros([n,s]) for s in sons(n)]),[[r+12,1],[r+95,1]])
end
function bros(a)
        r = a[1][1]-128
        dr = a[2][1]-r
        if a[1][2]==1
                if dr== 3 return [[r+67,1]] end
                if dr==11 return [[r+13,1]] end
                if dr==17 return [[r+18,1]] end
                if dr==27 return [[r+59,1]] end
                if dr==40 return [[r+48,1]] end
                if dr==61 return [[r+65,1]] end
                if dr==68 return [[r+84,1]] end
                if dr==22 return [[r+24,1],[r+25,1]] end
                if dr==70 return [[r+78,1],[r+82,1]] end
                if dr==88 return [[r+92,1],[r+93,1]] end                
        end
        if dr==13 return [[r+20,2]] end
        if dr==60 return [[r+79,2]] end
        return []
end
global cube = [i for i in 1:96 if i!=47]
global nb = 190
source = [nb,2]
list = [source]
rest = [source]
global arcs=[]
global nodes=[]
while length(list)>0
        p = pop!(list)
        for s in allsons(p)
                push!(arcs,[p,s])
                if s[1]>0
                        if !(s in rest)
                                push!(nodes,s)
                                push!(list,s)
                                push!(rest,s)
                        end
                end
        end
end
global allnodes = [n for n in nodes]
push!(allnodes,source)
function pere(n)
        return [a[1] for a in arcs if a[2]==n]
end
function cardpere(n)
        return length(pere(n))
end

env = Gurobi.Env()
m = Model(with_optimizer(Gurobi.Optimizer,
                         PoolSearchMode=2,
                         PoolSolutions=10,
                         PoolGap = 2000000000,
                         OutputFlag=1, env))
x = Dict{Tuple{Array{Int64,1},Array{Int64,1}},VariableRef}()

@variable(m,x[arcs],Bin)
@variable(m,k[1:128],Bin)
@variable(m,v[1:128],Bin)

@constraint(m,uniquesource,sum(x[[source,s]] for s in sons(source))==1)
@constraint(m,uniquefils[n in nodes],sum(x[[n,s]] for s in sons(n))<=1)

@constraint(m,bro[n in nodes,s in sons(n),b in bros([n,s])],x[[n,s]]==x[[n,b]])

@constraint(m,relou12a[n in allnodes],x[[n,[n[1]-128+12,1]]]>=x[[n,[n[1]-128+ 8,2]]])
@constraint(m,relou12b[n in allnodes],x[[n,[n[1]-128+12,1]]]>=x[[n,[n[1]-128+94,2]]])
@constraint(m,relou12z[n in allnodes],x[[n,[n[1]-128+12,1]]]<=x[[n,[n[1]-128+ 8,2]]]+x[[n,[n[1]-128+94,2]]])

@constraint(m,relou95a1[n in allnodes;n[2]==1],x[[n,[n[1]-128+95,1]]]>=x[[n,[n[1]-128+42,2]]])
@constraint(m,relou95b1[n in allnodes;n[2]==1],x[[n,[n[1]-128+95,1]]]>=x[[n,[n[1]-128+94,2]]])
@constraint(m,relou95c1[n in allnodes;n[2]==1],x[[n,[n[1]-128+95,1]]]>=x[[n,[n[1]-128+88,1]]])
@constraint(m,relou95z1[n in allnodes;n[2]==1],x[[n,[n[1]-128+95,1]]]<=x[[n,[n[1]-128+42,2]]]+x[[n,[n[1]-128+94,2]]]+x[[n,[n[1]-128+88,1]]])

@constraint(m,relou95a2[n in allnodes;n[2]==2],x[[n,[n[1]-128+95,1]]]>=x[[n,[n[1]-128+42,2]]])
@constraint(m,relou95b2[n in allnodes;n[2]==2],x[[n,[n[1]-128+95,1]]]>=x[[n,[n[1]-128+94,2]]])
@constraint(m,relou95z2[n in allnodes;n[2]==2],x[[n,[n[1]-128+95,1]]]<=x[[n,[n[1]-128+42,2]]]+x[[n,[n[1]-128+94,2]]])

@constraint(m,flot1[n in nodes],sum(x[[p,n]] for p in pere(n))>=sum(x[[n,s]] for s in sons(n)))
@constraint(m,flot2[n in nodes],sum(x[[p,n]] for p in pere(n))<=sum(x[[n,s]] for s in sons(n))*cardpere(n))

@constraint(m,pk[r=1:length(k)],k[r]<=sum(x[[p,[-r+1,1]]] for p in pere([-r+1,1])))
@constraint(m,dk[r=1:length(k),p=pere([-r+1,1])],k[r]>=x[[p,[-r+1,1]]])
@constraint(m,pv[r=1:length(v)],v[r]<=sum(x[[p,[-r+1,2]]] for p in pere([-r+1,2])))
@constraint(m,dv[r=1:length(v),p=pere([-r+1,2])],v[r]>=x[[p,[-r+1,2]]])

@constraint(m,cube1[i in 1:96;i!=47],v[i]==1)
@constraint(m,cube0[i in 1:96;i==47],v[i]==0)


#@constraint(m,cstv1[i in 97:127],v[i]==1)
@constraint(m,cstv0[i in 128:128],v[i]==0)

@objective(m,Max,sum(k[i] for i in 1:96))



if true
optimize!(m)
if termination_status(m)==MOI.OPTIMAL
        x = round.(Int,value.(m[:x]))
        for i in arcs if x[i]>0 #println(i)
        end end
        [print("x",i) for i in 1:length(k) if value.(m[:k])[i]==1]
        println()
        [print("v",i) for i in 1:96 if value.(m[:v])[i]==1]
        println()

end
end
#x6x8x14x27x28x30x32x33x39x40x41x42x49x57x66x67x68x71x72x73x74x81x83x89x97
#v1v2v3v4v5v6v7v8v9v10v11v12v13v14v15v16v17v18v19v20v21v22v23v24v25v26v27v28v29v30v31v32v33v34v35v36v37v38v39v40v41v42v43v44v45v46v48v49v50v51v52v53v54v55v56v57v58v59v60v61v62v63v64v65v66v67v68v69v70v71v72v73v74v75v76v77v78v79v80v81v82v83v84v85v86v87v88v89v90v91v92v93v94v95v96
