#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <cstdint>
#include <set>
#include <algorithm>
#include <bitset>

#include "gurobi_c++.h"

using namespace std;

class TriviumNode {
public:
  TriviumNode(int R, int reg, int c) : ro(R), re(reg), ch(c) {};
  TriviumNode(TriviumNode const &) = default;
  TriviumNode(TriviumNode &&) = default;

  TriviumNode & operator=(TriviumNode const &) = default;
  TriviumNode & operator=(TriviumNode &&) = default;

  int const & round() const {return this->ro;};
  int const & registre() const {return this->re;};
  int const & choice() const {return this->ch;};

private:
  int ro;
  int re;
  int ch;
};

bool operator<(TriviumNode const & a, TriviumNode const & b) {
  if (a.round() != b.round()) return a.round() < b.round();
  if (a.registre() != b.registre()) return a.registre() < b.registre();
  return a.choice() < b.choice();
}

bool operator==(TriviumNode const & a, TriviumNode const & b) {
  return (a.round() == b.round()) && (a.registre() == b.registre()) && (a.choice() == b.choice());
}

vector<pair<vector<TriviumNode>, unsigned>> generateBranches(int R, int registre, bool forbid) {
  static map<tuple<int, int, bool>, vector<pair<vector<TriviumNode>, unsigned>>> mapSave;
  static int offsets[3][5] = {{69, 66, 111, 109, 110}, {78, 66, 93, 91, 92}, {87, 69, 84, 82, 83}};
  if (R <= 0) {
    if (registre == 0 && R <= -80) return vector<pair<vector<TriviumNode>, unsigned>>();
    if (registre == 1 && R <= -80) return vector<pair<vector<TriviumNode>, unsigned>>();
    if (registre == 2 && R > -108) return vector<pair<vector<TriviumNode>, unsigned>>();
    // cube
    if (registre == 1 && R == -33) return vector<pair<vector<TriviumNode>, unsigned>>();
    if (registre == 1 && R == -46) return vector<pair<vector<TriviumNode>, unsigned>>();

    vector<TriviumNode> v = (registre == 2) ? vector<TriviumNode> (1, TriviumNode(0, registre, -1)) : vector<TriviumNode> (1, TriviumNode(R, registre, -1));
    pair<vector<TriviumNode>, unsigned> pu = (registre == 1) ? make_pair(v, 1) : make_pair(v,0);

    return vector<pair<vector<TriviumNode>, unsigned>> (1, pu);
  }
  auto t = make_tuple(R, registre, forbid);
  auto it = mapSave.find(t);
  if (it != mapSave.end()) return it->second;
  vector<vector<pair<vector<TriviumNode>, unsigned>>> v (5);
  v[0] = generateBranches(R-offsets[registre][0], registre, forbid);
  v[1] = generateBranches(R-offsets[registre][1], (registre+2)%3, forbid);
  if (offsets[registre][2] < R && offsets[registre][4] < R) v[2] = generateBranches(R-offsets[registre][2], (registre+2)%3, forbid);
  else v[2] = generateBranches(R-offsets[registre][2], (registre+2)%3, forbid);
  if (!forbid) {
    v[3] = generateBranches(R-offsets[registre][3], (registre+2)%3, forbid);
    v[4] = generateBranches(R-offsets[registre][4], (registre+2)%3, forbid);
  }

  map<pair<vector<TriviumNode>, unsigned>, unsigned> my_map;
  for (unsigned i = 0; i < 3; ++i) {
    for (auto const & x : v[i]) my_map[x] += 1;
  }
  if (!forbid) {
    if (v[3].empty()) v[4].clear();
    if (v[4].empty()) v[3].clear();
    unsigned max[5] = {0,0,0,0,0};
    for (int i = 3; i < 5; ++i) {
      for (auto const & x : v[i]) if (max[i] < x.second) max[i] = x.second;
    }
    swap(max[3], max[4]);
    for (int i = 3; i < 5; ++i) {
      for (auto & x : v[i]) {
        x.first.insert(x.first.begin(), TriviumNode(R-offsets[registre][i], (registre+2)%3, 0));
        x.first.insert(x.first.begin(), TriviumNode(R, registre, -1));
        x.second += max[i];
        my_map[x] += 1;
      }
    }
  }
  vector<pair<vector<TriviumNode>, unsigned>> res;
  for (auto const & p : my_map) {
    if (p.second%2 != 0) res.emplace_back(p.first);
  }

  mapSave[t] = res;
  return res;
}

void generateModel(int RR) {
  GRBEnv env = GRBEnv(true);
  env.set("LogFile", "mip1.log");
  env.start();
  int base[6] = {65, 92, 161, 176, 242, 287};
  //map<bitset<80>, unsigned> superpoly;
  for (auto x : base) {
    int registre;
    int R;
    if (x < 93) {R = RR-x; registre = 0;}
    else if (x < 177) {R = RR-x+93; registre = 1;}
    else {R = RR-x+177; registre = 2;}
    //auto ttt = test(R, registre, false, 78);
    auto T = generateBranches(R, registre, false);
    TriviumNode start (R, registre, 0);
    cout << "T: " << T.size() << endl;
    unsigned cpt = 0;
    for (auto const & pv : T) if (pv.second >= 78) ++cpt;
    cout << "cpt: " << cpt << endl;
    cpt = 0;
    map<unsigned, vector<vector<TriviumNode>>> mymap;
    for (auto & pv : T) {
      pv.first.insert(pv.first.begin(), start);
      if (pv.first.back().registre() != 1) continue;
      if (pv.second >= 78 ) {
          mymap[-pv.first.back().round()].emplace_back(pv.first);
      }
    }
    for (auto const & v : mymap) cout << v.second.size() << " ";
    cout << endl;
    set<TriviumNode> s_node;
    set<pair<TriviumNode, TriviumNode>> s_arc;
    for (auto const & pv : mymap) {
      for (auto const & v : pv.second) {
        for (auto const & x : v) s_node.emplace(x);
        for (unsigned i = 1; i < v.size(); ++i) s_arc.emplace(make_pair(v[i-1], v[i]));
      }
    }
    cout << "nodes: " << s_node.size() << endl;
    cout << "arcs: " << s_arc.size() << endl;
    unsigned nVars = 0;
    map<pair<TriviumNode, TriviumNode>, unsigned> mapNodeUint;
    for (auto const & tu : s_arc) mapNodeUint[tu] = nVars++;
    map<TriviumNode, set<unsigned>> nodeForward;
    map<TriviumNode, set<unsigned>> nodeBackward;
    for (auto const & p : s_arc) {
      nodeForward[p.first].emplace(mapNodeUint[p]);
      nodeBackward[p.second].emplace(mapNodeUint[p]);
    }
    GRBModel model = GRBModel(env);
    //model.set(GRB_IntParam_OutputFlag, 0);
    model.read("tune1.prm");

    //model.set(GRB_IntParam_PoolSolutions, 2000);
    //model.set(GRB_IntParam_PoolSearchMode, 2);

    vector<GRBVar> X (nVars);
    for (unsigned i = 0; i < nVars; ++i) X[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    for (auto const & p : nodeForward) {
      if (p.first.choice() == 0) {
        GRBLinExpr e;
        for (auto i : p.second) e += X[i];
        model.addConstr(e <= 1);
        auto it = nodeBackward.find(p.first);
        if (it == nodeBackward.end()) continue;
        GRBLinExpr f;
        for (auto i : it->second) f += X[i];
        model.addConstr(e == f);
      }
      else {
        auto it = nodeBackward.find(p.first);
        if (it == nodeBackward.end()) continue;
        GRBLinExpr f;
        for (auto i : it->second) f += X[i];
        GRBLinExpr e;
        for (auto i : p.second) e += X[i];
        model.addConstr(f <= e);
        model.addConstr(p.second.size()*f >= e);
      }
    }
    for (auto const & p : nodeBackward) {
      auto it = nodeForward.find(p.first);
      if (it == nodeForward.end()) {
        GRBLinExpr e;
        for (auto i : p.second) e += X[i];
        model.addConstr(e >= 1);
      }
    }
    model.optimize();
    getchar();
  }
}



int main(int argc, char const *argv[]) {
  //milpTriviumDP(840);
  //getchar();

  unsigned R = stoi(argv[1]);

  generateModel(R);

  return 0;

}
