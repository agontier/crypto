import org.chocosolver.cutoffseq.LubyCutoffStrategy;
import org.chocosolver.util.tools.ArrayUtils;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDeg;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDeg;
import org.chocosolver.util.tools.ArrayUtils;

public class GrainTableV1 {

	 public static int NBROUNDS;
	 public static int OPT;
	 public static Model m;
	 public static Tuples table;
	 
	public static void main(String [] args) {
		Model m = new Model();	 		 
		long start = System.currentTimeMillis();
		NBROUNDS = Integer.parseInt(args[0]);
		OPT = Integer.parseInt(args[1]);
		solve(m,NBROUNDS,OPT);
		long end = System.currentTimeMillis();
		System.out.println("time for GrainTableV1:" + NBROUNDS  + " with Option " + OPT + ":" + (end - start) + " in milliseconds");	 		
	}
	
	public static void solve(Model m, int Nb,int Opt) {
		BoolVar[][] B = new BoolVar[Nb + 1][128];
		BoolVar[][] S = new BoolVar[Nb + 1][128];
		
		
		BoolVar[] S7 = new BoolVar[Nb]; 	   //F		
		BoolVar[] S13 = new BoolVar[Nb]; // H
		BoolVar[] S20= new BoolVar[Nb]; // H
		BoolVar[] S38 = new BoolVar[Nb];  // F
		BoolVar[] S60= new BoolVar[Nb]; // H
		BoolVar[] S70 = new BoolVar[Nb]; 	   // F	
		BoolVar[] S79= new BoolVar[Nb]; // H
		BoolVar[] S81 = new BoolVar[Nb]; // F	
		BoolVar[] S93 = new BoolVar[Nb]; // H		
		BoolVar[] S96 = new BoolVar[Nb]; 	    // F
		
		BoolVar[] B2 = new BoolVar[Nb]; // H
		BoolVar[] B15 = new BoolVar[Nb]; // H
		BoolVar[] B26 = new BoolVar[Nb]; 	   //G		
		BoolVar[] B36 = new BoolVar[Nb];// H
		BoolVar[] B45 = new BoolVar[Nb];// H		
		BoolVar[] B56 = new BoolVar[Nb]; 	   //G
		BoolVar[] B64 = new BoolVar[Nb];// H
		BoolVar[] B73 = new BoolVar[Nb];// H
		BoolVar[] B89 = new BoolVar[Nb];// H
		BoolVar[] B91 = new BoolVar[Nb]; 	   //G		
		BoolVar[] B96 = new BoolVar[Nb]; 	   //G
			
		
		BoolVar[][] G = new BoolVar[Nb][10];    	
		BoolVar[][] H = new BoolVar[Nb][3];	
		BoolVar[][] Z = new BoolVar[Nb][3];
		
		
		table = createTable();	       
	    
	     //Declaration
		    for (int r=0;r < Nb;r++) {   
		       for (int i=1;i<=3;i++) {H[r][i-1] = m.boolVar();}
		       for (int i=1;i<=10; ++i) {G[r][i-1] =m.boolVar();}
		       for (int i=1;i<=3;i++) {Z[r][i-1] = m.boolVar();}
		       
		       S7[r] = m.boolVar(); 
		       S13[r] = m.boolVar(); 
		       S20[r] = m.boolVar(); 
		       S38[r] = m.boolVar(); 
		       S60[r] = m.boolVar();
		       S70[r] = m.boolVar(); 
		       S79[r] = m.boolVar(); 
		       S81[r] = m.boolVar(); 
		       S93[r] = m.boolVar(); 
		       S96[r] = m.boolVar();    
		       	   	
				B2[r] = m.boolVar(); 
				B15[r] = m.boolVar();
				B26[r] = m.boolVar();
				B36[r] = m.boolVar();
				B45[r] = m.boolVar(); 
				B56[r] = m.boolVar();
				B64[r] = m.boolVar();
				B73[r] = m.boolVar();
				B89[r] = m.boolVar();
				B91[r] = m.boolVar();
				B96[r] = m.boolVar();
		}
		

		    // initialisation
		       B[0][34-1] = m.boolVar(true);
		       B[0][39-1] = m.boolVar(true);
		       B[0][53-1] = m.boolVar(true);
		       B[0][62-1] = m.boolVar(true);
		       B[0][64-1] = m.boolVar(true);
		       B[0][81-1] = m.boolVar(true);
		       B[0][83-1] = m.boolVar(true);
		       B[0][84-1] = m.boolVar(true);
		       B[0][95-1] = m.boolVar(true);
		       B[0][125-1] = m.boolVar(true);
			       
		       //B[0][32] = m.boolVar(true);
		       for (int i=1;i<=128; ++i)  {if (B[0][i-1] == null) {B[0][i-1] =m.boolVar(false);}}
		       
		       
		       for (int i= 1; i<=46; i++) S[0][i-1] = m.boolVar(true);
		       S[0][47-1] = m.boolVar(false);
		       for (int i= 48; i<=96; i++) S[0][i-1] = m.boolVar(true);
		       S[0][128-1] = m.boolVar(false);
		       
		       for (int i=1;i<=128; ++i)  {if (S[0][i-1] == null) {S[0][i-1] =m.boolVar();}}
		       
		         
	       
	       // propagation	 
			 for (int r=1; r<= Nb ; r++) {
				 for (int i=1; i<=128; i++) {
					 if (i!=26 && i!=56 && i!=91 && i!=96 && i!=3 && i!=67 && i!=11 && 
					  i!=13 && i!=17 && i!=18 && i!=27 && i!=59 && i!=40 && i!=48 && i!=61 && i!=65 && i!=68 && i!=84
					  && i!= 88 && i!=92 && i!=93 && i!=95 && i!=22 && i!=24 && i!=25 && i!=70 && i!=78 && i!=82
					  && i!=12 && i!=2 && i!=15 && i!=36 && i!=45 && i!=64 && i!=73 && i!= 89 && i!=128) {
					  B[r][i -1] = B[r - 1][i];
					   } else B[r][i-1] =m.boolVar();
					 if (i!=7 && i!=38 && i!=70 && i!=81 && i!=96 && i!=8 && i!=13 && i!=20 && i!=42 && i!=60 && i!=79 && 
							 i!=94 && i!=93 && i!=128) {
					  S[r][i -1] = S[r - 1][i];
					  } else S[r][i-1] =m.boolVar();
				 }
				 
				 m.table(new BoolVar[]{B[r-1][12],B[r-1][88], B[r-1][92],B[r-1][93],B[r-1][95],S[r-1][8],S[r-1][42],S[r-1][94],
						               H[r-1][0],G[r-1][7],
						               B[r][11],B[r][87], B[r][91],B[r][92],B[r][94],S[r][7],S[r][41],S[r][93]}, table).post();
				 
				   //Copy	
				    m.max(B[r-1][2],  new BoolVar[] {B2[r-1],B[r][1]}).post();//B2
				    m.max(B[r-1][15], new BoolVar[] {B15[r-1],B[r][14]}).post();//B15
				  
				    m.max(B[r-1][36], new BoolVar[] {B36[r-1],B[r][35]}).post();//B36
				    m.max(B[r-1][45], new BoolVar[] {B45[r-1],B[r][44]}).post();//B45
				    m.max(B[r-1][64], new BoolVar[] {B64[r-1],B[r][63]}).post();//B64
				    m.max(B[r-1][73], new BoolVar[] {B73[r-1],B[r][72]}).post();//B73
				    m.max(B[r-1][89], new BoolVar[] {B89[r-1],B[r][88]}).post();//B89
			
				    m.max(B[r-1][26], new BoolVar[] {B26[r-1],B[r][25]}).post();
				    m.max(B[r-1][56], new BoolVar[] {B56[r-1],B[r][55]}).post();
				    m.max(B[r-1][91], new BoolVar[] {B91[r-1],B[r][90]}).post();
				    m.max(B[r-1][96], new BoolVar[] {B96[r-1],B[r][95]}).post();
			
			
				    m.max(S[r-1][13], new BoolVar[] {S13[r-1],S[r][12]}).post();//S13
				    m.max(S[r-1][20], new BoolVar[] {S20[r-1],S[r][19]}).post();//S20
				    m.max(S[r-1][60], new BoolVar[] {S60[r-1],S[r][59]}).post();//S60
				    m.max(S[r-1][79], new BoolVar[] {S79[r-1],S[r][78]}).post();//S79
				    
				    m.max(S[r-1][93], new BoolVar[] {S93[r-1],S[r][92]}).post();//S93
					 
			
				    
				    m.max(S[r-1][7], new BoolVar[] {S7[r-1],S[r][6]}).post();
				    m.max(S[r-1][38], new BoolVar[] {S38[r-1],S[r][37]}).post();
				    m.max(S[r-1][70], new BoolVar[] {S70[r-1],S[r][69]}).post();
				    m.max(S[r-1][81], new BoolVar[] {S81[r-1],S[r][80]}).post();
				    m.max(S[r-1][96], new BoolVar[] {S96[r-1],S[r][95]}).post();
						      
					
					
					 // Calcul de z = (s0) + h+  b12s8 + s13s20 + b95s42 + s60s79 + b12b95s94 
				
					 S13[r-1].eq(H[r-1][1]).post(); 
					 S20[r-1].eq(H[r-1][1]).post(); 
					
					
					 S60[r-1].eq(H[r-1][2]).post(); 
					 S79[r-1].eq(H[r-1][2]).post(); 
					
				
				
				      BoolVar[] tmpZ = {
				    		  S[r-1][0],
				    		  H[r-1][0], H[r-1][1], H[r-1][2], 				    		  
				    		  S93[r-1], B2[r-1], B15[r-1],B36[r-1],B45[r-1],B64[r-1],B73[r-1],B89[r-1]
				      };
				      m.sum(tmpZ, "=", Z[r-1][0]).post(); 
		
			   	// Copy de Z
				     m.max(Z[r-1][0],  new BoolVar[] {Z[r-1][2],Z[r-1][1]}).post();	
				     
				  // Calcul s127
				     BoolVar[] tmpS = {
				    		S7[r-1],S38[r-1],S70[r-1],S81[r-1],S96[r-1],
				    		  Z[r-1][2] };
				        m.sum(tmpS, "=", S[r][127]).post(); 
				        
				        
				     // Calcul b127
				        // calcul de g: (b0 + b26 + b56 + b91 + b96) + b3b67 + b11b13 + b17b18 + b27b59 + b40b48 + b61b65 + b68b84
				        // + b88b92b93b95 + b22b24b25 + b70b78b82
				       
						
				        m.max(B[r - 1][3], new BoolVar[]{G[r-1][0], B[r][2]}).post();// b3 is copied in G[0]
						m.max(B[r - 1][67], new BoolVar[]{G[r-1][0], B[r][66]}).post();// b67 is copied in G[0]
						
						m.max(B[r - 1][11], new BoolVar[]{G[r-1][1], B[r][10]}).post();// b11 is copied in G[1]
						m.max(B[r - 1][13], new BoolVar[]{G[r-1][1], B[r][12]}).post();// b13 is copied in G[1]
						
						m.max(B[r - 1][17], new BoolVar[]{G[r-1][2], B[r][16]}).post();// b17 is copied in G[2]
						m.max(B[r - 1][18], new BoolVar[]{G[r-1][2], B[r][17]}).post();// b18 is copied in G[2]
						
						m.max(B[r - 1][27], new BoolVar[]{G[r-1][3], B[r][26]}).post();// b27 is copied in G[3]
						m.max(B[r - 1][59], new BoolVar[]{G[r-1][3], B[r][58]}).post();// b59 is copied in G[3]
					
						m.max(B[r - 1][40], new BoolVar[]{G[r-1][4], B[r][39]}).post();// b39 is copied in G[4]
						m.max(B[r - 1][48], new BoolVar[]{G[r-1][4], B[r][47]}).post();// b47 is copied in G[4]
					
						m.max(B[r - 1][61], new BoolVar[]{G[r-1][5], B[r][60]}).post();// b61 is copied in G[5]
						m.max(B[r - 1][65], new BoolVar[]{G[r-1][5], B[r][64]}).post();// b65 is copied in G[5]
						
						m.max(B[r - 1][68], new BoolVar[]{G[r-1][6], B[r][67]}).post();// b68 is copied in G[6]
						m.max(B[r - 1][84], new BoolVar[]{G[r-1][6], B[r][83]}).post();// b84 is copied in G[6]
						
					
						m.max(B[r - 1][22], new BoolVar[]{G[r-1][8], B[r][21]}).post();// b22 is copied in G[8]
						m.max(B[r - 1][24], new BoolVar[]{G[r-1][8], B[r][23]}).post();// b24 is copied in G[8]
						m.max(B[r - 1][25], new BoolVar[]{G[r-1][8], B[r][24]}).post();// b25 is copied in G[8]
						
						m.max(B[r - 1][70], new BoolVar[]{G[r-1][9], B[r][69]}).post();// b70 is copied in G[9]
						m.max(B[r - 1][78], new BoolVar[]{G[r-1][9], B[r][77]}).post();// b78 is copied in G[9]
						m.max(B[r - 1][82], new BoolVar[]{G[r-1][9], B[r][81]}).post();// b82 is copied in G[9]
					
						  BoolVar[] tmpB = {
					    		  B[r-1][0],B26[r-1],B56[r-1],B91[r-1],B96[r-1],
					    		  G[r-1][0], G[r-1][1],G[r-1][2],G[r-1][3],G[r-1][4],G[r-1][5], 
					    		  G[r-1][6],G[r-1][7],G[r-1][8],G[r-1][9], Z[r-1][1]};
						      m.sum(tmpB, "=", B[r][127]).post(); 	       
						 	 
				        
			 } // fin boucle for propagation
				 
		        
				// Last state
				// z <- b12s8 + s13s20 + b95s42 + s60s79 + b12b95s94 + s93 + b2 + b15 + b36 + b45 + b64 + b73 + b89
				
				// option 1
				if (Opt == 1) {
				for (int i =0; i<128;i++) {		 
					 if (i != 93)	 		 { S[Nb][i].eq(0).post();}
					 if (i != 2 && i!=15 && i!= 36 && i!=45 && i!=64 && i!=73 && i!=89)	 { B[Nb][i].eq(0).post();}
				 }
					 
				 m.sum(ArrayUtils.append(S[Nb],B[Nb]),"=",1).post();
			    }
			   
				// option 2 b12s8
				if (Opt == 2) {
				//  R=184 -> 0 sol
				B[Nb][12].eq(1).post();	
				for (int i=0;i<128;i++) {if (i!=12) {B[Nb][i].eq(0).post();} }
				S[Nb][8].eq(1).post();
				for (int i=0;i<128;i++) {if (i!=8) {S[Nb][i].eq(0).post();} }
				}
				
				// option 3 s13s20
				if (Opt == 3) {
				// R=184 -> 0 sol
					for (int i=0;i<128;i++) {B[Nb][i].eq(0).post();} 
						S[Nb][13].eq(1).post();
						S[Nb][20].eq(1).post();
						for (int i=0;i<128;i++) {if (i!=13 && i!=20) {S[Nb][i].eq(0).post();} }
				}
				
				// option 4 b95s42
				if (Opt == 4) {
				// R=184 -> ? sol
					B[Nb][95].eq(1).post();	
					for (int i=0;i<128;i++) {if (i!=95) {B[Nb][i].eq(0).post();} }
					S[Nb][42].eq(1).post();
					for (int i=0;i<128;i++) {if (i!=42) {S[Nb][i].eq(0).post();} }
				}
				
					// option 5 s60s79
				if (Opt == 5) {
					// R=184 -> 0 sol
					for (int i=0;i<128;i++) {B[Nb][i].eq(0).post();} 
					S[Nb][60].eq(1).post();
					S[Nb][79].eq(1).post();
					for (int i=0;i<128;i++) {if (i!=60 && i!=79) {S[Nb][i].eq(0).post();} }
				}
				
					// option 6 b12b95s94
				if (Opt == 6) {
					// R=184 -> ? sol
					B[Nb][12].eq(1).post();
					B[Nb][95].eq(1).post();
					for (int i=0;i<128;i++) {if (i!=12 && i!=95) {B[Nb][i].eq(0).post();} }
					S[Nb][94].eq(1).post();
					for (int i=0;i<128;i++) {if (i!=94) {S[Nb][i].eq(0).post();} }
				}
					 				 
						// solving part	 
							
							   BoolVar[] varsfirst = new BoolVar[2*(NBROUNDS+1)];
							   BoolVar[] varssecond = new BoolVar[256*(NBROUNDS+1)];
							   
			                   int cvarsfirst = 0;
			                  int cvarssecond =0 ;
			                
			                   for (int r = NBROUNDS; r >= 0; r--) {
			                    		  varsfirst[cvarsfirst++] = S[r][127];
			                    		  varsfirst[cvarsfirst++] = B[r][127];
			                         		
			                    		  for (int i=0; i<128;i++)
			                   			{
			                    			  varssecond[cvarssecond++] = S[r][i];
			                    			  varssecond[cvarssecond++] = B[r][i];
			                         			
			                   			}
			                   }	
			                                               
					 int cpt =0;
					 Solver solver = m.getSolver();
					solver.setSearch(Search.inputOrderUBSearch(ArrayUtils.append(varsfirst, varssecond)));
					 
					// solver.setSearch(
				      //       Search.lastConflict(
				        //                new DomOverWDeg(ArrayUtils.append(varsfirst, varssecond), 0, new IntDomainMin())));
					// solver.setSearch(
				      //        Search.lastConflict(
				        //               new DomOverWDeg(varsfirst, 0, new IntDomainMin())));
					 		

					 while(solver.solve()) {	
						 cpt++;
						 System.out.println("Solution Number:" + cpt);			
						
						// for (int i=0; i<= Nb; i++) {
						 PrintPositionS(S,0);
						 PrintPositionB(B,0);
						 System.out.println();
						 PrintPositionS(S,Nb);
						 PrintPositionB(B,Nb);
					 System.out.println();
						 
				
					 }
					 solver.printShortStatistics();
					 System.out.println("Total Solutions:" + cpt);
							return;
					 		 
	       
	}
	

	
	
	public static void PrintPositionS(BoolVar[][] X, int N) {
		for (int i=0; i<=127;i++) {if (X[N][i].getValue() == 1) {System.out.print("s" + i + " ");}}
		System.out.println();
		
	}

	public static void PrintPositionB(BoolVar[][] X, int N) {
		for (int i=0; i<=127;i++) {if (X[N][i].getValue() == 1) {System.out.print("b" + i + " ");}}
		System.out.println();
		
	}
	
	public static Tuples createTable() {
	//	b12 b88 b92 b93 b95 s8 s42 s94 --> b12s8 + b95s42 + b12b95s94, b88b92b93b95, 8 variables tours suivant
		Tuples tuples = new Tuples(true);
	
		int b12=0;
		int b88=0;
		int b92=0;
		int b93=0;
		int b95=0;
		int s8=0;
		int s42=0;
		int s94=0;
		
		// i1 to i10 encode b12s8 + b95s42 + b12b95s94, b88b92b93b95, b12, b88, b92, b93, b95, s8, s42, s94 (next round)
		// sanns doute quelques doublons - codages à améliorer.
		for (int i1=0; i1<=1; i1++) {
			for (int i2=0; i2<=1; i2++) {
				for (int i3=0; i3<=1; i3++) {
					for (int i4=0; i4<=1; i4++) {
						for (int i5=0; i5<=1; i5++) {
							for (int i6=0; i6<=1; i6++) {
								for (int i7=0; i7<=1; i7++) {
									for (int i8=0; i8<=1; i8++) {
										for (int i9=0; i9<=1; i9++) {
											for (int i10=0; i10<=1; i10++) {
												if (i10 == 1) {s94=1;}
												if (i9 == 1) {s42 =1;}
												if (i8 == 1) {s8 =1;}
												if (i7 == 1) {b95 =1;}
												if (i6 == 1) {b93=1;}
												if (i5 == 1) {b92 =1;}
												if (i4 == 1) {b88 =1;}
												if (i3 == 1) {b12 =1;}
												if (i2 == 1) {b88=1; b92=1;b93=1;b95=1;}
												if (i1 == 0) {tuples.add(b12,b88,b92,b93,b95,s8,s42,s94,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10); }
												if (i1 == 1) {
													if (b12 * s8 == 0) { tuples.add(1,b88,b92,b93,b95,1,s42,s94,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10);}
													if (b95 * s42 == 0) {tuples.add(b12,b88,b92,b93,1,s8,1,s94,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10);}
													if (b12 * b95 * s94 ==0) {tuples.add(1,b88,b92,b93,1,s8,s42,1,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10);}														
												}
												b12=0; b88=0; b92=0; b93=0; b95=0; s8=0; s42=0; s94=0;
												}	
										}
									}
								}
							}
						}
					}
				}
			}
		}
											
		
		return tuples;
	}	
		
		
}
