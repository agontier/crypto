======= R =106 ci-dessous =============
** GrainV1.java
Codage naif de Grain qui prend deux paramètres en entrée:
- nb de tours
- entier entre 1 et 6 pour indiquer le monome target.

Ce fichier contient l'initialisation pour retrouver l'exemple d'un papier de Todo et al.
Si on lance ce fichier sur R=106 et OPT=6, on doit trouver 8 solutions.


** GrainTableV0.java
Même chose que le précédent mais avec un codage Table.



============= R=184 ci-dessous ==========

** GrainV0.java
Codage naif de Grain qui prend deux paramètres en entrée:
- nb de tours
- entier entre 1 et 6 pour indiquer le monome target.

Ce fichier contient l'initialisation pour 184


** GrainTableV1.java
Même chose que le précédent mais avec un codage Table.


