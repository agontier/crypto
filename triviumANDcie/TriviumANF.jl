function printp(t)
        s = ""
        for tt in t
                ss = ""
                for ttt in tt
                        ss = string(ss,"x",ttt)
                end
                s = string(ss," + ",s)
        end
        println(s[1:end-3])
end
function prints(S)
        for i in 1:288
                print("y",i," = ")
                printp(S[i])
        end
end
function p(y,x)
        for i in 1:length(y)
                if x == y[i]
                        return [y[j] for j in 1:length(y) if j!=i]
                end
        end
        return vcat([x],y)
end
function compare(a,b)
	if length(a)<length(b)
		return true
	elseif length(a)==length(b)
		return a<b
	else 
		return false
	end
end
ch="/Users/agontier/Desktop/crypto/ANF/"
function printfs(t,r)
        open(string(ch,"ANF2trivium",r,".txt"),"w") do f
                for i in 1:288
                        write(f,string("y",i," = "))
			sort!(t[i],lt= compare)
                        for j in 1:length(t[i])
				isok = true
				for ttt in t[i][j]
					isok = isok && ttt<=80
				end
				if isok
                                	for ttt in t[i][j]
                                        	write(f,string("x",ttt))
	                                end
        	                        if j != length(t[i])
                	                        write(f," ")
                        	        end
				end
                        end
                        write(f,"\n")
                end
        end
end
function f(x,y)
        return vcat(x,[i for i in y if !(i in x)])
end
function dev(X,Y)# devellop the product of two polynoms X and Y
        if [] in X || [] in Y
                return []
        else
                return [sort(f(x,y)) for x in X for y in Y]
        end
end
function re(i,j,X)# get stack overflow because of recurcive calls
        if i >= length(X)
                return X
        elseif j >= length(X)+1
                return re(i+1,i+2,X)
        elseif X[i]!=X[j]
                return re(i,j+1,X)
        else
                return re(i,i+1,[X[k] for k in 1:length(X) if k!=i && k!=j])    
        end
end
function re2(i,j,X)
        while i<length(X)
                j = i+1
                flag = true
                while j<=length(X) && flag
                        if X[i]==X[j]
                                X = [X[k] for k in 1:length(X) if k!=i && k!=j]
                                flag = false
                        else
                                j = j + 1
                        end
                end
                if flag
                        i = i + 1
                end
        end
        
        return X
end                     
function red(X)# remove the pairwise equals monomials
        return re2(1,2,X)
end

X = dev([[1],[2,3]],[[3],[2,4],[1,3]])

printp(X)
printp(red(X))

X = [[1],[1],[1],[1],[1]]
printp(X)
printp(red(X))


function propagt(a,b,c,d,e)
        return red(vcat(a,dev(b,c),d,e))
end
function pt(S,a)
        return propagt(S[a[1]],S[a[2]],S[a[3]],S[a[4]],S[a[5]])
end


printp(propagt([1],[2],[3],[4],[5]))

global tt1 = [66,91,92,93,171]
global tt2 = [162,175,176,177,264]
global tt3 = [243,286,287,288,69]
global tt1r= [67,92,93,94,172]
global tt2r= [163,176,177,178,265]
global tt3r= [244,287,288,1,70]

global Nb = 1152

function TriviumANF(S0=[[[i]] for i in 1:288])
        S = [[[[i]] for i in 1:288] for r in 1:Nb+1]
        S[1]=S0
        cst = vcat([i for i in 81:93],[i for i in 93+81:285])
        for i in cst
                S[1][i] = []
        end
        prints(S[1])

        for r in 1:Nb
                println("ROUND ",r)
                println("    computing...")
                for i in 1:288
                        if i == 1
                                S[r+1][i]=pt(S[r],tt3)
                        elseif i == 94
                                S[r+1][i]=pt(S[r],tt1)
                        elseif i == 178
                                S[r+1][i]=pt(S[r],tt2)
                        else
                                S[r+1][i] = S[r][i-1]
                        end
                end
                println("    writing...")
                printfs(S[r+1],r)
        end
end
TriviumANF()
function enumtuples()
        T = ([1],[2],[3],propagt([1],[2],[3],[4],[5]),[5])
        for t in T
                printp(t)
                
        end
end
enumtuples()
