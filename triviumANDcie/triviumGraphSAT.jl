ch="/Users/agontier/Desktop/crypto/"
global Nb = 672

neg = -1*3+1

global cube = [4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80]

global source = Nb*3-66*3+2

global shifta = [-69*3, -66*3+2, -111*3+2, -110*3+2, -109*3+2]
global shiftb = [-78*3, -66*3-1, -93*3-1, -92*3-1, -91*3-1]
global shiftc = [-87*3, -69*3-1, -84*3-1, -83*3-1, -82*3-1]

global rshifta = [69*3, 66*3+1, 93*3+1, 92*3+1, 91*3+1]
global rshiftb = [78*3, 69*3+1, 84*3+1, 83*3+1, 82*3+1]
global rshiftc = [87*3, 66*3-2, 111*3-2, 110*3-2, 109*3-2]

function suc(i)
        if mod(i,3)==0      return [i+ii for ii in shifta]
        elseif mod(i,3)==1  return [i+ii for ii in shiftb]
        else                return [i+ii for ii in shiftc]
        end
end
function pre(i)
        if mod(i,3)==0      return [i+ii for ii in rshifta]
        elseif mod(i,3)==1  return [i+ii for ii in rshiftb]
        else                return [i+ii for ii in rshiftc]
        end
end

function mkp(f,i)
        p = 10000+i*5
        for pi in pre(i)
                write(f,string("-",p," -",pi," 0\n"))
        end
        write(f,string(p," ",pre(i)[1]," ",pre(i)[2]," ",pre(i)[3]," ",pre(i)[4]," ",pre(i)[5]," 0\n"))
end
function mksi(f,i)
        s = [10000+i*5+1,10000+i*5+2,10000+i*5+3]
        for ii in 1:3
                write(f,string("-",s[ii]," ",suc(i)[ii]," 0\n"))
                ss = string(" -",suc(i)[ii])
                for iii in 1:5
                        if iii!=ii
                                write(f,string("-",s[ii]," -",suc(i)[iii]," 0\n"))
                                ss = string(ss," ",suc(i)[iii])
                        end
                end
                write(f,string(s[ii],ss," 0\n"))
        end
end
function mksd(f,i)
        s = 10000+i*5+4
        ss = ""
        for ii in 1:3
                write(f,string("-",s," -",suc(i)[ii]," 0\n"))
                 ss = string(ss," ",suc(i)[ii])
        end
        for ii in 4:5
                write(f,string("-",s," ",suc(i)[ii]," 0\n"))
                 ss = string(ss," -",suc(i)[ii])
        end
        write(f,string(s,ss," 0\n"))
end
function disj(f,i)
        write(f,string(10000+i*5," ",10000+i*5+1," ",10000+i*5+2," ",10000+i*5+3," ",10000+i*5+4," "," 0\n"))
end

function main()
        open(string(ch,"trivium.cnf"),"w") do f
                for r in Nb:-1:1
                        for rr in 0:2
                                mkp(f,1000+r*3+rr)
                                mksi(f,1000+r*3+rr)
                                mksd(f,1000+r*3+rr)
                                disj(f,1000+r*3+rr)
                        end
                end
                for i in 1:80
                        if i in cube
                                write(f,string(1000-i*3-4," 0\n"))
                        else
                                write(f,string("-",1000-i*3-4," 0\n"))
                        end
                end
        end
end
        
main()
function solvez3()
        println("\n    Solving with z3\n")
        run(pipeline(`/Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf`, stdout = "/Users/agontier/Desktop/crypto/res.out"))
end
solvez3()

#=
function find(a,t)
        for i in 1:length(t)
                if t[i]==a
                        return i
                end
        end
        return 0
end                   
function ic(r,i)
        if r == 0
                return i
        elseif i in tt1r
                j = find(i,tt1r)
                return 288+(r-1)*ends+j
        elseif i in tt2r
                j = find(i,tt2r)
                return 288+(r-1)*ends+j+5
        elseif i in tt3r
                j = find(i,tt3r)
                return 288+(r-1)*ends+j+10
        else
                return ic(r-1,i-1)
        end
end
function ico(r,i)
        return 288+(r-1)ends+i
end
function ci(j)
        if j<=288
                return 0,j
        else
                r = 1
                j = j - 288
                while j>ends
                        r = r + 1
                        j = j - 24
                end
                return r,j
        end
end                     
function fixx(f,x,t)
        if t
	        write(f,string(x," 0\n"))
        else
                write(f,string("-",x," 0\n"))
        end
end
function fixx(f,x,t)
        if t
                for xi in x
	                write(f,string(xi," 0\n"))
                end
        else
                for xi in x
                        write(f,string("-",xi," 0\n"))
                end
        end
end
function copyt(f,a,b,c)
	write(f,string("-",a," ",b," ",c," 0\n"))
	write(f,string(a," -",b," 0\n"))
	write(f,string(a," -",c," 0\n"))
end
function xortriv(f,a,y,d,e,t)
	write(f,string(t," -",a," 0\n"))
	write(f,string(t," -",y," 0\n"))
	write(f,string(t," -",d," 0\n"))
	write(f,string(t," -",e," 0\n"))
	write(f,string(a," ",y," ",d," ",e," -",t," 0\n"))
        write(f,string("-",a," -",y," 0\n"))
        write(f,string("-",a," -",d," 0\n"))
        write(f,string("-",a," -",e," 0\n"))
        write(f,string("-",y," -",d," 0\n"))
        write(f,string("-",y," -",e," 0\n"))
        write(f,string("-",d," -",e," 0\n"))
end
function laststate(f)
        r = Nb
        fixx(f,[ic(Nb,i) for i in 1:288 if i != 66 && i != 93 && i != 162 && i != 177 && i != 243 && i != 288],false)
        write(f,string(ic(Nb,66)," ",ic(Nb,93)," ",ic(Nb,162)," ",ic(Nb,177)," ",ic(Nb,243)," ",ic(Nb,288)," 0\n"))
        write(f,string("-",ic(Nb,66)," -",ic(Nb,93)," 0\n"))
        write(f,string("-",ic(Nb,66)," -",ic(Nb,162)," 0\n"))
        write(f,string("-",ic(Nb,66)," -",ic(Nb,177)," 0\n"))
        write(f,string("-",ic(Nb,66)," -",ic(Nb,243)," 0\n"))
        write(f,string("-",ic(Nb,66)," -",ic(Nb,288)," 0\n"))
        write(f,string("-",ic(Nb,93)," -",ic(Nb,162)," 0\n"))
        write(f,string("-",ic(Nb,93)," -",ic(Nb,177)," 0\n"))
        write(f,string("-",ic(Nb,93)," -",ic(Nb,243)," 0\n"))
        write(f,string("-",ic(Nb,93)," -",ic(Nb,288)," 0\n"))
        write(f,string("-",ic(Nb,162)," -",ic(Nb,177)," 0\n"))
        write(f,string("-",ic(Nb,162)," -",ic(Nb,243)," 0\n"))
        write(f,string("-",ic(Nb,162)," -",ic(Nb,288)," 0\n"))
        write(f,string("-",ic(Nb,177)," -",ic(Nb,243)," 0\n"))
        write(f,string("-",ic(Nb,177)," -",ic(Nb,288)," 0\n"))
        write(f,string("-",ic(Nb,243)," -",ic(Nb,288)," 0\n"))
end
function f(f,r,t,copy)
        copyt(f,ic(r-1,t[1]),ico(r,copy[1]),ic(r,t[1]+1))
        copyt(f,ic(r-1,t[2]),ico(r,copy[2]),ic(r,t[2]+1))
        copyt(f,ic(r-1,t[3]),ico(r,copy[2]),ic(r,t[3]+1))
        copyt(f,ic(r-1,t[5]),ico(r,copy[3]),ic(r,t[5]+1))
        xortriv(f,ico(r,copy[1]),ico(r,copy[2]),ic(r-1,t[4]),ico(r,copy[3]),ic(r,t[6]))
end
function generate()
        open(string(ch,"trivium.cnf"),"w") do d
                # constantes à 0
                fixx(d,[i for i in 81:93],false)
                fixx(d,[i for i in 93+81:285],false)
                # cube
                cube = [4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80]
                fixx(d,cube,true)
                fixx(d,[i for i in 94:93+80 if !(i in cube)],false)
                fixx(d,93+34,false)
                fixx(d,93+47,false)
                # Monome clé (test papier 441 p16)
#                fixx(d,[i for i in 1:80 if i!= 12],false)
 #               fixx(d,12,true)
                # Last state
                laststate(d)
                
                t3 = [243, 286, 287, 288, 69, 1] #a+bc+d+e=t
                t1 = [66, 91, 92, 93, 171, 94]
                t2 = [162, 175, 176, 177, 264, 178]
                for r in 1:(Nb)
                        f(d,r,t3,copy3)
                        f(d,r,t1,copy1)
                        f(d,r,t2,copy2)
                end
        end
end
function solvez3()
        println("\n    Solving with z3\n")
        run(pipeline(`/Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf`, stdout = "/Users/agontier/Desktop/crypto/res.out"))
end
function solveglucose()
        println("\n    Solving with Glucose Parallel\n")
        run(pipeline(`/Users/agontier/Desktop/glucose-syrup-4.1/parallel/glucose-syrup /Users/agontier/Desktop/crypto/trivium.cnf`),wait=true)

end
function printv(v)
        for b in v
                print(if b "1" else "."end)
        end
        println()
end
function getsol(ss)
        t = zeros(Bool,841,288)
        for r in 0:Nb
                for i in 1:288
                        t[r+1,i]=ss[ic(r,i)]
                end
        end
        return t
end
function printsol(t)
        for r in 1:100
                println()
                for i in 1:93
                        print(if t[r,i]'1' else '.' end)
                end
                println()
                for i in 94:177
                        print(if t[r,i]'1' else '.' end)
                end
                println()
                for i in 178:288
                        print(if t[r,i]'1' else '.' end)
                end
        end
end
function writesol(t)
        open(string(ch,"sol.txt"),"w") do f
                for i in 1:288
                        write(f,string(if t[1,i]'1'else'0'end))
                end
                for r in 2:841
                        write(f,"\n")
                        for i in 1:288
                                write(f,string(if t[r,i]'1'else'0'end))
                        end
                end
        end
end
function interpret()
        open(string(ch,"res.out"),"r") do s
                s = readlines(s)
                if s[1]=="sat"
                        ss=[parse(Int,i)>0 for i in split(s[2][1:end-1]," ")]
                        print("\nK :")
                        printv(ss[1:80])
                        print("\nIV:")
                        printv(ss[94:173])
                        println("\nThere is at least one trail for K at z",Nb," but SAT needs to count now")
                        t = getsol(ss)
                        printsol(t)
                        #writesol(t)
                else
                        println("UNSAT there must be an error")
                end
        end
end
function main()
        generate()
        @time solvez3()
        interpret()
        #@time solveglucose()
        #multisolve()
end

main()

#=
/Applications/Julia-1.5.app/Contents/Resources/julia/bin/julia /Users/agontier/Desktop/crypto/triviumSATbrut.jl
/Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf > /Users/agontier/Desktop/crypto/res.out
/Users/agontier/Desktop/glucose-syrup-4.1/parallel/glucose-syrup /Users/agontier/Desktop/crypto/trivium.cnf
=#
=#
