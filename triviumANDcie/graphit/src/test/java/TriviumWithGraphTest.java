import org.chocosolver.graphsolver.GraphModel;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.SetType;
import org.kohsuke.args4j.CmdLineException;
import org.testng.Assert;
import org.testng.annotations.Test;
import propagators.PropReachableIV2;

import java.util.Arrays;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 02/07/2021
 */
public class TriviumWithGraphTest {

    @Test(groups = "1s")
    public void test1() throws ContradictionException {
        GraphModel m = new GraphModel();
        DirectedGraph lb = new DirectedGraph(m, 11, SetType.BITSET, true);
        lb.addArc(0, 1);
        lb.addArc(1, 2);
        lb.addArc(1, 3);
        lb.addArc(2, 4);
        lb.addArc(2, 5);
        lb.addArc(3, 5);
        lb.addArc(3, 6);
        lb.addArc(4, 7);
        lb.addArc(5, 8);
        lb.addArc(6, 9);
        lb.addArc(7, 10);
        lb.addArc(8, 10);
        lb.addArc(9, 10);
        DirectedGraphVar g = m.digraphVar("g1", lb, lb);
        DirectedGraph d = new DirectedGraph(11, SetType.BITSET, false);
        d.addArc(1, 2);
        d.addArc(1, 3);
        d.addArc(2, 4);
        d.addArc(2, 5);
        d.addArc(3, 5);
        d.addArc(3, 6);
        RealVar[] riv = new RealVar[11];
        riv[0] = m.realVar(3.);
        riv[1] = m.realVar(0., 5., 0.1);
        riv[2] = m.realVar(0., 5., 0.1);
        riv[3] = m.realVar(0., 5., 0.1);
        riv[4] = m.realVar(0., 5., 0.1);
        riv[5] = m.realVar(0., 5., 0.1);
        riv[6] = m.realVar(0., 5., 0.1);
        riv[7] = m.realVar(1., 1., 0.1);
        riv[8] = m.realVar(1., 1., 0.1);
        riv[9] = m.realVar(1., 1., 0.1);
        riv[10] = m.realVar(1.);
        new Constraint("reac", new PropReachableIV2(g, d, riv, 10)).post();
        m.getSolver().propagate();
        System.out.println(Arrays.toString(riv));
    }

    @Test(groups = "1s")
    public void test2() throws ContradictionException {
        GraphModel m = new GraphModel();
        DirectedGraph lb = new DirectedGraph(m, 11, SetType.BITSET, true);
        lb.addArc(0, 1);
        lb.addArc(1, 2);
        lb.addArc(1, 3);
        lb.addArc(2, 4);
        lb.addArc(2, 5);
        lb.addArc(3, 5);
        lb.addArc(3, 6);
        lb.addArc(4, 7);
        lb.addArc(5, 8);
        lb.addArc(6, 9);
        lb.addArc(7, 10);
        lb.addArc(8, 10);
        lb.addArc(9, 10);
        DirectedGraphVar g = m.digraphVar("g1", lb, lb);
        DirectedGraph d = new DirectedGraph(11, SetType.BITSET, false);
        d.addArc(1, 2);
        d.addArc(1, 3);
        d.addArc(2, 4);
        d.addArc(2, 5);
        d.addArc(3, 5);
        d.addArc(3, 6);
        RealVar[] riv = new RealVar[11];
        riv[0] = m.realVar(3.);
        riv[1] = m.realVar(0., 5., 0.1);
        riv[2] = m.realVar(0., 5., 0.1);
        riv[3] = m.realVar(0., 5., 0.1);
        riv[4] = m.realVar(0., 5., 0.1);
        riv[5] = m.realVar(0., 5., 0.1);
        riv[6] = m.realVar(0., 5., 0.1);
        riv[7] = m.realVar(1., 1., 0.1);
        riv[8] = m.realVar(1., 1., 0.1);
        riv[9] = m.realVar(1., 1., 0.1);
        riv[10] = m.realVar(1.);
        new Constraint("reac", new PropReachableIV2(g, d, riv, 10)).post();
        m.getSolver().propagate();
        System.out.println(Arrays.toString(riv));
        Assert.assertEquals(riv[1].getLB(), 3.);
        Assert.assertEquals(riv[1].getUB(), 3.);
        Assert.assertEquals(riv[2].getLB(), 1.5);
        Assert.assertEquals(riv[2].getUB(), 1.5);
        Assert.assertEquals(riv[3].getLB(), 1.5);
        Assert.assertEquals(riv[3].getUB(), 1.5);
        Assert.assertEquals(riv[4].getLB(), 1.);
        Assert.assertEquals(riv[4].getUB(), 1.);
        Assert.assertEquals(riv[5].getLB(), 1.);
        Assert.assertEquals(riv[5].getUB(), 1.);
        Assert.assertEquals(riv[6].getLB(), 1.);
        Assert.assertEquals(riv[6].getUB(), 1.);
    }


    @Test(groups = "1s")
    public void test672_2() throws CmdLineException {
        // int[] in = new int[]{0, 2, 3, 8, 23, 24, 59, 64, 83, 85, 150, 188, 191, 196, 231, 234, 358, 360, 363, 426, 429, 435, 438, 477, 480, 486, 629, 632, 633, 635, 638, 641, 672, 678, 681, 756, 810, 816, 825, 831
        //          };
        // --> expected 2 solutions
        for (int i = 1; i < 500; i++) {
            new TriviumWithGraph("-r", Integer.toString(i),
                    "-i", "iv_500", "-b", "c66");
        }
    }

    @Test(groups = "1s")
    public void testAll() throws CmdLineException {
        new TriviumWithGraph("-r", "386", "-i", "iv_386", "-b", "c66");
        new TriviumWithGraph("-r", "416", "-i", "iv_416", "-b", "c66");
        new TriviumWithGraph("-r", "429", "-i", "iv_429", "-b", "c66");
        new TriviumWithGraph("-r", "500", "-i", "iv_500", "-b", "c66");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_1", "-b", "c66");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_2", "-b", "c66");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_3", "-b", "c111");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_4", "-b", "c66");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_5", "-b", "c66");
        new TriviumWithGraph("-r", "672", "-i", "iv_672_6", "-b", "c66");
        new TriviumWithGraph("-r", "735", "-i", "iv_735_1", "-b", "a66");
        new TriviumWithGraph("-r", "735", "-i", "iv_735_2", "-b", "a66");
        new TriviumWithGraph("-r", "735", "-i", "iv_735_3", "-b", "a66");
    }

}