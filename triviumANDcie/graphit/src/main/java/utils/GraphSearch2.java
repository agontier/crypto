/*
 * Copyright (c) 1999-2014, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Ecole des Mines de Nantes nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Created by IntelliJ IDEA.
 * User: Jean-Guillaume Fages
 * Date: 08/08/12
 * Time: 15:27
 */

package utils;

import org.chocosolver.graphsolver.search.GraphAssignment;
import org.chocosolver.graphsolver.search.GraphDecision;
import org.chocosolver.graphsolver.search.strategy.GraphStrategy;
import org.chocosolver.graphsolver.variables.GraphVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.util.objects.setDataStructures.ISet;

public class GraphSearch2 extends GraphStrategy {


    //***********************************************************************************
    // CONSTRUCTORS
    //***********************************************************************************

    // variables
    private int n;
    private final GraphAssignment decisionType = GraphAssignment.graph_enforcer;
    private int from, to;
    private final boolean useLC = true;
    private int lastFrom = -1;
    private final RealVar[] riv;
    private final boolean arity;

    /**
     * Search strategy for graphs
     *
     * @param graphVar varriable to branch on
     */
    public GraphSearch2(GraphVar graphVar, RealVar[] riv, boolean arity) {
        super(graphVar, null, null, NodeArcPriority.ARCS);
        n = g.getNbMaxNodes();
        this.riv = riv;
        this.arity = arity;
    }

    @Override
    public GraphDecision getDecision() {
        if (g.isInstantiated()) {
            return null;
        }
        GraphDecision dec = pool.getE();
        if (dec == null) {
            dec = new GraphDecision(pool);
        }
        computeNextArc();
        dec.setArc(g, from, to, decisionType);
        lastFrom = from;
        return dec;
    }

    private void computeNextArc() {
        to = -1;
        from = -1;
        if (useLC && lastFrom != -1) {
            evaluateNeighbors(lastFrom);
            if (to != -1) {
                return;
            }
        }
        for (int i = 0; i < n; i++) {
            if (evaluateNeighbors(i)) {
                return;
            }
        }
        if (to == -1) {
            throw new UnsupportedOperationException();
        }
    }

    private boolean evaluateNeighbors(int i) {
        ISet set = g.getPotSuccOrNeighOf(i);
        if (set.size() == g.getMandSuccOrNeighOf(i).size()) {
            return false;
        }
        boolean found = false;
        double max = -1.;
        for (int j : set.toArray()) {
            if (!g.getMandSuccOrNeighOf(i).contains(j)) {
                if (riv[j].getUB() > max) {
                    from = i;
                    to = j;
                    if(arity){
                        max = riv[j].getUB();
                        found = true;
                    }else{
                        return true;
                    }
                }
            }
        }
        return found;
    }
}
