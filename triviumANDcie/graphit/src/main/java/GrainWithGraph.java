/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import org.chocosolver.graphsolver.GraphModel;
import org.chocosolver.graphsolver.cstrs.degree.PropNodeDegree_AtLeast_Coarse;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.graphs.Orientation;
import org.chocosolver.util.objects.setDataStructures.SetType;
import propagators.PropDoubleOrDie;
import utils.Reg;
import utils.Registre;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

/**
 * <br/>
 *
 * @author Charles Prud'homme, Arthur Gontier
 * @since 24/11/2020
 */
public class GrainWithGraph {

    private static class Shift {
        int[] shift;
        Reg[] regis;

        public Shift(int[] shift, Reg[] regis) {
            this.shift = shift;
            this.regis = regis;
        }
    }

    private static final HashMap<Reg, Shift> SHIFTS = new HashMap<>();


    static {
        SHIFTS.put(Reg.B, new Shift(new int[]{
                0, 26, 56, 91, 96, 3, 67, 11, 13, 17, 18, 27, 59, 40, 48, 61, 65, 68, 84, 88, 92, 93, 95, 22, 24, 25, 70, 78, 82,/*g*/
                0,/*s0*/
                12, 8, 13, 20, 95, 42, 60, 79, 12, 95, 94,/*h*/
                93, 2, 15, 36, 45, 64, 73, 89/*z*/
        }, new Reg[]{
                Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B,/*g*/
                Reg.S,/*s0*/
                Reg.B, Reg.S, Reg.S, Reg.S, Reg.B, Reg.S, Reg.S, Reg.S, Reg.B, Reg.B, Reg.S,/*h*/
                Reg.S, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B/*z*/
        }));
        SHIFTS.put(Reg.S, new Shift(new int[]{
                0, 7, 38, 70, 81, 96,/*f*/
                12, 8, 13, 20, 95, 42, 60, 79, 12, 95, 94,/*h*/
                93, 2, 15, 36, 45, 64, 73, 89/*z*/

        }, new Reg[]{
                Reg.S, Reg.S, Reg.S, Reg.S, Reg.S, Reg.S,/*f*/
                Reg.B, Reg.S, Reg.S, Reg.S, Reg.B, Reg.S, Reg.S, Reg.S, Reg.B, Reg.B, Reg.S,/*h*/
                Reg.S, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B, Reg.B/*z*/
        }));
    }


    private final Instance instance;
    private final Last last;
    private final int nbRound;
    private final HashMap<Registre, Integer> regNode;
    private final HashMap<Integer, Registre> nodReg;
    private int currentNode;
    private final Deque<Integer> toConnect;
    private DirectedGraph doublements;
    private DirectedGraph triplements;
    private DirectedGraph quadlements;
    private int source, sink;

    private DirectedGraphVar g;
    private IntVar[] outDegrees;
    private Set<Integer> IV;

    public GrainWithGraph(Instance instance, int i, Last last) {
        this.instance = instance;
        this.nbRound = i;
        this.last = last;
        GraphModel m = new GraphModel();

        this.regNode = new HashMap<>();
        this.nodReg = new HashMap<>();
        this.toConnect = new ArrayDeque<>();

        declareGraph(m, instance);
        declareConstraints(m, instance);

        m.getSolver().printShortFeatures();
        //m.getSolver().showDecisions(() -> "");
        /*m.getSolver().setSearch(
                new GraphSearch(g)
                        .configure(GraphSearch.LEX)
                        .useLastConflict()
        );*/
        StringBuilder monomes = new StringBuilder();
        Registre r = nodReg.get(source);
        monomes.append(r.reg).append(" = ");
        Hashtable<String, Integer> times
                = new Hashtable<String, Integer>();
        //TODO
        System.out.print("REGISTER B IS WRONG");
        while (m.getSolver().solve()) {
            StringBuilder monome = new StringBuilder();
            boolean isIV = true;
            List<String> vars = new ArrayList<String>();
            for (int p : g.getPotPredOf(sink)) {
                if (IV.contains(p)) continue;
                r = nodReg.get(p);
                isIV = false;
                vars.add((r.reg == Reg.B ? "b" : "s") + -r.round);
            }
            vars.sort(String.CASE_INSENSITIVE_ORDER);
            if (isIV) vars.add("1");
            for (String var : vars)
                monome.append(var);
            String monom = monome.toString();
            if (times.containsKey(monom))
                times.replace(monom, times.get(monom) + 1);
            else
                times.put(monom, 1);

            System.out.printf("%s | %d times\n", monome, times.get(monom));
            monomes.append(monome).append(" + ");
        }
        monomes.delete(monomes.length() - 3, (monomes.length()));
        System.out.printf("%s\n", monomes);
        System.out.println(times.toString());
        System.out.println("\nMonomes impairs : ");
//        for (String k : times.keySet())
//            if (times.get(k))
        m.getSolver().printShortStatistics();
    }

    /**
     * Declaration de la variable Graphe ainsi que les structures connexes
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareGraph(GraphModel m, Instance instance) {
        int n = nbRound * 2 + 2 * 128; // nb of nodes max
        DirectedGraph glb = new DirectedGraph(m, n, SetType.RANGESET, false);
        DirectedGraph gub = new DirectedGraph(m, n, SetType.RANGESET, false);
        doublements = new DirectedGraph(m, n, SetType.RANGESET, false);
        triplements = new DirectedGraph(m, n, SetType.RANGESET, false);
        quadlements = new DirectedGraph(m, n, SetType.RANGESET, false);
        // src is mandatory, that's the root node.
        source = lazyMakeNode(last.re, nbRound - last.ro);
        glb.addNode(source);
        gub.addNode(source);
        toConnect.add(source);
        while (!toConnect.isEmpty()) {
            connect(toConnect.pop(), gub);
        }
        // Declare a sink node, the last round should be connected to this one.
        sink = currentNode++;
        System.out.printf("Sink id:%d\n", sink);
        glb.addNode(sink);
        gub.addNode(sink);

        // Third
        connectLastRoundToSink(gub, Reg.B, sink, i -> true);
        connectLastRoundToSink(gub, Reg.S, sink, i -> i != -127);

        //TODO : a gérer  --
        disconnectLastRoundToSink(gub, sink);
        int[] iv = instance.iv;
        IV = new HashSet<>();
        for (int i : iv) {
            Integer node = getNodeOrNull(Reg.B, -i);
            if (node != null) {
                Registre r = nodReg.get(node);
                System.out.printf("x%d", -r.round + r.reg.pos + 1);
                glb.addNode(node);
                IV.add(node);
            }
        }
        System.out.println();
        g = m.digraphVar("trivium", glb, gub);
        //System.out.printf("%s\n", graphVizExport(g));
    }

    /**
     * Declaration des contraintes sur la variable Graph et creation des variables de degres sortants.
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareConstraints(GraphModel m, Instance instance) {
        int nbNodes = currentNode; // get max number of nodes
        // Chaque noeud retenu dans la solution doit avoir au moins un arc entrant, sauf la source
        declareMinInnerDegrees(g, nbNodes);
        // Chaque noeud retenu dans la solution doit avoir au moins 1 arc sortant, sauf le puits
        // et renvoie une variable par noeud comptant le nombre d'arcs sortants
        outDegrees = declareOuterDegrees(m, g, sink);
        // On borne inférieurement le nombre de bits activés dans le tour 0
        SetVar activated = m.predSet(g, sink);
        activated.getCard().ge(instance.nAct).post();
        // <-- EQUIVALENT AUX 2 LIGNES PREC.
        //List<BoolVar> activatedOnLastRound = getActivatedOnLastRound(m, g);
        //m.sum(activatedOnLastRound.toArray(new BoolVar[0]), ">=", 78).post();
        // -->
        // Gestion des doublements
//        doubleOrDie();
        // Si chemin le plus long, alors pas de doublement à suivre
//        longNoDouble();
        // Si doublement alors au plus 1 chemin plus long a suivre
//        doubleNoTwoLong();
    }

    /**
     * Soit ca double, soit ca ... double pas
     */
    private void doubleOrDie() {
        new Constraint("DOUBLEORDIE", new PropDoubleOrDie(g, doublements, outDegrees)).post();
    }

    public String graphVizExport() {
        boolean directed = true;
        String arc = " -> ";
        StringBuilder sb = new StringBuilder();
        sb.append("digraph Trivium").append("{\n");
        sb.append("node [color = red, fontcolor=red]; ");
        for (int i : g.getMandatoryNodes()) {
            sb.append(i).append(" ");
        }
        sb.append(";\n");
        for (int i : g.getMandatoryNodes()) {
            for (int j : g.getMandSuccOrNeighOf(i)) {
                sb.append(i).append(arc).append(j).append(" [color=red] ;\n");
            }
        }
        if (g.getMandatoryNodes().size() < g.getPotentialNodes().size()) {
            sb.append("node [color = black, fontcolor=black]; ");
            for (int i : g.getPotentialNodes()) if (!g.getMandatoryNodes().contains(i)) sb.append(i).append(" ");
            sb.append(";\n");
        }
        for (int i : g.getPotentialNodes()) {
            for (int j : g.getPotSuccOrNeighOf(i)) {
                if (!g.getMandSuccOrNeighOf(i).contains(j)) sb.append(i).append(arc).append(j).append(" ;\n");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void exportToGraphViz(long solcount) {
        //  dot -Tpng file_1.gv -o file_1.png
        try {
            Path path = Paths.get("solution_" + solcount + ".gv");
            StringBuilder txt = new StringBuilder("digraph g {\n");
            StringBuilder arcs = new StringBuilder();
            // first nodes
            boolean new_cluster = false;
            for (int t = -111; t <= 840; t++) {
                for (Reg r : Reg.values()) {
                    Integer node = getNodeOrNull(r, t);
                    if (node != null && g.getMandatoryNodes().contains(node)) {
                        if (!new_cluster) {
                            new_cluster = true;
                            txt.append("\tsubgraph cluster_").append(Integer.toString(t).replace('-', 'm')).append("{\n");
                            txt.append("\t\tlabel=\"R=").append(t).append(" \";\n");
                        }
                        txt.append(node)
                                .append(" [label=\"")
                                .append(r)
                                .append(t >= 0 ? "" : " (x" + (-t + r.pos + 1) + ")")
                                .append("\", layer=\"")
                                .append(t)
                                .append("\"];\n");
                        for (int to : g.getMandSuccOf(node)) {
                            arcs.append("\t").append(node).append("->").append(to).append("\n");
                        }
                    }
                }
                if (new_cluster) {
                    new_cluster = false;
                    txt.append("\t}\n");
                }
            }
            txt.append("\n").append(arcs.toString()).append("}");
            byte[] strToBytes = txt.toString().getBytes();
            Files.write(path, strToBytes);
            String homeDirectory = System.getProperty("user.home");
            Process process;
            boolean isWindows = System.getProperty("os.name")
                    .toLowerCase().startsWith("windows");
            if (isWindows) {
                process = Runtime.getRuntime()
                        .exec(String.format("cmd.exe /c dir %s", homeDirectory));
            } else {
                process = Runtime.getRuntime()
                        .exec(String.format("dot -Tpng solution_%d.gv -o solution_%d.png ", solcount, solcount));
            }
            int exitCode = process.waitFor();
            assert exitCode == 0;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * for any vertex i in g, |(j,i)| >= minDegree[i]
     * only holds on vertices that are mandatory
     */
    private void declareMinInnerDegrees(DirectedGraphVar g, int nbNodes) {
        int[] minDegrees = new int[nbNodes];
        for (int i : g.getPotentialNodes()) {
            minDegrees[i] = 1;
        }
        minDegrees[0] = 0;
        //m.minInDegrees(g, minDegrees).post();
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Coarse(g, Orientation.PREDECESSORS, minDegrees)).post();
    }

    /**
     * for any vertex i in g, |(i,j)| = degree[i]
     * only holds on vertices that are mandatory
     */
    private IntVar[] declareOuterDegrees(GraphModel m, DirectedGraphVar g, int sink) {
        IntVar[] degrees = new IntVar[g.getNbMaxNodes()];
        int[] minOutDegrees = new int[g.getNbMaxNodes()];
        Arrays.fill(degrees, m.intVar(0));
        for (int i : g.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            degrees[i] = m.intVar("deg " + nodReg.get(i),
                    0,
                    Math.min(4, g.getPotSuccOf(i).size()));
        }
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Coarse(g, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new propagators.PropNodeDegree_Var(g, Orientation.SUCCESSORS, degrees)).post();
        return degrees;
    }

    private void connectLastRoundToSink(DirectedGraph gub, Reg r, int sink, IntPredicate filter) {
        Integer node;
        for (int i = -1; i >= -r.siz; i--) {
            if ((node = getNodeOrNull(r, i)) != null && filter.test(i)) {
                gub.addArc(node, sink);
            }
        }
    }

    private void disconnectLastRoundToSink(DirectedGraph gub, int sink) {
        Reg r = Reg.S;
        Set<Integer> set = new HashSet<>();
        Arrays.stream(instance.iv).forEach(set::add);
        int[] C0 = IntStream.range(1, 96)
                .filter(i -> !set.contains(i))
                .toArray();
        Integer node;
        for (int j : C0) {
            if ((node = getNodeOrNull(r, -j)) != null) {
                gub.removeArc(node, sink);
            }
        }
    }

    private int lazyMakeNode(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            int n = currentNode++;
            r.setNode(n);
            regNode.put(r, n);
            nodReg.put(n, r);
        }
        return regNode.get(r);
    }

    private Integer getNodeOrNull(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            return null;
        }
        return regNode.get(r);
    }


    private void connect(int node, DirectedGraph gub) {
        Registre r = nodReg.get(node);
        doConnect(r, gub, SHIFTS.get(r.reg).shift, SHIFTS.get(r.reg).regis);
    }

    private void doConnect(Registre r, DirectedGraph gub, int[] D, Reg[] R) {
        int from = r.node;
        assert from != -1;
        if (r.round < 0) return;
        int Di;
        for (int i = 0; i < D.length; i++) {
            Di = 128 - D[i];//TODO vérifier l'effet de bord
            if (r.round - Di >= -R[i].siz) {
                int node = lazyMakeNode(R[i], r.round - Di);
                gub.addNode(node);
                gub.addArc(from, node);
                toConnect.add(node);
            }
        }
        if (r.round - D[3] >= -R[3].siz && r.round - D[4] >= -R[4].siz) {
//            doublements.addArc(from, lazyMakeNode(R[3], r.round - D[3]));
//            doublements.addArc(from, lazyMakeNode(R[4], r.round - D[4]));
        }
    }

    private enum Last {
        s93(Reg.S, 93),
        b89(Reg.B, 89),
        //TODO les autres sorties
        ;
        Reg re;
        int ro;

        Last(Reg a, int i) {
            this.re = a;
            this.ro = i;
        }
    }

    private enum Instance {
        iv_106(new int[]{1, 2, 3, 4, 5, 6, 7, 8}, 8);
        int[] iv;
        int nAct;

        Instance(int[] iv, int nAct) {
            this.iv = iv;
            this.nAct = nAct;
        }
    }

    public static void main(String[] args) {
        int round = 106;
        Instance inst = Instance.iv_106;
        new GrainWithGraph(inst, round, Last.s93);
        new GrainWithGraph(inst, round, Last.b89);
    }//les options sont dans l'option 6

}
