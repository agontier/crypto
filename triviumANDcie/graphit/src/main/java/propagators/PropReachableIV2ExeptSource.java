/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import gnu.trove.map.hash.TIntObjectHashMap;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import utils.Registre;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropReachableIV2ExeptSource extends PropReachableIV2 {


    public PropReachableIV2ExeptSource(DirectedGraphVar g, DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, RealVar[] reachableIV, TIntObjectHashMap<Registre> nodReg, int sink) {
        super(g, doublements, reachableIV, sink);
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int i : graph.getPotentialNodes()) {
            if (i != sink && i != 0 && !graph.getPotSuccOf(0).contains(i)) {
                updatelb(i);
            }
        }
    }
}