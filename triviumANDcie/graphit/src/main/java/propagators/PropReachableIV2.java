/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;

import java.util.Arrays;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropReachableIV2 extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final DirectedGraph d; // doublies
    final RealVar[] RIV;
    final int sink;
    final double[] mins, maxs;

    public PropReachableIV2(DirectedGraphVar g, DirectedGraph doublements, RealVar[] reachableIV, int sink) {
        super(new DirectedGraphVar[]{g},
                PropagatorPriority.LINEAR,
                false);
        this.graph = g;
        this.sink = sink;
        this.d = doublements;
        this.RIV = reachableIV;
        this.mins = new double[5];
        this.maxs = new double[5];
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.REMOVE_NODE.getMask();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int i : graph.getPotentialNodes()) {
            if (i != sink && i != 0) {
                updatelb(i);
            }
        }
    }


    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }

    public void updatelb_v0(int i) throws ContradictionException {
        double minlb = 9999;
        double maxub = 0;
        for (int p : graph.getPotPredOf(i)) {
            if (!d.arcExists(p, i)) {
                minlb = Math.min(minlb, RIV[p].getLB());
                maxub = Math.max(maxub, RIV[p].getUB());
            } else {//doubling arc
                int s4 = d.getSuccOf(p).max();
                int s5 = d.getSuccOf(p).min();
                if (graph.getPotSuccOf(p).contains(s4) && graph.getPotSuccOf(p).contains(s5)) {
                    if (i == s4) {
                        minlb = Math.min(minlb, RIV[p].getLB() - RIV[s5].getUB());
                        maxub = Math.max(maxub, RIV[p].getUB() - RIV[s5].getLB());
                    } else if (i == s5) {
                        minlb = Math.min(minlb, RIV[p].getLB() - RIV[s4].getUB());
                        maxub = Math.max(maxub, RIV[p].getUB() - RIV[s4].getLB());
                    }
                }
            }
        }
        if (RIV[i].getUB() < minlb || RIV[i].getLB() > maxub) {
            graph.removeNode(i, this);
        } else {
            RIV[i].updateBounds(minlb, maxub, this);
            if (minlb > 0) cheklb(i);
        }
    }

    void updatelb(int i) throws ContradictionException {
        double minlb1 = 0., minlb2 = 0.;
        double maxub1 = 999., maxub2 = 999.;
        int k = 0;
        // ingoing arcs
        k = ingoingArcs(i);
        // case: one predecessor, take the smallest
        minlb1 = mins[0];
        // case: one predecessor, take the largest
        maxub1 = maxs[k > 0 ? k - 1 : k];
        if (k >= 2) {
            // case 2 predecessors, take the two largest
            maxub1 += maxs[k - 2];
        }
        // flush structs
        Arrays.fill(mins, 0, k, 999.);
        Arrays.fill(maxs, 0, k, 0.);

        // outgoing arcs
        k = outgoingArcs(i);
        // case: one successor, take the smallest
        minlb2 = mins[0];
        // case: one successor, take the largest
        maxub2 = maxs[k > 0 ? k - 1 : k];
        if (k >= 2) {
            // case 2 successors, take the two largest
            maxub2 += maxs[k - 2];
        }
        // flush structs
        Arrays.fill(mins, 0, k, 999.);
        Arrays.fill(maxs, 0, k, 0.);

        double minlb = Math.max(minlb1, minlb2);
        double maxub = Math.min(maxub1, maxub2);

        // then filter
        if (RIV[i].getUB() < minlb || RIV[i].getLB() > maxub) {
            graph.removeNode(i, this);
        } else {
            if (RIV[i].updateLowerBound(minlb, this)) {
                cheklb(i);
            }
            RIV[i].updateUpperBound(maxub, this);
            // calling checkub seems both costly and useless
//            if (RIV[i].updateUpperBound(maxub, this)) {
//                chekub(i);
//            }
        }
    }

    private int ingoingArcs(int i) {
        int k = 0;
        for (int p : graph.getPotPredOf(i)) {
            if (!d.arcExists(p, i)) {
                mins[k] = RIV[p].getLB();
                maxs[k++] = RIV[p].getUB();
            } else {
                // doubling arcs
                int s = d.getSuccOf(p).min();
                if (s == i) {
                    s = d.getSuccOf(p).max();
                }
                if (graph.getPotSuccOf(p).contains(s)) {
                    mins[k] = RIV[p].getLB()
                            - RIV[s].getUB() / Math.max(1, graph.getMandPredOf(s).size());
                    maxs[k++] = RIV[p].getUB()
                            - RIV[s].getLB() / graph.getPotPredOf(s).size();
                }
            }
        }
        if (k > 1) {
            Arrays.sort(mins, 0, k);
            Arrays.sort(maxs, 0, k);
        }
        return k;
    }

    private int outgoingArcs(int i) {
        int k = 0;
        boolean skip = false;
        for (int s : graph.getPotSuccOf(i)) {
            if (s == sink) {
                mins[k] = 0.;
                maxs[k++] = 1.;
                break;
            }
            if (!d.arcExists(i, s)) {
                mins[k] = RIV[s].getLB() / graph.getPotPredOf(s).size();
                maxs[k++] = RIV[s].getUB() / Math.max(1, graph.getMandPredOf(s).size());
            } else if (!skip) {
                int s1 = d.getSuccOf(i).min();
                if (s == s1) {
                    s1 = d.getSuccOf(i).max();
                }
                if (graph.getPotSuccOf(i).contains(s1)) {
                    mins[k] = RIV[s1].getLB() / graph.getPotPredOf(s1).size()
                            + RIV[s].getLB() / graph.getPotPredOf(s).size();
                    maxs[k++] = RIV[s1].getUB() / Math.max(1, graph.getMandPredOf(s1).size())
                            + RIV[s].getUB() / Math.max(1, graph.getMandPredOf(s).size());
                    skip = true;
                }
            }
        }
        if (k > 1) {
            Arrays.sort(mins, 0, k);
            Arrays.sort(maxs, 0, k);
        }
        return k;
    }

    private void cheklb(int i) throws ContradictionException {
        double lb = RIV[i].getLB();
        for (int s : graph.getPotSuccOf(i)) {
            if (!d.arcExists(i, s)) {
                if (RIV[s].getUB() < lb) {
                    graph.removeArc(i, s, this);
                }
            }
        }
        // check doublies
        if (d.getSuccOf(i).size() > 0) {
            int s4 = d.getSuccOf(i).max();
            int s5 = d.getSuccOf(i).min();
            // to check
            if (graph.getPotSuccOf(i).contains(s4) && graph.getPotSuccOf(i).contains(s5)) {
                if (RIV[s4].getUB() + RIV[s5].getUB() < lb) {
                    graph.removeArc(i, s4, this);
                    graph.removeArc(i, s5, this);
                }
            }
        }
    }

    private void chekub(int i) throws ContradictionException {
        double ub = RIV[i].getUB();
        for (int p : graph.getPotPredOf(i)) {
            if (!d.arcExists(p, i)) {
                if (RIV[p].getLB() > ub) {
                    graph.removeArc(i, p, this);
                }
            }
        }
        // check doublies
        for (int p : d.getPredOf(i)) {
            int s = d.getSuccOf(i).max();
            if (s == i) {
                s = d.getSuccOf(i).min();
            }
            // to check
            if (graph.getPotSuccOf(p).contains(i)
                    && graph.getPotSuccOf(p).contains(s)) {
                if (ub + RIV[s].getUB() < RIV[p].getLB()) {
                    graph.removeArc(p, i, this);
                    graph.removeArc(p, s, this);
                }
            }
        }
    }

}
