/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.ISet;
import org.chocosolver.util.procedure.PairProcedure;
import org.chocosolver.util.tools.ArrayUtils;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropDoublePattern extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final IntVar[] degrees;
    final DirectedGraph l;
    final DirectedGraph d;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure arcEvent;
    private final int sink;

    public PropDoublePattern(DirectedGraphVar g, DirectedGraph doublies, DirectedGraph longuest, IntVar[] degrees, int sink) {
        super(ArrayUtils.append(degrees, new Variable[]{g}), PropagatorPriority.UNARY, true);
        this.graph = g;
        this.degrees = degrees;
        this.l = longuest;
        this.d = doublies;
        this.sink = sink;
        this.gdm = g.monitorDelta(this);
        this.arcEvent = (i, j) -> {
            if (d.arcExists(i, j)) {
                if (l.getPredOf(i).size() > 0)
                    checkPatternMand(l.getPredOf(i).min());
                checkPatternMand(i);
            } else if (l.arcExists(i, j)) {
                if (d.getPredOf(i).size() > 0)
                    for (int dd : d.getPredOf(i))
                        checkPatternMand(dd);
                checkPatternMand(i);
            }

//            for (int p : d.getPredOf(i)) checkPattern(p);
//            for (int p : l.getPredOf(i)) checkPattern(p);
        };
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.ADD_ARC.getMask();//ALL_EVENTS;//GraphEventType.ADD_NODE.getMask() + GraphEventType.ADD_ARC.getMask();
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        if (i < degrees.length) {//events sur les var degrees
            checkPattern(i);
        } else {
            gdm.freeze();//events sur le graph
//            if (GraphEventType.isAddNode(mask)) gdm.forEachNode(nodeEvent, GraphEventType.ADD_NODE);
            if (GraphEventType.isAddArc(mask)) gdm.forEachArc(arcEvent, GraphEventType.ADD_ARC);
//            if (GraphEventType.isRemNode(mask)) gdm.forEachNode(nodeEvent, GraphEventType.ADD_NODE);
//            if (GraphEventType.isRemArc(mask)) gdm.forEachArc(arcEvent, GraphEventType.ADD_ARC);
            gdm.unfreeze();
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int n : graph.getMandatoryNodes()) {
            checkPatternMand(n);
        }
        gdm.unfreeze();
    }

    /*
     *   r
     *  /|\
     * a c b
     * |/ \|
     * aa bb
     * */
    private void checkPattern(int r) throws ContradictionException {
        if (l.getSuccOf(r).size() == 1) {
            if (d.getSuccOf(r).size() == 2) {
                ISet sons = d.getSuccOf(r);
                int a = sons.min();
                int b = sons.max();
                int c = l.getSuccOf(r).max();
                ISet sa = l.getSuccOf(a);
                ISet sb = l.getSuccOf(b);
                ISet sc = l.getSuccOf(c);
                if (sa.size() == 0 || sb.size() == 0 || sc.size() == 0
                        || sa.contains(sink) || sb.contains(sink) || sc.contains(sink)) return;
                int aa = sa.min();
                int bb = sb.min();
                // case 1
                if (graph.getPotSuccOf(r).contains(c) && graph.getPotSuccOf(r).contains(aa)
                        && (!graph.getPotentialNodes().contains(a) || graph.getMandSuccOf(a).contains(aa))
                        && (!graph.getPotentialNodes().contains(b) || graph.getMandSuccOf(b).contains(bb))) {
                    // remove one among (r,c), (c,aa)
                    if (graph.getMandSuccOf(r).contains(c)) {
                        graph.removeArc(c, aa, this);
                    }
                    if (graph.getMandSuccOf(c).contains(aa)) {
                        graph.removeArc(r, c, this);
                    }
                }
                // case 2
                if (graph.getPotSuccOf(r).contains(a)
                        && graph.getPotSuccOf(a).contains(aa)
                        && graph.getPotSuccOf(b).contains(bb)
                        && (!graph.getPotentialNodes().contains(bb)
                        || graph.getMandSuccOf(c).contains(aa))) {
                    // remove one among (r,a), (a,aa), (b,bb)
                    if (graph.getMandSuccOf(r).contains(a) && graph.getMandSuccOf(a).contains(aa)) {
                        graph.removeArc(b, bb, this);
                    }
                    if (graph.getMandSuccOf(r).contains(a) && graph.getMandSuccOf(b).contains(bb)) {
                        graph.removeArc(a, aa, this);
                    }
                    if (graph.getMandSuccOf(a).contains(aa) && graph.getMandSuccOf(b).contains(bb)) {
                        graph.removeArc(r, a, this);
                    }
                }
            }
        }
    }

    private void checkPatternMand(int root) throws ContradictionException {
        if (d.getSuccOf(root).size() == 2 && l.getSuccOf(root).size() == 1) {
            ISet sons = d.getSuccOf(root);
            int a = sons.min();
            int b = sons.max();
            int c = l.getSuccOf(root).max();
            ISet sa = l.getSuccOf(a);
            ISet sb = l.getSuccOf(b);
            if (sa.size() == 0 || sb.size() == 0) return;
            int aa = sa.min();
            int bb = sb.min();
            if (d.getSuccOf(c).contains(aa)
                    && graph.getPotPredOf(aa).size()==1 && graph.getPotPredOf(bb).size()==1) {
                if (graph.getPotPredOf(a).size() == 0 && graph.getPotPredOf(b).size() == 0
                        && graph.getMandPredOf(c).contains(root) && graph.getMandPredOf(c).size() == 1
                        && graph.getMandSuccOf(c).contains(aa)
                ) { graph.removeNode(c, this);
                } else if (graph.getPotPredOf(c).size() == 0
                        && graph.getMandPredOf(a).contains(root) && graph.getMandPredOf(a).size() == 1 && graph.getMandPredOf(b).size() == 1
                        && graph.getMandSuccOf(a).contains(aa) && graph.getMandSuccOf(b).contains(bb)

                ) { graph.removeNode(a, this);
                }
            }
        }
    }

    @Override
    public ESat isEntailed() {
//        return ESat.TRUE;
        if (graph.isInstantiated()) {
            for (int root : l.getNodes()) {
                if (l.getSuccOf(root).size() == 1) {
                    if (d.getNodes().contains(root) && d.getSuccOf(root).size() == 2) {
                        ISet sons = d.getSuccOf(root);
                        int a = sons.min();
                        int b = sons.max();
                        int c = l.getSuccOf(root).max();
                        ISet sa = l.getSuccOf(a);
                        ISet sb = l.getSuccOf(b);
                        if (sa.size() == 0 || sb.size() == 0) continue;
                        int aa = sa.min();
                        int bb = sb.min();
                        if (d.getSuccOf(c).contains(aa) && d.getSuccOf(c).contains(bb)) {
                            // Pattern trouvé
                            if (graph.getPotPredOf(a).size() == 0 && graph.getPotPredOf(b).size() == 0 || graph.getPotPredOf(c).size() == 0) {
                                if (graph.getMandSuccOf(root).contains(c)
                                        && degrees[c].getValue() > 1) {
                                    return ESat.FALSE;
                                }
                                if (graph.getMandPredOf(a).contains(root)
                                        && graph.getMandSuccOf(root).contains(a)
                                        && graph.getMandSuccOf(root).contains(b)
                                        && graph.getMandSuccOf(a).contains(aa)
                                        && graph.getMandSuccOf(b).contains(bb)) {
                                    return ESat.FALSE;
                                }
                            }
                        }
                    }
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }
}
