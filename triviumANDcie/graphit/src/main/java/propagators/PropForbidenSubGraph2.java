/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.GraphVar;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.procedure.PairProcedure;
import org.jgrapht.alg.util.Pair;

import static org.chocosolver.util.ESat.*;

/**
 * <br/>
 *
 * @author Arthur Gontier
 * @since 1/2/2021
 */
public class PropForbidenSubGraph2 extends Propagator<GraphVar> {
    /**
     * Free mask
     */
    private static final byte F0 = 0b00;
    /**
     * Mask that indicates pos[0] as false
     */
    private static final byte F1 = 0b01;
    /**
     * Mask that indicates pos[1] as false
     */
    protected static final byte F2 = 0b10;
    /**
     * Store which pos, among 0 and 1, are false
     */
    private byte FL;
    /**
     * Literals of the clauses. Use to always get at position 0 a free literal.
     */
    private final int[] pos;

    final DirectedGraphVar graph;
    final Pair<Integer, Integer>[] arcs;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure onEvent;

    public PropForbidenSubGraph2(DirectedGraphVar g, Pair<Integer, Integer>[] forbiden) {
        super(new GraphVar[]{g}, PropagatorPriority.BINARY, true);
        this.graph = g;
        this.arcs = forbiden;
        this.pos = new int[arcs.length];
        this.gdm = g.monitorDelta(this);
        this.onEvent = (i, j) -> {
            if ((arcs[pos[0]].getFirst() == i && arcs[pos[0]].getSecond() == j)
                    || (arcs[pos[1]].getFirst() == i && arcs[pos[1]].getSecond() == j)) {
                forcePropagate(PropagatorEventType.CUSTOM_PROPAGATION);
            }
        };
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.ADD_ARC.getMask() + GraphEventType.REMOVE_ARC.getMask();
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        gdm.freeze();
        if (GraphEventType.isAddArc(mask))
            gdm.forEachArc(onEvent, GraphEventType.ADD_ARC);
        if (GraphEventType.isRemArc(mask))
            gdm.forEachArc(onEvent, GraphEventType.REMOVE_ARC);
        gdm.unfreeze();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        switch (check(pos[0])) {
            case TRUE:
                //FL = F0;
                setPassive();
                return;
            case FALSE:
                FL |= F1;
                break;
            case UNDEFINED:
                break;
        }
        switch (check(pos[1])) {
            case TRUE:
                FL = F0;
                setPassive();
                return;
            case FALSE:
                FL |= F2;
                break;
            case UNDEFINED:
                break;
        }
        if (FL != F0) {
            propagateClause();
        }
    }

    private ESat check(int p) {
        Pair<Integer, Integer> arc = arcs[p];
        if (graph.getMandSuccOf(arc.getFirst()).contains(arc.getSecond())) {
            // arc is in kernel
            return ESat.FALSE;
        } else if (!graph.getPotSuccOf(arc.getFirst()).contains(arc.getSecond())) {
            // arc not in envelop
            return ESat.TRUE;
        } else {
            // else
            return ESat.UNDEFINED;
        }
    }

    /**
     * Condition: at least one lit is false and none is true among l0 and l1.
     */
    private void propagateClause() throws ContradictionException {
        int k = 2;
        int to = pos.length;
        do {
            int p;
            if ((FL & F2) != 0) {
                p = 1;
                FL ^= F2;
            } else {
                p = 0;
                FL ^= F1;
            }
            // assertion: p is false
            int l0 = pos[0];
            int l1 = pos[1];
            if (p == 0) {
                // Make sure the false literal is pos[1]:
                int t = l0;
                pos[0] = l0 = l1;
                pos[1] = l1 = t;
            }
            // Look for new watch:
            boolean cont = false;
            for (; k < to; k++) {
                int l = pos[k];
                ESat b = check(l);
                if (b != FALSE) {
                    pos[1] = l;
                    pos[k] = pos[--to];
                    pos[to] = l1;
                    if (b == TRUE) {
                        setPassive();
                        FL = F0;
                        assert this.isEntailed() == TRUE;
                        return;
                    }
                    cont = true;
                    break;
                }
            }
            // Did not find watch -- clause is unit under assignment:
            if (!cont) {
                FL = F0;
                if (restrict(l0)) {
                    assert this.isEntailed() == TRUE;
                    setPassive();
                    return;
                } else {
                    assert this.isEntailed() != FALSE;
                }
            }
        } while (FL != F0);
    }

    private boolean restrict(int p) throws ContradictionException {
        return graph.removeArc(arcs[p].getFirst(), arcs[p].getSecond(), this);
    }

    @Override
    public final ESat isEntailed() {
        int i = 0;
        boolean u = false;
        while (i < pos.length) {
            ESat b = check(i);
            if (b == TRUE) {
                return TRUE;
            } else if (b == UNDEFINED) {
                u = true;
            }
            i++;
        }
        return u ? UNDEFINED : FALSE;
    }
}
