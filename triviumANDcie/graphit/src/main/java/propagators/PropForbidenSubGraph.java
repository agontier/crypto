/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.ISet;
import org.chocosolver.util.procedure.IntProcedure;
import org.chocosolver.util.procedure.PairProcedure;
import org.chocosolver.util.tools.ArrayUtils;
import org.jgrapht.alg.util.Pair;

import java.util.List;

import static org.chocosolver.solver.variables.events.IEventType.ALL_EVENTS;

/**
 * <br/>
 *
 * @author Arthur Gontier
 * @since 1/2/2021
 */
public class PropForbidenSubGraph extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final Pair<Integer, Integer>[] forbiden;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure arcEventAdd;
    private final PairProcedure arcEventRem;

    private int p1;
    private int p2;

    public PropForbidenSubGraph(DirectedGraphVar g, Pair<Integer, Integer>[] forbiden) {
        super(ArrayUtils.append(new Variable[]{g}), PropagatorPriority.BINARY, true);
        this.graph = g;
        this.forbiden = forbiden;
        this.gdm = g.monitorDelta(this);
        this.arcEventAdd = (i, j) -> {
            if (forbiden[p1].equals(new Pair<>(i, j))) update(p2);
            else if (forbiden[p2].equals(new Pair<>(i, j))) update(p1);
        };
        this.arcEventRem = (i, j) -> {
            Pair<Integer, Integer> p = new Pair<>(i, j);
            if (forbiden[p1].equals(p) || forbiden[p2].equals(p)) setPassive();
//            for (Pair<Integer, Integer> pp : forbiden)
//                if (p.equals(pp)) setPassive();
        };
    }

    public void update(int p) throws ContradictionException {
        for (int p3 = 0; p3 < forbiden.length; p3++)
            if (!forbiden[p].equals(forbiden[p3]))
                if (!graph.getPotSuccOf(forbiden[p3].getFirst()).contains(forbiden[p3].getSecond())) {
                    setPassive();
                    return;
                } else if (!graph.getMandSuccOf(forbiden[p3].getFirst()).contains(forbiden[p3].getSecond())) {
                    if (p == p1) this.p2 = p3;
                    else this.p1 = p3;
                    return;
                }
        graph.removeArc(forbiden[p].getFirst(), forbiden[p].getSecond(), this);
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.ADD_ARC.getMask() + GraphEventType.REMOVE_ARC.getMask();//ALL_EVENTS;//GraphEventType.ADD_NODE.getMask() + GraphEventType.ADD_ARC.getMask();
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        gdm.freeze();
        if (GraphEventType.isAddArc(mask))
            gdm.forEachArc(arcEventAdd, GraphEventType.ADD_ARC);
        else if (GraphEventType.isRemArc(mask))
            gdm.forEachArc(arcEventRem, GraphEventType.REMOVE_ARC);
        gdm.unfreeze();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        p1 = 0;
        while (p1 < forbiden.length && graph.getMandSuccOf(forbiden[p1].getFirst()).contains(forbiden[p1].getSecond()))
            p1++;
        if (p1 == forbiden.length) setPassive();
        else update(p1);
    }

    @Override
    public ESat isEntailed() {
        if (this.isCompletelyInstantiated()) {
            for (Pair<Integer, Integer> p : forbiden)
                if (!graph.getMandSuccOf(p.getFirst()).contains(p.getSecond()))
                    return ESat.TRUE;
            return ESat.FALSE;
        }
        return ESat.UNDEFINED;
    }
}
