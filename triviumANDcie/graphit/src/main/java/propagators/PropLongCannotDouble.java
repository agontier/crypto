/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.procedure.PairProcedure;
import org.chocosolver.util.tools.ArrayUtils;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropLongCannotDouble extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final IntVar[] degrees;
    final DirectedGraph l;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure arcEvent;

    public PropLongCannotDouble(DirectedGraphVar g, DirectedGraph longuest, IntVar[] degrees) {
        super(ArrayUtils.append(degrees, new Variable[]{g}), PropagatorPriority.UNARY, true);
        this.graph = g;
        this.degrees = degrees;
        this.l = longuest;
        this.gdm = g.monitorDelta(this);
        this.arcEvent = (i, j) -> {
            if (l.arcExists(i, j)) {
                degrees[j].updateUpperBound(1, this);
            }
        };
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        if (vIdx == 0) {
            return IntEventType.boundAndInst();
        } else {
            return GraphEventType.ADD_ARC.getMask();
        }
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        if (i < degrees.length) {
            if (degrees[i].getLB() > 1) {
                for (int p : l.getPredOf(i)) {
                    graph.removeArc(p, i, this);
                }
            }
        } else {
            gdm.freeze();
            if (GraphEventType.isAddNode(mask)) {
                gdm.forEachArc(arcEvent, GraphEventType.ADD_ARC);
            }
            gdm.unfreeze();
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int root : l.getNodes()) {
            if (l.getSuccOf(root).size() == 1) {
                int son = l.getSuccOf(root).max();
                if (graph.getMandSuccOf(root).contains(son) && graph.getMandPredOf(son).contains(root)) {
                    degrees[son].updateUpperBound(1, this);
                } else if (degrees[son].getLB() > 1) {
                    graph.removeArc(root, son, this);
                }
            }
        }
    }

    @Override
    public ESat isEntailed() {
        if (graph.isInstantiated()) {
            for (int n : graph.getMandatoryNodes()) {
                for (int s : graph.getMandSuccOf(n)) {
                    if (l.arcExists(n, s) && degrees[s].getValue() > 1) {
                        return ESat.FALSE;
                    }
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }
}
