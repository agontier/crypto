/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.ISet;
import org.chocosolver.util.procedure.IntProcedure;
import org.chocosolver.util.procedure.PairProcedure;
import org.chocosolver.util.tools.ArrayUtils;

import static org.chocosolver.solver.variables.events.IEventType.ALL_EVENTS;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropDoubleOrDie extends Propagator<Variable> {

    final DirectedGraph d;
    final DirectedGraphVar graph;
    final IntVar[] degrees;
    private final GraphDeltaMonitor gdm;
    private final PairProcedure arcEvent;
    private final IntProcedure nodeEvent;

    public PropDoubleOrDie(DirectedGraphVar g, DirectedGraph doublements, IntVar[] degrees) {
        super(ArrayUtils.append(degrees, new Variable[]{g}), PropagatorPriority.BINARY, true);
        this.graph = g;
        this.d = doublements;
        this.degrees = degrees;
        this.gdm = g.monitorDelta(this);
        this.nodeEvent = i -> {
            if (d.getNodes().contains(i)) {
                if (d.getSuccOf(i).size() != 2) {
                    i = d.getPredOf(i).min();
                }
                assert d.getSuccOf(i).size() == 2;
                dealWithDouble(i);
            }
        };
        this.arcEvent = (i, j) -> {
            if (d.getNodes().contains(i)) {
                dealWithDouble(i);
            }
            if (d.getNodes().contains(j)) {
                dealWithDouble(j);
            }
        };
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        /*if (vIdx < degrees.length) {
            return IntEventType.instantiation();
        } else {
            return GraphEventType.ADD_NODE.getMask() + GraphEventType.ADD_ARC.getMask()
            //+ GraphEventType.REMOVE_NODE.getMask() + GraphEventType.REMOVE_ARC.getMask()

        }*/
        return ALL_EVENTS;
    }

    @Override
    public void propagate(int i, int mask) throws ContradictionException {
        if (i < degrees.length) {
            if (d.getSuccOf(i).size() < 2) return;
            if (degrees[i].isInstantiatedTo(2)) {
                graph.enforceNode(i, this);
                ISet sons = d.getSuccOf(i);
                for (int son : sons) {
                    graph.enforceNode(son, this);
                    graph.enforceArc(i, son, this);
                }
            } else if (degrees[i].getUB() < 2) {
                if (degrees[i].isInstantiatedTo(1)) {
                    graph.enforceNode(i, this);
                }
                ISet sons = d.getSuccOf(i);
                for (int son : sons) {
                    graph.removeArc(i, son, this);
                }
            }
        } else {
            gdm.freeze();
            if (GraphEventType.isAddNode(mask)) {
                gdm.forEachNode(nodeEvent, GraphEventType.ADD_NODE);
            }
            if (GraphEventType.isRemNode(mask)) {
                gdm.forEachNode(nodeEvent, GraphEventType.REMOVE_NODE);
            }
            if (GraphEventType.isAddArc(mask)) {
                gdm.forEachArc(arcEvent, GraphEventType.ADD_ARC);
            }
            if (GraphEventType.isRemArc(mask)) {
                gdm.forEachArc(arcEvent, GraphEventType.REMOVE_ARC);
            }
            gdm.unfreeze();
        }
    }

    // Soient (r,s,t) avec 'r' dans 'd' avec 2 fils, et 's' et 't' ses fils
    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int root : d.getNodes().toArray()) {
            if (d.getSuccOf(root).size() == 2) {
                dealWithDouble(root);
                ISet sons = d.getSuccOf(root);
                if (degrees[root].isInstantiatedTo(2)) {
                    graph.enforceNode(root, this);
                    for (int son : sons) {
                        graph.enforceArc(root, son, this);
                    }
                } else if (degrees[root].getUB() < 2) {
                    if (degrees[root].isInstantiatedTo(1)) {
                        graph.enforceNode(root, this);
                    }
                    for (int son : sons) {
                        graph.removeArc(root, son, this);
                    }
                }
            }
        }
        gdm.unfreeze();
    }

    private void dealWithDouble(int root) throws ContradictionException {
        ISet sons = d.getSuccOf(root);
        if (sons.size() != 2) return;
        int son1 = sons.min();
        int son2 = sons.max();
        if (graph.getMandatoryNodes().contains(root)) {
            if (graph.getMandSuccOf(root).size() > 0) {
                boolean oneSon = graph.getMandSuccOf(root).contains(son1)
                        || graph.getMandSuccOf(root).contains(son2);
                if (oneSon) {
                    graph.enforceArc(root, son1, this);
                    graph.enforceArc(root, son2, this);
                    degrees[root].instantiateTo(2, this);
                } else {
                    degrees[root].instantiateTo(1, this);
                    graph.removeArc(root, son1, this);
                    graph.removeArc(root, son2, this);
                }
            } else if (!graph.getPotSuccOf(root).contains(son1)
                    || !graph.getPotSuccOf(root).contains(son2)) {
                degrees[root].instantiateTo(1, this);
                graph.removeArc(root, son1, this);
                graph.removeArc(root, son2, this);
            }
        } else if (!graph.getPotSuccOf(root).contains(son1)
                || !graph.getPotSuccOf(root).contains(son2)) {
            graph.removeArc(root, son1, this);
            graph.removeArc(root, son2, this);
        }
    }

    @Override
    public ESat isEntailed() {
        if (this.isCompletelyInstantiated()) {
            for (int n : graph.getMandatoryNodes()) {
                if (d.getNodes().contains(n)
                        && d.getSuccOf(n).size() == 2) {
                    ISet sons = graph.getMandSuccOf(n);
                    if (sons.size() == 1) {
                        if (d.getSuccOf(n).contains(sons.min())) {
                            return ESat.FALSE;
                        }
                    } else if (sons.size() == 2) {
                        if (!d.getSuccOf(n).contains(sons.min())) {
                            return ESat.FALSE;
                        }
                        if (!d.getSuccOf(n).contains(sons.max())) {
                            return ESat.FALSE;
                        }
                    }
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }
}
