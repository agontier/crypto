/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package propagators;

import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphEventType;
import org.chocosolver.graphsolver.variables.delta.GraphDeltaMonitor;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.ISet;
import org.chocosolver.util.procedure.IntProcedure;
import org.chocosolver.util.procedure.PairProcedure;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 25/11/2020
 */
public class PropDoubleCannotBothLong extends Propagator<Variable> {

    final DirectedGraphVar graph;
    final DirectedGraph d; // doublies
    final DirectedGraph l; // long
    private final GraphDeltaMonitor gdm;
    private final IntProcedure nodeEvent;
    private final PairProcedure arcEvent;

    public PropDoubleCannotBothLong(DirectedGraphVar g, DirectedGraph doublies,
                                    DirectedGraph longuest) {
        super(new DirectedGraphVar[]{g}, PropagatorPriority.BINARY, true);
        this.graph = g;
        this.d = doublies;
        this.l = longuest;
        this.gdm = g.monitorDelta(this);
        this.nodeEvent = i -> {
            if (d.getSuccOf(i).size() == 2) {
                checkNode(i);
            }
            for (int p : d.getPredOf(i)) {
                checkNode(p);
            }
        };
        this.arcEvent = (i, j) -> {
            if (d.arcExists(i, j)) {
                checkNode(i);
            }
            if (l.arcExists(i, j)) {
                for (int p : d.getPredOf(i)) {
                    checkNode(p);
                }
            }
        };
    }


    @Override
    public int getPropagationConditions(int vIdx) {
        return GraphEventType.ADD_NODE.getMask() + GraphEventType.ADD_ARC.getMask()
                + GraphEventType.REMOVE_NODE.getMask() + GraphEventType.REMOVE_ARC.getMask()
                ;
    }

    @Override
    public void propagate(int idxVarInProp, int mask) throws ContradictionException {
        gdm.freeze();
        if (GraphEventType.isAddArc(mask)) {
            gdm.forEachArc(arcEvent, GraphEventType.ADD_ARC);
        }
        if (GraphEventType.isRemArc(mask)) {
            //gdm.forEachArc(arcEvent, GraphEventType.REMOVE_ARC);
        }
        if (GraphEventType.isAddNode(mask)) {
            gdm.forEachNode(nodeEvent, GraphEventType.ADD_NODE);
        }
        if (GraphEventType.isRemNode(mask)) {
            //gdm.forEachNode(nodeEvent, GraphEventType.REMOVE_NODE);
        }
        gdm.unfreeze();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        // Soient (r,s,t) avec 'r' dans 'd' avec 2 fils, et 's' et 't' ses fils
        for (int n : graph.getMandatoryNodes()) {
            checkNode(n);
        }
        gdm.unfreeze();
    }

    private void checkNode(int n) throws ContradictionException {
        if (d.getNodes().contains(n) && d.getSuccOf(n).size() == 2) {
            ISet sons = d.getSuccOf(n);
            int s = sons.min();
            int t = sons.max();
            ISet ls = l.getSuccOf(s);
            ISet lt = l.getSuccOf(t);
            if (ls.size() == 0 || lt.size() == 0) return;
            assert l.getSuccOf(n).size() == 1;
            if (graph.getPotSuccOf(l.getSuccOf(n).min()).size() == 0
                    && graph.getPotSuccOf(ls.min()) == graph.getPotSuccOf(lt.min())) return;//before sink special case
            if (graph.getMandSuccOf(n).contains(s)
                    && graph.getMandSuccOf(n).contains(t)
                    && graph.getMandPredOf(s).contains(n)){
                //Si doublement, alors pas le plus long chemin pour les 2
                noBothLong(s, ls.min(), t, lt.min());
                noBothLong(t, lt.min(), s, ls.min());
            }
            if (graph.getMandSuccOf(s).contains(ls.min())
                    && graph.getMandSuccOf(t).contains(lt.min())
                    && graph.getMandPredOf(ls.min()).contains(s)) {
                graph.removeArc(n, s, this);
                graph.removeArc(n, t, this);
            }
        }
    }

    private void noBothLong(int s, int ls, int t, int lt) throws ContradictionException {
        if (graph.getMandSuccOf(s).contains(ls)) {
            graph.removeArc(t, lt, this);
        }
    }

    @Override
    public ESat isEntailed() {
        if (graph.isInstantiated()) {
            for (int n : graph.getMandatoryNodes()) {
                if (d.getNodes().contains(n) && d.getSuccOf(n).size() == 2) {
                    ISet sons = d.getSuccOf(n);
                    int s = sons.min();
                    int t = sons.max();
                    if (graph.getMandSuccOf(n).contains(s) && graph.getMandSuccOf(n).contains(s)) {
                        ISet ls = l.getSuccOf(s);
                        ISet lt = l.getSuccOf(t);
                        if (ls.size() == 0 || lt.size() == 0) continue;
                        if (graph.getPotSuccOf(l.getSuccOf(n).min()).size() == 0
                                && graph.getPotSuccOf(ls.min()) == graph.getPotSuccOf(lt.min()))
                            continue;//before sink special case
                        if (graph.getMandSuccOf(s).contains(ls.min())
                                && graph.getMandSuccOf(t).contains(lt.min())) {
                            return ESat.FALSE;
                        }
                    }
                }
            }
            return ESat.TRUE;
        }
        return ESat.UNDEFINED;
    }
}
