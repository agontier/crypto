/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.hash.TIntHashSet;
import org.chocosolver.graphsolver.GraphModel;
import org.chocosolver.graphsolver.search.strategy.GraphSearch;
import org.chocosolver.graphsolver.search.strategy.GraphStrategy;
import org.chocosolver.graphsolver.variables.DirectedGraphVar;
import org.chocosolver.graphsolver.variables.GraphVar;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Settings;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.strategy.AbstractStrategy;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.graphs.Orientation;
import org.chocosolver.util.objects.setDataStructures.SetType;
import org.jgrapht.alg.util.Pair;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import propagators.*;
import utils.GraphSearch2;
import utils.Reg;
import utils.Registre;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <br/>
 *
 * @author Charles Prud'homme, Arthur Gontier
 * @since 24/11/2020
 */
public class TriviumWithGraph {

    private static class Shift {
        int[] shift;
        Reg[] regis;

        public Shift(int[] shift, Reg[] regis) {
            this.shift = shift;
            this.regis = regis;
        }
    }

    private static final HashMap<Reg, Shift> SHIFTS = new HashMap<>();

    static {
        SHIFTS.put(Reg.A, new Shift(new int[]{69, 66, 111, 110, 109}, new Reg[]{Reg.A, Reg.C, Reg.C, Reg.C, Reg.C}));
        SHIFTS.put(Reg.B, new Shift(new int[]{78, 66, 93, 92, 91}, new Reg[]{Reg.B, Reg.A, Reg.A, Reg.A, Reg.A}));
        SHIFTS.put(Reg.C, new Shift(new int[]{87, 69, 84, 83, 82}, new Reg[]{Reg.C, Reg.B, Reg.B, Reg.B, Reg.B}));
    }

    @Option(name = "-r",
            aliases = {"--nb-round"},
            usage = "Nombre de tours (default: 672).",
            required = true)
    protected int nbRound;

    @Option(name = "-i",
            aliases = {"--instance"},
            usage = "Instance à charger (default: iv_672_1).",
            required = true)
    private Instance instance;

    @Option(name = "-b",
            aliases = {"--bit"},
            usage = "Dernier bit à 1 (default: c66).",
            required = true)
    protected Last last; //last bit set to 1 (source of the graph)

    @Option(name = "-dd",
            aliases = {"--debug"},
            usage = "Debug multi-thread")
    protected boolean debugPara = false;

    private final TObjectIntHashMap<Registre> regNode;
    private final TIntObjectHashMap<Registre> nodReg;
    private int currentNode;
    private final PriorityQueue<Registre> toConnect;
    private DirectedGraph doublements;//only doubling arcs
    private DirectedGraph longs;//only longs arcs
    private DirectedGraph loops;//only looping arcs
    private final int n; // nb of nodes max

    private final SetType dftSetType = SetType.RANGESET;
    private int source, sink;

    private DirectedGraphVar g;
    private IntVar[] outDegrees;
    private RealVar[] reachableIV;
    private Set<Integer> IV;
    private ArrayList<int[]> sols;

    /**
     * collect parralell solutions
     */
    public static class Soljoin {
        public ArrayList<int[]> sols;
        public int doublons;
        public int totalsol;

        public Soljoin() {
            sols = new ArrayList<>();
            doublons = 0;
            totalsol = 0;
        }

        private void soladd(int[] sol) {
            this.sols.add(sol);
        }

        private void soladdfilter(int[] sol, ArrayList<int[]> sols) {
            int[] removesol = new int[0];
            for (int[] sol2 : sols)
                if (sol2.length == sol.length) {
                    boolean eq = true;
                    for (int i : sol) {
                        boolean in = false;
                        for (int i2 : sol2)
                            in = in || i == i2;
                        eq = eq && in;
                    }
                    if (eq) removesol = sol2;
                }
            if (removesol.length == 0) sols.add(sol);
            else {
                sols.remove(removesol);
                doublons += 2;
            }
        }

        private void filterdoublies() {
            ArrayList<int[]> newsols = new ArrayList<>();
            for (int[] sol : sols)
                soladdfilter(sol, newsols);
            this.sols = newsols;
        }

        private void soladdall(ArrayList<int[]> sols) {
            sols.forEach(this::soladd);
        }
    }

    private int isatmax;//arc unique index stufs
    private HashMap<Pair<Integer, Integer>, Integer> isat;
    private HashMap<Integer, Pair<Integer, Integer>> sati;

    public final AtomicInteger remaining = new AtomicInteger(0);
    public final AtomicInteger active = new AtomicInteger(0);


    public BitSet[] gmatrix;
    public BitSet[] dmatrix;
    //TODO enlever le un et pas oubkier de le remettre
    //parallelisation
    //chemins durs
    //changer l'exp
    //coupe après le premier cycle
    //demain 9 heures parallel

    /**
     * model and solve trivium as a graph
     */
    public TriviumWithGraph(String... args) throws CmdLineException {
        // Charge les paramètres
        CmdLineParser cmdparser = new CmdLineParser(this);
        try {
            cmdparser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("TriviumWithGraph -r <round> -i <instance> -b <bit> -t <thread>");
            cmdparser.printUsage(System.err);
            throw e;
        }
        //cmdparser.getArguments();
        System.out.printf("== Run %s (%d) at %s with %d threads ==%n", instance, nbRound, last, ForkJoinPool.getCommonPoolParallelism());
        this.n = nbRound * 3 + 300;
        this.regNode = new TObjectIntHashMap<>();
        this.nodReg = new TIntObjectHashMap<>();
        this.toConnect = new PriorityQueue<>((o1, o2) -> -o1.compareTo(o2));
        GraphModel m = new GraphModel();
        disableVerification(m);
        declareGraph(m, instance);
        declareConstraints(m, instance);
        declareReachableIVconstraints(m, g);
        Solver solver = m.getSolver();
        try {
            solver.propagate();
        } catch (ContradictionException e) {
            //e.printStackTrace();
            return;
        }
//        for (int i = 0; i < 10; i++) print(reachableIV[i]);
        boolean solvewithtcut = ForkJoinPool.getCommonPoolParallelism() > 1;
        if (solvewithtcut) {
            gmatrix = matrix(g);
            dmatrix = matrix(doublements);
            int coupe = nodReg.get(source).round - 170;
            int recoupe = coupe - 55;
//            solvecutall(coupe, recoupe);
            smartrecuts(coupe, recoupe);
        }
        boolean solvewithoutcut = !solvewithtcut;
        if (solvewithoutcut) {
            solver.printShortFeatures();
            solver.setSearch(new GraphSearch2(g, reachableIV, false));
            ArrayList<int[]> sols = new ArrayList<>();
            int[] sol;
            while (solver.solve()) {
                sol = g.getMandPredOf(sink).toArray();
                soladd(sol, sols);//printsol(sol);
//                System.out.println(graphVizExport(g.getUB()));
            }
            printsuper(sols);
            solver.printShortStatistics();
        }
    }

    public String graphVizExport(DirectedGraph g) {
        String arc = " -> ";
        StringBuilder sb = new StringBuilder();
        sb.append("digraph Trivium").append("{\n");
        sb.append("node [color = grey, fontcolor=black]; ");
        for (int i : g.getNodes()) {
            sb.append(i).append(" ");
        }
        sb.append(";\n");
        for (int i : g.getNodes()) {
            for (int j : g.getSuccOrNeighOf(i)) {
                if (doublements.arcExists(i, j))
                    sb.append(i).append(arc).append(j).append(" [color=red] ;\n");
                else if (longs.arcExists(i, j))
                    sb.append(i).append(arc).append(j).append(" [color=green] ;\n");
                else if (loops.arcExists(i, j))
                    sb.append(i).append(arc).append(j).append(" [color=blue] ;\n");
                else
                    sb.append(i).append(arc).append(j).append(" [color=grey] ;\n");

            }
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * Declaration de la variable Graphe ainsi que les structures connexes
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareGraph(GraphModel m, Instance instance) {
        DirectedGraph glb = new DirectedGraph(m, n, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(m, n, dftSetType, false);
        doublements = new DirectedGraph(m, n, dftSetType, false);
        longs = new DirectedGraph(m, n, dftSetType, false);
        loops = new DirectedGraph(m, n, dftSetType, false);
        // src is mandatory, that's the root node.
        source = lazyMakeNode(last.re, nbRound - last.ro);
        glb.addNode(source);
        gub.addNode(source);
        // a first loop just to declare nodes once for all
        toConnect.add(nodReg.get(source));
        while (!toConnect.isEmpty()) {
            connect(regNode.get(toConnect.poll()), gub, true);
        }
        List<Registre> regs = nodReg.valueCollection().stream().sorted((o1, o2) -> -o1.compareTo(o2)).collect(Collectors.toList());
        // then rewrite
        nodReg.clear();
        regNode.clear();
        for (int i = 0; i < regs.size(); i++) {
            Registre r = regs.get(i);
            r.setNode(i);
            regNode.put(r, i);
            nodReg.put(i, r);
        }
        // and fill the graph
        toConnect.add(nodReg.get(source));
        while (!toConnect.isEmpty()) {
            connect(regNode.get(toConnect.poll()), gub, false);
        }
        // Declare a sink node, the last round should be connected to this one.
        sink = currentNode++;
//        System.out.printf("Sink id:%d\n", sink);
        glb.addNode(sink);
        gub.addNode(sink);

        // Third
        connectLastRoundToSink(gub, Reg.A, sink, i -> i >= -80);
//        connectLastRoundToSink(gub, utils.Reg.A, sink, i -> i == -80 || i == -79);
        connectLastRoundToSink(gub, Reg.B, sink, i -> i >= -80);
        connectLastRoundToSink(gub, Reg.C, sink, i -> i < -108);

        //TODO : a gérer  --
        disconnectLastRoundToSink(gub, sink);
        int[] iv = instance.iv;
        IV = new HashSet<>();
        for (int i : iv) {
            Integer node = getNodeOrNull(Reg.B, -i);
            if (node != null) {
                Registre r = nodReg.get(node);
//                System.out.printf("x%d", -r.round + r.reg.pos + 1);
                glb.addNode(node);
                IV.add(node);
            }
        }
//        System.out.println();
        g = m.digraphVar("trivium", glb, gub);
        //System.out.printf("%s\n", graphVizExport(g));
    }

    /**
     * Declaration des contraintes sur la variable Graph et creation des variables de degres sortants.
     *
     * @param m        GraphModel
     * @param instance instance a resoudre
     */
    private void declareConstraints(GraphModel m, Instance instance) {
        int nbNodes = currentNode; // get max number of nodes
        // Chaque noeud retenu dans la solution doit avoir au moins un arc entrant, sauf la source
        declareMinInnerDegrees(g, nbNodes);
        // Chaque noeud retenu dans la solution doit avoir au moins 1 arc sortant, sauf le puits
        // et renvoie une variable par noeud comptant le nombre d'arcs sortants
        outDegrees = declareOuterDegrees(m, g, sink);
        // On borne inférieurement le nombre de bits activés dans le tour 0
        SetVar activated = m.predSet(g, sink);
        activated.getCard().ge(instance.nAct).post();
        // <-- EQUIVALENT AUX 2 LIGNES PREC.
        //List<BoolVar> activatedOnLastRound = getActivatedOnLastRound(m, g);
        //m.sum(activatedOnLastRound.toArray(new BoolVar[0]), ">=", 78).post();
        // -->
        // Gestion des doublements
        doubleOrDie();
        triplectr(g, doublements, nodReg);

        //doublePattern();
        // Si chemin le plus long, alors pas de doublement à suivre
        //longNoDouble();
        // Si doublement alors au plus 1 chemin plus long a suivre
        //doubleNoTwoLong();
    }

    /**
     * retire le cas ou on a trois noeuds cote à cote dont les deux extremes doubles
     *  b   a   c
     * / \ / \ / \
     *   a0  a1
     */
    public void triplectr(DirectedGraphVar gv, DirectedGraph doublements, TIntObjectHashMap<Registre> nodReg) {
        ArrayList<Pair<Integer, Integer>[]> triples = new ArrayList<>();
        for (int a : doublements.getNodes()) {
            if (doublements.getSuccOf(a).size() == 2) {
                int a0 = doublements.getSuccOf(a).min();
                int a1 = doublements.getSuccOf(a).max();
                if (nodReg.get(a0).round < nodReg.get(a1).round) {
                    int t = a0;
                    a0 = a1;
                    a1 = t;
                }
                if (doublements.getPredOf(a0).size() == 2 && doublements.getPredOf(a1).size() == 2) {
                    int b;
                    if (nodReg.get(doublements.getPredOf(a0).min()).round != nodReg.get(a).round)
                        b = doublements.getPredOf(a0).min();
                    else b = doublements.getPredOf(a0).max();
                    int c;
                    if (nodReg.get(doublements.getPredOf(a1).min()).round != nodReg.get(a).round)
                        c = doublements.getPredOf(a1).min();
                    else c = doublements.getPredOf(a1).max();

                    Pair<Integer, Integer>[] ctr1 = new Pair[]{
                            new Pair<>(b, a0),
                            new Pair<>(c, a1),
                            new Pair<>(a, a0)
                    };
                    Pair<Integer, Integer>[] ctr2 = new Pair[]{
                            new Pair<>(b, a0),
                            new Pair<>(c, a1),
                            new Pair<>(a, longs.getSuccOf(a).min())
                    };
                    triples.add(ctr1);
                    triples.add(ctr2);
                }
            }
        }
        ForbiddenArcsSolver fsol = new ForbiddenArcsSolver(gv);
        for (Pair<Integer, Integer>[] ctr : triples) fsol.addClause(ctr);
        new Constraint("triplies", fsol).post();
    }

    /**
     * declare the constraints for the cube reachability arrity
     */
    private void declareReachableIVconstraints(GraphModel mi, DirectedGraphVar gi) {
//        T.put(source, costOf(source, T)); // weak version
//        T.put(source, smartcostOf(source, T, g, doublements, longs, IV, sink, nodReg));
        reachableIV = new RealVar[sink + 1];
        reachableIV[sink] = mi.realVar("ari sink", 1);
        reachableIV[source] = mi.realVar("ari " + nodReg.get(source), instance.nAct, instance.nAct, .01);
        for (int i : g.getPotPredOf(sink))
            if (IV.contains(i))
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 1);
            else
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 0);
        for (int i : g.getPotentialNodes())
            if (g.getPotSuccOf(i).size() == 0 && i != sink)
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 0);
        for (int n : gi.getPotentialNodes())
            if (reachableIV[n] == null)
                reachableIV[n] = mi.realVar("ari " + nodReg.get(n), 0, 300, .01);//T.get(n));
        new Constraint("reachable IV",
                new PropReachableIV(gi, doublements, longs, loops, reachableIV, nodReg, sink),
                new PropReachableIV2(gi, doublements, reachableIV, sink)).post();
    }

    /**
     * Soit ca double, soit ca ... double pas
     */
    private void doubleOrDie() {
        new Constraint("DOUBLEORDIE", new PropDoubleOrDie(g, doublements, outDegrees)).post();
    }

    /**
     * Fusion des deux contraintes suivantes
     */
    private void doublePattern() {
        new Constraint("DOUBLEPATERN", new PropDoublePattern(g, doublements, longs, outDegrees, sink)).post();
    }

    /**
     * Emprunter l'arc le plus loin ne peut pas être suivi d'un doublement
     */
    private void longNoDouble() {
        new Constraint("LONGNODOUBLE", new PropLongCannotDouble(g, longs, outDegrees)).post();
    }

    /**
     * Doubler ne peut pas être suivi de 2 chemins plus longs
     */
    private void doubleNoTwoLong() {
        new Constraint("BOHTNOLONG", new PropDoubleCannotBothLong(g, doublements, longs)).post();
    }

    public String graphVizExport() {
        boolean directed = true;
        String arc = " -> ";
        StringBuilder sb = new StringBuilder();
        sb.append("digraph Trivium").append("{\n");
        //sb.append("node [color = red, fontcolor=red]; ");
        for (int i : g.getMandatoryNodes()) {
            //sb.append(i).append(" ");
            sb.append(i)
                    .append(" [label= \"")
                    .append(i)
                    .append(String.format(" [%.1f..%.1f] ", reachableIV[i].getLB(), reachableIV[i].getUB()))
                    .append("\", color=red, fontcolor=red];\n");
        }
        //sb.append(";\n");
        for (int i : g.getMandatoryNodes()) {
            for (int j : g.getMandSuccOrNeighOf(i)) {
                sb.append(i);
                if (doublements.getSuccOf(i).contains(j)) {
                    sb.append("->").append(j).append("[color = \"red:none:red\"];\n");
                } else if (longs.getSuccOf(i).contains(j)) {
                    sb.append("->").append(j).append("[color=red];\n");
                } else if (nodReg.contains(j) && nodReg.contains(i) && nodReg.get(j).reg == nodReg.get(i).reg) {
                    sb.append("->").append(j).append("[color=red,style=dotted];\n");
                } else {
                    sb.append("->").append(j).append("[color=red,style=dashed];\n");
                }
            }
        }
        if (g.getMandatoryNodes().size() < g.getPotentialNodes().size()) {
            sb.append("node [color = black, fontcolor=black]; ");
            for (int i : g.getPotentialNodes()) {
                if (!g.getMandatoryNodes().contains(i)) {
                    sb.append(i)
                            .append(" [label= \"")
                            .append(i)
                            .append(String.format(" [%.1f..%.1f] ", reachableIV[i].getLB(), reachableIV[i].getUB()))
                            .append("\", color=black, fontcolor=black];\n");
                }
            }
            //sb.append(";\n");
        }
        for (int i : g.getPotentialNodes()) {
            for (int j : g.getPotSuccOrNeighOf(i)) {
                if (!g.getMandSuccOrNeighOf(i).contains(j)) {
                    sb.append(i);
                    if (doublements.getSuccOf(i).contains(j)) {
                        sb.append("->").append(j).append("[color = \"black:none:black\"];\n");
                    } else if (longs.getSuccOf(i).contains(j)) {
                        sb.append("->").append(j).append("[color=black];\n");
                    } else if (nodReg.contains(j) && nodReg.contains(i) && nodReg.get(j).reg == nodReg.get(i).reg) {
                        sb.append("->").append(j).append("[color=black,style=dotted];\n");
                    } else {
                        sb.append("->").append(j).append("[color=black,style=dashed];\n");
                    }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void exportToGraphViz(long solcount) {
        //  dot -Tpng file_1.gv -o file_1.png
        StringBuilder txt = new StringBuilder("digraph g {\n");
        StringBuilder arcs = new StringBuilder();
        // first nodes
        boolean new_cluster = false;
        for (int t = -111; t <= 840; t++) {
            for (Reg r : Reg.values()) {
                Integer node = getNodeOrNull(r, t);
                if (node != null && g.getMandatoryNodes().contains(node)) {
                    /*if (!new_cluster) {
                        new_cluster = true;
                        txt.append("\tsubgraph cluster_").append(Integer.toString(t).replace('-', 'm')).append("{\n");
                        txt.append("\t\tlabel=\"R=").append(t).append(" \";\n");
                    }*/
                    txt.append(node)
                            .append(" [label=<")
                            .append(r)
                            .append("<SUB>").append(t).append("</SUB>")
                            .append(String.format(" [%.1f..%.1f] ", reachableIV[node].getLB(), reachableIV[node].getUB()))
                            .append(t >= 0 ? ">" : " (x" + (-t + r.pos + 1) + ")>")
                            .append(", color=\"")
                            .append(reachableIV[node].getUB() <= 0.1 ? "red" : "black")
                            .append("\", fontcolor=\"")
                            .append(!IV.contains(node) ? "black" : "blue")
                            .append("\", layer=\"")
                            .append(t)
                            .append("\"];\n");
                    for (int to : g.getMandSuccOf(node)) {
                        arcs.append("\t").append(node);
                        if (doublements.getSuccOf(node).contains(to)) {
                            arcs.append("->").append(to).append("[color = \"black:none:black\"]\n");
                        } else if (longs.getSuccOf(node).contains(to)) {
                            arcs.append("->").append(to).append("\n");
                        } else if (nodReg.contains(to) && nodReg.contains(node) && nodReg.get(to).reg == nodReg.get(node).reg) {
                            arcs.append("->").append(to).append("[style=dotted]\n");
                        } else {
                            arcs.append("->").append(to).append("[style=dashed]\n");
                        }
                    }
                }
            }
            /*if (new_cluster) {
                new_cluster = false;
                txt.append("\t}\n");
            }*/
        }
        txt.append("\n").append(arcs).append("}");
        //System.out.println(txt);
        export("solution", solcount, txt.toString());
    }

    private void exportToGraphViz(TIntHashSet nodes) {
        //  dot -Tpng file_1.gv -o file_1.png
        StringBuilder txt = new StringBuilder("digraph g {\n");
        StringBuilder arcs = new StringBuilder();
        // first nodes
        boolean new_cluster = false;
        for (int node : nodes.toArray()) {
            Registre reg = nodReg.get(node);
            int t = reg.round;
            Reg r = reg.reg;
            if (!new_cluster) {
                new_cluster = true;
                txt.append("\tsubgraph cluster_").append(Integer.toString(t).replace('-', 'm')).append("{\n");
                txt.append("\t\tlabel=\"R=").append(t).append(" \";\n");
            }
            txt.append(node)
                    .append(" [label=\"")
                    .append(r)
                    .append(t >= 0 ? "" : " (x" + (-t + r.pos + 1) + ")")
                    .append("\", color=\"")
                    .append(reachableIV[node].getUB() > 0 ? "black" : "red")
                    .append("\", fontcolor=\"")
                    .append(!IV.contains(node) ? "black" : "blue")
                    .append("\", layer=\"")
                    .append(t)
                    .append("\"];\n");
            for (int to : g.getPotSuccOf(node)) {
                if (nodes.contains(to))
                    arcs.append("\t").append(node).append("->").append(to).append("\n");
            }
            if (new_cluster) {
                new_cluster = false;
                txt.append("\t}\n");
            }
        }
        txt.append("\n").append(arcs).append("}");
        //System.out.println(txt);
        export("nodes_diam", 0, txt.toString());
    }

    private void export(String prefix, long cnt, String txt) {
        try {
            Path path = Paths.get(prefix + "_" + cnt + ".gv");
            byte[] strToBytes = txt.toString().getBytes();
            Files.write(path, strToBytes);
            String homeDirectory = System.getProperty("user.home");
            Process process;
            boolean isWindows = System.getProperty("os.name")
                    .toLowerCase().startsWith("windows");
            if (isWindows) {
                process = Runtime.getRuntime()
                        .exec(String.format("cmd.exe /c dir %s", homeDirectory));
            } else {
                process = Runtime.getRuntime()
                        .exec(String.format("dot -Tpng %1$s_%2$d.gv -o %1$s_%2$d.png ", prefix, cnt));
            }
            int exitCode = process.waitFor();
            assert exitCode == 0;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * for any vertex i in g, |(j,i)| >= minDegree[i]
     * only holds on vertices that are mandatory
     */
    private void declareMinInnerDegrees(DirectedGraphVar g, int nbNodes) {
        int[] minDegrees = new int[nbNodes];
        for (int i : g.getPotentialNodes()) {
            minDegrees[i] = 1;
        }
        minDegrees[0] = 0;
        //m.minInDegrees(g, minDegrees).post();
        new Constraint("minInDegrees",
                new PropNodeDegree_AtLeast_Incr(g, Orientation.PREDECESSORS, minDegrees)).post();
    }

    /**
     * for any vertex i in g, |(i,j)| = degree[i]
     * only holds on vertices that are mandatory
     */
    private IntVar[] declareOuterDegrees(GraphModel m, DirectedGraphVar g, int sink) {
        IntVar[] degrees = new IntVar[g.getNbMaxNodes()];
        int[] minOutDegrees = new int[g.getNbMaxNodes()];
        Arrays.fill(degrees, m.intVar(0));
        for (int i : g.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            degrees[i] = m.intVar("deg " + nodReg.get(i),
                    0,
                    Math.min(2, g.getPotSuccOf(i).size()));
        }
        new Constraint("minOutDegrees",
                new PropNodeDegree_AtLeast_Incr(g, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(g, Orientation.SUCCESSORS, degrees)).post();
        return degrees;
    }

    private void connectLastRoundToSink(DirectedGraph gub, Reg r, int sink, IntPredicate filter) {
        Integer node;
        for (int i = -1; i >= -r.siz; i--) {
            if ((node = getNodeOrNull(r, i)) != null && filter.test(i)) {
                gub.addArc(node, sink);
            }
        }
    }

    private void disconnectLastRoundToSink(DirectedGraph gub, int sink) {
        Reg r = Reg.B;
        Set<Integer> set = new HashSet<>();
        Arrays.stream(instance.iv).forEach(set::add);
        int[] C0 = IntStream.range(1, 81)
                .filter(i -> !set.contains(i))
                .toArray();
        Integer node;
        for (int i = 0; i < C0.length; i++) {
            if ((node = getNodeOrNull(r, -C0[i])) != null) {
                gub.removeArc(node, sink);
            }
        }
    }

    private int lazyMakeNode(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            int n = currentNode++;
            r.setNode(n);
            regNode.put(r, n);
            nodReg.put(n, r);
        }
        return regNode.get(r);
    }

    private Integer getNodeOrNull(Reg reg, int tour) {
        Registre r = new Registre(tour, reg);
        if (!regNode.containsKey(r)) {
            return null;
        }
        return regNode.get(r);
    }

    private void connect(int node, DirectedGraph gub, boolean fake) {
        Registre r = nodReg.get(node);
        doConnect(r, gub, SHIFTS.get(r.reg).shift, SHIFTS.get(r.reg).regis, fake);
    }

    private void doConnect(Registre r, DirectedGraph gub, int[] D, Reg[] R, boolean fake) {
        int from = r.node;
        assert from != -1;
        if (r.round < 0) return;
        for (int i = 0; i < 5; i++) {
            if (r.round - D[i] >= -R[i].siz) {
                int node = lazyMakeNode(R[i], r.round - D[i]);
                toConnect.add(nodReg.get(node));
                if (fake) continue;
                gub.addNode(node);
                gub.addArc(from, node);
                if (i == 2) { // l'arc le plus long est toujours declare en 3ème position
                    longs.addArc(from, node);
                }
                if (i == 0) {
                    loops.addArc(from, node);
                }
            }
        }
        if (fake) return;
        if (r.round - D[3] >= -R[3].siz && r.round - D[4] >= -R[4].siz) {
            doublements.addArc(from, lazyMakeNode(R[3], r.round - D[3]));
            doublements.addArc(from, lazyMakeNode(R[4], r.round - D[4]));
        }
    }

    public void solvecutall(int coupe, int recoupe) {
        long time = -System.currentTimeMillis();

        print("Cut at : " + coupe);
        Soljoin cuts = new Soljoin();
        solvesourcesubgraph(coupe, cuts);
        time += System.currentTimeMillis();
        cuts.filterdoublies();
        print("    possible cuts: " + cuts.sols.size() + "/" + cuts.totalsol + " (doublons " + cuts.doublons + ")");
        System.out.printf("    >> %.3fs%n", time / 1000f);

        sols = new ArrayList<>();

        int min = 300;
        int max = 0;
        int sum = 0;
        for (int[] sol : cuts.sols) {
            sum = 0;
            for (int i : sol)
                sum += reachableIV[i].getUB();
            if (sum > max) max = sum;
            if (sum < min) min = sum;
        }
        int mid = 3 * (max - min) / 4 + min;

        for (int[] sol : cuts.sols) {
            sum = 0;
            for (int i : sol)
                sum += reachableIV[i].getUB();
            if (sum <= mid)
                sols.add(sol);
        }
        for (int[] sol : sols)
            cuts.sols.remove(sol);
        print(cuts.sols.size() + " Recuts at : " + (recoupe));
        Soljoin recuts = new Soljoin();
        time = -System.currentTimeMillis();
        if (debugPara) System.err.printf("Add 0: %d%n", remaining.addAndGet(cuts.sols.size()));
        cuts.sols.parallelStream().forEach(sol ->
                solvecutcut(sol, recoupe, source, sink, nodReg, dcopy(), recuts));
        time += System.currentTimeMillis();
        recuts.filterdoublies();
        print("    possible cuts: " + recuts.sols.size() + "/" + recuts.totalsol + " (doublons " + recuts.doublons + ")");
        System.out.printf("    >> %.3fs %n", time / 1000f);

        sols.addAll(recuts.sols);

        System.out.print("Solutions: ");
        time = -System.currentTimeMillis();
        Soljoin soljoin = new Soljoin();
        if (debugPara) System.err.printf("Add 2: %d%n", remaining.addAndGet(sols.size()));
        List<RecursiveAction> actions = new ArrayList<>();
        for (int[] sol : sols) {
            actions.add(new RecursiveAction() {
                @Override
                protected void compute() {
                    solvesaftercut(sol, IV, source, sink, nodReg, instance.nAct, dcopy(), longs, loops, soljoin);
                }
            });
        }
        ForkJoinTask.invokeAll(actions);
        time += System.currentTimeMillis();
        soljoin.filterdoublies();
        print(" = " + (soljoin.sols.size() + soljoin.doublons));
        System.out.printf(">>>>>> %.3fs %n", time / 1000f);

        printsuper(soljoin.sols);
    }

    public void smartrecuts(int coupe, int recoupe) {
        long time = -System.currentTimeMillis();

        print("Cut at : " + coupe);
        Soljoin cuts = new Soljoin();
        solvesourcesubgraph(coupe, cuts);
        time += System.currentTimeMillis();
        cuts.filterdoublies();
        print("    possible cuts: " + cuts.sols.size() + "/" + cuts.totalsol + " (doublons " + cuts.doublons + ")");
        System.out.printf("    >> %.3fs%n", time / 1000f);

        sols = new ArrayList<>();
        Soljoin soljoin = new Soljoin();
        print("dynamic recuts");
        System.out.print("Solutions: ");
        if (debugPara) System.err.printf("Add 1: %d%n", remaining.addAndGet(cuts.sols.size()));

        time = -System.currentTimeMillis();
        List<RecursiveAction> actions = new ArrayList<>();
        for (int[] sol : cuts.sols) {
            actions.add(new RecursiveAction() {
                @Override
                protected void compute() {
                    solveorcut(sol, recoupe, IV, source, sink, nodReg, instance.nAct, doublements, longs, loops, soljoin);
                }
            });
        }
        ForkJoinTask.invokeAll(actions);
        time += System.currentTimeMillis();
        soljoin.filterdoublies();
        print(" = " + (soljoin.sols.size() + soljoin.doublons));
        System.out.printf(">>>>>> %.3fs %n", time / 1000f);

        printsuper(soljoin.sols);
    }

    public GraphModel buildcuttoend(GraphModel mi, DirectedGraph glb, DirectedGraph gub, int[] coupe, RealVar[] reachableIV,
                                    Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                                    DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops) {
//        BitSet[] matrix = matrix();
        for (int n : coupe) {
            gub.addArc(source, n);
            glb.addArc(source, n);
            DirectedGraph sub = subgraph(n);
//            DirectedGraph sub = subgraph(n, matrix);
            for (int n2 : sub.getNodes())
                for (int s : sub.getSuccOf(n2))
                    gub.addArc(n2, s);
        }
        for (int n : IV) gub.addArc(n, sink);
        for (int n : IV) glb.addArc(n, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumafcuttoend", glb, gub);

        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes()) minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            minOutDegrees[source] = coupe.length;
            if (i == source)
                degrees[i] = mi.intVar("deg " + nodReg.get(i), coupe.length, coupe.length);
            else
                degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDieExeptSource(gi, doublements, degrees)).post();
//        new Constraint("doubling pattern", new PropDoublePattern(gi, doublements, longs, degrees)).post();
        TIntIntHashMap T = new TIntIntHashMap();

        reachableIV[sink] = mi.realVar("ari sink", 1);
        reachableIV[source] = mi.realVar("ari " + nodReg.get(0), instancenAct);
        for (int i : gi.getPotPredOf(sink))
            if (IV.contains(i))
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 1);
            else
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 0);
        for (int i : gi.getPotentialNodes())
            if (gi.getPotSuccOf(i).size() == 0 && i != sink)
                reachableIV[i] = mi.realVar("ari " + nodReg.get(i), 0);
        for (int n : gi.getPotentialNodes())
            if (reachableIV[n] == null)
                reachableIV[n] = mi.realVar("ari " + nodReg.get(n), 0, 300, 0.1);
        new Constraint("reachable IV",
                new PropReachableIVExeptSource(gi, doublements, longs, loops, reachableIV, nodReg, sink),
                new PropReachableIV2ExeptSource(gi, doublements, longs, loops, reachableIV, nodReg, sink)).post();
        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        return mi;
    }

    public GraphModel buildcuttocut(GraphModel mi, DirectedGraph glb, DirectedGraph gub, int[] coupe, int recoupe,
                                    int source, int sink, TIntObjectHashMap<Registre> nodReg, DirectedGraph doublements, DirectedGraph longs) {
        DirectedGraph ins = new DirectedGraph(n, dftSetType, false);
        Deque<Integer> list = new ArrayDeque<>();
        for (int n : coupe) {
            list.add(n);
            ins.addNode(n);
        }
        Registre reg;
        while (!list.isEmpty()) {
            int j = list.pop();
            reg = nodReg.get(j);
            if (reg.round > recoupe)
                for (int s = gmatrix[j].nextSetBit(0); s > -1; s = gmatrix[j].nextSetBit(s + 1)) {
                    ins.addArc(j, s);
                    list.add(s);
                }
        }
        glb.addNode(source);
        gub.addNode(source);
        for (int n : coupe) {
            gub.addArc(source, n);
            glb.addArc(source, n);
        }
        for (int n : ins.getNodes())
            for (int s : ins.getSuccOf(n))
                gub.addArc(n, s);
        glb.addNode(sink);
        gub.addNode(sink);
        for (int i : ins.getNodes())
            if (ins.getSuccOf(i).size() == 0)
                gub.addArc(i, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumcutcut", glb, gub);
        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes())
            minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            minOutDegrees[source] = coupe.length;
            if (i == source)
                degrees[i] = mi.intVar("deg " + nodReg.get(i), coupe.length, coupe.length);
            else
                degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDieExeptSource(gi, doublements, degrees)).post();
//        new Constraint("doubling pattern", new PropDoublePattern(gi, doublements, longs, degrees)).post();

        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        return mi;
    }

    public void solveorcut(int[] coupe, int recoupe,
                           Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                           DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, Soljoin soljoin) {
        if (debugPara) System.err.printf("+%d <- %d%n", active.incrementAndGet(), remaining.decrementAndGet());
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
        RealVar[] reachableIV = new RealVar[sink + 1];
        mi = buildcuttoend(mi, glb, gub, coupe, reachableIV, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops);
        try {
            mi.getSolver().propagate();

            int[] sol = new int[0];
            ArrayList<int[]> sols = new ArrayList<>();

            int arritylimit = instancenAct + 10;//4 * (this.reachableIV[0].getUB() - instancenAct) / 8 + instancenAct;
            boolean isrecutable =
                    arritycut(coupe, reachableIV) > arritylimit &&
                            recoupe > 300 && coupe.length < 8;// + 15 * (arritylimit - arritycut(coupe, reachableIV));

            if (!isrecutable) {
                while (mi.getSolver().solve()) {
                    sol = glb.getPredOf(sink).toArray();
                    sols.add(sol);
                }
                if (sol.length > 1) {
                    print("+" + mi.getSolver().getSolutionCount() + " solutions (" + (long) mi.getSolver().getTimeCount() + "s)");
                    synchronized (soljoin) {
                        soljoin.soladdall(sols);
                    }
                }
                if (debugPara) active.decrementAndGet();
                if (mi.getSolver().getTimeCount() > 10.)
                    print("~" + ForkJoinTask.getSurplusQueuedTaskCount() + " tasks left, uncut of size " + coupe.length + " arity " + arritycut(coupe, reachableIV) + " (" + (long) mi.getSolver().getTimeCount() + "s " + mi.getSolver().getSolutionCount() + " sols)");
            } else {
                mi = new GraphModel();
                glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
                gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
                mi = buildcuttocut(mi, glb, gub, coupe, recoupe, source, sink, nodReg, dcopy(), longs);

                ArrayList<int[]> recuts = new ArrayList<>();
                while (mi.getSolver().solve()) {
                    sol = glb.getPredOf(sink).toArray();
                    if (arritycut(sol, reachableIV) >= instancenAct)
                        soladd(sol, recuts);
                }
                if (recuts.size() > 1) {
                    if (debugPara) remaining.addAndGet(recuts.size());
                    if (debugPara) active.decrementAndGet();
                    print("+" + recuts.size() + " recuts (" + (long) mi.getSolver().getTimeCount() + "s)");
                    List<RecursiveAction> actions = new ArrayList<>();
                    for (int[] recut : recuts) {
                        actions.add(new RecursiveAction() {
                            @Override
                            protected void compute() {
                                solveorcut(recut, recoupe - 56, IV, source, sink, nodReg, instancenAct, doublements, longs, loops, soljoin);
                            }
                        });
                    }
                    ForkJoinTask.invokeAll(actions);
                } else {
                    print("inefficient recut");
                    solvesaftercut(coupe, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops, soljoin);
                    if (debugPara) active.decrementAndGet();
                }
            }
        } catch (ContradictionException ignored) {
            if (debugPara) active.decrementAndGet();
        }
    }

    public void solvecutcut(int[] start, int cut,
                            int source, int sink, TIntObjectHashMap<Registre> nodReg,
                            DirectedGraph doublements, Soljoin soljoin) {
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
        mi = buildcuttocut(mi, glb, gub, start, cut, source, sink, nodReg, dcopy(), longs);

        int[] sol = new int[0];
        ArrayList<int[]> sols = new ArrayList<>();
        int sumIV;
        while (mi.getSolver().solve()) {
            sol = glb.getPredOf(sink).toArray();
            sumIV = 0;
            for (int n : sol)
                sumIV += reachableIV[n].getUB();
            if (sumIV >= instance.nAct)
                sols.add(sol);
        }
        if (mi.getSolver().getSolutionCount() > 0) synchronized (soljoin) {
            soljoin.totalsol += mi.getSolver().getSolutionCount();
            soljoin.soladdall(sols);
        }
    }

    /**
     * solve all solutions until a certain round
     *
     * @param cut round limit
     */
    public void solvesourcesubgraph(int cut, Soljoin soljoin) {
        DirectedGraph ins = sourcesubgraph(cut);
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, n, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, n, dftSetType, false);
        glb.addNode(source);
        gub.addNode(source);
        for (int n : ins.getNodes())
            for (int s : ins.getSuccOf(n))
                gub.addArc(n, s);
        glb.addNode(sink);
        gub.addNode(sink);
        for (int i : ins.getNodes())
            if (ins.getSuccOf(i).size() == 0)
                gub.addArc(i, sink);
        DirectedGraphVar gi = mi.digraphVar("triviumsource", glb, gub);

        int[] minDegrees = new int[gi.getNbMaxNodes()];
        for (int i : gi.getPotentialNodes()) minDegrees[i] = 1;
        minDegrees[source] = 0;
        IntVar[] degrees = new IntVar[gi.getNbMaxNodes()];
        int[] minOutDegrees = new int[gi.getNbMaxNodes()];
        Arrays.fill(degrees, mi.intVar(0));
        for (int i : gi.getPotentialNodes()) {
            minOutDegrees[i] = i == sink ? 0 : 1;
            degrees[i] = mi.intVar("deg " + nodReg.get(i), 0, Math.min(2, gi.getPotSuccOf(i).size()));
        }
        new Constraint("minInDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.PREDECESSORS, minDegrees)).post();
        new Constraint("minOutDegrees", new PropNodeDegree_AtLeast_Incr(gi, Orientation.SUCCESSORS, minOutDegrees)).post();
        new Constraint("outDegrees", new PropNodeDegree_Var(gi, Orientation.SUCCESSORS, degrees)).post();
        new Constraint("DOUBLEORDIE", new PropDoubleOrDie(gi, doublements, degrees)).post();
        triplectr(gi, doublements, nodReg);
        disableVerification(mi);
        mi.getSolver().setSearch(new GraphSearch(gi).configure(GraphSearch.LEX).useLastConflict());
        int[] sol;
        int sumIV;
        while (mi.getSolver().solve()) {
            sol = glb.getPredOf(sink).toArray();
            sumIV = 0;
            for (int n : sol)
                sumIV += reachableIV[n].getUB();
            if (sumIV >= instance.nAct) soljoin.soladd(sol);
        }
        if (mi.getSolver().getSolutionCount() > 0)
            soljoin.totalsol += mi.getSolver().getSolutionCount();
    }

    /**
     * solve a submodel from a cut to the sink
     *
     * @param coupe set of starting nodes
     */
    public void solvesaftercut(int[] coupe,
                               Set<Integer> IV, int source, int sink, TIntObjectHashMap<Registre> nodReg, int instancenAct,
                               DirectedGraph doublements, DirectedGraph longs, DirectedGraph loops, Soljoin soljoin) {
        GraphModel mi = new GraphModel();
        DirectedGraph glb = new DirectedGraph(mi, sink + 1, dftSetType, false);
        DirectedGraph gub = new DirectedGraph(mi, sink + 1, dftSetType, false);
        RealVar[] reachableIV = new RealVar[sink + 1];
        mi = buildcuttoend(mi, glb, gub, coupe, reachableIV, IV, source, sink, nodReg, instancenAct, dcopy(), longs, loops);
        int[] sol = new int[0];
        ArrayList<int[]> sols = new ArrayList<>();
        while (mi.getSolver().solve()) {
            sol = glb.getPredOf(sink).toArray();
            sols.add(sol);
        }
        if (sol.length > 1) {// || mi.getSolver().getTimeCount() > 1.) {
            System.out.print("+" + mi.getSolver().getSolutionCount());
            synchronized (soljoin) {
                soljoin.soladdall(sols);
            }
        }
//        if (mi.getSolver().getTimeCount() > 10.)
//            print("innefficient taille " + coupe.length + " arrité " + arritycut(coupe, reachableIV));
    }

    private Pair<Integer, Integer>[] graphtoarcarray(DirectedGraph gra) {
        int cpt = 0;
        for (int n : gra.getNodes()) cpt += gra.getSuccOf(n).size();
        Pair<Integer, Integer>[] res = new Pair[cpt];
        cpt = 0;
        for (int n : gra.getNodes())
            for (int s : gra.getSuccOf(n))
                res[cpt++] = new Pair<>(n, s);
        return res;
    }

    private DirectedGraph arcarraytograph(Pair<Integer, Integer>[] gra) {
        DirectedGraph res = new DirectedGraph(gra.length * 2, dftSetType, false);
        for (Pair<Integer, Integer> p : gra)
            res.addArc(p.getFirst(), p.getSecond());
        return res;
    }

    private DirectedGraph subgraph(DirectedGraph graph, int i) {
        DirectedGraph sub = new DirectedGraph(nbRound * 4, dftSetType, false);
        sub.addNode(i);
        Deque<Integer> list = new ArrayDeque<>();
        list.add(i);
        while (!list.isEmpty()) {
            int j = list.pop();
            for (int s : graph.getSuccOf(j)) {
                sub.addArc(j, s);
                list.add(s);
            }
        }
        return sub;
    }

    private BitSet[] matrix(DirectedGraph g) {
        BitSet[] matrix = new BitSet[sink + 1];
        for (int n : g.getNodes()) {
            matrix[n] = new BitSet(sink + 1);
            for (int s : g.getSuccOf(n)) {
                //System.out.printf("%d -> %s%n", n, s);
                matrix[n].set(s);
            }
        }
        return matrix;
    }

    private BitSet[] matrix(DirectedGraphVar g) {
        BitSet[] matrix = new BitSet[sink + 1];
        for (int n : g.getPotentialNodes()) {
            matrix[n] = new BitSet(sink + 1);
            for (int s : g.getPotSuccOf(n)) {
                //System.out.printf("%d -> %s%n", n, s);
                matrix[n].set(s);
            }
        }
        return matrix;
    }

    private DirectedGraph subgraph(int i) {
        DirectedGraph sub = new DirectedGraph(nbRound * 4, dftSetType, false);
        sub.addNode(i);
        Deque<Integer> list = new ArrayDeque<>();
        list.add(i);
        while (!list.isEmpty()) {
            int j = list.pop();
            if (gmatrix[j] != null)
                for (int s = gmatrix[j].nextSetBit(0); s > -1; s = gmatrix[j].nextSetBit(s + 1)) {
                    sub.addArc(j, s);
                    list.add(s);
                }
        }
        return sub;
    }

    public DirectedGraph sourcesubgraph(int r) {
        DirectedGraph gs = new DirectedGraph(n, dftSetType, false);
        gs.addNode(source);

        Deque<Integer> list = new ArrayDeque<>();
        list.add(source);
        Registre reg;
        while (!list.isEmpty()) {
            int j = list.pop();
            reg = nodReg.get(j);
            if (reg.round > r) {
                for (int s : g.getPotSuccOf(j)) {
                    gs.addArc(j, s);
                    list.add(s);
                }
            }
        }
        return gs;
    }

    public double arritycut(int[] cut, RealVar[] reachableIV) {
        int sumIV = 0;
        for (int n : cut)
            sumIV += reachableIV[n].getUB();
        return sumIV;
    }

    private DirectedGraph union(DirectedGraph a, DirectedGraph b) {
        DirectedGraph c = new DirectedGraph(nbRound * 4, dftSetType, false);
        for (int n : a.getNodes())
            for (int s : a.getSuccOf(n))
                c.addArc(n, s);
        for (int n : b.getNodes())
            for (int s : b.getSuccOf(n))
                c.addArc(n, s);
        return c;
    }

    private DirectedGraph copy(DirectedGraph a) {
        DirectedGraph c = new DirectedGraph(a.getNbMaxNodes(), dftSetType, false);
        for (int n : a.getNodes()) {
            c.addNode(n);
            for (int s : a.getSuccOf(n))
                c.addArc(n, s);
        }
        return c;
    }

    private DirectedGraph dcopy() {
        DirectedGraph c = new DirectedGraph(n, dftSetType, false);
        for (int i = 0; i < sink; i++) {
            if (dmatrix[i] != null)
                for (int s = dmatrix[i].nextSetBit(0); s > -1; s = dmatrix[i].nextSetBit(s + 1))
                    c.addArc(i, s);
        }
        return c;
    }

    private int copy(int i) {
        int j = 0;
        j += i;
        return j;
    }

    private Set<Integer> copy(Set<Integer> a) {
        Set<Integer> b = new HashSet<>();
        for (int n : a) b.add(n);
        return b;
    }

    private HashMap<Integer, Registre> copy(HashMap<Integer, Registre> a) {
        HashMap<Integer, Registre> b = new HashMap<>();
        for (Integer i : a.keySet())
            b.put(i, a.get(i));
        return b;
    }

    private Instance copy(Instance a) {
        return a;
    }

    private DirectedGraph getIVgraph() {// usless
        DirectedGraph graph = new DirectedGraph(nbRound * 4, dftSetType, false);
        graph.addNode(sink);
        for (int i : IV) graph.addArc(i, sink);
        Deque<Integer> list = new ArrayDeque<>();
        list.add(sink);
        list.addAll(IV);
        while (!list.isEmpty()) {
            int j = list.pop();
            for (int p : g.getPotPredOf(j)) {
                graph.addArc(p, j);
                list.add(p);
            }
        }
        return graph;
    }

    /**
     * Disable the verification after thr choco resolution (reduce solving time)
     *
     * @param m model
     */
    private void disableVerification(Model m) {
        m.set(new Settings() {
            @Override
            public AbstractStrategy makeDefaultSearch(Model model) {
                // overrides default search strategy to handle graph vars
                AbstractStrategy other = Search.defaultSearch(model);
                GraphVar[] gvs = ((GraphModel) model).retrieveGraphVars();
                if (gvs.length == 0) {
                    return other;
                } else {
                    AbstractStrategy[] gss = new AbstractStrategy[gvs.length + 1];
                    for (int i = 0; i < gvs.length; i++) {
                        gss[i] = new GraphStrategy(gvs[i]);
                    }
                    gss[gvs.length] = other;
                    return Search.sequencer(gss);
                }
            }

            @Override
            public boolean checkModel(Solver solver) {
                return true;
            }
        });
    }

    private void soladd(int[] sol, ArrayList<int[]> sols) {
        int[] removesol = new int[0];
        for (int[] sol2 : sols)
            if (sol2.length == sol.length) {
                boolean eq = true;
                for (int i : sol) {
                    boolean in = false;
                    for (int i2 : sol2)
                        in = in || i == i2;
                    eq = eq && in;
                }
                if (eq) removesol = sol2;
            }
        if (removesol.length == 0) sols.add(sol);
        else sols.remove(removesol);
    }

    private void printsuper(ArrayList<int[]> sols) {
        if (sols.size() > 0) {
            int[][] solsint = new int[sols.size()][];
            Arrays.setAll(solsint, i -> sols.get(i));
            fusionSort(solsint, 0, solsint.length - 1);
            print("");
            System.out.print("SUPERPOLY: ");
            for (int[] sol : solsint) printsol(sol);
            print("");
            print("");
        }
    }

    private void printsol(int[] sol) {
        List<String> vars = new ArrayList<String>();
        boolean isIV = true;
        for (int p : sol) {
            if (!IV.contains(p)) {
                Registre r = nodReg.get(p);
                isIV = false;
                vars.add((r.reg == Reg.A ? "x" : r.reg == Reg.B ? "v" : "z") + -r.round);
            }
        }
        vars.sort(String.CASE_INSENSITIVE_ORDER);
        if (isIV) vars.add("1");
        for (String var : vars)
            System.out.print(var);
        System.out.print(" ");
    }

    private void makeP(FileWriter f, int n) throws IOException {
        int P = P(n);
        StringBuilder s = new StringBuilder("" + P);
        for (int pi : g.getPotPredOf(n)) {
            f.write("-" + P + " -" + isat(pi, n) + " 0\n");
            s.append(" ").append(isat(pi, n));
        }
        f.write(s + " 0\n");
    }

    private void makeSi(FileWriter f, int n) throws IOException {
        int[] S = {Si(n, 1), Si(n, 2), Si(n, 3)};
        int i = 0;
        for (int si : g.getPotSuccOf(n).toArray()) {
            if (!doublements.arcExists(n, si)) {
                f.write("-" + S[i] + " " + isat(n, si) + " 0\n");
                StringBuilder Si = new StringBuilder("" + S[i] + " -" + isat(n, si));
                for (int sj : g.getPotSuccOf(n)) {
                    if (sj != si) {
                        f.write("-" + S[i] + " -" + isat(n, sj) + " 0\n");
                        Si.append(" ").append(isat(n, sj));
                    }
                }
                f.write(Si + " 0\n");
                i++;
            }
        }
    }

    private void makeSd(FileWriter f, int n) throws IOException {
        int Sd = Sd(n);
        StringBuilder s = new StringBuilder("" + Sd);
        for (int si : g.getPotSuccOf(n)) {
            if (doublements.arcExists(n, si)) {
                f.write("-" + Sd + " " + isat(n, si) + " 0\n");
                s.append(" -").append(isat(n, si));
            } else {
                f.write("-" + Sd + " -" + isat(n, si) + " 0\n");
                s.append(" ").append(isat(n, si));
            }
        }
        f.write(s + " 0\n");
    }

    /**
     * generate dimax version of triviumgraph
     */
    private void writeSAT() {
        String ch = "/Users/agontier/Desktop/crypto/trivium.cnf";
        try {
            File myObj = new File(ch);
            if (myObj.createNewFile()) System.out.println("File created: " + myObj.getName());
            else System.out.println("File already exists.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
        try {
            FileWriter f = new FileWriter(ch);
            satindex();
            for (int n : g.getPotentialNodes()) {
                if (n == source) {
                    makeSi(f, n);
                    makeSd(f, n);
                    f.write(Si(n, 1) + " " + Si(n, 2) + " " + Si(n, 3) + " " + Sd(n) + " 0\n");
                } else if (n == sink) {
                } else if (IV.contains(n)) {
                    f.write(isat(n, sink) + " 0\n");
                    makeP(f, n);
                    f.write("-" + P(n) + " 0\n");
                } else if (g.getPotSuccOf(n).size() == 0) {
                    makeP(f, n);
                    f.write(P(n) + " 0\n");
                } else if (!g.getPotSuccOf(n).contains(sink)) {
                    makeP(f, n);
                    makeSi(f, n);
                    makeSd(f, n);
                    f.write(P(n) + " " + Si(n, 1) + " " + Si(n, 2) + " " + Si(n, 3) + " " + Sd(n) + " 0\n");
                    for (int si : g.getPotSuccOf(n))
                        f.write("-" + P(n) + " -" + isat(n, si) + " 0\n");
                }
            }
            f.close();
            System.out.println("DIMAX generated in " + ch);
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
//        ch="/Users/agontier/Desktop/crypto/trivium.cnf"
//        /Users/agontier/Desktop/glucose-syrup-4.1/parallel/glucose-syrup /Users/agontier/Desktop/crypto/trivium.cnf
//        /Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf
//        /Users/agontier/z3/build/z3 /Users/agontier/Desktop/crypto/trivium.cnf > /Users/agontier/Desktop/crypto/res.out
    }

    /**
     * build a unique index for each arc of the graph g
     */
    private void satindex() {
        isat = new HashMap<>(sink * 5);
        sati = new HashMap<>(sink * 5);
        int i = 1;
        for (int n : g.getPotentialNodes())
            for (int s : g.getPotSuccOf(n)) {
                Pair<Integer, Integer> p = new Pair<>(n, s);
                isat.put(p, i);
                sati.put(i, p);
                i++;
            }
        isatmax = i - 1;
    }

    private int isat(int a, int b) {
        return isat.get(new Pair<>(a, b));
    }

    private int P(int n) {
        return isatmax + 1 + n * 5;
    }

    private int Si(int n, int i) {
        return isatmax + 1 + n * 5 + i;
    }

    private int Sd(int n) {
        return isatmax + 1 + n * 5 + 4;
    }

    /**
     * interpret a sat solution
     */
    private void Interpret() {
        DirectedGraph graph = new DirectedGraph(nbRound * 4, dftSetType, false);
        try {
            File myObj = new File("/Users/agontier/Desktop/crypto/res.out");
            Scanner f = new Scanner(myObj);
            String sat = f.nextLine();
            if (sat.equals("sat")) {
                f.useDelimiter(" ");
                String s = "";
                while (f.hasNext()) {
                    s = f.next();
                    if (s.length() > 0 && s.charAt(0) != '-' && s.charAt(0) != '\n') {
                        int n = Integer.parseInt(s);
                        if (n <= isatmax) graph.addArc(sati.get(n).getFirst(), sati.get(n).getSecond());
                    }
                }
            }
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        print(graphVizExport(graph));
    }

    private enum Last {
        a1(Reg.A, 1),
        a66(Reg.A, 66),
        a93(Reg.A, 93),
        b69(Reg.B, 69),
        b84(Reg.B, 84),
        c66(Reg.C, 66),
        c111(Reg.C, 111),
        ;
        Reg re;
        int ro;

        Last(Reg a, int i) {
            this.re = a;
            this.ro = i;
        }
    }

    private enum Instance {
        //        iv_100(new int[]{1, 2, 3, 4, 5, 6}, 6),
        iv_100(new int[]{}, 0),
        iv_361(new int[]{1, 2}, 2),
        iv_386(new int[]{1, 2, 3, 4}, 4),
        iv_387(new int[]{1, 2, 3, 4}, 4),
        iv_388(new int[]{1, 2, 3}, 3),
        iv_416(new int[]{1, 2, 3, 4, 5}, 5),
        iv_429(new int[]{1, 2, 3, 4, 5, 6}, 6),
        iv_500(new int[]{1, 2, 3, 4, 5, 6}, 6),
        iv_570(new int[]{1, 11, 21, 31, 41, 51, 61}, 7),
        iv_590(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),
        iv_591(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),

        iv_672_1(new int[]{4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80}, 12), // 1+ x2 + x26 dans c66
        iv_672_2(new int[]{3, 6, 8, 11, 15, 25, 28, 40, 50, 57, 58, 62}, 12), // 1+x15 dans c66
        iv_672_3(new int[]{5, 10, 11, 16, 22, 25, 33, 37, 38, 49, 53, 74}, 12), // x20 dans c111
        iv_672_4(new int[]{4, 6, 15, 17, 19, 21, 34, 57, 58, 66, 74, 76}, 12), //1+x25 dans c66
        iv_672_5(new int[]{5, 11, 16, 22, 25, 29, 33, 49, 50, 65, 72, 74}, 12), // x33 dans c66
        iv_672_6(new int[]{4, 15, 17, 19, 21, 24, 33, 47, 57, 58, 66, 74}, 12), // x45 + x63 dans c66

        iv_675_1(new int[]{3, 14, 21, 25, 38, 43, 44, 47, 54, 56, 58, 68}, 12), // 1+ x1 + x10 + x51  (1+x1+x10 dans b84 et x51 dans c66)
        //49000 sols -> patern doublants -> 1384
        iv_675_2(new int[]{2, 4, 7, 8, 13, 19, 23, 39, 48, 59, 68, 75}, 12), //x8 dans b84
        iv_675_3(new int[]{2, 7, 8, 13, 19, 22, 30, 34, 35, 46, 50, 71}, 12), // x17 dans c111
        iv_675_4(new int[]{12, 17, 21, 23, 36, 44, 47, 52, 56, 59, 63, 64}, 12), // 1 + x21 + x51 dans c66

        iv_681_1(new int[]{6, 8, 11, 14, 16, 18, 29, 41, 48, 74, 77, 80}, 12), // x61
        iv_685_1(new int[]{1, 3, 10, 12, 14, 38, 45, 48, 50, 69, 75, 79}, 12), // x16

        iv_735_1(new int[]{2, 5, 9, 12, 13, 14, 19, 28, 36, 38, 40, 47, 49, 51, 52, 53, 55, 57, 63, 64, 66, 73, 79}, 23), //1 + x1 dans a66
        iv_735_2(new int[]{3, 4, 6, 9, 16, 17, 23, 27, 29, 38, 41, 46, 51, 56, 62, 63, 65, 66, 71, 72, 75, 78, 80}, 23), //x5
        iv_735_3(new int[]{5, 8, 12, 13, 15, 18, 19, 23, 25, 31, 34, 38, 39, 41, 51, 53, 64, 65, 67, 71, 73, 75, 78}, 23), //x9 dans a66


        iv_840_1(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,/*34,*/35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46,/*47,*/48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80}, 78),
        iv_840_2(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,/*34,*/35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46,/*47,*/48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, /*71,*/ 72, /*73,*/ 74, /*75,*/ 76, /*77,*/ 78, /*79,*/ 80}, 78),
        iv_840_3(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, /*73,*/ 74, /*75,*/ 76, /*77,*/ 78, /*79,*/ 80}, 78);
        int[] iv;
        int nAct;

        Instance(int[] iv, int nAct) {
            this.iv = iv;
            this.nAct = nAct;
        }

    }

    public static void main(String[] args) throws CmdLineException {
        if (args.length < 6) {
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "a66");
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "a93");
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "b69");
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "b84");
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "c66");
            new TriviumWithGraph(args[0], args[1], args[2], args[3], "-b", "c111");
        } else {
            new TriviumWithGraph(args);
        }
    }

    public static boolean compare(String s1, String s2) {
        if (s1.length() > s2.length()) return true;
        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) > s2.charAt(i)) return true;
                if (s1.charAt(i) < s2.charAt(i)) return false;
            }
        }
        return false;
    }

    public static boolean compare(int[] s1, int[] s2) {
        if (s1.length > s2.length) return true;
        if (s1.length == s2.length) {
            for (int i = 0; i < s1.length; i++) {
                if (s1[i] > s2[i]) return true;
                if (s1[i] < s2[i]) return false;
            }
        }
        return false;
    }

    public static void fusion(int[][] t, int d, int m, int f) {
        int[][] tmp = new int[f - d + 1][];//tab temporaire trié
        int di = d;//Indice courant partie gauche
        int fi = m + 1;//Indice courant partie droite
        for (int i = 0; i < tmp.length; i++) {
            if (fi > f) tmp[i] = t[di++];//Partie droite finie
            else if (di > m) tmp[i] = t[fi++];//Partie gauche finie
            else if (compare(t[di], t[fi])) tmp[i] = t[fi++];
            else tmp[i] = t[di++];
        }
        for (int i = 0; i < tmp.length; i++) {//recopie du tableau trié
            t[d + i] = tmp[i];
        }
    }

    public static void fusionSort(int[][] t, int d, int f) {
        if (d == f) return;
        int m = (f - d) / 2 + d;
        fusionSort(t, d, m);
        fusionSort(t, m + 1, f);
        fusion(t, d, m, f);
    }

    public static boolean equals(String s1, String s2) {
        return !compare(s1, s2) && !compare(s2, s1);
    }

    public static void fusion(String[] t, int d, int m, int f) {
        String[] tmp = new String[f - d + 1];//tab temporaire trié
        int di = d;//Indice courant partie gauche
        int fi = m + 1;//Indice courant partie droite
        for (int i = 0; i < tmp.length; i++) {
            if (fi > f) tmp[i] = t[di++];//Partie droite finie
            else if (di > m) tmp[i] = t[fi++];//Partie gauche finie
            else if (compare(t[di], t[fi])) tmp[i] = t[fi++];
            else tmp[i] = t[di++];
        }
        for (int i = 0; i < tmp.length; i++) {//recopie du tableau trié
            t[d + i] = tmp[i];
        }
    }

    public static void fusionSort(String[] t, int d, int f) {
        if (d == f) return;
        int m = (f - d) / 2 + d;
        fusionSort(t, d, m);
        fusionSort(t, m + 1, f);
        fusion(t, d, m, f);
    }

    public static void print(String[] t) {
        for (String a : t) System.out.print(a + " ");
        System.out.println();
    }

    public static void print(int[] t) {
        for (int a : t)
            if (a == 0)
                System.out.print(". ");
            else
                System.out.print(a + " ");
        System.out.println();
    }

    public static void print(String a) {
        System.out.println(a);
    }

    public int[][] init() {
        int[][] t = new int[nbRound * 15][2];
        for (int[] i : t) i[0] = 0;
        return t;
    }

    public static void add(int[][] t, int a, int b) {
        for (int[] i : t) {
            if (i[0] == 0) {
                i[0] = a;
                i[1] = b;
                return;
            }
            if (i[0] == a && i[1] == b) return;
        }
    }

    public static boolean in(int[][] t, int a, int b) {
        for (int[] i : t) {
            if (i[0] == 0) return false;
            if (i[0] == a && i[1] == b) return true;
        }
        return false;
    }

    public static void print(char a) {
        System.out.println(a);
    }

    public static void print(boolean a) {
        System.out.println(a);
    }

    public static void print(int a) {
        System.out.println(a);
    }

    public static void print(float a) {
        System.out.println(a);
    }

    public static void print(Pair<Integer, Integer> a) {
        System.out.println(a);
    }

    public static void print(int[][] a) {
        System.out.println(a);
    }

    public static void print(BoolVar a) {
        System.out.println(a);
    }

    public static void print(IntVar a) {
        System.out.println(a);
    }

    public static void print(RealVar a) {
        System.out.println(a);
    }

    public static void print(IntVar[] a) {
        for (IntVar v : a) print(v);
    }

    public static void print(IntVar[] a, DirectedGraphVar g) {
        for (int i = 0; i < a.length; i++)
            if (g.getPotentialNodes().contains(i)) print(a[i]);
    }

    public static void println() {
        System.out.println("");
    }

    public static void print(Last last) {
        System.out.println(last.re + " " + last.ro);
    }

    private void print(ArrayList<Pair<Integer, Integer>[]> pairs) {
        for (Pair<Integer, Integer>[] p : pairs) System.out.println(p);
    }
}
