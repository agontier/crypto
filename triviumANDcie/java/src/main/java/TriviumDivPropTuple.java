import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

public class TriviumDivPropTuple extends TriviumTools {
    public static Tuples propagT;

    public static void main(String[] args) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        int Nb = NBROUNDS;
        propagT = propagTTuples();
        BoolVar[][] X = new BoolVar[Nb + 1][288];
        setConstants(m, X);

//        int[] notcube = new int[]{34, 47};
//        setNotCube(m, X, notcube);
//        int[] monom = new int[]{69,70};// Monome clé (test papier 441 p16)
//        setMonomial(m, X, monom);

        int[] cube = new int[]{1, 3, 5, 7, 9, 10, 12, 14, 16, 18, 20, 22, 25, 27, 29, 31, 33, 35, 37, 40, 42, 44, 46, 48, 50, 52, 55, 57, 59,
                61, 63, 65, 67, 70, 72, 74, 76, 78, 80};
        setCube(m, X, cube);
        int[] notmonomial = new int[]{31, 32, 57, 58, 59, 80};
        setNotMonomial(m, X, notmonomial);

//        int[] cube = new int[]{};
//        setCube(m,X,cube);
//        int[] monom = new int[]{23,51};
//        setMonomial(m, X, monom);

        // propagation
        for (int r = 1; r < Nb + 1; r++) {
            for (int j = 0; j < 288; j++)
                if (!in(j, ttr)) {
                    assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
                    X[r][j + 1] = X[r - 1][j];//shift
                }
            for (int j : ttr) assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
            for (int j : ttrr) assert X[r][j] == null : r + " " + j + " : " + X[r][j];
            for (int j : ttrr) if (X[r][j] == null) X[r][j] = m.boolVar();
            m.table(new BoolVar[]{X[r - 1][242], X[r - 1][285], X[r - 1][286], X[r - 1][287], X[r - 1][68],
                    X[r][243], X[r][286], X[r][287], X[r][0], X[r][69]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][65], X[r - 1][90], X[r - 1][91], X[r - 1][92], X[r - 1][170],
                    X[r][66], X[r][91], X[r][92], X[r][93], X[r][171]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][161], X[r - 1][174], X[r - 1][175], X[r - 1][176], X[r - 1][263],
                    X[r][162], X[r][175], X[r][176], X[r][177], X[r][264]}, propagT).post();
        }
        setLastStateZ(m, X);
//        int[] last = new int[]{1};//{66, 93, 162, 177, 243, 288};
//        setLastState(m, X, last);

//        constraintHamming(m,X);
//        constraintMaxInIV(m,X);//error
//        constraintFibo(m,X);

        Solver solver = m.getSolver();
        solver.propagate();

//        setStrategyT(m, X);
//        instanciateSol(m,X);

//        solver.setSearch(Search.conflictHistorySearch(ArrayUtils.concat(ArrayUtils.concat(X[0], X[NBROUNDS]),X[200])));
//        Search.Restarts.LUBY.declare(solver, 50, 0, 5000);
//        solver.setNoGoodRecordingFromRestarts();

//        solver.showDashboard(); // a supprimer pour accélerer
//        solver.constraintNetworkToGephi("gephi.gexf");//génère le graphe
//        ZoomPanel panel = new ZoomPanel(X);
//        solver.plugMonitor(panel);
//        while (solver.solve())
//            panel.save((int) solver.getSolutionCount());

        solver.printShortFeatures();
        solver.printShortStatistics();
        while (solver.solve()) {
            solver.printShortStatistics();
        }
        solver.printStatistics();
    }
}