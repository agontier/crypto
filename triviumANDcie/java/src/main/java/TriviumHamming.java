import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;

public class TriviumHamming extends TriviumTools {
    public static Tuples propagT;

    public static void main(String[] args) throws ContradictionException {
        for (int r = 80 ; r <= 252; r++)
//        int r = 302;
            for (int i :ttt)
                print("r"+r+" y" + i + ": " + computeHW(i, r));
    }

    public static int computeHW(int ii, int rr) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        int Nb = rr;
        propagT = propagTTuples();
        BoolVar[][] X = new BoolVar[Nb + 1][288];
        setConstants(m, X);

        for (int i = 1; i <= 80; ++i)
            X[0][i - 1] = m.boolVar();

        for (int i = 1; i <= 80; ++i)
            X[0][i + 92] = m.boolVar();
        BoolVar[] IV = new BoolVar[80];
        for (int i = 1; i <= 80; ++i)
            IV[i - 1] = X[0][i + 92];

        IntVar obj = m.intVar("objective", 0, 999);
        m.sum(IV, "=", obj).post();
        m.setObjective(Model.MAXIMIZE, obj);
        // propagation
        for (int r = 1; r < Nb + 1; r++) {
            for (int j = 0; j < 288; j++)
                if (!in(j, ttr)) {
                    assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
                    X[r][j + 1] = X[r - 1][j];//shift
                }
            for (int j : ttr) assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
            for (int j : ttrr) assert X[r][j] == null : r + " " + j + " : " + X[r][j];
            for (int j : ttrr) if (X[r][j] == null) X[r][j] = m.boolVar();
            m.table(new BoolVar[]{X[r - 1][242], X[r - 1][285], X[r - 1][286], X[r - 1][287], X[r - 1][68],
                    X[r][243], X[r][286], X[r][287], X[r][0], X[r][69]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][65], X[r - 1][90], X[r - 1][91], X[r - 1][92], X[r - 1][170],
                    X[r][66], X[r][91], X[r][92], X[r][93], X[r][171]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][161], X[r - 1][174], X[r - 1][175], X[r - 1][176], X[r - 1][263],
                    X[r][162], X[r][175], X[r][176], X[r][177], X[r][264]}, propagT).post();
        }
        X[rr][ii - 1].eq(1).post();
        for (int i = 1; i <= 288; ++i)
            if (i != ii)
                X[rr][i - 1].eq(0).post();

        Solver solver = m.getSolver();
        solver.propagate();

//        setStrategyT(m, X);
//        solver.showDashboard();

        Solution sol = solver.findOptimalSolution(obj, true);
        return sol.getIntVal(obj);
    }
}