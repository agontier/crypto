/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.constraints.extension.Tuples;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * <br/>
 *
 * @author Charles Prud'homme, Arthur Gontier
 * @since 02/10/2020
 */
public class TriviumDivPropCopy extends TriviumTools {

    public TriviumDivPropCopy(Instance instance) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        int Nb = NBROUNDS;
        BoolVar[][] X = new BoolVar[Nb + 1][288];
        BoolVar[][] Copy1 = new BoolVar[Nb + 1][3];
        BoolVar[][] Copy2 = new BoolVar[Nb + 1][3];
        BoolVar[][] Copy3 = new BoolVar[Nb + 1][3];
        propagT = propagTTuples();

        setConstants(m, X);
        setCube(m, X, instance.iv);
        setFreeMonomial(m, X);

        for (int r = 1; r < Nb + 1; r++) {//Constraints for each round
            for (int j = 0; j < 288; j++) {
                if (!in(j, ttr)) {
                    assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
                    X[r][j + 1] = X[r - 1][j];//shift
                }
            }
//            roundCopy(m, X, r, Copy1, Copy2, Copy3);
//            roundEquations(m, X, r);
            roundTable(m, X, r);
        }
        setLastStateZ(m, X);
//        int[] last = new int[]{1};
//        setLastState(m, X, last);

        Solver solver = m.getSolver();
        solver.propagate();
//        solver.showDashboard(); // a supprimer pour accélerer
//        solver.constraintNetworkToGephi("gephi.gexf");//génère le graphe

        solver.printShortFeatures();
        solver.printShortStatistics();
        solveAndPrintSuperpoly(m, solver, instance, X);
    }

    public static void main(String[] args) throws ContradictionException {
//        NBROUNDS = 207;
        NBROUNDS = 672;
//        Instance inst = Instance.iv_100;
        Instance inst = Instance.iv_672;

        new TriviumDivPropCopy(inst);
    }
}
