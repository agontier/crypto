//import com.sun.org.apache.xpath.internal.operations.Bool;

import org.chocosolver.solver.Cause;
import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class TriviumTools {
    public static int NBROUNDS = 207;
    public static Tuples propagT;
    public static String solpath = "/Users/agontier/Desktop/crypto/";
    public static int[] tt = {243, 286, 287, 288, 69, 66, 91, 92, 93, 171, 162, 175, 176, 177, 264};
    public static int[] tt3 = {243, 286, 287, 288, 69, 1};//a+bc+d+e=t
    public static int[] tt1 = {66, 91, 92, 93, 171, 94};
    public static int[] tt2 = {162, 175, 176, 177, 264, 178};
    public static int[] ttz = {66, 93, 162, 177, 243, 288};
    public static int[] ttr = {242, 285, 286, 287, 68, 65, 90, 91, 92, 170, 161, 174, 175, 176, 263};
    public static int[] ttt = {1, 94, 178};
    public static int[] ttrr = {243, 286, 287, 0, 69, 66, 91, 92, 93, 171, 162, 175, 176, 177, 264, 177};

    public enum Instance {
        // 100
        iv_100(new int[]{}, 1),
        // 590
        iv_590(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),
        // 591
        iv_591(new int[]{1, 11, 21, 31, 41, 51, 61, 71}, 8),
        // 672
        iv_672(new int[]{4, 14, 19, 27, 39, 41, 48, 50, 56, 58, 67, 80}, 12),
        // 840
        iv_840(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,/*34,*/35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45, 46,/*47,*/48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80}, 78);
        int[] iv;
        int nAct;

        Instance(int[] iv, int nAct) {
            this.iv = iv;
            this.nAct = nAct;
        }
    }

    public static void roundTable(Model m, BoolVar[][] X, int r) {
        for (int j : ttr) assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
        for (int j : ttrr) assert X[r][j] == null : r + " " + j + " : " + X[r][j];
        for (int j : ttrr) if (X[r][j] == null) X[r][j] = m.boolVar();
        m.table(new BoolVar[]{X[r - 1][242], X[r - 1][285], X[r - 1][286], X[r - 1][287], X[r - 1][68],
                X[r][243], X[r][286], X[r][287], X[r][0], X[r][69]}, propagT).post();
        m.table(new BoolVar[]{X[r - 1][65], X[r - 1][90], X[r - 1][91], X[r - 1][92], X[r - 1][170],
                X[r][66], X[r][91], X[r][92], X[r][93], X[r][171]}, propagT).post();
        m.table(new BoolVar[]{X[r - 1][161], X[r - 1][174], X[r - 1][175], X[r - 1][176], X[r - 1][263],
                X[r][162], X[r][175], X[r][176], X[r][177], X[r][264]}, propagT).post();
    }

    public static void roundCopy(Model m, BoolVar[][] X, int r, BoolVar[][] Copy1, BoolVar[][] Copy2, BoolVar[][] Copy3) {
        propagT(m, 1 - 1, tt3, r, X, Copy3);
        propagT(m, 94 - 1, tt1, r, X, Copy1);
        propagT(m, 178 - 1, tt2, r, X, Copy2);
    }

    public static void roundEquations(Model m, BoolVar[][] X, int r) {
        equations(m, X, r, tt3);
        equations(m, X, r, tt1);
        equations(m, X, r, tt2);
    }

    public static void propagT(Model m, int t, int[] a, int r, BoolVar[][] X, BoolVar[][] Copy) {
        for (int i = 0; i < 5; i++) {//Variable creation
            assert X[r - 1][a[i] - 1] != null : "c1 " + (r - 1) + " " + (a[i] - 1) + " : " + X[r - 1][a[i] - 1];
            if (i != 3 && X[r][a[i]] == null) X[r][a[i]] = m.boolVar();
        }
        for (int i = 0; i < 3; i++) {
            assert Copy[r][i] == null : "c2 " + r + " " + i + " : " + Copy[r][i];
            Copy[r][i] = m.boolVar();
        }
        assert r >= NBROUNDS || X[r][t] == null : "c3 " + r + " " + t + " : " + X[r][t];
        if (X[r][t] == null) X[r][t] = m.boolVar();
        m.max(X[r - 1][a[0] - 1], new BoolVar[]{Copy[r][0], X[r][a[0]]}).post();//a ia copied in a'
        m.max(X[r - 1][a[1] - 1], new BoolVar[]{Copy[r][1], X[r][a[1]]}).post();//b is copied in y
        m.max(X[r - 1][a[2] - 1], new BoolVar[]{Copy[r][1], X[r][a[2]]}).post();//c is copied in y too because y=b'*c'
        m.max(X[r - 1][a[4] - 1], new BoolVar[]{Copy[r][2], X[r][a[4]]}).post();//e is copied in e' (d is not copied because it's the leaving bit)
        BoolVar[] tmp = {Copy[r][0], Copy[r][1], X[r - 1][a[3] - 1], Copy[r][2]};
        m.sum(tmp, "=", X[r][t]).post();//a' + y + d + e' = t
    }

    public static void solveAndPrintSuperpoly(Model m, Solver solver, Instance instance, BoolVar[][] X) {
        Hashtable<String, Integer> times = new Hashtable<String, Integer>();
        while (solver.solve()) {
            StringBuilder monome = new StringBuilder();
            boolean isIV = true;
            List<String> vars = new ArrayList<String>();
            for (int i = 0; i < 288; i++) {
                if (in(i, instance.iv)) continue;
                if (X[0][i].isInstantiatedTo(1)) {
                    String var = i < 93 ? ("x" + (i + 1)) : i < 177 ? ("v" + (i - 63 + 1)) : ("z" + (i - 177 + 1));
                    if (i < 93 || i > 280) vars.add(var);
                    isIV = false;
                }
            }
            vars.sort(String.CASE_INSENSITIVE_ORDER);
            if (isIV) vars.add("1");
            for (String var : vars) monome.append(var);
            String monom = monome.toString();
            if (times.containsKey(monom)) times.replace(monom, times.get(monom) + 1);
            else times.put(monom, 1);
        }
        System.out.println(times.toString());
        System.out.print("\nSUPERPOLY :");
        int cpt = 0;
        for (String k : times.keySet()) if (times.get(k) % 2 != 0) cpt++;
        String[] odd = new String[cpt];
        cpt = 0;
        for (String k : times.keySet()) if (times.get(k) % 2 != 0) odd[cpt++] = k;
        fusionSort(odd, 0, odd.length - 1);
        print(odd);
        m.getSolver().printShortStatistics();
    }

    public static void constraintCycle(Model m, BoolVar[][] X) {

    }

    public static void constraintLoop(Model m, BoolVar[][] X) {

    }

    public static void constraintMaxInIV(Model m, BoolVar[][] X) {
        for (int r = 288; r < NBROUNDS - 10; r++) {
            BoolVar[] tmp = new BoolVar[288];
            int[] t;
            for (int i = 0; i < 288; i++) {
                t = itrivium(r, i);
                int rr = t[0];
                int ii = t[1];
                tmp[i] = X[rr][ii];
            }
            m.sum(tmp, ">", 78).post();
        }
    }

    public static int[] itrivium(int r, int i) {
        int[] t = {0, 0};
        if (i < 93) {
            t[0] = r - i;
            t[1] = 0;
        } else if (i < 177) {
            t[0] = r - i + 93;
            t[1] = 93;
        } else {
            t[0] = r - i + 177;
            t[1] = 177;
        }
        return t;
    }

    public static void constraintFibo(Model m, BoolVar[][] X) {
        for (int r = 1; r < NBROUNDS; r++)
            m.sum(X[NBROUNDS - r], "<=", fibo(2 + (int) Math.ceil(r / 82.0))).post();
    }

    public static void constraintHamming(Model m, BoolVar[][] X) {
        m.sum(X[745], ">=", 1).post();
        m.sum(X[605], ">=", 2).post();
        m.sum(X[531], ">=", 3).post();
        m.sum(X[483], ">=", 4).post();
        m.sum(X[444], ">=", 5).post();
        m.sum(X[394], ">=", 6).post();
        m.sum(X[372], ">=", 7).post();
        m.sum(X[354], ">=", 8).post();
        m.sum(X[347], ">=", 9).post();
        m.sum(X[285], ">=", 11).post();
        m.sum(X[283], ">=", 12).post();
        m.sum(X[260], ">=", 14).post();
        m.sum(X[203], ">=", 19).post();
        m.sum(X[199], ">=", 25).post();
        m.sum(X[112], ">=", 38).post();
        m.sum(X[3], ">=", 77).post();
    }

    public static void setConstants(Model m, BoolVar[][] X) {
        // Constantes à 0
        for (int i = 81; i <= 93; i++) X[0][i - 1] = m.boolVar(false);
        for (int i = 93 + 81; i <= 285; i++) X[0][i - 1] = m.boolVar(false);
        // Constantes à 1
        for (int i = 286; i <= 288; i++) X[0][i - 1] = m.boolVar();
    }

    public static void setConstantsExaust(Model m, BoolVar[][] X) {
        // Constantes à 0
        for (int i = 81; i <= 93; i++) X[0][i - 1].eq(0).post();
        for (int i = 93 + 81; i <= 285; i++) X[0][i - 1].eq(0).post();
        // Constantes à 1
//        for (int i = 286; i <= 288; i++) X[0][i - 1].eq(0).post();
    }

    public static void setCube(Model m, BoolVar[][] X, int[] cube) {
        for (int i : cube)
            X[0][i + 92] = m.boolVar(true);
        for (int i = 1; i <= 80; ++i)
            if (!in(i, cube))
                X[0][i + 92] = m.boolVar(false);
    }

    public static void setFreeCube(Model m, BoolVar[][] X) {
        for (int i = 1; i <= 80; ++i) X[0][i + 92] = m.boolVar();
    }

    public static void setNotCube(Model m, BoolVar[][] X, int[] cube) {
        for (int i : cube)
            X[0][i + 92] = m.boolVar(false);
        for (int i = 1; i <= 80; ++i)
            if (!in(i, cube))
                X[0][i + 92] = m.boolVar(true);
    }

    public static void setCubeExaust(Model m, BoolVar[][] X, int[] cube) {
        // cube
        for (int i : cube)
            X[0][i + 92].eq(1).post();
        for (int i = 1; i <= 80; ++i)
            if (!in(i, cube))
                X[0][i + 92].eq(0).post();
    }

    public static void setNotCubeExaust(Model m, BoolVar[][] X, int[] cube) {
        for (int i : cube)
            X[0][i + 92].eq(0).post();
        for (int i = 1; i <= 80; ++i)
            if (!in(i, cube))
                X[0][i + 92].eq(1).post();
    }

    public static void setMonomial(Model m, BoolVar[][] X, int[] monom) {
        for (int i : monom)
            X[0][i - 1] = m.boolVar(true);
        for (int i = 1; i <= 80; ++i)
            if (!in(i, monom))
                X[0][i - 1] = m.boolVar(false);
    }

    public static void setFreeMonomial(Model m, BoolVar[][] X) {
        for (int i = 1; i <= 80; ++i) X[0][i - 1] = m.boolVar();
    }

    public static void setNotMonomial(Model m, BoolVar[][] X, int[] monom) {
        for (int i : monom)
            X[0][i - 1] = m.boolVar(false);
        for (int i = 1; i <= 80; ++i)
            if (!in(i, monom))
                X[0][i - 1] = m.boolVar(true);
    }

    public static void setMonomialExaust(Model m, BoolVar[][] X, int[] monom) {
        for (int i : monom)
            X[0][i - 1].eq(1).post();
        for (int i = 1; i <= 80; ++i)
            if (!in(i, monom))
                X[0][i - 1].eq(0).post();
    }

    public static void setNotMonomialExaust(Model m, BoolVar[][] X, int[] monom) {
        for (int i : monom)
            X[0][i - 1].eq(0).post();
        for (int i = 1; i <= 80; ++i)
            if (!in(i, monom))
                X[0][i - 1].eq(1).post();
    }

    public static void setLastState(Model m, BoolVar[][] X, int[] monom) {
        for (int i : monom)
            X[NBROUNDS][i - 1].eq(1).post();
        for (int i = 1; i <= 288; ++i)
            if (!in(i, monom))
                X[NBROUNDS][i - 1].eq(0).post();
    }

    public static void setLastStateZ(Model m, BoolVar[][] X) {
        for (int i = 0; i < 288; i++)
            if (!in(i + 1, ttz))
                X[NBROUNDS][i].eq(0).post();
        BoolVar[] tmp = new BoolVar[6];
        for (int i = 0; i < 6; i++)
            tmp[i] = X[NBROUNDS][ttz[i] - 1];
        m.sum(tmp, "=", 1).post();
    }

    public static void setStrategyT(Model m, BoolVar[][] X) {
        BoolVar[] varsorder = new BoolVar[288 * (NBROUNDS + 1)];
        int cvarsorder = 0;
        for (int r = NBROUNDS; r >= 0; r--) {
            varsorder[cvarsorder++] = X[r][1 - 1];
            varsorder[cvarsorder++] = X[r][94 - 1];
            varsorder[cvarsorder++] = X[r][178 - 1];
        }
        for (int r = NBROUNDS; r >= 0; r--) {
            for (int i : tt1) if (i != 94) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt2) if (i != 178) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt3) if (i != 1) varsorder[cvarsorder++] = X[r][i - 1];
        }
        for (int r = NBROUNDS; r >= 0; r--)
            for (int j = 0; j < 288; j++)
                if (!in(j + 1, tt1) && !in(j + 1, tt2) && !in(j + 1, tt3)
                        && (j + 1 != 94) && (j + 1 != 178) && (j + 1 != 1))
                    varsorder[cvarsorder++] = X[r][j];
        m.getSolver().setSearch(Search.inputOrderLBSearch(varsorder));
    }

    public static void setStrategy(Model m, BoolVar[][] X) {
        BoolVar[] varsorder = new BoolVar[288 * (NBROUNDS + 1)];
        int cvarsorder = 0;
        for (int r = NBROUNDS; r >= 0; r--) {
            for (int i : tt1) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt2) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt3) varsorder[cvarsorder++] = X[r][i - 1];
            for (int j = 0; j < 288; j++)
                if (!in(j + 1, tt1) && !in(j + 1, tt2) && !in(j + 1, tt3))
                    varsorder[cvarsorder++] = X[r][j];
        }
        m.getSolver().setSearch(Search.inputOrderLBSearch(varsorder));
    }

    public static void instanciateSol(Model m, BoolVar[][] X) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(solpath + "/sol.txt"),
                StandardCharsets.UTF_8)) {
            String line;
            int r = 0;
            while ((line = bufferedReader.readLine()) != null && r <= 840) {
                if (line.length() < 2)
                    line = bufferedReader.readLine();
//                print("line: " + line);
                int sum = 0;
                for (int i = 0; i < line.length(); i++) {
                    assert X[r][i] != null : r + " " + i + " : " + X[r][i];
//                    print(r + "  " + i + " = " + line.charAt(i) + " == " + X[r][i]);
                    X[r][i].instantiateTo(Integer.parseInt(String.valueOf(line.charAt(i))), Cause.Null);
                    if (Integer.parseInt(String.valueOf(line.charAt(i))) == 1)
                        sum += 1;
                }
                print(String.valueOf(r) + " " + String.valueOf(sum));
                r = r + 1;
            }
            for (BoolVar b : m.retrieveBoolVars()) {
                if (b.getDomainSize() != 1) {
                    print(b);
                }
            }
        } catch (IOException | ContradictionException ex) {
            System.out.format("I/O exception: " + ex);
        }
    }

    public static boolean compare(String s1, String s2) {
        if (s1.length() > s2.length()) return true;
        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) > s2.charAt(i)) return true;
                if (s1.charAt(i) < s2.charAt(i)) return false;
            }
        }
        return false;
    }

    public static boolean equals(String s1, String s2) {
        return !compare(s1, s2) && !compare(s2, s1);
    }

    public static void fusion(String[] t, int d, int m, int f) {
        String[] tmp = new String[f - d + 1];//tab temporaire trié
        int di = d;//Indice courant partie gauche
        int fi = m + 1;//Indice courant partie droite
        for (int i = 0; i < tmp.length; i++) {
            if (fi > f) tmp[i] = t[di++];//Partie droite finie
            else if (di > m) tmp[i] = t[fi++];//Partie gauche finie
            else if (compare(t[di], t[fi])) tmp[i] = t[fi++];
            else tmp[i] = t[di++];
        }
        for (int i = 0; i < tmp.length; i++) {//recopie du tableau trié
            t[d + i] = tmp[i];
        }
    }

    public static void fusionSort(String[] t, int d, int f) {
        if (d == f) return;
        int m = (f - d) / 2 + d;
        fusionSort(t, d, m);
        fusionSort(t, m + 1, f);
        fusion(t, d, m, f);
    }

    public static void equations(Model m, BoolVar[][] X, int r, int[] T) {// T = {66, 91, 92, 93, 171, 94};
        int a = T[0];
        int b = T[1];
        int c = T[2];
        int d = T[3];
        int e = T[4];
        int t = T[5] - 1;
        m.arithm(X[r][t], ">=", X[r - 1][d - 1]).post();
        m.arithm(X[r - 1][a - 1], ">=", X[r][a]).post();
        m.arithm(X[r - 1][e - 1], ">=", X[r][e]).post();
        m.arithm(X[r - 1][b - 1], ">=", X[r][b]).post();
        m.arithm(X[r - 1][c - 1], ">=", X[r][c]).post();
        m.scalar(new BoolVar[]{X[r][a], X[r - 1][d - 1], X[r - 1][a - 1]}, new int[]{1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][t], X[r][a], X[r - 1][a - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][e], X[r - 1][d - 1], X[r - 1][e - 1]}, new int[]{1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][t], X[r][e], X[r - 1][e - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][b], X[r - 1][d - 1], X[r - 1][b - 1]}, new int[]{1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r - 1][c - 1], X[r][b], X[r - 1][b - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][t], X[r][b], X[r - 1][b - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][c], X[r - 1][d - 1], X[r - 1][c - 1]}, new int[]{1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r - 1][b - 1], X[r][c], X[r - 1][c - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][t], X[r][c], X[r - 1][c - 1]}, new int[]{1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r][a], X[r][e], X[r - 1][a - 1], X[r - 1][e - 1]}, new int[]{1, 1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][a], X[r][b], X[r - 1][a - 1], X[r - 1][b - 1]}, new int[]{1, 1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][e], X[r][b], X[r - 1][e - 1], X[r - 1][b - 1]}, new int[]{1, 1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][a], X[r][c], X[r - 1][a - 1], X[r - 1][c - 1]}, new int[]{1, 1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r][e], X[r][c], X[r - 1][e - 1], X[r - 1][c - 1]}, new int[]{1, 1, -1, -1}, ">=", -1).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][a - 1], X[r - 1][e - 1], X[r - 1][b - 1], X[r][t]}, new int[]{1, 1, 1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][a - 1], X[r - 1][e - 1], X[r - 1][c - 1], X[r][t]}, new int[]{1, 1, 1, 1, -1}, ">=", 0).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][b - 1], X[r][t], X[r][a], X[r][e]}, new int[]{1, 1, -1, -1, -1}, ">=", -2).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][c - 1], X[r][t], X[r][a], X[r][e]}, new int[]{1, 1, -1, -1, -1}, ">=", -2).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][e - 1], X[r][t], X[r][a], X[r][b], X[r][c]}, new int[]{1, 1, -1, -1, -1, -1}, ">=", -3).post();
        m.scalar(new BoolVar[]{X[r - 1][d - 1], X[r - 1][a - 1], X[r][t], X[r][e], X[r][b], X[r][c]}, new int[]{1, 1, -1, -1, -1, -1}, ">=", -3).post();
    }

    public static Tuples propagTTuples() {
        Tuples tuples = new Tuples(true);
        tuples.add(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        tuples.add(1, 0, 0, 0, 0, 1, 0, 0, 0, 0);
        tuples.add(0, 1, 0, 0, 0, 0, 1, 0, 0, 0);
        tuples.add(1, 1, 0, 0, 0, 1, 1, 0, 0, 0);
        tuples.add(0, 0, 1, 0, 0, 0, 0, 1, 0, 0);
        tuples.add(1, 0, 1, 0, 0, 1, 0, 1, 0, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 1, 1, 0, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 1, 1, 0, 0);
        tuples.add(1, 0, 0, 0, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 0, 0, 1, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 1, 0);
        tuples.add(1, 0, 0, 0, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 0, 0, 1, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 0, 0, 0, 1, 1, 0, 0, 1, 0);
        tuples.add(1, 1, 0, 0, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 0, 1, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 1, 0);
        tuples.add(1, 1, 0, 0, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 0, 1, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 0, 0, 1, 1, 1, 0, 1, 0);
        tuples.add(1, 0, 1, 0, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 0, 1, 1, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 1, 0);
        tuples.add(1, 0, 1, 0, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 0, 1, 1, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 0, 1, 0, 1, 1, 0, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 1, 1, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 0, 1, 1, 1, 0);
        tuples.add(0, 1, 1, 1, 0, 0, 1, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 1, 0, 1, 1, 1, 0);
        tuples.add(1, 1, 1, 1, 0, 1, 1, 1, 1, 0);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 1, 0);
        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
        tuples.add(1, 0, 0, 0, 1, 1, 0, 0, 0, 1);
        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 0, 1);
        tuples.add(1, 1, 0, 0, 1, 1, 1, 0, 0, 1);
        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 0, 1);
        tuples.add(1, 0, 1, 0, 1, 1, 0, 1, 0, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 1, 1, 0, 1);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 0, 1);
        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 1, 1);
        tuples.add(1, 0, 0, 0, 1, 0, 0, 0, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 0, 0, 1, 1);
        tuples.add(0, 0, 0, 1, 1, 0, 0, 0, 1, 1);
        tuples.add(1, 1, 1, 0, 1, 1, 0, 0, 1, 1);
        tuples.add(1, 0, 0, 1, 1, 1, 0, 0, 1, 1);
        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 1, 1);
        tuples.add(1, 1, 0, 0, 1, 0, 1, 0, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 1, 0, 1, 1);
        tuples.add(0, 1, 0, 1, 1, 0, 1, 0, 1, 1);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 0, 1, 1);
        tuples.add(1, 1, 0, 1, 1, 1, 1, 0, 1, 1);
        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 1, 1);
        tuples.add(1, 0, 1, 0, 1, 0, 0, 1, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 0, 1, 1, 1);
        tuples.add(0, 0, 1, 1, 1, 0, 0, 1, 1, 1);
        tuples.add(1, 1, 1, 0, 1, 1, 0, 1, 1, 1);
        tuples.add(1, 0, 1, 1, 1, 1, 0, 1, 1, 1);
        tuples.add(1, 1, 1, 0, 1, 0, 1, 1, 1, 1);
        tuples.add(0, 1, 1, 1, 1, 0, 1, 1, 1, 1);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 1, 1);
        tuples.add(1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
        return tuples;
    }

    public static void maingrain(String[] args) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        int Nb = 100;
        BoolVar[][] B = new BoolVar[Nb + 1][128];
        BoolVar[][] S = new BoolVar[Nb + 1][128];

        BoolVar[][] S0 = new BoolVar[Nb][2]; //F + B
        BoolVar[] S7 = new BoolVar[Nb];       //F
        BoolVar[][] S8 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[][] S13 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[][] S20 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[] S38 = new BoolVar[Nb];  // F
        BoolVar[][] S42 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[][] S60 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[] S70 = new BoolVar[Nb];       // F
        BoolVar[][] S79 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[] S81 = new BoolVar[Nb]; // F
        BoolVar[][] S93 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[][] S94 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[] S96 = new BoolVar[Nb];        // F

        BoolVar[][] B2 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[][] B12 = new BoolVar[Nb][4]; // HSx2 + HBx2
        BoolVar[][] B15 = new BoolVar[Nb][2]; // HS + HB
        BoolVar[] B26 = new BoolVar[Nb];       //G
        BoolVar[][] B36 = new BoolVar[Nb][2];// HS + HB
        BoolVar[][] B45 = new BoolVar[Nb][2];// HS + HB
        BoolVar[] B56 = new BoolVar[Nb];       //G
        BoolVar[][] B64 = new BoolVar[Nb][2];// HS + HB
        BoolVar[][] B73 = new BoolVar[Nb][2];// HS + HB
        BoolVar[][] B89 = new BoolVar[Nb][2];// HS + HB
        BoolVar[] B91 = new BoolVar[Nb];       //G
        BoolVar[][] B95 = new BoolVar[Nb][5]; // HSx2 + HBx2 + G
        BoolVar[] B96 = new BoolVar[Nb];       //G


        BoolVar[][] G = new BoolVar[Nb][10];
        BoolVar[][] HS = new BoolVar[Nb][5];
        BoolVar[][] HB = new BoolVar[Nb][5];

        //Declaration
        for (int r = 0; r < Nb; r++) {
            for (int i = 1; i <= 5; i++) {
                HS[r][i - 1] = m.boolVar();
            }
            for (int i = 1; i <= 5; i++) {
                HB[r][i - 1] = m.boolVar();
            }
            for (int i = 1; i <= 10; ++i) {
                G[r][i - 1] = m.boolVar();
            }
            for (int i = 1; i <= 5; i++) {
                B95[r][i - 1] = m.boolVar();
            }
            for (int i = 1; i <= 4; i++) {
                B12[r][i - 1] = m.boolVar();
            }

            S0[r][0] = m.boolVar();
            S0[r][1] = m.boolVar();
            S7[r] = m.boolVar();
            S8[r][0] = m.boolVar();
            S8[r][1] = m.boolVar();
            S13[r][0] = m.boolVar();
            S13[r][1] = m.boolVar();
            S20[r][0] = m.boolVar();
            S20[r][1] = m.boolVar();
            S38[r] = m.boolVar();
            S42[r][0] = m.boolVar();
            S42[r][1] = m.boolVar();
            S60[r][0] = m.boolVar();
            S60[r][1] = m.boolVar();
            S70[r] = m.boolVar();
            S79[r][0] = m.boolVar();
            S79[r][1] = m.boolVar();
            S81[r] = m.boolVar();
            S93[r][0] = m.boolVar();
            S93[r][1] = m.boolVar();
            S94[r][0] = m.boolVar();
            S94[r][1] = m.boolVar();
            S96[r] = m.boolVar();

            B2[r][0] = m.boolVar();
            B2[r][1] = m.boolVar();
            B15[r][0] = m.boolVar();
            B15[r][1] = m.boolVar();
            B26[r] = m.boolVar();
            B36[r][0] = m.boolVar();
            B36[r][1] = m.boolVar();
            B45[r][0] = m.boolVar();
            B45[r][1] = m.boolVar();
            B56[r] = m.boolVar();
            B64[r][0] = m.boolVar();
            B64[r][1] = m.boolVar();
            B73[r][0] = m.boolVar();
            B73[r][1] = m.boolVar();
            B89[r][0] = m.boolVar();
            B89[r][1] = m.boolVar();
            B91[r] = m.boolVar();
            B96[r] = m.boolVar();
        }
        for (int i = 1; i <= 128; ++i) {
            if (B[0][i - 1] == null) {
                B[0][i - 1] = m.boolVar();
            }
        }


        for (int i = 1; i <= 9; i++) S[0][i - 1] = m.boolVar(true);

        for (int i = 1; i <= 128; ++i) {
            if (S[0][i - 1] == null) {
                S[0][i - 1] = m.boolVar();
            }
        }

        // propagation
        for (int r = 1; r <= Nb; r++) {
            for (int i = 1; i <= 128; i++) {
                if (i != 26 && i != 56 && i != 91 && i != 96 && i != 3 && i != 67 && i != 11 &&
                        i != 13 && i != 17 && i != 18 && i != 27 && i != 59 && i != 40 && i != 48 && i != 61 && i != 65 && i != 68 && i != 84
                        && i != 88 && i != 92 && i != 93 && i != 95 && i != 22 && i != 24 && i != 25 && i != 70 && i != 78 && i != 82
                        && i != 12 && i != 2 && i != 15 && i != 36 && i != 45 && i != 64 && i != 73 && i != 89 && i != 128) {
                    B[r][i - 1] = B[r - 1][i];
                } else B[r][i - 1] = m.boolVar();
                if (i != 7 && i != 38 && i != 70 && i != 81 && i != 96 && i != 8 && i != 13 && i != 20 && i != 42 && i != 60 && i != 79 &&
                        i != 94 && i != 93 && i != 128) {
                    S[r][i - 1] = S[r - 1][i];
                } else S[r][i - 1] = m.boolVar();
            }
            m.max(B[r - 1][95], new BoolVar[]{B95[r - 1][0], B95[r - 1][1], B95[r - 1][2], B95[r - 1][3], B95[r - 1][4], B[r][94]}).post();//B95
            m.max(B[r - 1][12], new BoolVar[]{B12[r - 1][0], B12[r - 1][1], B12[r - 1][2], B12[r - 1][3], B[r][11]}).post();//B12
            m.max(B[r - 1][2], new BoolVar[]{B2[r - 1][0], B2[r - 1][1], B[r][1]}).post();//B2
            m.max(B[r - 1][15], new BoolVar[]{B15[r - 1][0], B15[r - 1][1], B[r][14]}).post();//B15

            m.max(B[r - 1][36], new BoolVar[]{B36[r - 1][0], B36[r - 1][1], B[r][35]}).post();//B36
            m.max(B[r - 1][45], new BoolVar[]{B45[r - 1][0], B45[r - 1][1], B[r][44]}).post();//B45
            m.max(B[r - 1][64], new BoolVar[]{B64[r - 1][0], B64[r - 1][1], B[r][63]}).post();//B64
            m.max(B[r - 1][73], new BoolVar[]{B73[r - 1][0], B73[r - 1][1], B[r][72]}).post();//B73
            m.max(B[r - 1][89], new BoolVar[]{B89[r - 1][0], B89[r - 1][1], B[r][88]}).post();//B89

            m.max(B[r - 1][26], new BoolVar[]{B26[r - 1], B[r][25]}).post();
            m.max(B[r - 1][56], new BoolVar[]{B56[r - 1], B[r][55]}).post();
            m.max(B[r - 1][91], new BoolVar[]{B91[r - 1], B[r][90]}).post();
            m.max(B[r - 1][96], new BoolVar[]{B96[r - 1], B[r][95]}).post();


            m.max(S[r - 1][8], new BoolVar[]{S8[r - 1][0], S8[r - 1][1], S[r][7]}).post();//S8
            m.max(S[r - 1][13], new BoolVar[]{S13[r - 1][0], S13[r - 1][1], S[r][12]}).post();//S13
            m.max(S[r - 1][20], new BoolVar[]{S20[r - 1][0], S20[r - 1][1], S[r][19]}).post();//S20
            m.max(S[r - 1][42], new BoolVar[]{S42[r - 1][0], S42[r - 1][1], S[r][41]}).post();//S42
            m.max(S[r - 1][60], new BoolVar[]{S60[r - 1][0], S60[r - 1][1], S[r][59]}).post();//S60
            m.max(S[r - 1][79], new BoolVar[]{S79[r - 1][0], S79[r - 1][1], S[r][78]}).post();//S79
            m.max(S[r - 1][94], new BoolVar[]{S94[r - 1][0], S94[r - 1][1], S[r][93]}).post();//S94

            m.max(S[r - 1][93], new BoolVar[]{S93[r - 1][0], S93[r - 1][1], S[r][92]}).post();//S93

            m.max(S[r - 1][0], new BoolVar[]{S0[r - 1][0], S0[r - 1][1]}).post();

            m.max(S[r - 1][7], new BoolVar[]{S7[r - 1], S[r][6]}).post();
            m.max(S[r - 1][38], new BoolVar[]{S38[r - 1], S[r][37]}).post();
            m.max(S[r - 1][70], new BoolVar[]{S70[r - 1], S[r][69]}).post();
            m.max(S[r - 1][81], new BoolVar[]{S81[r - 1], S[r][80]}).post();
            m.max(S[r - 1][96], new BoolVar[]{S96[r - 1], S[r][95]}).post();


            // Calcul s127

            // Calcul de h: b12s8 + s13s20 + b95s42 + s60s79 + b12b95s94

            B12[r - 1][0].eq(HS[r - 1][0]).post();
            S8[r - 1][0].eq(HS[r - 1][0]).post();

            S13[r - 1][0].eq(HS[r - 1][1]).post();
            S20[r - 1][0].eq(HS[r - 1][1]).post();

            B95[r - 1][0].eq(HS[r - 1][2]).post();
            S42[r - 1][0].eq(HS[r - 1][2]).post();

            S60[r - 1][0].eq(HS[r - 1][3]).post();
            S79[r - 1][0].eq(HS[r - 1][3]).post();

            B12[r - 1][1].eq(HS[r - 1][4]).post();
            B95[r - 1][1].eq(HS[r - 1][4]).post();
            S94[r - 1][0].eq(HS[r - 1][4]).post();


            BoolVar[] tmpS = {
                    S0[r - 1][0], S7[r - 1], S38[r - 1], S70[r - 1], S81[r - 1], S96[r - 1],
                    HS[r - 1][0], HS[r - 1][1], HS[r - 1][2], HS[r - 1][3], HS[r - 1][4],
                    S93[r - 1][0], B2[r - 1][0], B15[r - 1][0], B36[r - 1][0], B45[r - 1][0], B64[r - 1][0], B73[r - 1][0], B89[r - 1][0]
            };
            m.sum(tmpS, "=", S[r][127]).post();


            // Calcul b127

            // Calcul de h: b12s8 + s13s20 + b95s42 + s60s79 + b12b95s94

            B12[r - 1][2].eq(HB[r - 1][0]).post();
            S8[r - 1][1].eq(HB[r - 1][0]).post();

            S13[r - 1][1].eq(HB[r - 1][1]).post();
            S20[r - 1][1].eq(HB[r - 1][1]).post();

            B95[r - 1][2].eq(HB[r - 1][2]).post();
            S42[r - 1][1].eq(HB[r - 1][2]).post();

            S60[r - 1][1].eq(HB[r - 1][3]).post();
            S79[r - 1][1].eq(HB[r - 1][3]).post();

            B12[r - 1][3].eq(HB[r - 1][4]).post();
            B95[r - 1][3].eq(HB[r - 1][4]).post();
            S94[r - 1][1].eq(HB[r - 1][4]).post();


            // calcul de g: (b0 + b26 + b56 + b91 + b96) + b3b67 + b11b13 + b17b18 + b27b59 + b40b48 + b61b65 + b68b84
            // + b88b92b93b95 + b22b24b25 + b70b78b82


            m.max(B[r - 1][3], new BoolVar[]{G[r - 1][0], B[r][2]}).post();// b3 is copied in G[0]
            m.max(B[r - 1][67], new BoolVar[]{G[r - 1][0], B[r][66]}).post();// b67 is copied in G[0]

            m.max(B[r - 1][11], new BoolVar[]{G[r - 1][1], B[r][10]}).post();// b11 is copied in G[1]
            m.max(B[r - 1][13], new BoolVar[]{G[r - 1][1], B[r][12]}).post();// b13 is copied in G[1]

            m.max(B[r - 1][17], new BoolVar[]{G[r - 1][2], B[r][16]}).post();// b17 is copied in G[2]
            m.max(B[r - 1][18], new BoolVar[]{G[r - 1][2], B[r][17]}).post();// b18 is copied in G[2]

            m.max(B[r - 1][27], new BoolVar[]{G[r - 1][3], B[r][26]}).post();// b27 is copied in G[3]
            m.max(B[r - 1][59], new BoolVar[]{G[r - 1][3], B[r][58]}).post();// b59 is copied in G[3]

            m.max(B[r - 1][40], new BoolVar[]{G[r - 1][4], B[r][39]}).post();// b39 is copied in G[4]
            m.max(B[r - 1][48], new BoolVar[]{G[r - 1][4], B[r][47]}).post();// b47 is copied in G[4]

            m.max(B[r - 1][61], new BoolVar[]{G[r - 1][5], B[r][60]}).post();// b61 is copied in G[5]
            m.max(B[r - 1][65], new BoolVar[]{G[r - 1][5], B[r][64]}).post();// b65 is copied in G[5]

            m.max(B[r - 1][68], new BoolVar[]{G[r - 1][6], B[r][67]}).post();// b68 is copied in G[6]
            m.max(B[r - 1][84], new BoolVar[]{G[r - 1][6], B[r][83]}).post();// b84 is copied in G[6]

            m.max(B[r - 1][88], new BoolVar[]{G[r - 1][7], B[r][87]}).post();// b88 is copied in G[7]
            m.max(B[r - 1][92], new BoolVar[]{G[r - 1][7], B[r][91]}).post();// b92 is copied in G[7]
            m.max(B[r - 1][93], new BoolVar[]{G[r - 1][7], B[r][92]}).post();// b93 is copied in G[7]
            B95[r - 1][4].eq(G[r - 1][7]).post(); // b95 is in G[7]

            m.max(B[r - 1][22], new BoolVar[]{G[r - 1][8], B[r][21]}).post();// b22 is copied in G[8]
            m.max(B[r - 1][24], new BoolVar[]{G[r - 1][8], B[r][23]}).post();// b24 is copied in G[8]
            m.max(B[r - 1][25], new BoolVar[]{G[r - 1][8], B[r][24]}).post();// b25 is copied in G[8]

            m.max(B[r - 1][70], new BoolVar[]{G[r - 1][9], B[r][69]}).post();// b70 is copied in G[9]
            m.max(B[r - 1][78], new BoolVar[]{G[r - 1][9], B[r][77]}).post();// b78 is copied in G[9]
            m.max(B[r - 1][82], new BoolVar[]{G[r - 1][9], B[r][81]}).post();// b82 is copied in G[9]


            // g + s0 + z
            BoolVar[] tmpB = {
                    B[r - 1][0], B26[r - 1], B56[r - 1], B91[r - 1], B96[r - 1],
                    G[r - 1][0], G[r - 1][1], G[r - 1][2], G[r - 1][3], G[r - 1][4], G[r - 1][5],
                    G[r - 1][6], G[r - 1][7], G[r - 1][8], G[r - 1][9],
                    S0[r - 1][1],
                    HB[r - 1][0], HB[r - 1][1], HB[r - 1][2], HB[r - 1][3], HB[r - 1][4],
                    S93[r - 1][1], B2[r - 1][1], B15[r - 1][1], B36[r - 1][1], B45[r - 1][1], B64[r - 1][1], B73[r - 1][1], B89[r - 1][1]
            };
            m.sum(tmpB, "=", B[r][127]).post();

            Solver solver = m.getSolver();

            solver.constraintNetworkToGephi("gephi.gexf");//génère le graphe
        }
    }

    public static int fibo(int r) {
        if (r == 0)
            return 0;
        else if (r == 1 || r == 2)
            return 1;
        else
            return fibo2(1, 1, r - 3);
    }

    public static int fibo2(int a, int b, int r) {
        if (r > 0)
            return fibo2(b, a + b, r - 1);
        else
            return a + b;
    }

    public static int[] range(Character s) {
        if (s == 'A') {
            int[] t = new int[93];
            for (int i = 0; i < 93; i++)
                t[i] = i;
            return t;
        } else if (s == 'B') {
            int[] t = new int[84];
            for (int i = 0; i < 84; i++)
                t[i] = i;
            return t;
        } else {
            int[] t = new int[111];
            for (int i = 0; i < 111; i++)
                t[i] = i;
            return t;
        }
    }

    public static int[] range(int a, int b) {
        assert b >= a;
        int[] t = new int[b - a];
        for (int i = 0; i < b - a; i++)
            t[i] = a + i;
        return t;
    }

    public static boolean in(int a, int[] t) {
        for (int i : t)
            if (i == a)
                return true;
        return false;
    }

    public static void print(String a) {
        System.out.println(a);
    }

    public static void print(String[] t) {
        for (String a : t) System.out.print(a + " ");
        System.out.println();
    }

    public static void print(char a) {
        System.out.println(a);
    }

    public static void print(boolean a) {
        System.out.println(a);
    }

    public static void print(int a) {
        System.out.println(a);
    }

    public static void print(int[] a) {
        System.out.println(a);
    }

    public static void print(int[][] a) {
        System.out.println(a);
    }

    public static void print(BoolVar a) {
        System.out.println(a);
    }

    public static void print(IntVar a) {
        System.out.println(a);
    }
}