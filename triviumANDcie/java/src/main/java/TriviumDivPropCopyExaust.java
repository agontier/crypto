/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.exception.ContradictionException;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 02/10/2020
 */
public class TriviumDivPropCopyExaust extends TriviumTools {

    public static void main(String[] args) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        int Nb = NBROUNDS;
        BoolVar[][] X = /*new BoolVar[Nb + 1][288];/*/m.boolVarMatrix("X", Nb + 1, 288);
        BoolVar[][] Copy1 = /*new BoolVar[Nb + 1][5];/*/m.boolVarMatrix("C1", Nb + 1, 3);
        BoolVar[][] Copy2 = /*new BoolVar[Nb + 1][5];/*/m.boolVarMatrix("C2", Nb + 1, 3);
        BoolVar[][] Copy3 = /*new BoolVar[Nb + 1][5];/*/m.boolVarMatrix("C3", Nb + 1, 3);
        //****** VARIABLES CREATION
        setConstantsExaust(m, X);

        int[] notcube = new int[]{34, 47};
        setNotCubeExaust(m, X, notcube);
        int[] monom = new int[]{12};// Monome clé (test papier 441 p16)
        setMonomialExaust(m, X, monom);

//        int[] cube = new int[]{};
//        setCubeExaust(m,X,cube);
//        int[] monom = new int[]{62};
//        setMonomialExaust(m, X, monom);
//        int[] last = new int[]{1};
//        setLastState(m,X,last);

        //Constraint for each round
        for (int r = 1; r < Nb + 1; r++) {
            for (int j = 0; j < 288; j++) {
                if (!in(j, ttr)) {
                    X[r][j + 1].eq(X[r - 1][j]).post();
                }
                propagT(m, 1 - 1, tt3, r, X, Copy3);
                propagT(m, 94 - 1, tt1, r, X, Copy1);
                propagT(m, 178 - 1, tt2, r, X, Copy2);
            }
        }
        setLastStateZ(m, X);

//        PreProcessing.detectIntEqualities(m);
        Solver solver = m.getSolver();
        setStrategyT(m, X);
        instanciateSol(m, X);

        solver.printShortFeatures();
        solver.printShortStatistics();
        while (solver.solve()) {
            solver.printShortStatistics();
        }
        solver.printStatistics();
    }

    public static void propagT(Model m, int t, int[] a, int r, BoolVar[][] X, BoolVar[][] Copy) {
        m.max(X[r - 1][a[0] - 1], new BoolVar[]{Copy[r][0], X[r][a[0]]}).post();//a ia copied in a'
        m.max(X[r - 1][a[1] - 1], new BoolVar[]{Copy[r][1], X[r][a[1]]}).post();//b is copied in y
        m.max(X[r - 1][a[2] - 1], new BoolVar[]{Copy[r][1], X[r][a[2]]}).post();//c is copied in y too because y=b'*c'
        m.max(X[r - 1][a[4] - 1], new BoolVar[]{Copy[r][2], X[r][a[4]]}).post();//e is copied in e' (d is not copied because it's the leaving bit)
        BoolVar[] tmp = {Copy[r][0], Copy[r][1], X[r - 1][a[3] - 1], Copy[r][2]};
        m.sum(tmp, "=", X[r][t]).post();//a' + y + d + e' = t
    }

}
