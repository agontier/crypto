import org.chocosolver.solver.Cause;
import org.chocosolver.solver.DefaultSettings;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.trace.CPProfiler;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.util.tools.PreProcessing;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TriviumWithEq {
    public static int NBROUNDS;
    public static int NBONE;
    public static Tuples propagT;
    public static String solpath = "/Users/agontier/Desktop/crypto/";
    public static int[] tt = {243, 286, 287, 288, 69, 66, 91, 92, 93, 171, 162, 175, 176, 177, 264};
    public static int[] tt3 = {243, 286, 287, 288, 69, 1};//a+bc+d+e=t
    public static int[] tt1 = {66, 91, 92, 93, 171, 94};
    public static int[] tt2 = {162, 175, 176, 177, 264, 178};
    public static int[] ttz = {66, 93, 162, 177, 243, 288};
    public static int[] ttr = {242, 285, 286, 287, 68, 65, 90, 91, 92, 170, 161, 174, 175, 176, 263};
    public static int[] ttrr = {243, 286, 287, 0, 69, 66, 91, 92, 93, 171, 162, 175, 176, 177, 264, 178};

    public static void main(String[] args) throws ContradictionException {
        NBROUNDS = Integer.parseInt(args[0]);
        NBONE = Integer.parseInt(args[1]); // 0 means no obj to improve
        solve(NBROUNDS, NBONE);
    }

    public static void solve(int Nb, int s) throws ContradictionException {
        Model m = new Model(new DefaultSettings().setEnableSAT(false));
        BoolVar[][] X = m.boolVarMatrix("X", Nb+1, 288);
        propagT = createpropagT();
        // Constantes à 0
        for (int i = 81; i <= 93; i++) X[0][i - 1].eq(0).post();
        for (int i = 93 + 81; i <= 285; i++) X[0][i - 1].eq(0).post();
        // Constantes à 1
        // cube
        for (int i = 1; i <= 80; ++i)
            if (i != 34 && i != 47)
                X[0][i + 92].eq(1).post();
            else
                X[0][i + 92].eq(0).post();
        // Monome clé (test papier 441 p16)
        for (int i = 1; i <= 80; ++i)
            if (i != 12)
                X[0][i - 1].eq(0).post();
            else
                X[0][i - 1].eq(1).post();
        // Last state
        for (int i = 0; i < 288; i++)
            if (!in(i + 1, ttz))
                X[Nb][i].eq(0).post();

        // propagation
        for (int r = 1; r < Nb + 1; r++) {
            for (int j = 0; j < 288; j++)
                if (!in(j, ttr)) {
                    assert X[r - 1][j] != null : r - 1 + " " + j + " : " + X[r - 1][j];
                    X[r][j + 1].eq(X[r - 1][j]).post();//shift
                }
            m.table(new BoolVar[]{X[r - 1][242], X[r - 1][285], X[r - 1][286], X[r - 1][287], X[r - 1][68], X[r][243], X[r][286], X[r][287], X[r][0], X[r][69]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][65], X[r - 1][90], X[r - 1][91], X[r - 1][92], X[r - 1][170], X[r][66], X[r][91], X[r][92], X[r][93], X[r][171]}, propagT).post();
            m.table(new BoolVar[]{X[r - 1][161], X[r - 1][174], X[r - 1][175], X[r - 1][176], X[r - 1][263], X[r][162], X[r][175], X[r][176], X[r][177], X[r][264]}, propagT).post();
        }
        BoolVar[] tmp = new BoolVar[6];
        for (int i = 0; i < 6; i++)
            tmp[i] = X[Nb][ttz[i] - 1];
        m.sum(tmp, "=", 1).post();

        PreProcessing.detectIntEqualities(m);
        Solver solver = m.getSolver();
        solver.propagate();

        setStrategyT(m, X);
        solver.printShortFeatures();
        solver.printShortStatistics();
        //solver.showDecisions(()->"");
        //solver.limitNode(20);
        //solver.limitSolution(4);
        //solver.showDecisions(() -> "");
        solver.showDashboard();
         try (CPProfiler profiler = new CPProfiler(solver, false)) {
             while (solver.solve()) ;
        } catch (IOException e) {
             e.printStackTrace();
         }
        int cpt = 0;
        while (solver.solve()) {
//            if (cpt == 50000) {
                solver.printShortStatistics();
//                cpt = 0;
//            }
//            cpt++;
        }
        solver.printStatistics();
    }

    public static void setStrategyT(Model m, BoolVar[][] X) {
        BoolVar[] varsorder = new BoolVar[288 * (NBROUNDS + 1)];
        int cvarsorder = 0;
        for (int r = NBROUNDS; r >= 0; r--) {
            varsorder[cvarsorder++] = X[r][1 - 1];
            varsorder[cvarsorder++] = X[r][94 - 1];
            varsorder[cvarsorder++] = X[r][178 - 1];
        }
        for (int r = NBROUNDS; r >= 0; r--) {
            for (int i : tt1) if (i != 94) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt2) if (i != 178) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt3) if (i != 1) varsorder[cvarsorder++] = X[r][i - 1];
        }
        for (int r = NBROUNDS; r >= 0; r--)
            for (int j = 0; j < 288; j++)
                if (!in(j + 1, tt1) && !in(j + 1, tt2) && !in(j + 1, tt3))
                    varsorder[cvarsorder++] = X[r][j];
        m.getSolver().setSearch(Search.inputOrderUBSearch(varsorder));
    }

    public static void setStrategy(Model m, BoolVar[][] X) {
        BoolVar[] varsorder = new BoolVar[288 * (NBROUNDS + 1)];
        int cvarsorder = 0;
        for (int r = NBROUNDS; r >= 0; r--) {
            for (int i : tt1) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt2) varsorder[cvarsorder++] = X[r][i - 1];
            for (int i : tt3) varsorder[cvarsorder++] = X[r][i - 1];
            for (int j = 0; j < 288; j++)
                if (!in(j + 1, tt1) && !in(j + 1, tt2) && !in(j + 1, tt3))
                    varsorder[cvarsorder++] = X[r][j];
        }
        m.getSolver().setSearch(Search.inputOrderLBSearch(varsorder));
    }

    public static void instanciateSol(Model m, BoolVar[][] X) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(solpath + "/sol.txt"), StandardCharsets.UTF_8)) {
            String line;
            int r = 0;
            while ((line = bufferedReader.readLine()) != null && r <= 840) {
//                print("line: " + line);
                for (int i = 0; i < line.length(); i++) {
                    assert X[r][i] != null : r + " " + i + " : " + X[r][i];
                    print(r + "  " + i + " = " + line.charAt(i) + " == " + X[r][i]);
                    X[r][i].instantiateTo(Integer.parseInt(String.valueOf(line.charAt(i))), Cause.Null);
                    m.getSolver().propagate();
                }
                r = r + 1;
            }
            for (BoolVar b : m.retrieveBoolVars()) {
                if (b.getDomainSize() != 1) {
                    print(b);
                }
            }
        } catch (IOException | ContradictionException ex) {
            System.out.format("I/O exception: ", ex);
        }
    }

    public static Tuples createpropagT() {
        Tuples tuples = new Tuples(true);
        tuples.add(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        tuples.add(1, 0, 0, 0, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 0, 0, 1, 0, 0, 0, 0, 1, 0);
        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 1, 0);

        tuples.add(1, 0, 0, 0, 1, 0, 0, 0, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 0, 0, 1, 1);
        tuples.add(0, 0, 0, 1, 1, 0, 0, 0, 1, 1);
        tuples.add(0, 0, 0, 0, 1, 0, 0, 0, 1, 1);

        tuples.add(0, 0, 1, 0, 0, 0, 0, 1, 0, 0);

        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 0, 1);

        tuples.add(1, 0, 1, 0, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 0, 1, 1, 0, 0, 0, 1, 1, 0);
        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 1, 0);

        tuples.add(1, 0, 1, 0, 1, 0, 0, 1, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 0, 1, 1, 1);
        tuples.add(0, 0, 1, 1, 1, 0, 0, 1, 1, 1);
        tuples.add(0, 0, 1, 0, 1, 0, 0, 1, 1, 1);

        tuples.add(0, 1, 0, 0, 0, 0, 1, 0, 0, 0);

        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 0, 1);

        tuples.add(1, 1, 0, 0, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 0, 1, 0, 0, 1, 0, 1, 0);
        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 1, 0);

        // page 2
        tuples.add(1, 1, 0, 0, 1, 0, 1, 0, 1, 1);
        tuples.add(0, 1, 0, 1, 1, 0, 1, 0, 1, 1);
        tuples.add(0, 1, 0, 0, 1, 0, 1, 0, 1, 1);
        tuples.add(0, 1, 1, 0, 1, 0, 1, 0, 1, 1);

        tuples.add(0, 1, 1, 0, 0, 0, 1, 1, 0, 0);

        tuples.add(0, 1, 1, 0, 1, 0, 1, 1, 0, 1);

        tuples.add(1, 1, 1, 0, 0, 0, 1, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 0, 0, 1, 1, 1, 0);
        tuples.add(0, 1, 1, 1, 0, 0, 1, 1, 1, 0);
        tuples.add(0, 1, 1, 0, 1, 0, 1, 1, 1, 0);

        tuples.add(1, 0, 0, 0, 0, 1, 0, 0, 0, 0);

        tuples.add(1, 0, 0, 0, 1, 1, 0, 0, 0, 1);

        tuples.add(1, 0, 0, 0, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 0, 0, 1, 0, 1, 0, 0, 1, 0);
        tuples.add(1, 0, 0, 0, 1, 1, 0, 0, 1, 0);

        //tuples.add(1,0,0,0,1,1,0,0,1,1);
        tuples.add(1, 1, 1, 0, 1, 1, 0, 0, 1, 1);
        tuples.add(1, 0, 0, 1, 1, 1, 0, 0, 1, 1);
        //tuples.add(1,0,0,0,1,1,0,0,1,1);

        tuples.add(1, 0, 1, 0, 0, 1, 0, 1, 0, 0);

        tuples.add(1, 0, 1, 0, 1, 1, 0, 1, 0, 1);

        tuples.add(1, 0, 1, 0, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 0, 1, 1, 0, 1, 0, 1, 1, 0);
        tuples.add(1, 0, 1, 0, 1, 1, 0, 1, 1, 0);

        // page 3

        // tuples.add(1,0,1,0,1,1,0,1,1,1);
        tuples.add(1, 1, 1, 0, 1, 1, 0, 1, 1, 1);
        tuples.add(1, 0, 1, 1, 1, 1, 0, 1, 1, 1);

        tuples.add(1, 1, 0, 0, 0, 1, 1, 0, 0, 0);

        tuples.add(1, 1, 0, 0, 1, 1, 1, 0, 0, 1);

        tuples.add(1, 1, 0, 0, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 1, 0, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 0, 1, 0, 1, 1, 0, 1, 0);
        tuples.add(1, 1, 0, 0, 1, 1, 1, 0, 1, 0);

        // tuples.add(1,1,0,0,1,1,1,0,1,1);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 0, 1, 1);
        tuples.add(1, 1, 0, 1, 1, 1, 1, 0, 1, 1);

        tuples.add(1, 1, 1, 0, 0, 1, 1, 1, 0, 0);

        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 0, 1);

        // tuples.add(1,1,1,0,0,1,1,1,1,0);
        tuples.add(1, 1, 1, 1, 0, 1, 1, 1, 1, 0);
        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 1, 0);

        tuples.add(1, 1, 1, 0, 1, 1, 1, 1, 1, 1);
        tuples.add(1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        return tuples;
    }

    public static boolean in(int a, int[] t) {
        for (int i : t)
            if (i == a)
                return true;
        return false;
    }

    public static void print(String a) {
        System.out.println(a);
    }

    public static void print(char a) {
        System.out.println(a);
    }

    public static void print(boolean a) {
        System.out.println(a);
    }

    public static void print(int a) {
        System.out.println(a);
    }

    public static void print(int[] a) {
        System.out.println(a);
    }

    public static void print(int[][] a) {
        System.out.println(a);
    }

    public static void print(BoolVar a) {
        System.out.println(a);
    }
}