#include <iostream>
#include <map>
#include <algorithm>
#include <execution>

using namespace std;

class Perm {
public:
    map<int, int> link;
    map<int, int> elink;
    map<int, int> invlink;
    map<int, int> invelink;
};

unsigned nbNum(int first, int last, int k, int nextV, int nextR, Perm & P, int r) {
    if (r == 0) {
        if (first == last) return 1;
        if (first >= 0 || last < 0) return 0;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return 0;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return 0;
        return 1;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1) + nbNum(first, it->second, k, nextV, nextR, P, r);
        }
    }
    unsigned res = 0;
    if (it_link_first != P.link.end()) res += nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1);
    else {
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                res += nbNum(i, last, k, nextV, nextR, P, r-1);
                P.link.erase(first);
                P.invlink.erase(i);
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            res += nbNum(nextV, last, k, nextV+1, nextR, P, r-1);
            P.link.erase(first);
            P.invlink.erase(nextV);
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            res += nbNum(nextR, last, k, nextV, nextR-1, P, r-1);
            P.link.erase(first);
            P.invlink.erase(nextR);
        }
    }
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) res += nbNum(it_elink_first->second, last, k, nextV, nextR, P, r);
        else {
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    res += nbNum(i, last, k, nextV, nextR, P, r);
                    P.elink.erase(first);
                    P.invelink.erase(i);
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                res += nbNum(nextV, last, k, nextV+1, nextR, P, r);
                P.elink.erase(first);
                P.invelink.erase(nextV);
            }
        }
    }
    return res;
}

unsigned nbNum(int first, int last, int k, int nextV, int nextR, Perm & P, int r, unsigned bound) {
    if (r == 0) {
        if (first == last) return 1;
        if (first >= 0 || last < 0) return 0;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return 0;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return 0;
        return 1;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1, bound);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1, bound);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            auto tmp1 = nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1, bound);
            if (tmp1 > bound) return tmp1;
            else return tmp1 + nbNum(first, it->second, k, nextV, nextR, P, r, bound-tmp1);
        }
    }
    unsigned res = 0;
    if (it_link_first != P.link.end()) {
        res += nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1, bound);
        if (res > bound) return res;
    }
    else {
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                res += nbNum(i, last, k, nextV, nextR, P, r-1, bound - res);
                P.link.erase(first);
                P.invlink.erase(i);
                if (res > bound) return res;
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            res += nbNum(nextV, last, k, nextV+1, nextR, P, r-1, bound - res);
            P.link.erase(first);
            P.invlink.erase(nextV);
            if (res > bound) return res;
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            res += nbNum(nextR, last, k, nextV, nextR-1, P, r-1, bound - res);
            P.link.erase(first);
            P.invlink.erase(nextR);
            if (res > bound) return res;
        }
    }
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) res += nbNum(it_elink_first->second, last, k, nextV, nextR, P, r, bound - res);
        else {
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    res += nbNum(i, last, k, nextV, nextR, P, r, bound - res);
                    P.elink.erase(first);
                    P.invelink.erase(i);
                    if (res > bound) return res;
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                res += nbNum(nextV, last, k, nextV+1, nextR, P, r, bound - res);
                P.elink.erase(first);
                P.invelink.erase(nextV);
                if (res > bound) return res;
            }
        }
    }
    return res;
}

bool hasPath(int first, int last, Perm & P, int r) {
    if (r == 0) {
        if (first == last) return true;
        if (first >= 0 || last < 0) return false;
        auto it = P.elink.find(first);
        return (it != P.elink.end() && it->second == last);
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return hasPath(it_link_first->second,last, P, r-1);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return hasPath(first, it_link_last->second, P, r-1);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            return hasPath(first, it_link_last->second, P, r-1) || hasPath(first, it->second, P, r);
        }
    }
    if (it_link_first != P.link.end() && hasPath(it_link_first->second, last, P, r-1)) return true;
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end() && hasPath(it_elink_first->second, last, P, r)) return true;
    }
    return false;
}

void allPerm(int first, int last, int k, int nextV, int nextR, Perm & P, int r, vector<Perm> & res) {
    if (r == 0) {
        if (first == last) {res.emplace_back(P); return;}
        if (first >= 0 || last < 0) return;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return;
        if (it != P.elink.end()) res.emplace_back(P);
        else {
            P.elink[first] = last;
            P.invelink[last] = first;
            res.emplace_back(P);
            P.elink.erase(first);
            P.invelink.erase(last);
        }
        return;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return allPerm(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return allPerm(first, it_link_last->second, k, nextV, nextR, P, r-1, res);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            allPerm(first, it_link_last->second, k, nextV, nextR, P, r-1, res);
            allPerm(first, it->second, k, nextV, nextR, P, r, res);
            return;
        }
    }
    if (it_link_first != P.link.end()) allPerm(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    else {
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                allPerm(i, last, k, nextV, nextR, P, r-1, res);
                P.link.erase(first);
                P.invlink.erase(i);
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            allPerm(nextV, last, k, nextV+1, nextR, P, r-1, res);
            P.link.erase(first);
            P.invlink.erase(nextV);
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            allPerm(nextR, last, k, nextV, nextR-1, P, r-1, res);
            P.link.erase(first);
            P.invlink.erase(nextR);
        }
    }
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) allPerm(it_elink_first->second, last, k, nextV, nextR, P, r, res);
        else {
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    allPerm(i, last, k, nextV, nextR, P, r, res);
                    P.elink.erase(first);
                    P.invelink.erase(i);
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                allPerm(nextV, last, k, nextV+1, nextR, P, r, res);
                P.elink.erase(first);
                P.invelink.erase(nextV);
            }
        }
    }
}

void findPerm(Perm P, int r, int k) {
    unsigned res = ~0;
    auto nextV = max(P.link.rbegin()->first, P.invlink.rbegin()->first);
    if (!P.invelink.empty()) nextV = max(nextV, P.invelink.rbegin()->first);
    nextV += 1;
    auto nextR = min(P.link.begin()->first, P.invlink.begin()->first);
    if (!P.elink.empty()) nextR = min(nextR, P.elink.begin()->first);
    nextR -= 1;
    int p1 = -1;
    int p2 = 0;
    int rr = 0;
    while (res == ~0) {
        for (int i1 = 0; i1 < nextV; ++i1) {
            for (int i2 = -1; i2 > nextR; --i2) {
                if (!hasPath(i1, i2, P, r-1)) {
                    auto tmp = nbNum(i1, i2, k, nextV, nextR, P, r-1, res);
                    if (tmp < res) {
                        res = tmp;
                        p1 = i1;
                        p2 = i2;
                        rr = r-1;
                    }
                }
                if (!hasPath(i1, i2, P, r)) {
                    auto tmp = nbNum(i1, i2, k, nextV, nextR, P, r, res);
                    if (tmp < res) {
                        res = tmp;
                        p1 = i1;
                        p2 = i2;
                        rr = r;
                    }
                }
            }
        }
        if (res == ~0) {
            for (int i1 = -1; i1 > nextR; --i1) {
                for (int i2 = -1; i2 > nextR; --i2) {
                    if (!hasPath(i1, i2, P, r-1)) {
                        auto tmp = nbNum(i1, i2, k, nextV, nextR, P, r-1, res);
                        if (tmp < res) {
                            res = tmp;
                            p1 = i1;
                            p2 = i2;
                            rr = r-1;
                        }
                    }
                    if (!hasPath(i1, i2, P, r)) {
                        auto tmp = nbNum(i1, i2, k, nextV, nextR, P, r, res);
                        if (tmp < res) {
                            res = tmp;
                            p1 = i1;
                            p2 = i2;
                            rr = r;
                        }
                    }
                }
            }
        }
        if (res == ~0) {
            if (nextR >= -k) nextR -= 1;
            else if (nextV < k) nextV += 1;
            else {cout << "Yeah!!!" << endl; getchar();}
        }
    }
    if (res > 0) {
        vector<Perm> vPerm;
        vPerm.reserve(res);

        // cout << " ------ " << endl;
        // for (auto const & p : P.link) cout << p.first << " --> " << p.second << endl;
        // cout << " --- " << endl;
        // for (auto const & p : P.elink) cout << p.first << " --> " << p.second << endl;
        // cout << endl;
        // getchar();
        allPerm(p1, p2, k, nextV, nextR, P, rr, vPerm);
        //cout << p1 << " " << p2 << " " << vPerm.size() << endl;
        for_each(execution::par, vPerm.begin(), vPerm.end(), [r,k](auto & Q){
            findPerm(move(Q), r, k);
        });
    }
}


int main(int argc, const char *argv[])
{
    int r = stoi(argv[1]);
    int k = stoi(argv[2]);
    {
        Perm P;

        P.link[0] = 1;
        P.invlink[1] = 0;
        //P.link[1] = 2;
        //P.invlink[2] = 1;
        P.link[1] = -1;
        P.invlink[-1] = 1;

        findPerm(P, r, k);
    }

        return 0;
}
