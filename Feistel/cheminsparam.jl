gcpt = 0

k = 8
R = 8
mandatorysbox = 1
a = 0 #coefficient de nonevenoditer
nbsol = 1#000000000


function makeway(deb, fin, perm, epsi, newv, newr,
                 tperm, tepsi, trr, trv, tvr, tvv,
                 untested, euntested)#copy de untested ?
        #untested = copy(untested)
        #euntested = copy(euntested)
        #untested = ones(Bool,2*k,2*k)
        #euntested = ones(Bool,k,k)
        #global gcpt +=1
        #cpt = gcpt
        #println(cpt," ",deb,' ',fin,' ',perm,' ',epsi,' ',newv,' ',newr,' ',trr, trv, tvr, tvv,' ',tperm)
        if tperm == R-1
                if deb == fin && tepsi >= mandatorysbox && length(pool) < nbsol
                        isperm = false
                        return witchway(perm, epsi, newv, newr, trr, trv, tvr, tvv, untested, euntested)
                else    return false end
        else
                isway = false
                if perm[deb] > 0
                        isway |= makeway(perm[deb], fin, perm, epsi, newv, newr,
                                         tperm + 1, tepsi, trr, trv, tvr, tvv,
                                         untested, euntested)
                elseif deb > k #deb is red
                        if trr < a
                                for oldr in 1:newr - 1 if untested[deb,oldr + k] && !((oldr + k) in perm)
                                        perm[deb] = oldr + k
                                        if trr > 0 || perm[k+1] > k+1
                                                isway |= makeway(perm[deb], fin, perm, epsi, newv, newr,
                                                                 tperm + 1, tepsi, trr + 1, trv , tvr, tvv,
                                                                 untested, euntested)
                                                #untested[deb,oldr + k] = false
                                        end end end
                                if newr <= k
                                        perm[deb] = newr + k
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv, newr + 1,
                                                         tperm + 1, tepsi, trr + 1, trv, tvr, tvv,
                                                         untested, euntested)
                                end end
                        perm[deb] = 0
                        if trv < k - a && (a == 0 || deb != k+1)
                                for oldv in 1:newv - 1 if untested[deb,oldv] && !(oldv in perm) && (a == 0 || oldv != 1)
                                        perm[deb] = oldv
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv, newr,
                                                         tperm + 1, tepsi, trr, trv + 1, tvr, tvv,
                                                         untested, euntested)
                                        #untested[deb,oldv] = false
                                end end 
                                if newv <= k
                                        perm[deb] = newv
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv + 1, newr,
                                                         tperm + 1, tepsi, trr, trv + 1, tvr, tvv,
                                                         untested, euntested)
                                end end
                        perm[deb] = 0
                else #deb is green
                        if tvv < a
                                for oldv in 1:newv - 1 if untested[deb,oldv] && !((oldv) in perm)
                                        perm[deb] = oldv
                                        if trr > 0 ||  (1 in perm[1:k])
                                                isway |= makeway(perm[deb], fin, perm, epsi, newv, newr,
                                                                 tperm + 1, tepsi, trr, trv , tvr, tvv + 1,
                                                                 untested, euntested)
                                                #untested[deb,oldv] = false
                                        end end end
                                if newv <= k
                                        perm[deb] = newv
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv + 1, newr,
                                                         tperm + 1, tepsi, trr, trv, tvr, tvv + 1,
                                                         untested, euntested)
                                end end
                        perm[deb] = 0
                        if tvr < k - a
                                for oldr in 1:newr - 1 if untested[deb,oldr + k] && !((oldr + k) in perm)
                                        perm[deb] = oldr + k
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv, newr,
                                                         tperm + 1, tepsi, trr, trv, tvr + 1, tvv,
                                                         untested, euntested)
                                        #untested[deb,oldr + k] = false
                                end end 
                                if newr <= k
                                        perm[deb] = newr + k
                                        isway |= makeway(perm[deb], fin, perm, epsi, newv, newr + 1,
                                                         tperm + 1, tepsi, trr, trv, tvr + 1, tvv,
                                                         untested, euntested)
                                end end
                        perm[deb] = 0
                end #epsilon cases
                if deb > k
                        if epsi[deb - k] > 0
                                isway |= makeway(epsi[deb - k], fin, perm, epsi, newv, newr,
                                                 tperm, tepsi + 1, trr, trv, tvr, tvv,
                                                 untested, euntested)
                        else
                                for oldv in 1:newv - 1 if euntested[deb - k,oldv] && !(oldv in epsi)
                                        epsi[deb - k] = oldv
                                        isway |= makeway(epsi[deb - k], fin, perm, epsi, newv, newr,
                                                         tperm, tepsi + 1, trr, trv, tvr, tvv,
                                                         untested, euntested)
                                        #euntested[deb - k,oldv] = false
                                end end
                                if newv <= k
                                        epsi[deb - k] = newv
                                        isway |= makeway(epsi[deb - k], fin, perm, epsi, newv + 1, newr,
                                                         tperm, tepsi + 1, trr, trv, tvr, tvv,
                                                         untested, euntested)
                                end
                                epsi[deb - k] = 0
                        end end
                return isway
        end
end

function countway(deb, fin, perm, epsi, newv, newr,
                  tperm, tepsi, trr, trv, tvr, tvv)
        if tperm == R-1
                if deb == fin && tepsi >= mandatorysbox
                        return 1
                else    return 0 end
        else
                nbway = false
                if perm[deb] > 0
                        nbway += countway(perm[deb], fin, perm, epsi, newv, newr,
                                          tperm + 1, tepsi, trr, trv , tvr, tvv)
                elseif deb > k #deb is red
                        if trr < a
                                for oldr in 1:newr - 1 if !((oldr + k) in perm)
                                        perm[deb] = oldr + k
                                        if trr > 0 || perm[k+1] > k+1
                                                nbway += countway(perm[deb], fin, perm, epsi, newv, newr,
                                                                  tperm + 1, tepsi, trr + 1, trv , tvr, tvv)
                                        end end end
                                if newr <= k
                                        perm[deb] = newr + k
                                        nbway += countway(perm[deb], fin, perm, epsi, newv, newr + 1,
                                                          tperm + 1, tepsi, trr + 1, trv, tvr, tvv)
                                end end
                        perm[deb] = 0
                        if trv < k - a && (a == 0 || deb != k+1)
                                for oldv in 1:newv - 1 if !(oldv in perm) && (a == 0 || oldv != 1)
                                        perm[deb] = oldv
                                        nbway += countway(perm[deb], fin, perm, epsi, newv, newr,
                                                          tperm + 1, tepsi, trr, trv + 1, tvr, tvv)
                                end end 
                                if newv <= k
                                        perm[deb] = newv
                                        nbway += countway(perm[deb], fin, perm, epsi, newv + 1, newr,
                                                          tperm + 1, tepsi, trr, trv + 1, tvr, tvv)
                                end end
                        perm[deb] = 0
                else #deb is green
                        if tvv < a
                                for oldv in 1:newv - 1 if !((oldv) in perm)
                                        perm[deb] = oldv
                                        if trr > 0 ||  (1 in perm[1:k])
                                                nbway += countway(perm[deb], fin, perm, epsi, newv, newr,
                                                                  tperm + 1, tepsi, trr, trv , tvr, tvv + 1)
                                        end end end
                                if newv <= k
                                        perm[deb] = newv
                                        nbway += countway(perm[deb], fin, perm, epsi, newv + 1, newr,
                                                          tperm + 1, tepsi, trr, trv, tvr, tvv + 1)
                                end end
                        perm[deb] = 0
                        if tvr < k - a
                                for oldr in 1:newr - 1 if !((oldr + k) in perm)
                                        perm[deb] = oldr + k
                                        nbway += countway(perm[deb], fin, perm, epsi, newv, newr,
                                                          tperm + 1, tepsi, trr, trv, tvr + 1, tvv)
                                end end 
                                if newr <= k
                                        perm[deb] = newr + k
                                        nbway += countway(perm[deb], fin, perm, epsi, newv, newr + 1,
                                                          tperm + 1, tepsi, trr, trv, tvr + 1, tvv)
                                end end
                        perm[deb] = 0
                end #epsilon cases
                if deb > k
                        if epsi[deb - k] > 0
                                nbway += countway(epsi[deb - k], fin, perm, epsi, newv, newr,
                                                  tperm, tepsi + 1, trr, trv, tvr, tvv)
                        else
                                for oldv in 1:newv - 1 if !(oldv in epsi)
                                        epsi[deb - k] = oldv
                                        nbway += countway(epsi[deb - k], fin, perm, epsi, newv, newr,
                                                          tperm, tepsi + 1, trr, trv, tvr, tvv)
                                end end
                                if newv <= k
                                        epsi[deb - k] = newv
                                        nbway += countway(epsi[deb - k], fin, perm, epsi, newv + 1, newr,
                                                          tperm, tepsi + 1, trr, trv, tvr, tvv)
                                end
                                epsi[deb - k] = 0
                        end
                end
                return nbway
        end
end

function haspath(deb,fin,tperm,tepsi,perm,epsi)
        if deb == 0
                return false
        elseif tperm == 0
                return deb == fin && tepsi >= mandatorysbox
        elseif haspath(perm[deb],fin,tperm-1,tepsi,perm,epsi)
                return true
        elseif deb > k
                return haspath(epsi[deb-k],fin,tperm,tepsi+1,perm,epsi)
        else return false end
end

function witchway(perm, epsi, newv, newr, trr, trv, tvr, tvv, untested, euntested)
        newdeb = newfin = tmp = 0
        minch = 2048
        if a > 0
                if !haspath(1,k + 1,R - 3,0,perm,epsi) return makeway(1,k + 1,perm, epsi, newv, newr,
                                                          2, 0, trr, trv, tvr, tvv,
                                                          untested, euntested) end
                if !haspath(1,k + 1,R - 2,0,perm,epsi) return makeway(1,k + 1,perm, epsi, newv, newr,
                                                          1, 0, trr, trv, tvr, tvv,
                                                          untested, euntested) end
                for RR in R - 3:R - 1
                        for vvdeb in 1:k if vvdeb in perm[1:k]
                                for finrr in k + 1:2*k if perm[finrr] > k
                                        if !haspath(vvdeb,finrr,RR,0,perm,epsi)
                                                tmp = countway(vvdeb,finrr, perm, epsi, newv, newr,
                                                               R-1 - RR, 0, trr, trv, tvr, tvv)
                                                if minch > tmp
                                                        minch = tmp
                                                        newdeb = vvdeb
                                                        newfin = finrr
                                                end end end end end end
                        if newdeb > 0 return makeway(newdeb, newfin, perm, epsi, newv, newr,
                                                  R-1 - RR, 0, trr, trv, tvr, tvv,
                                                  untested, euntested) end end
                for RR in R - 2:R - 1
                        for vvdeb in 1:k if vvdeb in perm[1:k]
                                for finrv in k + 1:2*k if perm[finrv] <= k
                                        if !haspath(vvdeb,finrv,RR,0,perm,epsi)
                                                tmp = countway(vvdeb,finrv, perm, epsi, newv, newr,
                                                               R-1 - RR, 0, trr, trv, tvr, tvv)
                                                if minch > tmp
                                                        minch = tmp
                                                        newdeb = vvdeb
                                                        newfin = finrv
                                                end end end end end end
                        if newdeb > 0 return makeway(newdeb, newfin, perm, epsi, newv, newr,
                                                  R-1 - RR, 0, trr, trv, tvr, tvv,
                                                  untested, euntested) end
                        for rvdeb in 1:k if !(rvdeb in perm[1:k])
                                for finrr in k + 1:2*k if perm[finrr] > k
                                        if !haspath(rvdeb,finrr,RR,0,perm,epsi)
                                                tmp = countway(rvdeb,finrr, perm, epsi, newv, newr,
                                                               R-1 - RR, 0, trr, trv, tvr, tvv)
                                                if minch > tmp
                                                        minch = tmp
                                                        newdeb = rvdeb
                                                        newfin = finrr
                                                end end end end end end
                        if newdeb > 0 return makeway(newdeb, newfin, perm, epsi, newv, newr,
                                                  R-1 - RR, 0, trr, trv, tvr, tvv,
                                                  untested, euntested) end end end
        for rvdeb in 1:k
                for finrv in  (k + 1):2*k
                        if !haspath(rvdeb, finrv, R-1,0, perm, epsi)
                                tmp = countway(rvdeb, finrv, perm, epsi, newv, newr,
                                               0, 0, trr, trv, tvr, tvv)
                                if minch > tmp
                                        minch = tmp
                                        newdeb = rvdeb
                                        newfin = finrv
                                end end end end
        if newdeb > 0 return makeway(newdeb, newfin, perm, epsi, newv, newr,
                                  0, 0, trr, trv, tvr, tvv,
                                  untested, euntested)
        else
                push!(pool,(copy(perm),copy(epsi)))
                return true
        end
end

function printglobal()
        println(" -   -   -   -   -   -   -   -   - ")
        println("k=",k)
        println("R=",R)
        println("a=",a)
        println("msb=",mandatorysbox)
        println("nbsol=",nbsol)
        println("pool=",if length(pool)<10 pool else length(pool) end)
        println("   -   -   -   -   -   -   -   -   ")
end

function printmat(m)
        space = ' '
        if findmax(m[:,i])[1]>9
                space = "  "
        end
        for i in 1:size(m)[1]
                for j in 1:size(m)[2]
                        if m[i,j]==0
                                print(space,'.')
                        elseif m[i,j]>9
                                print(' ',m[i,j])
                        else
                                print(space,m[i,j])
                        end
                end
                println()
        end
        println()
end

function s1(p)
        for i in 1:length(p)
                if i<length(p)
                        print(string(i,'/',p[i],"/,"))
                else
                        print(string(i,'/',p[i],"/"))
                end
        end
        println()
end

function s2(p)
        pr = "\\node[noeudrou] ("
        pv = "\\node[noeudver] ("
        cords = [") at (6,14)  {",
                 ") at (8,14)  {",
                 ") at (11,13)  {",
                 ") at (13,11)  {",
                 ") at (14,8)  {",
                 ") at (14,6)  {",
                 ") at (13,3)  {",
                 ") at (11,1)  {",
                 ") at (8,0)  {",
                 ") at (6,0)  {",
                 ") at (3,1)  {",
                 ") at (1,3)  {",
                 ") at (0,6)  {",
                 ") at (0,8)  {",
                 ") at (1,11)  {",
                 ") at (3,13)  {"]
        post = "};"
        i = 1
        ic = 1
        tabou = []
        while length(tabou)<length(p)
                if i in tabou
                        i = i+1
                else
                        if i>k
                                println(string(pr,i,cords[ic],i,post))
                        else
                                println(string(pv,i,cords[ic],i,post))
                        end
                        push!(tabou,i)
                        i=p[i]
                        ic = ic+1
                end
        end
end

function grap(s)
        p = s[1]
        ep = s[2]
        println("\\begin{tikzpicture}[scale=0.5,auto=left]")
        s2(p)
        println("\\foreach \\from/\\to/\\arr in {")
        s1(p)
        println(string("}\\draw[tv] (\\from) -- (\\to)node[sloped,above,legende]{\\arr};
\\foreach \\from/\\to/\\arr in {"))
        for i in 1:k-1
                print(i+k,'/',ep[i],"/,")
        end
        print(2*k,'/',ep[k],"/")
        println(string("}\\draw[tb] (\\from) to[] (\\to)node[sloped,above,legende]{\\arr};
\\end{tikzpicture}"))
        println()
end

function grapj(s)
        p = s[1]
        ep = s[2]
        k = length(ep)
        println("\\begin{tikzpicture}[scale=0.4,auto=left]")
        global res
        print(string("\\node[whiteFill] (0) at (3,14)  {"))
        for i in 1:5
                print(res[i],' ')
        end
        println("};")
        pj = "\\node[noeudjau] ("
        cords = [") at (7,14)  {",
                 ") at (12,12)  {",
                 ") at (14,7)  {",
                 ") at (12,2)  {",
                 ") at (7,0)  {",
                 ") at (2,2)  {",
                 ") at (0,7)  {",
                 ") at (2,12)  {"]
        post = "};"
        i = 1
        ic = 1
        tabou = []
        while length(tabou)<k
                if i in tabou
                        i = i+1
                else
                        println(string(pj,i,cords[ic],i,post))
                        push!(tabou,i)
                        i=ep[p[i]-k]
                        ic = ic+1
                end
        end
        for i in 1:k
                if i==ep[p[i]-k]
                        println(string("\\draw (",i,") edge [loop left,blue,densely dotted] node {} ();"))
                end
        end
        print("\\foreach \\from/\\to/\\arr in {")

        s = ""
        for i in 1:length(ep)
                if i!=ep[p[i]-k]
                        s = s*string(i,'/',ep[p[i]-k],"/,")
                end
        end
        println(s[1:end-1])
        println(string("}\\draw[tb] (\\from) to[] (\\to)node[sloped,above,legende]{\\arr};"))


        for i in 1:k
                if i==p[p[i]]
                        println(string("\\draw (",i,") edge [loop right] node {} ();"))
                end
        end
        print("\\foreach \\from/\\to/\\arr in {")
        s = ""
        for i in 1:length(ep)
                if i!=p[p[i]]
                        s = s*string(i,'/',p[p[i]],"/,")
                end
        end
        println(s[1:end-1])
        println(string("}\\draw[tv] (\\from) -- (\\to)node[sloped,above,legende]{\\arr};\n\\end{tikzpicture}"))
end


function fibo(n,a,b)
        if n == 0 return 0
        elseif n == 1 return b
        else return fibo(n-1,b,a+b) end
end
function fibo(n)
        return fibo(n,0,1)
        #if n<0 return -99999999
        #elseif n==0 return 0
        #elseif n<3 return 1
        #else return fibo(n-1)+fibo(n-2) end
end

function analysemat(pool,verbose,verbosegrap,verboseinv)
        max = 0
        min = 999
        for sol in pool
                p = sol[1]
                ep = sol[2]
                if 0 in p || 0 in ep
                        push!(brokenpool,sol)
                end
                mat = zeros(Int8,2*k,2*k)
                for i in 1:2*k if p[i]>0
                        mat[i,p[i]] = 1
                end end
                for i in 1:k if ep[i]>0
                        mat[i + k,:] = mat[i + k,:] + mat[ep[i],:]
                end end
                matrm1 = mat^(R-1)
                if verbose print("SUM OF PATHS (from green to red nodes): ") end
                s = [0 for i in 1:k]
                for i in 1:k
                        s[i] = sum(matrm1[i,k+1:2*k])
                        if verbose if false&&s[i]<10 print(s[i],"  + ") else print(s[i]," + ") end end
                end
                if verbose println(". = ",sum(s)) end
                if verbose #&& sum(s)>104
                        println(sol)
                        printmat(mat)
                        printmat(matrm1)
                        printmat(mat^R)
                end
                if verbosegrap && sum(s)>104
                        if 0 in sol[1] || 0 in sol[2]
                                println(sol)
                        else
                                println("sum=",s,"=",sum(s),"\\\\")
                                grap(sol)
                        end
                end
                if sum(s) > k*fibo(R-1) && length(findall(x-> x < fibo(R-1),s))==0
                        println("CEST LA FEISTE")
                        println(sol)
                        printmat(mat)
                        printmat(matrm1)
                        printmat(mat^R)
                        println("sum=",s,"=",sum(s),"\\\\")
                end
                if verboseinv && sum(s) > k*fibo(R-1)
                        invmat = zeros(Int8,2*k,2*k)
                        for i in 1:2*k if p[i]>0
                                invmat[p[i],i] = 1
                        end end
                        for i in 1:k if ep[i]>0
                                invmat[i + k,:] = invmat[i + k,:] + invmat[ep[i],:]
                        end end
                        invmatrm1 = invmat^(R-1)
                        
                        sr = [0 for i in 1:k]
                        positif = true
                        for i in 1:k
                                if 0 in invmatrm1[1:k,i+k] positif = false end
                                sr[i] = sum(invmatrm1[1:k,i+k])
                        end
                        if sum(sr) >  k*fibo(R-1)
                                println("k=",k," R=",R," msb=",mandatorysbox," a=",a," fibosum=",k*fibo(R-1))
                                println(sol)
                                println(positif)
                                println("sum =",s,"=",sum(s),"\\\\")
                                println("sumr=",sr,"=",sum(sr),"\\\\")
                        end
                end
                if sum(s)>max max = sum(s) end
                if sum(s)<min min = sum(s) end
        end
        return [min,max]
end

function printtabular(kk,RR)
        global k = kk
        global R = RR
        amax = k - 1
        msbmax = (R+1) ÷ 2
        print("\\begin{tabular}{")
        for i in 0:amax print("|c") end
        println("|c|}")
        println("\\hline")
        print("k=",k," R=",R)
        for aa in 0:amax print(" & a=",aa) end
        println("\\\\\\hline")
        for msb in 0:msbmax
                global mandatorysbox = msb
                print("msb=",msb," & ")
                for aa in 0:amax
                        global pool = []
                        global a = aa
                        #res = makeway(1, k+1, zeros(Int8,2*k), zeros(Int8,k), 2, 2,
                        #              0, 0, 0, 0, 0, 0,
                        #              ones(Bool,2*k,2*k), ones(Bool,k,k))
                        pool = getpool(kk,RR,msb,a)
                        minmax = analysemat(pool,false,false,false)
                        if minmax[2]>0 print(length(pool),"(",minmax[1],"/",minmax[2],")")
                        else print("no") end
                        if aa < amax print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")
end

function printalltabular(kmax,rmax)
        for k in 2:kmax
                for R in 4:rmax
                        printtabular(k,R)
                end
        end
end

function makeperm()
        res = makeway(1, k+1, zeros(Int8,2*k), zeros(Int8,k), 2, 2,
                      0, 0, 0, 0, 0, 0,
                      ones(Bool,2*k,2*k), ones(Bool,k,k))
        #minmax = analysemat(pool,false,true,false)
end

function getpool(kk,RR,msb,a)
        return bigpool[kk-1][RR-3][msb+1][a+1]
end
function setpool(kk,RR,msb,a,pool)
        bigpool[kk-1][RR-3][msb+1][a+1] = pool
end

function searchall(kmax,rmax)
        global nbsol = 1000000000
        for kk in 2:kmax
                for RR in 4:rmax
                        global k = kk
                        global R = RR
                        println(k,R)
                        amax = k - 1
                        msbmax = (R+1) ÷ 2
                        for msb in 0:msbmax
                                global mandatorysbox = msb
                                for aa in 0:amax
                                        global pool = []
                                        global a = aa
                                        res = makeway(1, k+1, zeros(Int8,2*k), zeros(Int8,k), 2, 2,
                                                      0, 0, 0, 0, 0, 0,
                                                      ones(Bool,2*k,2*k), ones(Bool,k,k))
                                        #minmax = analysemat(pool,false,false)
                                        setpool(kk,RR,msb,a,pool)
                                end
                        end
                end
        end
end

function mainsearchall()
        k = 8
        R = 8
        mandatorysbox = 0
        a = 0 #coefficient de nonevenoditer
        nbsol = 1000000000
        pool = []
        brokenpool = []
        amax = k - 1
        msbmax = (R+1) ÷ 2
        kmax = k
        rmax = R
        #bigpool = [[[[[] for a in 0:amax] for msb in 0:msbmax] for RR in 4:rmax] for kk in 2:kmax]
        #searchall(k,R)
        #makeperm()
        #printalltabular(8,8)
end

sumpool = 0
for kk in 2:kmax
        for RR in 4:rmax
                global k = kk
                global R = RR
                #println(k,R)
                amax = k - 1
                msbmax = (R+1) ÷ 2
                for msb in 0:msbmax
                        global mandatorysbox = msb
                        for aa in 0:amax
                                global pool = []
                                global a = aa
                                pool = getpool(kk,RR,msb,a)
                                sumpool = sumpool + length(pool)
                                #minmax = analysemat(pool,false,false,true)
                        end
                end
        end
end

function f(r,m)
        if m == 0 return fibo(r)
        elseif r <= 0 return 0
        else return f(r-1,m-1)+f(r-2,m)
        end
end

function printtabularbound(rmax)
        print("\\begin{tabular}{")
        for i in 4:rmax print("|c") end
        println("|c|}")
        println("\\hline")
        for r in 4:rmax print(" & R=",r) end
        println("\\\\\\hline")
        for msb in 0:(rmax+1) ÷ 2
                print("msb=",msb," & ")
                for r in 4:rmax
                        res = f(r,msb)
                        print(res)
                        if r < rmax print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")
end

#printtabularbound(20)

#Even odd and msb
function searchmsb(kmax,rmax,msbmax)
        global nbsol = 1
        global a = 0
        for msb in 0:msbmax
                global mandatorysbox = msb
                for kk in 2:kmax
                        global k = kk
                        global R = 4
                        global pool = []
                        while R < rmax && length(pool) == 0
                                println(k,R)
                                res = makeway(1, k+1, zeros(Int8,2*k), zeros(Int8,k), 2, 2,
                                              0, 0, 0, 0, 0, 0,
                                              ones(Bool,2*k,2*k), ones(Bool,k,k))
                                if length(pool) > 0
                                        msbpool[msb+1][k] = [copy(pool[1][1]),copy(pool[1][2]),R]
                                end
                                global R = R + 1
                        end
                end
        end
end

function printtabularmsb(kmax,rmax,msbmax)
        print("\\begin{tabular}{")
        for i in 2:kmax print("|c") end
        println("|c|}")
        println("\\hline")
        for r in 2:kmax print(" & k=",r) end
        println("\\\\\\hline")
        for msb in 0:msbmax
                print("msb=",msb," & ")
                for kk in 2:kmax
                        pool = msbpool[msb+1][kk]
                        if length(pool)>0
                                print("R=",pool[3]) end
                        if kk < kmax print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")        
end

function mainsearchmsb()
        kmax = 8
        rmax = 12
        msbmax = 6
        #msbpool = [[[] for i in 1:kmax] for j in 0:msbmax]
        #searchmsb(kmax,rmax,msbmax)

        printtabularmsb(kmax,rmax,msbmax)
end

function stephtree(couleur, x, t,verbose)
        global a
        global k
        if couleur == 'R' nbr[R-t+1] = nbr[R-t+1]+x
        end
        if t==0
                if verbose println(couleur,' ',x) end
        else
                if verbose
                        if x<10 print(couleur,' ',x,"  --- ")
                        else print(couleur,' ',x," --- ")
                        end end
                nr = 0
                nv = 0
                if couleur == 'V'
                        nr = Base.min(k-a,x)#pas forcement le pire ?
                        nv = x-nr
                else
                        nr = Base.min(k,2*x,x+a)
                        nv = 2*x-nr
                end
                stephtree('R', nr, t-1,verbose)
                if nv>0 && t>1
                        if verbose 
                                [print("         ") for i in 0:R-t]
                                println('|')
                                [print("         ") for i in 0:R-t]
                        end
                        stephtree('V', nv, t-1,verbose)
                end  
        end
end

function mainsteph()
        global a = 0
        global k = 16
        global R = 10

        for aa in 1:8
                global a = aa
                global nbr = [0 for i in 0:R]
                stephtree('R',a,R,false)
                println("a=",a)
                res = vcat(vcat([0,0],nbr)',[fibo(i)*a for i in 0:R+2]')
                show(stdout,"text/plain",res)
                println()
        end
end

function transformpermut(p)
        k = length(p) ÷ 2
        p = p.+1
        p = vcat([i for i in 1:2k]',p)
        #show(stdout,"text/plain",p)
        #println()
        ep = [i for i in 1:k]
        for i in 1:2k
                for l in 1:2
                        if p[l,i]%2==0 # rouge ou vert ca depend du sens du schema TWINE est inverse
                                p[l,i] = p[l,i] ÷ 2
                        else
                                p[l,i] = p[l,i] ÷ 2 + k + 1
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res,ep
end

function onlypcole(p,ep)
        k = length(ep)
        p = vcat([i for i in 1:2k]',p')
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#ordonne les p selon ep identite
                for l in 1:2
                        if p[l,i] > k
                                p[l,i] = ep[p[l,i]-k]+k
                        end
                end
        end
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#met les verts et rouges ensemble
                for l in 1:2
                        if p[l,i]>k #rouges sur les pairs 2:2k
                                p[l,i] = 2(p[l,i] - k)
                        else #verts sur les impairs 1:2k+1
                                p[l,i] = 2(p[l,i]) - 1 
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res
end

function maxmsb(deb,fin,tperm,tepsi,perm,epsi)
        if deb == 0
                return 0
        elseif tperm == 0
                if deb == fin return tepsi
                else return 0 end
        elseif deb > k
                return Base.max(maxmsb(perm[deb],fin,tperm-1,tepsi,perm,epsi),
                                maxmsb(epsi[deb-k],fin,tperm,tepsi+1,perm,epsi))
        else    return maxmsb(perm[deb],fin,tperm-1,tepsi,perm,epsi) end
end

function testsolmsb(p,ep)
        global mandatorysbox
        tmp = mandatorysbox
        mandatorysbox = 0
        hp = [[haspath(deb,fin,R-1,0,p,ep) for fin in k+1:2k] for deb in 1:k]
        mandatorysbox = tmp        
        if true in [false in hp[i] for i in 1:k]
                println("NO Path with R=",R)
                return [[-1 for fin in k+1:2k] for deb in 1:k]
        else
                return [[maxmsb(deb,fin,R-1,0,p,ep) for fin in k+1:2k] for deb in 1:k]
        end
end

function printmaxsb(m)
        k = length(m) 
        print("\\begin{tabular}{")
        for i in 1:k print("|c") end
        println("|c|}")
        println("\\hline")
        print("R = ",R)
        for r in 1:k print(" & R",r) end
        println("\\\\\\hline")
        for k1 in 1:k
                print("V",k1," & ")
                for k2 in 1:k
                        print(m[k1][k2])
                        if k2 < k print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")  
end
function printmaxsbmat(m)
        k = size(m,1) 
        print("\\begin{tabular}{")
        for i in 1:k print("|c") end
        println("|c|}")
        println("\\hline")
        print("R = ",R)
        for r in 1:k print(" & R",r) end
        println("\\\\\\hline")
        for k1 in 1:k
                print("V",k1," & ")
                for k2 in 1:k
                        print(m[k1,k2])
                        if k2 < k print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")  
end

function comparemsbmat(p,ep)
        global k = length(p) ÷ 2
        global R = 8
        #analysemat([(p,ep)],true,false,false)
        pool = zeros(Int8,k,k,24)
        for r in 8:24
                R = r
                m = testsolmsb(p,ep)
                #printmaxsb(m)
                for i in 1:k
                        pool[i,:,R] = copy(m[i])
                end
        end
        grap((p,ep))

        for i in 8:8
                for j in i+1:24
                        global R = string(j," - ",i," = ",j-i)
                        diff = pool[:,:,j] - pool[:,:,i]
                        #show(stdout,"text/plain",diff)
                        printmaxsbmat(diff)
                end
        end
        for i in 1:4
                pool[:,:,i] = pool[:,:,8+i] - pool[:,:,8]
                global R = string(8+i," - ",8," = ",i)
                printmaxsbmat(pool[:,:,i])
        end
        for i in 8:16
                global R = i
                printmaxsbmat(pool[:,:,i])
        end
        for i in 8:11
                for j in 1:5
                        if i+4j<24
                        println(i,' ',4j,' ',pool[:,:,i+4j] == pool[:,:,i] + j*pool[:,:,4])
                        end
                end
        end
        println(vcat([-1 for i in 1:7],[findmin(pool[:,:,i])[1] for i in 8:24]))
        return pool
end
function analyseTWINE()
        p1 = [5 0 1 4 7 12 3 8 13 6 9 2 15 10 11 14]#TWINE
        (p,ep) = transformpermut(p1)
        return comparemsbmat(p,ep)
end
function analyseLBlock()
        p = [11 12 13 14 15 16 9 10 1 2 3 4 5 6 7 8]
        ep = [4 6 3 5 8 2 7 1]
        return comparemsbmat(p,ep)
end

function analyseWARP()
        p2 = [31 6 29 14 1 12 21 8 27 2 3 0 25 4 23 10 15 22 13 30 17 28 5 24 11 18 19 16 9 20 7 26]
        (p,ep) = transformpermut(p2)
        
        global k = length(p) ÷ 2
        global R = 10

        m = testsolmsb(p,ep)

        printmaxsb(m)
end

function boolar(nbi)
        res = [zeros(Bool,nbi) for i in 1:2^nbi]
        for i in 1:2^nbi
                res[i] = digits(Bool, i-1, base=2, pad=nbi)
        end
        return res
end

function makenode(m,i,j,k,curr)
        if j<=R                
                inds = findall(curr)
                nbi = length(inds)
                ars = boolar(nbi)
                for ar in ars
                        if sum(ar)==0
                                makenode(m,i,j+1,k,curr)
                        else
                                newcurr = copy(curr)
                                global cpt = cpt+1
                                newk = cpt
                                m[:,:,newk] = copy(m[:,:,k])
                                for iar in 1:nbi
                                        if ar[iar]==1
                                                newcurr[inds[iar]+2^(R-j)÷2] = true
                                                m[inds[iar],j,newk] = 1
                                        end
                                end
                                makenode(m,i,j+1,newk,newcurr)
                        end
                end
        end
end

function buildtree(R)
        m = zeros(Int8,2^(R-1),R,999999R)
        curr = zeros(Bool,2^(R-1))
        curr[1] = true
        makenode(m,1,1,1,curr)

        for i in 1:size(m,3)
                if sum(m[:,R,i])>fibo(R)
                        show(stdout,"text/plain",m[:,:,i])
                        println()
                end
        end
        R = 4
        cpt = 1
        buildtree(R)
        println(cpt)

        println("4: Ah 1806")
        println("5: Oups 3263442 ? ")
end


function makewayeo(deb, fin, perm, epsi, newv, newr,tperm, tepsi)
        if length(pool) < nbsol
                if deb > k  #epsilon cases
                        if epsi[deb - k] > 0
                                makewayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                        else
                                for oldv in 1:newv - 1 if !(oldv in epsi) #&& perm[deb]!=oldv && perm[oldv]!=deb
                                        epsi[deb - k] = oldv
                                        makewayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                                end end
                                if newv <= k
                                        epsi[deb - k] = newv
                                        makewayeo(epsi[deb - k], fin, perm, epsi, newv + 1, newr, tperm, tepsi + 1)
                                end
                                epsi[deb - k] = 0
                        end
                end
                if tperm == R-3
                        if deb == fin && tepsi >= mandatorysbox
                                witchwayeo(perm, epsi, newv, newr)
                        end
                else
                        if perm[deb] > 0
                                makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                        elseif deb > k #deb is red
                                for oldv in 1:newv - 1 if !(oldv in perm) #&& epsi[deb-k]!=oldv
                                        perm[deb] = oldv
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                                end end 
                                if newv <= k
                                        perm[deb] = newv
                                        makewayeo(perm[deb], fin, perm, epsi, newv + 1, newr, tperm + 1, tepsi)
                                end 
                                perm[deb] = 0
                        else #deb is green
                                for oldr in 1:newr - 1 if !((oldr + k) in perm) #&& epsi[oldr]!=deb
                                        perm[deb] = oldr + k
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                                end end 
                                if newr <= k
                                        perm[deb] = newr + k
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr + 1, tperm + 1, tepsi)
                                end
                                perm[deb] = 0
                        end
                end
        end
end

function countwayeo(deb, fin, perm, epsi, newv, newr, tperm, tepsi)
        nbway = 0
        if deb > k #epsilon cases
                if epsi[deb - k] > 0
                        nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                else
                        for oldv in 1:newv - 1 if !(oldv in epsi)
                                epsi[deb - k] = oldv
                                nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                        end end
                        if newv <= k
                                epsi[deb - k] = newv
                                nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv + 1, newr, tperm, tepsi + 1)
                        end
                        epsi[deb - k] = 0
                end
        end
        if tperm == R-3
                if deb == fin && tepsi >= mandatorysbox
                        return 1
                else    return 0 end
        else
                if perm[deb] > 0
                        nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                elseif deb > k #deb is red
                        for oldv in 1:newv - 1 if !(oldv in perm)
                                perm[deb] = oldv
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                        end end 
                        if newv <= k
                                perm[deb] = newv
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv + 1, newr, tperm + 1, tepsi)
                        end 
                        perm[deb] = 0
                else #deb is green
                        for oldr in 1:newr - 1 if !((oldr + k) in perm)
                                perm[deb] = oldr + k
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                        end end 
                        if newr <= k
                                perm[deb] = newr + k
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr + 1, tperm + 1, tepsi)
                        end
                        perm[deb] = 0
                end
        end
        return nbway
end

function haspatheo(deb,fin,tperm,tepsi,perm,epsi)
        if deb == 0
                return false
        elseif tperm == 0
                if  deb == fin && tepsi >= mandatorysbox
                        return true
                elseif deb > k
                        return haspatheo(epsi[deb-k],fin,tperm,tepsi+1,perm,epsi)
                else
                        return false
                end
        elseif haspatheo(perm[deb],fin,tperm-1,tepsi,perm,epsi)
                return true
        elseif deb > k
                return haspatheo(epsi[deb-k],fin,tperm,tepsi+1,perm,epsi)
        else
                return false
        end
end

function witchwayeo(perm, epsi, newv, newr)
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in (k + 1):2*k
                for fin in 1:k
                        if !haspatheo(deb, fin, R-3, 0, perm, epsi)
                                tmp = countwayeo(deb, fin, perm, epsi, newv, newr, 0, 0)
                                if minch > tmp
                                        minch = tmp
                                        newdeb = deb
                                        newfin = fin
                                end end end end
        if newdeb > 0
                makewayeo(newdeb, newfin, perm, epsi, newv, newr, 0, 0)
        else
                #=
                printglobal()
                global R
                tmp = R
                m = zeros(Int8,k,k,9)
                for r in 8:9
                        R = r
                        mm = testsolmsb(p,ep)
                        for i in 1:k
                                m[i,:,R] = copy(mm[i])
                        end
                end
                R = tmp
                r = vcat([-1 for i in 1:7],[findmin(m[:,:,i])[1] for i in 8:9])
                println(r)=#
                #if r[8]==2 #&& r[9]==3 
                        push!(pool,(copy(perm),copy(epsi)))
                #end
        end
end

function bestmsb(rmax)
        global k = 8
        global a = 0
        global nbsol = 1
        global pool = []
        sol = zeros(Int8,rmax)
        lastmsb = 0
        for r in 8:rmax
                global R = r
                msb = lastmsb
                isperm = true
                while isperm
                        global pool = []
                        global mandatorysbox = msb
                        lastmsb = msb
                        #isperm = makeway(1, k+1, zeros(Int8,2*k), zeros(Int8,k), 2, 2, 0, 0, 0, 0, 0, 0, ones(Bool,2*k,2*k), ones(Bool,k,k))
                        makewayeo(k+1, 1, zeros(Int8,2*k), zeros(Int8,k), 2, 2, 0, 0)
                        isperm = length(pool)==1
                        msb = msb+1
                        println(msb)
                end
                sol[r] = lastmsb-1
                println(sol)
        end
        return sol
        bestmsb(24)

end

global k = 8
global a = 0
global nbsol = 1
global pool = []
global R = 8
global mandatorysbox = 0


function maketab()
        cmax = 24
        C = 8:cmax
        print("\\begin{tabular}{")
        for i in C print("|c") end
        println("|c|}")
        println("\\hline")
        print("nbSbox")
        for r in C print(" & R=",r) end
        println("\\\\\\hline")
        m = [0, 0, 0, 0, 0, 0, 0, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 0, 0, 0]
        print("Best sols & ")
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        m = [0, 0, 0, 0, 0, 0, 0, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        print("Best sols sans a* & ")
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        print("TWINE & ")
        m = [0, 0, 0, 0, 0, 0, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        m = [0, 0, 0, 0, 0, 0, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        print("LBlock & ")
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        m = [-1, -1, -1, -1, -1, -1, -1, 0, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        print("R10b6 sol 1 & ")
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        m = [-1, -1, -1, -1, -1, -1, -1, -1, -1, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        print("R10b6 sol 2 & ")
        for k2 in C
                print(m[k2])
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        println("\\end{tabular}\\\\")  
end


function makek8r10b6()
        global k = 8
        global a = 0
        global nbsol = 100
        global pool = []
        global R = 10
        global mandatorysbox = 6
        makewayeo(k+1, 1, zeros(Int8,2*k), zeros(Int8,k), 2, 2, 0, 0)
        for i in 1:length(pool)
                (p,ep) = pool[i]
                global aaa = comparemsbmat(p,ep)
        end
        return pool
end

function getDDT(S)
        n = length(S)
        ddt = zeros(Int8,n,n)#[[0 for i in 1:n] for j in 1:n]
        for i in 0:n-1
                for j in 0:n-1
                        ddt[j+1, (S[i+1] ⊻ S[((i) ⊻ (j))+1])+1] += 1
                end
        end
        return ddt
end

function mainDDT()
        S = [12,6,9,0,1,10,2,11,3,8,5,13,4,14,7,15]

        #TWINE Sbox
        S = [12,0,15,10,2,11,9,5,8,3,13,7,1,14,6,4]


        #Lblock sboxes
        SS = [[14, 9, 15, 0, 13, 4, 10, 11, 1, 2, 8, 3, 7, 6, 12, 5],
              [4, 11, 14, 9, 15, 13, 0, 10, 7, 12, 5, 6, 2, 8, 1, 3],
              [1, 14, 7, 12, 15, 13, 0, 6, 11, 5, 9, 3, 2, 4, 8, 10],
              [7, 6, 8, 11, 0, 15, 3, 14, 9, 10, 12, 13, 5, 2, 4, 1],
              [14, 5, 15, 0, 7, 2, 12, 13, 1, 8, 4, 9, 11, 10, 6, 3],
              [2, 13, 11, 12, 15, 14, 0, 9, 7, 10, 6, 3, 1, 8, 4, 5],
              [11, 9, 4, 14, 0, 15, 10, 13, 6, 12, 5, 7, 3, 8, 1, 2],
              [13, 10, 15, 0, 14, 4, 9, 11, 2, 1, 8, 3, 7, 5, 12, 6],
              [8, 7, 14, 5, 15, 13, 0, 6, 11, 12, 9, 10, 2, 4, 1, 3],
              [11, 5, 15, 0, 7, 2, 9, 13, 4, 8, 1, 12, 14, 10, 3, 6]]

        for S in SS
                printmat(getDDT(S))
        end
end


function troncated(nbr,nbv,nrv,n)
        c = 1
        res = zeros(Int8,n+1,3)
        #for nrv in 0:Base.min(nbr,nbv)
                nv = nbv - nrv
                nr = nbr - nrv
                for cancel in 0:nrv
                        res[c,1] = nr + nv + nrv - cancel
                        res[c,2] = nr + nrv
                        res[c,3] = nr + nrv - cancel
                        c = c + 1
                end
        #end
        #println(" ",nbr," rouges ",nbv," verts")
        #printmat(res')
        return res'
end

function troncateddifftable(n)
        res = zeros(Int8,3,n+1,n+1,n+1,n+1)#binomial(n+2,2)
        for nbr in 0:n
                for nbv in 0:n
                        for nrv in 0:Base.min(nbr,nbv)
                                res[:,:,nbr+1,nbv+1,nrv+1] = troncated(nbr,nbv,nrv,n)
                        end
                end
        end
        return res
end

function maintronc()
        n = 8
        res = troncateddifftable(n)
        for nbr in 0:n
                for nbv in 0:n
                        for nrv in 0:Base.min(nbr,nbv)
                                println(nbr," ",nbv," ",nrv)
                                printmat(res[:,:,nbr+1,nbv+1,nrv+1])
                        end
                end
        end
        maintronc()
end

function gettronc(p,ep,diff,tperm,active)
        n=length(p)
        k = n÷2
        if tperm == 0
                global minactive
                if active < minactive
                        println(diff,active)
                        minactive = active
                        global diffmin = diff
                end
        else
                newdiff = zeros(Bool,n)
                nvr = 0
                tvr = []
                for i in 1:k #Fixed things
                        if diff[i+k]
                                newdiff[p[i+k]] = true
                                active+=1
                                if !diff[ep[i]]
                                        newdiff[p[ep[i]]] = true
                                end
                        end
                        if diff[i] && !diff[findfirst(x->x==i,ep)+k]
                                newdiff[p[i]] = true
                        end
                        if diff[i+k] && diff[ep[i]]
                                push!(tvr,ep[i])
                                nvr+=1
                        end
                end
                if nvr==0
                        gettronc(p,ep,newdiff,tperm-1,active)
                else
                        ars = boolar(nvr) #arranged things
                        for ar in ars
                                newnewdiff = copy(newdiff)
                                for i in 1:nvr
                                        if ar[i]
                                                newnewdiff[p[tvr[i]]] = true
                                        end
                                end
                                gettronc(p,ep,newnewdiff,tperm-1,active)
                        end
                end
        end
end


function teststep1()
        for i in 1:16
                R = 10
                println(i)
                println("lblock")

                p = [11, 12, 13, 14, 15, 16, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8]
                ep = [4, 6, 3, 5, 8, 2, 7, 1]

                diffa = zeros(Bool,16)
                diffa[i] = true

                global minactive = 999
                global diffmin = zeros(Bool,16)
                gettronc(p,ep,diffa,R,0)
                println(" TWINE  ")

                
                p1 = [5 0 1 4 7 12 3 8 13 6 9 2 15 10 11 14]#TWINE
                (p,ep) = transformpermut(p1)

                diffa = zeros(Bool,16)
                diffa[i] = true

                minactive = 999
                diffmin = zeros(Bool,16)
                gettronc(p,ep,diffa,R,0)
                println(" Sol1  ")

                (p,ep) = (Int8[9, 10, 11, 12, 13, 14, 15, 16, 2, 5, 3, 8, 4, 7, 1, 6], Int8[1, 3, 4, 5, 6, 7, 8, 2])

                diffa = zeros(Bool,16)
                diffa[i] = true

                minactive = 999
                diffmin = zeros(Bool,16)
                gettronc(p,ep,diffa,R,0)
                println(" Sol2  ")

                (p,ep) = (Int8[10, 11, 12, 13, 14, 15, 9, 16, 8, 1, 5, 7, 3, 6, 4, 2], Int8[1, 2, 3, 4, 5, 6, 7, 8])

                diffa = zeros(Bool,16)
                diffa[i] = true

                minactive = 999
                diffmin = zeros(Bool,16)
                gettronc(p,ep,diffa,R,0)
        end
        teststep1()
end
function printindddiff(p)
        n = length(p)
        for i in 1:n
                if p[i]==0
                        print("  ")
                else
                        print(" ",if i>=p[i] i-p[i] else i-p[i]+n end)
                end
        end
        println("    ",p)
end


function V2type(p,n,ecar)
        if ecar == n-2
                V2type(p,n,ecar+1)
        else
                #printindddiff(p)
                if ecar==n-1
                        #println("YES")
                        printindddiff(p)
                else
                        for i in 1:n-1
                                if p[i]==0
                                        if i>=ecar
                                                if !(i-ecar in p)
                                                        p[i] = i-ecar
                                                        V2type(p,n,ecar+1)
                                                        p[i] = 0
                                                end
                                        elseif !(i+n-ecar in p)
                                                p[i] = i+n-ecar
                                                V2type(p,n,ecar+1)
                                                p[i] = 0
                                        end
                                        
                                end
                        end
                end
        end
end
function makepV2type(n)
        p = zeros(Int8,n)
        p[1] = 1
        V2type(p,n,1)
end

using SparseArrays

function depreciated()

        (p,ep) = (Int8[9, 10, 11, 12, 13, 14, 15, 16, 2, 5, 3, 8, 4, 7, 1, 6], Int8[1, 3, 4, 5, 6, 7, 8, 2])
        n=length(p)
        k = n÷2
        m = spzeros(Int8,2^2k,2^2k)
        a = digits(Bool, 1, base=2, pad=2k)
        b = digits(Bool, 1, base=2, pad=2k)
        R1 = V1 = R2 = V2 = i = s = 0
        respect = true
        for dif1 in 1:2^2k
                println(dif1)
                for dif2 in 1:2^2k
                        a = digits(Bool, dif1, base=2, pad=2k)
                        b = digits(Bool, dif2, base=2, pad=2k)
                        respect = true
                        i = 0
                        s = 0
                        while respect && i<k
                                i += 1
                                R1 = a[i+k]
                                V1 = a[ep[i]]
                                R2 = b[p[i+k]]
                                V2 = b[p[ep[i]]]
                                if R1 && R2 && V2
                                        s += 1
                                elseif V1 && ((R1 && !R2 && V2) || (!R1 && R2 && !V2))
                                else respect = false
                                end
                        end
                        if respect
                                m[dif1,dif2] = s
                        end
                end
        end
end
        
# C de la merde


using BitBasis
function getnextdiff(dif,p,ep)
        k = length(p)÷2
        m = spzeros(Int8,2^2k)
        diff = digits(Bool, dif, base=2, pad=2k)
        
        newdiff = zeros(Bool,n)
        nvr = 0
        tvr = []
        active = 0
        for i in 1:k #Fixed things
                if diff[i+k]
                        newdiff[p[i+k]] = true
                        active+=1
                        if !diff[ep[i]]
                                newdiff[p[ep[i]]] = true
                        end
                end
                if diff[i] && !diff[findfirst(x->x==i,ep)+k]
                        newdiff[p[i]] = true
                end
                if diff[i+k] && diff[ep[i]]
                        push!(tvr,ep[i])
                        nvr+=1
                end
        end
        if nvr==0
                m[packbits(newdiff)+1] = active
        else
                ars = boolar(nvr) #arranged things
                for ar in ars
                        newnewdiff = copy(newdiff)
                        for i in 1:nvr
                                if ar[i]
                                        newnewdiff[p[tvr[i]]] = true
                                end
                        end
                        m[packbits(newnewdiff)+1] = active
                end
        end
        return m
end

function miangetnextdiff()
        (p,ep) = (Int8[9, 10, 11, 12, 13, 14, 15, 16, 2, 5, 3, 8, 4, 7, 1, 6], Int8[1, 3, 4, 5, 6, 7, 8, 2])

        k = length(p)÷2

        a = [getnextdiff(dif,p,ep) for dif in 1:2^2k]

        [println(Base.min(aa)) for aa in a]

        aa = a[1]
        for i in 2:length(a)
                aa = hcat(aa,a[i])
        end
end
# attention au vrais 0
#itee2rer le table en calculant le min


function makepepkm2(a)
        k = length(a)
        ep = vcat([1],[i for i in 3:k],[2])#decomp en e-sous-cycles de taille 1 et k-1
        p  = vcat([i+k for i in 1:k],[i+1 for i in a])
        return p,ep
end

function makepeppq(p0,q)
        k = length(p0)
        ep = [i for i in 1:k]
        p  = vcat([i+k+1 for i in p0],q.+1)
        return (p,ep)
end

function difrec(b,k,dif,dif2,active)
        global newdifs
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,k,dif,dif2,active)
                elseif c==1 #juste un vert
                        difrec(b+1,k,dif,dif2 | 1 << 2b,active)
                elseif c==2 #juste un rouge
                        difrec(b+1,k,dif,dif2 | 3 << 2b,active+1)
                elseif c==3 #doubling
                        difrec(b+1,k,dif,dif2 | 2 << 2b,active+1)
                        difrec(b+1,k,dif,dif2 | 3 << 2b,active+1)
                end
        end
end

function apply(p,dif)
        k = length(p)÷2
        dif2 = 0
        for i in 0:2k-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        if dif2==0
                println(dif," ",dif2,p)
        end
        return dif2
end
        
function getdiff2(difs,r,p)
        global newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,k,dif,0,difs[dif])
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        if r == 0
                return difs
        else
                return getdiff2(difs,r-1,p)
        end
end


function onetabdiff(p,ep,cmin,cmax,C)
        res = [0 for i in C]
        global k = length(p)÷2
        pp = onlypcole(p,ep)
        difs = ones(Int8,2^2k-1)
        difs = getdiff2(difs,cmin-2,pp)
        #difs = getdiff(difs,cmin-1,p,ep)        
        for k2 in C
                difs = getdiff2(difs,0,pp)
                res[k2-cmin+1] = findmin(difs)[1]-1
                print(findmin(difs)[1]-1)
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        return res
end

function maketabdiff()
        cmax = 36
        cmin = 2
        C = cmin:cmax
        names = ["rounds"]
        res = [i for i in C]
        print("\\begin{tabular}{")
        for i in C print("|c") end
        println("|c|}")
        println("\\hline")
        print("step1")
        for r in C print(" & R",r) end
        println("\\\\\\hline")

        name = "TWINE"
        print(name," & ")
        push!(names,name)
        p1 = [5 0 1 4 7 12 3 8 13 6 9 2 15 10 11 14]#TWINE
        (p,ep) = transformpermut(p1)
        res = hcat(onetabdiff(p,ep,cmin,cmax,C),res)
        
        name = "LBlock"
        print(name," & ")
        push!(names,name)
        p = [11, 12, 13, 14, 15, 16, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8]#LBlock
        ep = [4, 6, 3, 5, 8, 2, 7, 1]
        res = hcat(onetabdiff(p,ep,cmin,cmax,C),res)
        
        aa = [[3,0,1,6,2,5,7,4,], [3,0,6,2,7,5,4,1,], [3,0,7,6,4,2,5,1,], [3,0,5,7,2,4,6,1,], [3,0,2,6,1,4,7,5,],
              [3,0,7,4,1,5,2,6,], [3,0,5,2,1,6,4,7,], [3,0,6,4,2,1,5,7,], 
              [3,0,6,2,4,1,7,5,], [3,0,6,2,5,1,4,7,], [3,0,2,7,5,1,4,6,], [3,0,7,2,5,1,6,4,],
              [3,0,1,6,5,2,4,7,], [3,0,1,4,7,2,6,5,], [3,0,5,1,4,2,7,6,], [3,0,1,6,4,2,7,5,],
              [3,0,6,1,5,4,2,7,], [3,0,1,7,4,6,2,5,], [3,0,2,1,7,6,5,4,]]
        for a in aa
                name = "["
                for i in a name = name * string(i) * " " end
                name = name * "]"
                print(name," & ")
                push!(names,name)
                (p,ep) = makepepkm2(a)
                res = hcat(onetabdiff(p,ep,cmin,cmax,C),res)
        end
        
        DSMITM = [[15,2,13,4,11,6,3,8,1,10,5,0,7,12,9,14], [7,2,13,4,15,6,1,8,5,10,3,0,11,12,9,14],
                  [15,2,9,4,1,6,11,8,3,10,13,0,7,12,5,14], [7,2,11,4,9,6,1,8,15,10,13,0,3,12,5,14],
                  [13,2,15,4,11,6,3,8,1,10,5,0,9,12,7,14], [7,2,11,4,9,6,1,8,13,10,15,0,5,12,3,14],
                  [13,2,9,4,1,6,11,8,3,10,15,0,5,12,7,14], [7,2,15,4,13,6,1,8,5,10,3,0,9,12,11,14],
                  [9,2,7,4,11,6,15,8,13,10,5,0,1,12,3,14], [5,2,9,4,13,6,15,8,3,10,7,0,1,12,11,14],
                  [9,2,7,4,11,6,13,8,15,10,5,0,3,12,1,14], [5,2,9,4,15,6,13,8,3,10,7,0,11,12,1,14]]

        for a in DSMITM
                (p,ep) = transformpermut(a')
                name = "["
                for i in a[1:2:2k] name = name * string(i) * " " end
                name = name * "]"
                print(name," & ")
                push!(names,name)
                res = hcat(onetabdiff(p,ep,cmin,cmax,C),res)
        end
        println("\\end{tabular}\\\\")
        return res,names
end

function maindiff()
        megares,names = maketabdiff()
        p,q = [1,2,3,4,5,0,7,8,9,10,11,6,13,14,12,15],[2,6,12,10,1,13,4,15,7,9,14,5,8,3,11,0]
        p,ep = makepeppq(p,q)
        cmax = 20
        cmin = 2
        C = cmin:cmax
        onetabdiff(p,ep,cmin,cmax,C)
end




function hasXpatheo(deb,fin,tperm,tepsi,perm,epsi)
        if deb == 0
                return false,0
        elseif tperm == 0
                if  deb == fin 
                        return true,0
                elseif deb > k
                        return epsi[deb-k] == fin,1
                else
                        return false,0
                end
        else
                a,b = hasXpatheo(perm[deb],fin,tperm-1,tepsi,perm,epsi)
                c,d = false,0
                if deb > k
                        c,d = hasXpatheo(epsi[deb-k],fin,tperm,tepsi+1,perm,epsi)
                end
                e = a||c
                f = if a b else 0 end + if c d+1 else 0 end 
                return e,f
        end
end

function makewayeo(deb, fin, perm, epsi, newv, newr, tperm, tepsi, tway, truedeb, way, ways)
        #way encode un chemin, les 1 sont des transi et les 0 des epsilon. way commence toujours par 1
        if length(pool) < nbsol
                if deb > k  #epsilon cases
                        if epsi[deb - k] > 0
                                makewayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1, tway, truedeb, way<<1, ways)
                        else
                                for oldv in 1:newv - 1 if !(oldv in epsi)
                                        epsi[deb - k] = oldv
                                        makewayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1, tway, truedeb, way<<1, ways)
                                end end
                                if newv <= k
                                        epsi[deb - k] = newv
                                        makewayeo(epsi[deb - k], fin, perm, epsi, newv + 1, newr, tperm, tepsi + 1, tway, truedeb, way<<1, ways)
                                end
                                epsi[deb - k] = 0
                        end
                end
                if count_ones(way)-1 == R-3 #trailing_zeros()
                        if deb == fin &&  (ways[way] == 0 || ways[way] >= tway)
                                ways[way] = tway
                                if hasXpatheo(truedeb, fin, R-3, 0, perm, epsi)[2] < mandtreesb
                                        makewayeo(truedeb, fin, perm, epsi, newv, newr, 0, 0, tway + 1, truedeb, 1, ways)
                                else
                                        witchwayeo(perm, epsi, newv, newr)
                                end
                        end
                else
                        if perm[deb] > 0
                                makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi, tway, truedeb, way<<1|1, ways)
                        elseif deb > k #deb is red
                                for oldv in 1:newv - 1 if !(oldv in perm)
                                        perm[deb] = oldv
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi, tway, truedeb, way<<1|1, ways)
                                end end 
                                if newv <= k
                                        perm[deb] = newv
                                        makewayeo(perm[deb], fin, perm, epsi, newv + 1, newr, tperm + 1, tepsi, tway, truedeb, way<<1|1, ways)
                                end 
                                perm[deb] = 0
                        else #deb is green
                                for oldr in 1:newr - 1 if !((oldr + k) in perm)
                                        perm[deb] = oldr + k
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi, tway, truedeb, way<<1|1, ways)
                                end end 
                                if newr <= k
                                        perm[deb] = newr + k
                                        makewayeo(perm[deb], fin, perm, epsi, newv, newr + 1, tperm + 1, tepsi, tway, truedeb, way<<1|1, ways)
                                end
                                perm[deb] = 0
                        end
                end
        end
end

function countwayeo(deb, fin, perm, epsi, newv, newr, tperm, tepsi)
        nbway = 0
        if deb > k #epsilon cases
                if epsi[deb - k] > 0
                        nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                else
                        for oldv in 1:newv - 1 if !(oldv in epsi)
                                epsi[deb - k] = oldv
                                nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv, newr, tperm, tepsi + 1)
                        end end
                        if newv <= k
                                epsi[deb - k] = newv
                                nbway += countwayeo(epsi[deb - k], fin, perm, epsi, newv + 1, newr, tperm, tepsi + 1)
                        end
                        epsi[deb - k] = 0
                end

        end
        if tperm == R-3
                return deb == fin
        else
                if perm[deb] > 0
                        nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                elseif deb > k #deb is red
                        for oldv in 1:newv - 1 if !(oldv in perm)
                                perm[deb] = oldv
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                        end end 
                        if newv <= k
                                perm[deb] = newv
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv + 1, newr, tperm + 1, tepsi)
                        end 
                        perm[deb] = 0
                else #deb is green
                        for oldr in 1:newr - 1 if !((oldr + k) in perm)
                                perm[deb] = oldr + k
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr, tperm + 1, tepsi)
                        end end 
                        if newr <= k
                                perm[deb] = newr + k
                                nbway += countwayeo(perm[deb], fin, perm, epsi, newv, newr + 1, tperm + 1, tepsi)
                        end
                        perm[deb] = 0
                end
        end
        return nbway
end

function witchwayeo(perm, epsi, newv, newr)
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in k+1:2k
                for fin in 1:k
                        if hasXpatheo(deb, fin, R-3, 0, perm, epsi)[2] < mandtreesb
                                tmp = countwayeo(deb, fin, perm, epsi, newv, newr, 0, 0)
                                if minch > tmp
                                        minch = tmp
                                        newdeb = deb
                                        newfin = fin
                                end end end end
        if newdeb > 0
                makewayeo(newdeb, newfin, perm, epsi, newv, newr, 0, 0, 1, newdeb, 1, spzeros(Int8,2^(2R-4)))
        else
                if ! (0 in perm) && !(0 in epsi)
                        if !((perm,epsi) in pool)
                                push!(pool,(deepcopy(perm),deepcopy(epsi)))
                        end
                end
        end
        global verbose
        if verbose println(perm,epsi) end
end

function getXways(a,b,c)
        global k = a
        global mandtreesb = b
        global R = c
        global pool = []

        #fixer la moitie de la perm
        perm = zeros(Int8,2*k)
        for i in 1:k
                perm[i] = i+k
        end
        makewayeo(k+1, 1, perm, zeros(Int8,k), 2, 2, 0, 0, 1, k+1, 1, spzeros(Int8,2^(2R-4)))
        println(pool)
end

function mainX1()
        global nbsol = 50

        cmax = 14
        cmin = 10
        C = cmin:cmax
        res = [i for i in C]'
        
        getXways(8,1,10)
        for i in 1:10:nbsol
                res = vcat(res,onetabdiff(pool[i][1],pool[i][2],cmin,cmax,C)')
        end
        println()
        getXways(8,2,10)
        res = vcat(res,[i for i in C]')
        for i in 1:10:length(pool)
                res = vcat(res,onetabdiff(pool[i][1],pool[i][2],cmin,cmax,C)')
        end
        println()
        getXways(8,3,10)
        res = vcat(res,[i for i in C]')
        for i in 1:10:nbsol
                res = vcat(res,onetabdiff(pool[i][1],pool[i][2],cmin,cmax,C)')
        end
        println()
        getXways(8,4,10)
        res = vcat(res,[i for i in C]')
        for i in 1:10:nbsol
                res = vcat(res,onetabdiff(pool[i][1],pool[i][2],cmin,cmax,C)')
        end
        return res
        
        global k = 8
        global mandtreesb = 22
        global nbsol = 20
        global pool = []
        global R = 10
        global done = zeros(Bool,k,k)

        global nbsol = 100000
        for j in 6:-1:3
                getXways(8,j,9)
                println(length(pool))
        end
        resres = [10,11,12,13,14]

        cmax = 14
        cmin = 10
        C = cmin:cmax
        for i in 1:length(pool)
                res = onetabdiff(pool[i][1],pool[i][2],cmin,cmax,C)
                resres = hcat(resres,res)
                #println(resres)
        end



        res = []
        for i in 2:3:length(pool)-3
                global res = resres[:,i+1]
                grapj(pool[i])
                global res = resres[:,i+2]
                grapj(pool[i+1])
                global res = resres[:,i+3]
                grapj(pool[i+2])
                println("\\\\")
        end

end




function nbpart(n)
        return length(partitions(n))#(ℯ^(π*√(2n/3))/(4n*√3))
end

function printXpaths(p,ep)
        k = 8
        print("\\begin{tabular}{")
        for i in 1:k print("|c") end
        println("|c|}")
        println("\\hline")
        for r in 1:k print(" & V",r) end
        println("\\\\\\hline")
        for deb in k+1:2k
                print("R",deb-k," & ")
                for fin in 1:k
                        print(hasXpatheo(deb, fin, R-3, 0, p, ep)[2])
                        if fin < k print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")
end
function analyseTWINE()
        p1 = [5 0 1 4 7 12 3 8 13 6 9 2 15 10 11 14]#TWINE
        (p,ep) = transformpermut(p1)
        printXpaths(p,ep)
end
function analyseLBlock()
        p = [11 12 13 14 15 16 9 10 1 2 3 4 5 6 7 8]
        ep = [4 6 3 5 8 2 7 1]
        printXpaths(p,ep)
end

using SparseArrays

using Combinatorics

function getpep(p,ep)
        return vcat([i+k for i in 1:k],p),ep
end

function onlypcole(p,ep)
        k = length(ep)
        p = vcat([i for i in 1:2k]',p')
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#ordonne les p selon ep identite
                for l in 1:2
                        if p[l,i] > k
                                p[l,i] = ep[p[l,i]-k]+k
                        end
                end
        end
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#met les verts et rouges ensemble
                for l in 1:2
                        if p[l,i]>k #rouges sur les pairs 2:2k
                                p[l,i] = 2(p[l,i] - k)
                        else #verts sur les impairs 1:2k+1
                                p[l,i] = 2(p[l,i]) - 1 
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res
end

function hasJpath(x,p,fin,t)
        if x == 0
                return false,0
        elseif t == -1
                return  x == fin,0
        elseif t == 0
                return ep[x] == fin,1
        else
                a,b = hasJpath(p[x],p,fin,t-2)
                c,d = hasJpath(ep[x],p,fin,t-1)
                return a|c,if a b else 0 end + if c d+1 else 0 end 
        end
end

function cancel(difs,p,fin,t)
        for dif in difs
                
        end
end
function hasJpathcancel(x,p,fin,t)
        #successeur epsi de x plus luis mee3me a truediff
        #la diff d'un noeud jaune c'est son pred epsi or/xor son pred p. -> NON
        return cancel(dif,p,fin,t)
end

function makeJway(x,p,bp,deb,fin,way,ways,lvl)
        if length(pool) < nbsol
                if count_ones(way) < R-3
                        makeJway(ep[x],p,bp,deb,fin,way<<2|2,ways,lvl)
                        if p[x] > 0
                                makeJway(p[x],p,bp,deb,fin,way<<2|3,ways,lvl)
                        else
                                for y in 1:k if !bp[y]
                                        p[x] = y
                                        bp[y] = true
                                        makeJway(y,p,bp,deb,fin,way<<2|3,ways,lvl)
                                        bp[y] = false
                                end end
                                p[x] = 0
                        end
                else
                        if count_ones(way) == R-3
                                x = ep[x]
                                way = way<<2|2
                        end
                        if x == fin && (ways[way] == 0 || ways[way] >= lvl)
                                ways[way] = lvl
                                if hasJpath(deb,p,fin,R-3)[2] < mandtreesb
                                        makeJway(deb,p,bp,deb,fin,0,ways,lvl+1)
                                else
                                        witchJway(p, bp)
                                end
                        end
                end
        end
end

function countJway(x,p,bp,fin,t)
        nbway = 0
        if t > 0
                nbway += countJway(ep[x],p,bp,fin,t-1)
                if p[x] > 0
                        nbway += countJway(p[x],p,bp,fin,t-2)
                else
                        for y in 1:k if !bp[y]
                                p[x] = y
                                bp[y] = true
                                nbway += countJway(y,p,bp,fin,t-2)
                                bp[y] = false
                        end end
                        p[x] = 0
                end
                return nbway
        else
                if t == 0
                        x = ep[x]
                end
                return x == fin
        end
end

function witchJway(p, bp)
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:k
                for fin in 1:k
                        if hasJpath(deb,p,fin,R-3)[2] < mandtreesb
                                tmp = countJway(deb,p,bp,fin,R-3)
                                if minch > tmp
                                        minch = tmp
                                        newdeb = deb
                                        newfin = fin
                                end end end end
        if newdeb > 0
                makeJway(newdeb,p,bp,newdeb,newfin,0,spzeros(Int8,2^(2R-4)),1)
        else
                if ! (0 in p)
                        if !((p,ep) in pool)
                                push!(pool,(deepcopy(p),deepcopy(ep)))
                        end
                end
        end
end

function partJway(a,b,c)
        global k = a
        global mandtreesb = b
        global R = c
        global nbsol = 1
        global pool = []

        lp = 0
        part = collect(partitions(k))
        for i in 2:length(part)-1
                global ep = zeros(Int8,k)
                j = 1
                for c in part[i]
                        if c==1
                                ep[j] = j
                                j +=1
                        else
                                for l in j:j+c-2
                                        ep[l] = l+1
                                end
                                ep[j+c-1] = j
                                j = j+c
                        end
                end
                if truepart[i] && lp < nbsol
                        println(part[i])
                        makeJway(1, zeros(Int8,k), zeros(Bool,k), 1, 1, 0, spzeros(Int8,2^(2R-4)),1)#makeJway(x,p,bp,deb,fin,way,ways,lvl)
                        nlp = length(pool)
                        if nlp > lp
                                println(nlp,pool[lp+1:nlp])
                                lp = nlp
                        else
                                truepart[i] = false
                        end
                end
        end
end

global k = 8
global mandtreesb = 6
global nbsol = 1
global pool = []
global R = 9

global ep = zeros(Int8,k)


global truepart = ones(Bool,length(partitions(k)))
        partJway(8,19,11)

#partJway(8,2,11)

#partJway(8,2,8)

for j in 20:100
        println("X = ",j)
        partJway(8,j,13)
        #println(length(pool))
        if length(pool)==0
                break
        end
end


partJway(8,j,9)


resres = [10,11,12,13,14]

cmax = 14
cmin = 10
C = cmin:cmax
for i in 1:length(pool)
        p,ep = getpep(pool[i][1],pool[i][2])
        res = onetabdiff(p,ep,cmin,cmax,C)
        resres = hcat(resres,res)
        #println(resres)
end




pool = [(Int8[8, 5, 7, 6, 4, 2, 1, 3], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[8, 6, 7, 5, 2, 4, 1, 3], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[7, 5, 8, 6, 4, 2, 3, 1], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[7, 6, 8, 5, 2, 4, 3, 1], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[5, 7, 6, 8, 3, 1, 4, 2], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[6, 7, 5, 8, 1, 3, 4, 2], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[5, 8, 6, 7, 3, 1, 2, 4], Int8[2, 3, 4, 1, 6, 5, 7, 8]), (Int8[6, 8, 5, 7, 1, 3, 2, 4], Int8[2, 3, 4, 1, 6, 5, 7, 8])]

pool = [(Int8[6, 1, 4, 7, 2, 5, 8, 3], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[7, 1, 4, 8, 3, 2, 6, 5], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[5, 1, 4, 6, 8, 7, 3, 2], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[8, 1, 4, 5, 6, 3, 2, 7], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[4, 3, 7, 6, 1, 5, 8, 2], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[4, 3, 8, 7, 2, 1, 6, 5], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[4, 3, 6, 5, 8, 7, 2, 1], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[4, 3, 5, 8, 6, 2, 1, 7], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[2, 5, 8, 3, 6, 1, 4, 7], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[2, 6, 5, 3, 8, 7, 1, 4], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[2, 7, 6, 3, 4, 5, 8, 1], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[2, 8, 7, 3, 1, 4, 6, 5], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[5, 8, 2, 1, 6, 4, 3, 7], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[6, 5, 2, 1, 8, 7, 4, 3], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[7, 6, 2, 1, 3, 5, 8, 4], Int8[2, 3, 4, 1, 6, 7, 8, 5]), (Int8[8, 7, 2, 1, 4, 3, 6, 5], Int8[2, 3, 4, 1, 6, 7, 8, 5])]

pool = [(Int8[7, 2, 6, 5, 3, 8, 4, 1], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[4, 3, 7, 2, 5, 8, 6, 1], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[1, 5, 4, 2, 8, 3, 6, 7], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[2, 6, 1, 4, 8, 5, 3, 7], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[6, 2, 8, 3, 1, 7, 4, 5], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[5, 1, 3, 7, 6, 4, 8, 2], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[5, 7, 3, 8, 4, 2, 1, 6], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[4, 3, 1, 8, 2, 5, 7, 6], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[1, 8, 2, 7, 6, 3, 5, 4], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[2, 7, 8, 1, 4, 6, 3, 5], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[8, 1, 6, 5, 2, 4, 7, 3], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[7, 5, 4, 1, 3, 6, 8, 2], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[8, 6, 2, 4, 1, 7, 5, 3], Int8[2, 3, 4, 5, 6, 7, 1, 8]), (Int8[6, 8, 7, 3, 5, 2, 1, 4], Int8[2, 3, 4, 5, 6, 7, 1, 8])]

pool = [(Int8[1, 5, 8, 2, 4, 7, 3, 6], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[1, 5, 7, 2, 4, 8, 6, 3], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[1, 8, 6, 5, 3, 7, 2, 4], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[1, 7, 6, 5, 3, 8, 4, 2], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[4, 7, 1, 3, 8, 6, 5, 2], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[4, 8, 1, 3, 7, 6, 2, 5], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[8, 5, 4, 2, 7, 6, 1, 3], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[7, 5, 4, 2, 8, 6, 3, 1], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[7, 6, 2, 8, 5, 3, 4, 1], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[8, 6, 2, 7, 5, 3, 1, 4], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[6, 7, 3, 1, 8, 4, 5, 2], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[6, 8, 3, 1, 7, 4, 2, 5], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[6, 5, 1, 7, 8, 4, 3, 2], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[6, 5, 1, 8, 7, 4, 2, 3], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[5, 1, 6, 2, 7, 8, 4, 3], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[5, 1, 6, 2, 8, 7, 3, 4], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[7, 2, 8, 1, 6, 4, 3, 5], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[7, 2, 6, 8, 3, 5, 4, 1], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[8, 2, 7, 1, 6, 4, 5, 3], Int8[2, 3, 4, 5, 6, 1, 7, 8]), (Int8[8, 2, 6, 7, 3, 5, 1, 4], Int8[2, 3, 4, 5, 6, 1, 7, 8])]



resres = [10,11,12,13,14]

cmax = 14
cmin = 10
C = cmin:cmax
for i in 1:length(pool)
        p,ep = getpep(pool[i][1],pool[i][2])
        res = onetabdiff(p,ep,cmin,cmax,C)
        resres = hcat(resres,res)
        #println(resres)
end





res = []
for i in 1:3:length(pool)
        global res = resres[:,i+1]
        grapj(getpep(pool[i][1],pool[i][2]))
        if length(pool)>=i+1
                global res = resres[:,i+2]
                grapj(getpep(pool[i+1][1],pool[i+1][2]))
        end
        if length(pool)>=i+2
                global res = resres[:,i+3]
                grapj(getpep(pool[i+2][1],pool[i+2][2]))
        end
        println("\\\\")
end





#k = 4
#5^k*factorial(k)*length(partitions(k))
#=

Hwloc.num_physical_cores()
k6c9 122.927522 seconds (7.28 G allocations: 122.553 GiB, 13.23% gc time)
k6c8 120.214620 seconds (7.28 G allocations: 122.498 GiB, 12.86% gc time)
k6c7 118.449658 seconds (7.28 G allocations: 122.552 GiB, 14.26% gc time)
#using StaticArrays
=#
#=
JULIA_NUM_THREADS=128 ./julia
Threads.@Threads:static

cd Applications/Julia-1.5.app/Contents/Resources/julia/bin/ 
JULIA_NUM_THREADS=8 ./julia
=#




using Combinatorics

function difrec(b,dif,dif2,active,newdifs)
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end


#newdif[i] = min(difs[pred1]+x1,difs[pred2]+x2,difs[pred3]+x3,...)

function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end

function getdiff2(difs,p)
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end

function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,np,na,14)
        io = open("res.txt", "w")
        for i in 1:np
                println(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                Threads.@threads for jt in 0:Threads.nthreads()-1
                        for ja in 1:charge
                                j = jt*charge+ja
                                if j<=na
                                        q = allperm[j]
                                        pq = zeros(Int8,2k)
                                        for l in 1:k
                                                pq[2l] = 2p[l]-1
                                                pq[2l-1] = 2q[l]
                                        end
                                        #pq =  [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                        difs = ones(Int8,2^2k-1)
                                        util = true
                                        for r in 1:14
                                                difs = getdiff2(difs,pq)                                                
                                                res[i,j,r] += findmin(difs)[1]-1
                                        end
                                end
                        end
                end
                println(partoch[i]," ",res[i,:,end])
                write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        end
        close(io)
        return res
end


const k = 3
const partoch = collect(partitions(k))
const np = length(partoch)
const allperm = collect(permutations([i for i in 1:k]))
const na = length(allperm)
const charge = div(na,Threads.nthreads())+1
@time res2 = paramain()

#Algo pour comparer les criteres:
#Tout est fais avec l'algo des chemins sur les noeuds rouge (on s'arrete toujours sur les noeuds verts).

#1 Diffusion round: Diffusion totale au plus tot
#=crit1 R8
[8]               144 sols     345 deja vu      
[7, 1]               238 sols     1092 deja vu     40 incomplete     
[6, 2]               276 sols     706 deja vu      
[6, 1, 1]               246 sols     655 deja vu      
[5, 3]               87 sols     270 deja vu     82 incomplete     
[5, 2, 1]               120 sols     376 deja vu      
[5, 1, 1, 1]               150 sols     1582 deja vu      
[4, 4]               192 sols     436 deja vu      
[4, 3, 1]               204 sols     685 deja vu      
[4, 2, 2]               64 sols     92 deja vu      
[4, 2, 1, 1]               64 sols     208 deja vu      
[4, 1, 1, 1, 1]                  
[3, 3, 2]               270 sols     584 deja vu      
[3, 3, 1, 1]               108 sols     230 deja vu      
[3, 2, 2, 1]               24 sols     54 deja vu      
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
1.367286 seconds (8.30 k allocations: 833.891 KiB)=#
#=crit1 gways
[8]               144 sols     173 deja vu      
[7, 1]               236 sols     591 deja vu     32 incomplete     
[6, 2]               276 sols     270 deja vu      
[6, 1, 1]               246 sols     263 deja vu      
[5, 3]               84 sols     70 deja vu     39 incomplete     
[5, 2, 1]               120 sols     126 deja vu      
[5, 1, 1, 1]               150 sols     337 deja vu      
[4, 4]               192 sols     231 deja vu      
[4, 3, 1]               204 sols     266 deja vu      
[4, 2, 2]               64 sols     24 deja vu      
[4, 2, 1, 1]               64 sols     74 deja vu      
[4, 1, 1, 1, 1]                  
[3, 3, 2]               270 sols     242 deja vu      
[3, 3, 1, 1]               108 sols     133 deja vu      
[3, 2, 2, 1]               24 sols     22 deja vu      
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
  1.760546 seconds (674.39 k allocations: 53.579 MiB, 0.23% gc time)=#

using Combinatorics
using SparseArrays
function haspath(x,p,fin,t)
        #if sum(p[2:2:2k])> 0 println(x," ",p," ",fin," ",t) end
        if t > 1
                x = p[x]#go to next red
                if haspath(x-1,p,fin,t-1)
                        return true
                elseif p[x]>0
                        return haspath(p[x],p,fin,t-2)
                else return false end
        else
                if t==1 x=p[x]-1 end
                return x==fin
        end
end
function makeway(x,p,cp,deb,fin,way,fways,gways)
        #println(x," ",p[x]," ",p,way," ",count_ones(way)<R-3)
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,gways)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,gways)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,gways)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                if x == fin
                        fways[way] = 1
                        gways[(deb+1)>>1,(fin+1)>>1][way] = -1
                        gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                        witchway(p, cp, gways)
                        gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                        gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                end
        end
end
function countway(x,p,cp,fin,way)
        cw = 0
        if count_ones(way) < R-3
                x = p[x]
                cw += countway(x-1,p,cp,fin,way<<2|2)
                if p[x] > 0
                        cw += countway(p[x],p,cp,fin,way<<2|3)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                cw += countway(p[x],p,cp|1<<y,fin,way<<2|3)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                cw += x==fin
        end
        return cw
end
function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end
function witchway(p,cp,gways)
        for deb in 1:2:2k for fin in 1:2:2k
                if hasforbidden(deb,p,fin,gways)
                        #println(deb," ",fin," ",p)
                        return;
                end end end
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:2:2k for fin in 1:2:2k if !haspath(deb,p,fin,R-2)
                tmp = countway(deb,p,cp,fin,0)
                if minch > tmp
                        minch = tmp
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,0,spzeros(Int8,2^(2R-4)),gways)
        else
                if 0 in p global zeroin+=1#println("0 in sol ",p)
                elseif p in pool global dejavu+=1#println("DEJA VU ",p)
                else
                        push!(pool,copy(p))
                end
        end
end

function crit1(p)
        gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
        witchway(p,0,gways)
end

const k=8
const R=8
const partoch = collect(partitions(k))
const np = length(partoch)
global pool = []
global bigpool = []
global zeroin = 0
global dejavu = 0
function chmain()
        for i in 1:np
                print(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                pq = zeros(Int8,2k)
                for l in 1:k
                        pq[2l-1] = 2p[l]
                end
                global pool = []
                global zeroin = 0
                global dejavu = 0
                crit1(pq)
                println("               ",
                if length(pool)>0 string(length(pool)," sols     ") else " " end,
                if dejavu>0 string(dejavu," deja vu     ") else " " end,
                if zeroin>0 string(zeroin," incomplete     ") else " " end)
                push!(bigpool,deepcopy(pool))
        end
end
@time chmain()


#2 Best ways: Difusion totale au plus tot avec des chemins sbox-maximaux

#Chemins V2 voir code minizinc

#3 At least X ways: Difusion totale au plus tot avec au moins X chemins entre chaque paire
#=crit3 R8 mand1
[8]               144 sols     345 deja vu      
[7, 1]               238 sols     1092 deja vu     40 incomplete     
[6, 2]               276 sols     706 deja vu      
[6, 1, 1]               246 sols     655 deja vu      
[5, 3]               87 sols     270 deja vu     82 incomplete     
[5, 2, 1]               120 sols     376 deja vu      
[5, 1, 1, 1]               150 sols     1582 deja vu      
[4, 4]               192 sols     436 deja vu      
[4, 3, 1]               204 sols     685 deja vu      
[4, 2, 2]               64 sols     92 deja vu      
[4, 2, 1, 1]               64 sols     208 deja vu      
[4, 1, 1, 1, 1]                  
[3, 3, 2]               270 sols     584 deja vu      
[3, 3, 1, 1]               108 sols     230 deja vu      
[3, 2, 2, 1]               24 sols     54 deja vu      
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
 12.283804 seconds (691.74 k allocations: 526.745 MiB, 0.51% gc time)
+gways
[8]               139 sols       
[7, 1]               227 sols      7 incomplete     
[6, 2]               270 sols       
[6, 1, 1]               244 sols       
[5, 3]               75 sols      15 incomplete     
[5, 2, 1]               119 sols       
[5, 1, 1, 1]               150 sols       
[4, 4]               192 sols       
[4, 3, 1]               201 sols       
[4, 2, 2]               64 sols       
[4, 2, 1, 1]               64 sols       
[4, 1, 1, 1, 1]                  
[3, 3, 2]               258 sols       
[3, 3, 1, 1]               108 sols       
[3, 2, 2, 1]               22 sols       
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
  1.986581 seconds (628.47 k allocations: 884.513 MiB, 5.69% gc time)
R9 mand2
[8]               48 sols     846 deja vu      
[7, 1]               14 sols     1478 deja vu      
[6, 2]               48 sols     1244 deja vu      
[6, 1, 1]               91 sols     7967 deja vu      
[5, 3]               58 sols     1831 deja vu      
[5, 2, 1]               70 sols     4682 deja vu      
[5, 1, 1, 1]                  
[4, 4]               16 sols     512 deja vu      
[4, 3, 1]               60 sols     5220 deja vu      
[4, 2, 2]                  
[4, 2, 1, 1]               8 sols     632 deja vu      
[4, 1, 1, 1, 1]                  
[3, 3, 2]               70 sols     3926 deja vu      
[3, 3, 1, 1]               144 sols     7338 deja vu      
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
156.259569 seconds (2.75 M allocations: 13.726 GiB, 0.69% gc time)
+gways
[8]               23 sols       
[7, 1]               4 sols       
[6, 2]               22 sols       
[6, 1, 1]               91 sols       
[5, 3]               43 sols       
[5, 2, 1]               59 sols       
[5, 1, 1, 1]                  
[4, 4]               12 sols       
[4, 3, 1]               46 sols       
[4, 2, 2]                  
[4, 2, 1, 1]               8 sols       
[4, 1, 1, 1, 1]                  
[3, 3, 2]               48 sols       
[3, 3, 1, 1]               133 sols       
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
20.469430 seconds (2.31 M allocations: 4.744 GiB, 1.89% gc time)=#
#= R10 m3 +gways
[8]               13 sols       
[7, 1]               10 sols       
[6, 2]               40 sols       
[6, 1, 1]               533 sols       
[5, 3]               13 sols       
[5, 2, 1]               153 sols       
[5, 1, 1, 1]               327 sols       
[4, 4]               24 sols       
[4, 3, 1]               102 sols       
[4, 2, 2]               38 sols       
[4, 2, 1, 1]               154 sols       
[4, 1, 1, 1, 1]                  
[3, 3, 2]               16 sols       
[3, 3, 1, 1]               137 sols       
[3, 2, 2, 1]               17 sols       
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
63.688223 seconds (3.19 M allocations: 44.783 GiB, 3.59% gc time)=#
#= R11 m5 +gways
[8]                  
[7, 1]               2 sols       
[6, 2]               6 sols       
[6, 1, 1]               35 sols       
[5, 3]                  
[5, 2, 1]               71 sols       
[5, 1, 1, 1]               23 sols       
[4, 4]               1 sols       
[4, 3, 1]               22 sols       
[4, 2, 2]               11 sols       
[4, 2, 1, 1]               61 sols       
[4, 1, 1, 1, 1]                  
[3, 3, 2]               9 sols       
[3, 3, 1, 1]               174 sols       
[3, 2, 2, 1]               14 sols       
[3, 2, 1, 1, 1]               15 sols       
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
222.838919 seconds (6.18 M allocations: 284.287 GiB, 3.82% gc time)=#


using Combinatorics
using SparseArrays
function nbways(x,p,fin,t)
        #if sum(p[2:2:2k])> 0 println(x," ",p," ",fin," ",t) end
        if t > 1
                x = p[x]#go to next red
                return nbways(x-1,p,fin,t-1) +
                        if p[x]>0 nbways(p[x],p,fin,t-2)
                        else 0 end
        else
                if t==1 x=p[x]-1 end
                return x==fin 
        end
end
#[[[chemins interdits]]] passent dans les instances plus basses de la generation de chemins
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways)
        #println(x," ",p[x]," ",p,way," ",count_ones(way)<R-3)
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        if nbways(deb,p,fin,R-2) < mand
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function countway(x,p,cp,fin,way)
        cw = 0
        if count_ones(way) < R-3
                x = p[x]
                cw += countway(x-1,p,cp,fin,way<<2|2)
                if p[x] > 0
                        cw += countway(p[x],p,cp,fin,way<<2|3)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                cw += countway(p[x],p,cp|1<<y,fin,way<<2|3)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                cw += x==fin
        end
        return cw
end
function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end
function witchway(p,cp,gways)
        for deb in 1:2:2k for fin in 1:2:2k
                if hasforbidden(deb,p,fin,gways)
                        #println(deb," ",fin," ",p)
                        return;
                end end end
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:2:2k for fin in 1:2:2k if nbways(deb,p,fin,R-2) < mand
                tmp = countway(deb,p,cp,fin,0)
                if minch > tmp
                        minch = tmp
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,0,zeros(Int8,2^(2R-4)),1,gways)
        else
                if 0 in p global zeroin+=1#println("0 in sol ",p)
                elseif p in pool global dejavu+=1#println("DEJA VU ",p)
                else
                        push!(pool,copy(p))
                end
        end
end

function crit3(p)
        gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
        witchway(p,0,gways)
end

function chmain()
        for i in 1:np
                print(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                pq = zeros(Int8,2k)
                for l in 1:k
                        pq[2l-1] = 2p[l]
                end
                global pool = []
                global zeroin = 0
                global dejavu = 0
                crit3(pq)
                println("               ",
                        if length(pool)>0 string(length(pool)," sols     ") else " " end,
                        if dejavu>0 string(dejavu," deja vu     ") else " " end,
                        if zeroin>0 string(zeroin," incomplete     ") else " " end)
                push!(bigpool,deepcopy(pool))
        end
end

const k=8
const partoch = collect(partitions(k))
const np = length(partoch)


global R=11
global pool = []
global bigpool = []
global zeroin = 0
global dejavu = 0
global mand = 5
@time chmain()




#4 At least X sbox: Pour un R donne difusion totale avec au moins X sboxes entre chaque noeud (ou variantes)
#= R9 m4 
[8]                  
[7, 1]                  
[6, 2]                  
[6, 1, 1]                  
[5, 3]                  
[5, 2, 1]                  
[5, 1, 1, 1]                  
[4, 4]               16 sols     224 deja vu      
[4, 3, 1]                  
[4, 2, 2]                  
[4, 2, 1, 1]                  
[4, 1, 1, 1, 1]                  
[3, 3, 2]                  
[3, 3, 1, 1]                  
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
 43.960915 seconds (965.38 k allocations: 45.513 MiB, 0.01% gc time)=#
#= R9 m4 gways
[8]                  
[7, 1]                  
[6, 2]                  
[6, 1, 1]                  
[5, 3]                  
[5, 2, 1]                  
[5, 1, 1, 1]                  
[4, 4]               12 sols       
[4, 3, 1]                  
[4, 2, 2]                  
[4, 2, 1, 1]                  
[4, 1, 1, 1, 1]                  
[3, 3, 2]                  
[3, 3, 1, 1]                  
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
5.414999 seconds (255.03 k allocations: 21.179 MiB, 0.07% gc time)=#
#= R10 m5 +gways
[8]               27 sols      1 incomplete     
[7, 1]                  
[6, 2]               7 sols       
[6, 1, 1]                  
[5, 3]               50 sols      10 incomplete     
[5, 2, 1]                  
[5, 1, 1, 1]                  
[4, 4]                  
[4, 3, 1]                  
[4, 2, 2]               16 sols       
[4, 2, 1, 1]                  
[4, 1, 1, 1, 1]                  
[3, 3, 2]                  
[3, 3, 1, 1]                  
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
20.104291 seconds (1.06 M allocations: 109.880 MiB, 0.03% gc time)=#
#= R11 m6 +gways
[8]               45 sols      43 incomplete     
[7, 1]               10 sols      1 incomplete     
[6, 2]               45 sols      15 incomplete     
[6, 1, 1]                  
[5, 3]               5 sols      1 incomplete     
[5, 2, 1]                  
[5, 1, 1, 1]                  
[4, 4]               37 sols      5 incomplete     
[4, 3, 1]                  
[4, 2, 2]                  
[4, 2, 1, 1]                  
[4, 1, 1, 1, 1]                  
[3, 3, 2]                  
[3, 3, 1, 1]                  
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
73.852471 seconds (4.05 M allocations: 552.258 MiB, 0.05% gc time)=#
#= R12 m7 +gways
[8]               30 sols      33 incomplete     
[7, 1]               5 sols      2 incomplete     
[6, 2]               5 sols       
[6, 1, 1]                  
[5, 3]                  
[5, 2, 1]                  
[5, 1, 1, 1]                  
[4, 4]               276 sols      199 incomplete     
[4, 3, 1]                  
[4, 2, 2]                  
[4, 2, 1, 1]                  
[4, 1, 1, 1, 1]                  
[3, 3, 2]                  
[3, 3, 1, 1]                  
[3, 2, 2, 1]                  
[3, 2, 1, 1, 1]                  
[3, 1, 1, 1, 1, 1]                  
[2, 2, 2, 2]                  
[2, 2, 2, 1, 1]                  
[2, 2, 1, 1, 1, 1]                  
[2, 1, 1, 1, 1, 1, 1]                  
[1, 1, 1, 1, 1, 1, 1, 1]                  
297.615801 seconds (11.91 M allocations: 2.254 GiB, 0.08% gc time)=#

function getactive(x,p,fin,t,active)
        #if sum(p[2:2:2k])> 0 println(x," ",p," ",fin," ",t) end
        if t > 1
                x = p[x]#go to next red
                return getactive(x-1,p,fin,t-1,active|1<<(x>>1)) |
                        if p[x]>0 getactive(p[x],p,fin,t-2,active)
                        else 0 end
        else
                if t==1
                        active = active | 1<<(p[x]>>1)
                        x=p[x]-1
                end
                if x==fin return active
                else return 0 end
        end
end
function nbactive(deb,p,fin,t)
        #println(digits(getactive(deb,p,fin,t,0), base = 2))
        return count_ones(getactive(deb,p,fin,t,0))
end
#Any[Int8[4, 15, 6, 7, 8, 5, 2, 9, 12, 13, 14, 11, 16, 3, 10, 1], Int8[4, 9, 6, 7, 8, 5, 2, 11, 12, 1, 14, 15, 16, 13, 10, 3], Int8[4, 11, 6, 7, 8, 5, 2, 13, 12, 3, 14, 1, 16, 9, 10, 15], Int8[4, 13, 6, 7, 8, 5, 2, 15, 12, 9, 14, 3, 16, 1, 10, 11], Int8[4, 5, 6, 3, 8, 9, 2, 15, 12, 13, 14, 11, 16, 1, 10, 7], Int8[4, 5, 6, 3, 8, 11, 2, 9, 12, 7, 14, 15, 16, 13, 10, 1], Int8[4, 5, 6, 3, 8, 13, 2, 11, 12, 1, 14, 7, 16, 9, 10, 15], Int8[4, 5, 6, 3, 8, 15, 2, 13, 12, 9, 14, 1, 16, 7, 10, 11], Int8[4, 1, 6, 11, 8, 9, 2, 3, 12, 5, 14, 15, 16, 13, 10, 7], Int8[4, 1, 6, 13, 8, 11, 2, 3, 12, 7, 14, 5, 16, 9, 10, 15], Int8[4, 1, 6, 15, 8, 13, 2, 3, 12, 9, 14, 7, 16, 5, 10, 11], Int8[4, 1, 6, 9, 8, 15, 2, 3, 12, 13, 14, 11, 16, 7, 10, 5], Int8[4, 11, 6, 9, 8, 1, 2, 7, 12, 3, 14, 15, 16, 13, 10, 5], Int8[4, 13, 6, 11, 8, 1, 2, 7, 12, 5, 14, 3, 16, 9, 10, 15], Int8[4, 15, 6, 13, 8, 1, 2, 7, 12, 9, 14, 5, 16, 3, 10, 11], Int8[4, 9, 6, 15, 8, 1, 2, 7, 12, 13, 14, 11, 16, 5, 10, 3]]
#Any[Int8[4, 15, 6, 7, 8, 5, 2, 9, 12, 13, 14, 11, 16, 3, 10, 1], Int8[4, 9, 6, 7, 8, 5, 2, 11, 12, 1, 14, 15, 16, 13, 10, 3], Int8[4, 11, 6, 7, 8, 5, 2, 13, 12, 3, 14, 1, 16, 9, 10, 15], Int8[4, 13, 6, 7, 8, 5, 2, 15, 12, 9, 14, 3, 16, 1, 10, 11], Int8[4, 5, 6, 3, 8, 9, 2, 15, 12, 13, 14, 11, 16, 1, 10, 7], Int8[4, 5, 6, 3, 8, 11, 2, 9, 12, 7, 14, 15, 16, 13, 10, 1], Int8[4, 5, 6, 3, 8, 13, 2, 11, 12, 1, 14, 7, 16, 9, 10, 15], Int8[4, 5, 6, 3, 8, 15, 2, 13, 12, 9, 14, 1, 16, 7, 10, 11], Int8[4, 1, 6, 11, 8, 9, 2, 3, 12, 5, 14, 15, 16, 13, 10, 7], Int8[4, 1, 6, 13, 8, 11, 2, 3, 12, 7, 14, 5, 16, 9, 10, 15], Int8[4, 1, 6, 15, 8, 13, 2, 3, 12, 9, 14, 7, 16, 5, 10, 11], Int8[4, 1, 6, 9, 8, 15, 2, 3, 12, 13, 14, 11, 16, 7, 10, 5]]
#[[[chemins interdits]]] passent dans les instances plus basses de la generation de chemins
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways)
        #println(x," ",p[x]," ",p,way," ",count_ones(way)<R-3)
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        if nbactive(deb,p,fin,R-2) < mand
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function countway(x,p,cp,fin,way)
        cw = 0
        if count_ones(way) < R-3
                x = p[x]
                cw += countway(x-1,p,cp,fin,way<<2|2)
                if p[x] > 0
                        cw += countway(p[x],p,cp,fin,way<<2|3)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                cw += countway(p[x],p,cp|1<<y,fin,way<<2|3)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                cw += x==fin
        end
        return cw
end
function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end
function witchway(p,cp,gways)
        for deb in 1:2:2k for fin in 1:2:2k
                if hasforbidden(deb,p,fin,gways)
                        #println(deb," ",fin," ",p)
                        return;
                end
        end end
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:2:2k for fin in 1:2:2k if nbactive(deb,p,fin,R-2) < mand
                tmp = countway(deb,p,cp,fin,0)
                if minch > tmp
                        minch = tmp
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,0,spzeros(Int8,2^(2R-4)),one(Int8),gways)
        else
                if 0 in p global zeroin+=1#println("0 in sol ",p)
                elseif p in pool global dejavu+=1#println("DEJA VU ",p)
                else
                        push!(pool,copy(p))
                end
        end
end

function crit4(p)
        gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
        witchway(p,0,gways)
end

function chmain()
        for i in 1:np
                print(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                pq = zeros(Int8,2k)
                for l in 1:k
                        pq[2l-1] = 2p[l]
                end
                global pool = []
                global zeroin = 0
                global dejavu = 0
                crit4(pq)
                println("               ",
                        if length(pool)>0 string(length(pool)," sols     ") else " " end,
                        if dejavu>0 string(dejavu," deja vu     ") else " " end,
                        if zeroin>0 string(zeroin," incomplete     ") else " " end)
                push!(bigpool,deepcopy(pool))
        end
end

using SparseArrays
using Combinatorics

const k=8
const partoch = collect(partitions(k))
const np = length(partoch)

global R=9
global pool = []
global bigpool = []
global zeroin = 0
global dejavu = 0
global mand = 3
@time chmain()


globalscore = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 887040 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 887040 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 560714 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 560714 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 560714 0 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 271869 288845 34204 203468 88654 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 271869 288845 34204 30700 172768 48984 39670 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 271869 14264 308785 0 96785 3049 157390 219 34679 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 248720 23149 4807 175042 143296 17041 26233 96260 91025 29489 23276 8477 173 52 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 253402 18467 3667 115978 60515 41042 130264 16871 71712 49502 72513 17196 28939 5186 1738 48 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 253719 1651 20166 20489 105292 9638 104650 7002 121666 27144 69234 21647 74057 6209 35459 1367 7626 0 24 0 0 0 0 0 0; 0 0 0 0 0 0 0 246978 6741 1089 13267 61904 46529 19880 47887 42162 48959 53905 65698 41839 46080 41070 45552 24962 20981 9317 2108 84 48 0 0 0; 0 0 0 0 0 0 0 0 248024 5695 1000 9924 57619 25740 35180 19158 43307 27261 43617 26895 62102 44389 61860 30809 48059 34964 35112 19003 5942 1244 136 0]

function getscore(dif)
        return [(887040-sum(globalscore[i,dif[i]:32]))/887040 for i in 2:14]
end

a = [0 1 2 3 4 6 6 7 8 9 10 12 12 13]
a = [0 1 2 3 4 6 7 9 10 13 15 17 19 22]
a = [0 1 2 3 4 6 7 9 10 13 15 17 20 23]
a = [0 1 2 3 4 6 8 11 14 16 18 21 25 30]
getscore(a)



function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end

function difrec3(b,dif,dif2,active,adi,adt)
        if b==k
                push!(adi,dif2)
                adt[dif] = active
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec3(b+1,dif,dif2,active,adi,adt)
                elseif c==1 #juste un vert
                        difrec3(b+1,dif,dif2 | 1 << 2b,active,adi,adt)
                elseif c==2 #juste un rouge
                        difrec3(b+1,dif,dif2 | 3 << 2b,active+1,adi,adt)
                elseif c==3 #doubling
                        difrec3(b+1,dif,dif2 | 2 << 2b,active+1,adi,adt)
                        difrec3(b+1,dif,dif2 | 3 << 2b,active+1,adi,adt)
                end
        end
end
function getAD()
        ADI = Vector{Vector{Int}}(undef,2^2k-1)
        ADT = Vector{Int8}(undef,2^2k-1)
        for dif in 1:2^2k-1
                adi = Vector{Int}(undef,0)
                ADI[dif] = adi
                difrec3(0,dif,0,0,adi,ADT)
        end
        return ADI,ADT
end
global ADI,ADT = getAD()
function getdiff3(difs,p)
        newdifs = [findmin(difs[ADI[dif]])[1]+ADT[dif] for dif in 1:2^2k-1]
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end




function difrec(b,dif,dif2,active,newdifs)
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end
function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end
function getdiff2(difs,p)
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end
function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,na,16)
        #io = open("res.txt", "w")
        Threads.@threads for jt in 0:Threads.nthreads()-1
                difs = ones(Int8,2^2k-1)
                pq = a[1]
                for ja in 1:charge
                        j = jt*charge+ja
                        if j<=na
                                pq = a[j]# [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                difs = ones(Int8,2^2k-1)
                                for r in 1:16
                                        difs = getdiff2(difs,pq)
                                        res[j,r] += findmin(difs)[1]-1
                                end
                        end
                end
        end
        #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        #close(io)
        return res
end
function paramain3()
        println("Charge par th: ",charge)
        res = zeros(Int8,na,16)
        #io = open("res.txt", "w")
        Threads.@threads for jt in 0:Threads.nthreads()-1
                difs = ones(Int8,2^2k-1)
                pq = a[1]
                for ja in 1:charge
                        j = jt*charge+ja
                        if j<=na
                                pq = a[j]# [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                difs = ones(Int8,2^2k-1)
                                for r in 1:16
                                        difs = getdiff3(difs,pq)
                                        res[j,r] += findmin(difs)[1]-1
                                end
                        end
                end
        end
        #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        #close(io)
        return res
end



a = Any[Int8[4, 15, 6, 7, 8, 5, 2, 9, 12, 13, 14, 11, 16, 3, 10, 1], Int8[4, 9, 6, 7, 8, 5, 2, 11, 12, 1, 14, 15, 16, 13, 10, 3], Int8[4, 11, 6, 7, 8, 5, 2, 13, 12, 3, 14, 1, 16, 9, 10, 15], Int8[4, 13, 6, 7, 8, 5, 2, 15, 12, 9, 14, 3, 16, 1, 10, 11], Int8[4, 5, 6, 3, 8, 9, 2, 15, 12, 13, 14, 11, 16, 1, 10, 7], Int8[4, 5, 6, 3, 8, 11, 2, 9, 12, 7, 14, 15, 16, 13, 10, 1], Int8[4, 5, 6, 3, 8, 13, 2, 11, 12, 1, 14, 7, 16, 9, 10, 15], Int8[4, 5, 6, 3, 8, 15, 2, 13, 12, 9, 14, 1, 16, 7, 10, 11], Int8[4, 1, 6, 11, 8, 9, 2, 3, 12, 5, 14, 15, 16, 13, 10, 7], Int8[4, 1, 6, 13, 8, 11, 2, 3, 12, 7, 14, 5, 16, 9, 10, 15], Int8[4, 1, 6, 15, 8, 13, 2, 3, 12, 9, 14, 7, 16, 5, 10, 11], Int8[4, 1, 6, 9, 8, 15, 2, 3, 12, 13, 14, 11, 16, 7, 10, 5]]
a = Any[Int8[4, 9, 6, 17, 8, 15, 10, 11, 12, 7, 14, 3, 2, 13, 16, 1, 18, 5], Int8[4, 7, 6, 17, 8, 3, 10, 5, 12, 15, 14, 11, 2, 9, 16, 1, 18, 13], Int8[4, 13, 6, 17, 8, 9, 10, 7, 12, 5, 14, 15, 2, 3, 16, 1, 18, 11], Int8[4, 15, 6, 7, 8, 13, 10, 3, 12, 9, 2, 5, 16, 1, 14, 17, 18, 11], Int8[4, 7, 6, 15, 8, 9, 10, 13, 12, 5, 2, 11, 16, 3, 14, 17, 18, 1], Int8[4, 1, 6, 9, 8, 15, 10, 11, 12, 13, 2, 7, 16, 5, 14, 17, 18, 3], Int8[4, 9, 6, 3, 8, 11, 10, 15, 12, 1, 2, 13, 16, 7, 14, 17, 18, 5], Int8[4, 13, 6, 11, 8, 5, 10, 1, 12, 15, 2, 3, 16, 9, 14, 17, 18, 7], Int8[4, 5, 6, 13, 8, 1, 10, 7, 12, 3, 2, 15, 16, 11, 14, 17, 18, 9], Int8[4, 15, 6, 13, 8, 5, 10, 11, 12, 9, 2, 7, 16, 3, 14, 17, 18, 1], Int8[4, 9, 6, 15, 8, 13, 10, 7, 12, 1, 2, 11, 16, 5, 14, 17, 18, 3], Int8[4, 1, 6, 11, 8, 15, 10, 13, 12, 9, 2, 3, 16, 7, 14, 17, 18, 5], Int8[4, 9, 6, 3, 8, 15, 10, 13, 12, 1, 2, 11, 16, 7, 14, 17, 18, 5], Int8[4, 5, 6, 3, 8, 1, 10, 15, 12, 13, 2, 11, 16, 9, 14, 17, 18, 7], Int8[4, 1, 6, 7, 8, 5, 10, 3, 12, 15, 2, 13, 16, 11, 14, 17, 18, 9], Int8[4, 13, 6, 3, 8, 9, 10, 7, 12, 5, 2, 15, 16, 1, 14, 17, 18, 11], Int8[4, 13, 6, 9, 8, 15, 10, 17, 12, 7, 2, 3, 14, 1, 16, 5, 18, 11], Int8[4, 17, 6, 11, 8, 15, 10, 9, 12, 13, 2, 3, 14, 1, 16, 7, 18, 5], Int8[4, 9, 6, 17, 8, 15, 10, 11, 12, 13, 2, 3, 14, 1, 16, 5, 18, 7], Int8[4, 13, 6, 17, 8, 15, 10, 5, 12, 9, 2, 3, 14, 1, 16, 7, 18, 11], Int8[4, 17, 6, 3, 8, 11, 10, 15, 12, 13, 2, 7, 14, 1, 16, 9, 18, 5], Int8[4, 13, 6, 3, 8, 9, 10, 15, 12, 7, 2, 17, 14, 1, 16, 11, 18, 5], Int8[4, 15, 6, 7, 8, 17, 10, 11, 2, 5, 14, 3, 12, 9, 16, 1, 18, 13], Int8[4, 5, 6, 3, 8, 11, 2, 9, 12, 15, 14, 1, 10, 17, 18, 13, 16, 7], Int8[4, 9, 6, 3, 8, 11, 2, 7, 12, 15, 14, 1, 10, 17, 18, 13, 16, 5], Int8[4, 9, 6, 15, 8, 13, 2, 11, 12, 5, 14, 3, 10, 1, 16, 17, 18, 7], Int8[4, 13, 6, 15, 8, 11, 2, 9, 12, 3, 14, 1, 10, 5, 16, 17, 18, 7], Int8[4, 7, 6, 3, 8, 9, 2, 17, 12, 13, 14, 15, 10, 5, 16, 1, 18, 11], Int8[4, 7, 6, 3, 8, 9, 2, 17, 12, 5, 14, 15, 10, 11, 16, 1, 18, 13], Int8[4, 15, 6, 3, 8, 9, 2, 17, 12, 5, 14, 7, 10, 11, 16, 1, 18, 13], Int8[4, 15, 6, 3, 8, 11, 2, 17, 12, 13, 14, 5, 10, 7, 16, 1, 18, 9], Int8[4, 15, 6, 3, 8, 13, 2, 17, 12, 7, 14, 9, 10, 5, 16, 1, 18, 11], Int8[4, 15, 6, 3, 8, 9, 2, 17, 12, 7, 14, 5, 10, 11, 16, 1, 18, 13], Int8[4, 15, 6, 3, 8, 11, 2, 17, 12, 13, 14, 7, 10, 5, 16, 1, 18, 9], Int8[4, 15, 6, 3, 8, 13, 2, 17, 12, 5, 14, 9, 10, 7, 16, 1, 18, 11], Int8[4, 15, 6, 9, 8, 3, 2, 17, 12, 7, 14, 5, 10, 11, 16, 1, 18, 13], Int8[4, 15, 6, 11, 8, 3, 2, 17, 12, 13, 14, 7, 10, 5, 16, 1, 18, 9], Int8[4, 15, 6, 13, 8, 3, 2, 17, 12, 5, 14, 9, 10, 7, 16, 1, 18, 11], Int8[4, 7, 6, 9, 8, 11, 2, 17, 12, 3, 14, 5, 10, 15, 16, 1, 18, 13], Int8[4, 7, 6, 11, 8, 13, 2, 17, 12, 15, 14, 3, 10, 5, 16, 1, 18, 9], Int8[4, 7, 6, 13, 8, 9, 2, 17, 12, 5, 14, 15, 10, 3, 16, 1, 18, 11], Int8[4, 5, 6, 9, 8, 15, 2, 17, 12, 7, 14, 3, 10, 11, 16, 1, 18, 13], Int8[4, 5, 6, 9, 8, 15, 2, 17, 12, 3, 14, 7, 10, 11, 16, 1, 18, 13], Int8[4, 5, 6, 11, 8, 15, 2, 17, 12, 13, 14, 7, 10, 3, 16, 1, 18, 9], Int8[4, 5, 6, 11, 8, 15, 2, 17, 12, 13, 14, 3, 10, 7, 16, 1, 18, 9], Int8[4, 5, 6, 13, 8, 15, 2, 17, 12, 3, 14, 9, 10, 7, 16, 1, 18, 11], Int8[4, 5, 6, 13, 8, 15, 2, 17, 12, 7, 14, 9, 10, 3, 16, 1, 18, 11], Int8[4, 17, 6, 7, 8, 9, 2, 15, 12, 5, 10, 13, 14, 1, 16, 3, 18, 11], Int8[4, 17, 6, 7, 8, 11, 2, 15, 12, 13, 10, 5, 14, 1, 16, 3, 18, 9], Int8[4, 7, 6, 13, 2, 9, 10, 3, 12, 15, 8, 11, 16, 1, 14, 17, 18, 5], Int8[4, 9, 6, 13, 2, 11, 10, 7, 12, 3, 8, 15, 16, 1, 14, 17, 18, 5], Int8[4, 11, 6, 13, 2, 7, 10, 15, 12, 9, 8, 3, 16, 1, 14, 17, 18, 5], Int8[4, 9, 6, 7, 2, 13, 10, 5, 12, 15, 8, 11, 16, 3, 14, 17, 18, 1], Int8[4, 11, 6, 9, 2, 13, 10, 7, 12, 5, 8, 15, 16, 3, 14, 17, 18, 1], Int8[4, 7, 6, 11, 2, 13, 10, 15, 12, 9, 8, 5, 16, 3, 14, 17, 18, 1], Int8[4, 1, 6, 7, 2, 13, 10, 15, 12, 5, 8, 9, 16, 3, 14, 17, 18, 11], Int8[4, 13, 6, 9, 2, 7, 10, 1, 12, 15, 8, 11, 16, 5, 14, 17, 18, 3], Int8[4, 13, 6, 11, 2, 9, 10, 7, 12, 1, 8, 15, 16, 5, 14, 17, 18, 3], Int8[4, 13, 6, 7, 2, 11, 10, 15, 12, 9, 8, 1, 16, 5, 14, 17, 18, 3], Int8[4, 9, 6, 3, 2, 15, 10, 5, 12, 13, 8, 1, 16, 7, 14, 17, 18, 11], Int8[4, 11, 6, 3, 2, 15, 10, 1, 12, 5, 8, 13, 16, 9, 14, 17, 18, 7], Int8[4, 7, 6, 3, 2, 15, 10, 13, 12, 1, 8, 5, 16, 11, 14, 17, 18, 9], Int8[4, 13, 6, 7, 2, 5, 10, 3, 12, 15, 8, 9, 16, 1, 14, 17, 18, 11], Int8[4, 13, 6, 9, 2, 5, 10, 11, 12, 3, 8, 15, 16, 1, 14, 17, 18, 7], Int8[4, 13, 6, 11, 2, 5, 10, 15, 12, 7, 8, 3, 16, 1, 14, 17, 18, 9]]


k = 9

difrec 86.266454 seconds (1.91 G allocations: 36.795 GiB, 6.37% gc time)
ADTADI 38.212189 seconds (2.18 G allocations: 62.158 GiB, 24.56% gc time)

 76.807808 seconds (3.00 G allocations: 103.753 GiB, 21.95% gc time)


        global na = length(a)
        global charge = div(na,Threads.nthreads())+1


@time res1 = paramain()

@time res3 = paramain3()
#=
 43690
 43691
 43694
 43695
 43706
...
 65514
 65515
 65518
 65519
 65530
 65531
 65534
 65535
=#

function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                if       x&1==0 && hasforrec( x-1,p,deb,fin,way<<1  ,gways) return true end
                return p[x] > 0 && hasforrec(p[x],p,deb,fin,way<<1|1,gways)
        else
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end

function countway(x,p,cp,fin,vv,rr,way)
        cw = 0
        if count_ones(way) < R-1
                if x&1==0 cw+=countway( x-1,p,cp,fin,vv,rr,way<<1  ) end
                if p[x]>0 cw+=countway(p[x],p,cp,fin,vv,rr,way<<1|1)
                else
                        if x&1==1
                        for y in vv if cp>>y & 1==0
                                p[x] = y
                                cw+=countway(p[x],p,cp|1<<y,fin,vv,rr,way<<1|1)
                        end end elseif x in rr
                        for y in 2:2:2k if cp>>y & 1==0
                                p[x] = y
                                cw+=countway(p[x],p,cp|1<<y,fin,vv,rr,way<<1|1)
                        end end else
                        for y in 1:2:2k if cp>>y & 1==0
                                p[x] = y
                                cw+=countway(p[x],p,cp|1<<y,fin,vv,rr,way<<1|1)
                        end end end
                        p[x] = 0
                end
        else cw+= x == fin end
        return cw
end

#=
[7][1]               238 sols     2449 deja vu     55 incomplete     
[6, 1][1]               246 sols     1330 deja vu      
[5, 2][1]               120 sols     1033 deja vu      
[5, 1, 1][1]               150 sols     3434 deja vu      
[4, 3][1]               204 sols     1940 deja vu      
[4, 2, 1][1]               64 sols     608 deja vu      
[4, 1, 1, 1][1]                  
[3, 3, 1][1]               108 sols     450 deja vu      
[3, 2, 2][1]               24 sols     282 deja vu      
[3, 2, 1, 1][1]                  
[3, 1, 1, 1, 1][1]                  
[2, 2, 2, 1][1]                  
[2, 2, 1, 1, 1][1]                  
[2, 1, 1, 1, 1, 1][1]                  
[1, 1, 1, 1, 1, 1, 1][1]                  
[6][2]               276 sols     1448 deja vu      
[6][1, 1]               522 sols     2778 deja vu      
[5, 1][2]               120 sols     436 deja vu      
[5, 1][1, 1]               270 sols     3870 deja vu      
[4, 2][2]               64 sols     315 deja vu      
[4, 2][1, 1]               128 sols     923 deja vu      
[4, 1, 1][2]               64 sols     606 deja vu      
[4, 1, 1][1, 1]               64 sols     606 deja vu      
[3, 3][2]               270 sols     3224 deja vu      
[3, 3][1, 1]               378 sols     3674 deja vu      
[3, 2, 1][2]               24 sols     174 deja vu      
[3, 2, 1][1, 1]               24 sols     174 deja vu      
[3, 1, 1, 1][2]                  
[3, 1, 1, 1][1, 1]                  
[2, 2, 2][2]                  
[2, 2, 2][1, 1]                  
[2, 2, 1, 1][2]                  
[2, 2, 1, 1][1, 1]                  
[2, 1, 1, 1, 1][2]                  
[2, 1, 1, 1, 1][1, 1]                  
[1, 1, 1, 1, 1, 1][2]                  
[1, 1, 1, 1, 1, 1][1, 1]                  
[5][3]               89 sols     507 deja vu     84 incomplete     
[5][2, 1]               209 sols     1675 deja vu     84 incomplete     
[5][1, 1, 1]               688 sols     6573 deja vu     181 incomplete     
[4, 1][3]               204 sols     1341 deja vu      
[4, 1][2, 1]               268 sols     2235 deja vu      
[4, 1][1, 1, 1]               600 sols     4990 deja vu      
[3, 2][3]               270 sols     1981 deja vu      
[3, 2][2, 1]               294 sols     2263 deja vu      
[3, 2][1, 1, 1]               612 sols     4586 deja vu      
[3, 1, 1][3]               108 sols     328 deja vu      
[3, 1, 1][2, 1]               108 sols     328 deja vu      
[3, 1, 1][1, 1, 1]               216 sols     684 deja vu      
[2, 2, 1][3]               24 sols     236 deja vu      
[2, 2, 1][2, 1]               24 sols     236 deja vu      
[2, 2, 1][1, 1, 1]               48 sols     492 deja vu      
[2, 1, 1, 1][3]                  
[2, 1, 1, 1][2, 1]                  
[2, 1, 1, 1][1, 1, 1]                  
[1, 1, 1, 1, 1][3]                  
[1, 1, 1, 1, 1][2, 1]                  
[1, 1, 1, 1, 1][1, 1, 1]                  
[4][4]               192 sols     726 deja vu      
[4][3, 1]               396 sols     2966 deja vu      
[4][2, 2]               256 sols     1158 deja vu      
[4][2, 1, 1]               920 sols     6179 deja vu      
[4][1, 1, 1, 1]               3360 sols     23405 deja vu      
[3, 1][4]               204 sols     1559 deja vu      
[3, 1][3, 1]               312 sols     2339 deja vu      
[3, 1][2, 2]               228 sols     1994 deja vu      
[3, 1][2, 1, 1]               648 sols     4854 deja vu      
[3, 1][1, 1, 1, 1]               2160 sols     15259 deja vu      
[2, 2][4]               64 sols     568 deja vu      
[2, 2][3, 1]               88 sols     880 deja vu      
[2, 2][2, 2]               64 sols     720 deja vu      
[2, 2][2, 1, 1]               176 sols     1676 deja vu      
[2, 2][1, 1, 1, 1]               576 sols     5196 deja vu      
[2, 1, 1][4]               64 sols     512 deja vu      
[2, 1, 1][3, 1]               64 sols     512 deja vu      
[2, 1, 1][2, 2]               64 sols     564 deja vu      
[2, 1, 1][2, 1, 1]               128 sols     1032 deja vu      
[2, 1, 1][1, 1, 1, 1]               384 sols     3324 deja vu      
[1, 1, 1, 1][4]                  
[1, 1, 1, 1][3, 1]                  
[1, 1, 1, 1][2, 2]                  
[1, 1, 1, 1][2, 1, 1]                  
[1, 1, 1, 1][1, 1, 1, 1]                  
[3][5]               87 sols     618 deja vu     42 incomplete     
[3][4, 1]               291 sols     3750 deja vu     42 incomplete     
[3][3, 2]               357 sols     4352 deja vu     42 incomplete     
[3][3, 1, 1]               963 sols     10612 deja vu     91 incomplete     
[3][2, 2, 1]               945 sols     10801 deja vu     87 incomplete     
[3][2, 1, 1, 1]               3504 sols     37531 deja vu     263 incomplete     
[2, 1][5]               120 sols     1107 deja vu      
[2, 1][4, 1]               184 sols     2144 deja vu      
[2, 1][3, 2]               144 sols     1267 deja vu      
[2, 1][3, 1, 1]               392 sols     3896 deja vu      
[2, 1][2, 2, 1]               352 sols     3842 deja vu      
[2, 1][2, 1, 1, 1]               1224 sols     11951 deja vu      
[1, 1, 1][5]               150 sols     1461 deja vu      
[1, 1, 1][4, 1]               150 sols     1461 deja vu      
[1, 1, 1][3, 2]               150 sols     1461 deja vu      
[1, 1, 1][3, 1, 1]               300 sols     2844 deja vu      
[1, 1, 1][2, 2, 1]               300 sols     3060 deja vu      
[1, 1, 1][2, 1, 1, 1]               900 sols     9156 deja vu      
[2][6]               276 sols     1256 deja vu      
[2][5, 1]               396 sols     3240 deja vu      
[2][4, 2]               340 sols     1757 deja vu      
[2][3, 3]               546 sols     3093 deja vu      
[2][4, 1, 1]               920 sols     6937 deja vu      
[2][3, 2, 1]               1030 sols     7512 deja vu      
[2][2, 2, 2]               744 sols     4753 deja vu      
[2][3, 1, 1, 1]               3372 sols     25880 deja vu      
[2][2, 2, 1, 1]               3156 sols     26808 deja vu      
[1, 1][6]               246 sols     1364 deja vu      
[1, 1][5, 1]               396 sols     5212 deja vu      
[1, 1][4, 2]               310 sols     1976 deja vu      
[1, 1][3, 3]               354 sols     1836 deja vu      
[1, 1][4, 1, 1]               856 sols     8839 deja vu      
[1, 1][3, 2, 1]               814 sols     8750 deja vu      
[1, 1][2, 2, 2]               684 sols     5562 deja vu      
[1, 1][3, 1, 1, 1]               2784 sols     28008 deja vu      
[1, 1][2, 2, 1, 1]               2612 sols     27878 deja vu      
[1][7]               238 sols     2392 deja vu     20 incomplete     
[1][6, 1]               484 sols     5214 deja vu     20 incomplete     
[1][5, 2]               358 sols     2891 deja vu     20 incomplete     
[1][4, 3]               442 sols     3883 deja vu     20 incomplete     
[1][5, 1, 1]               1238 sols     13386 deja vu     42 incomplete     
[1][4, 2, 1]               1110 sols     11796 deja vu     44 incomplete     
[1][3, 3, 1]               1238 sols     12493 deja vu     46 incomplete     
[1][3, 2, 2]               944 sols     8480 deja vu     37 incomplete     
[1][4, 1, 1, 1]               4314 sols     47200 deja vu     127 incomplete     
[1][3, 2, 1, 1]               4106 sols     44697 deja vu     130 incomplete     
[1][2, 2, 2, 1]               3515 sols     41113 deja vu     137 incomplete     
548.178847 seconds (3.42 G allocations: 101.510 GiB, 1.08% gc time)
130 cases
=#

using SparseArrays
using Combinatorics


function haspath(x,p,fin,t)
        if t > 0
                if     x&1==0 && haspath( x-1,p,fin,t  ) return true end
                return p[x]>0 && haspath(p[x],p,fin,t-1)
        else
                return x==fin
        end
end
function makeway(x,p,cp,deb,fin,vv,rr,way,pool)
        if count_ones(way) < R-1
                if x&1==0 makeway( x-1,p,cp,deb,fin,vv,rr,way<<1  ,pool) end
                if p[x]>0 makeway(p[x],p,cp,deb,fin,vv,rr,way<<1|1,pool)
                else
                        if x&1==1 #x is green
                        for y in vv if cp>>y & 1==0
                                p[x] = y
                                makeway(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool)
                        end end elseif x in rr #x go to red
                        for y in 2:2:2k if cp>>y & 1==0
                                p[x] = y
                                makeway(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool)
                        end end else
                        for y in 1:2:2k if cp>>y & 1==0
                                p[x] = y
                                makeway(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool)
                        end end end
                        p[x] = 0
                end
        elseif x == fin
                #if !(0 in p) println(p) end
                witchway(p,cp,vv,rr,pool)
        end
end
function witchway(p,cp,vv,rr,pool)
        newdeb = newfin = 0
        for deb in 2k-1:-2:1 for fin in 2:2:2k
                if newdeb == 0 if !haspath(deb,p,fin,R-1)
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,vv,rr,0,pool)
        else
                if 0 in p pool[2]+=1#global zeroin+=1#println("0 in sol ",p)
                elseif p in pool pool[1]+=1;#global dejavu+=1#println("DEJA VU ",p)
                else
                        push!(pool,copy(p))
                end
        end
end
function makechaines()
        tasks = []
        for cl in 1:k-1
                partoch = collect(partitions(k-cl))
                for part in partoch if part[1] > 0
                        used = 0
                        pc = zeros(Int8,k)
                        ii = 1
                        for c in part
                                if c==1
                                        pc[ii] = ii
                                        ii +=1
                                else
                                        for l in ii:ii+c-2
                                                pc[l] = l+1
                                        end
                                        pc[ii+c-1] = ii
                                        ii = ii+c
                                end
                        end
                        p = zeros(Int8,2k)
                        for l in 1:k
                                if pc[l]>0
                                        p[2l-1] = 2pc[l]
                                        used|=1<<2pc[l]
                                end
                        end
                        tofix = ii
                        partoch2 = collect(partitions(cl))
                        for a in 1:k>>1
                                for part2 in partoch2
                                        if length(part2) == a
                                                vv = Vector{Int8}(undef,0)
                                                rr = Vector{Int8}(undef,0)
                                                used2 = used
                                                pa = zeros(Int8,k)
                                                ii = tofix
                                                for c in part2
                                                        push!(vv,2ii)
                                                        if c==1
                                                                ii +=1
                                                        else
                                                                for l in ii:ii+c-2
                                                                        pa[l] = l+1
                                                                end
                                                                ii = ii+c
                                                        end
                                                        push!(rr,2(ii-1)-1)
                                                end
                                                pp = copy(p)
                                                for l in 1:k
                                                        if pa[l]>0
                                                                pp[2l-1] = 2pa[l]
                                                                used2|= 1<<2pa[l]
                                                        end
                                                end
                                                #print(part,part2)
                                                pool = []
                                                push!(pool,0,0)
                                                push!(tasks,[pp,used2,vv,rr,pool,part,part2])
                                                
                                        end
                                end
                        end
                end end
        end
        println(length(tasks)," tasks")
        return tasks
end

function parachaines()
        tasks = makechaines()
        Threads.@threads for t in tasks
                pool = t[5]
                witchway(t[1],t[2],t[3],t[4],pool)
                dejavu = pool[1]
                zeroin = pool[2]
                nbsol = length(pool)-2
                
                println(t[6],t[7],"               ",
                        if nbsol>0 string(nbsol," sols     ") else " " end,
                        if dejavu>0 string(dejavu," deja vu     ") else " " end,
                        if zeroin>0 string(zeroin," incomplete     ") else " " end)
        end
end


global R = 8
global k = 12

#@time makechaines()
@time parachaines()


#R7k11 160.204814 seconds (4.30 G allocations: 127.922 GiB, 10.29% gc time)
#R7k10 67.919474 seconds (1.67 G allocations: 49.544 GiB, 9.41% gc time)
#JULIA_NUM_THREADS=128 ./julia
