
const partoch = collect(partitions(k))
const np = length(partoch)
const allperm = collect(permutations([i for i in 1:k]))
const na = length(allperm)

path = "/Users/agontier/Desktop/crypto/Feistel/sols/"

data = []
for i in 1:22
        include(string(path,i,".jl"))
        push!(data,a)
end

data[1]


dmax = zeros(Int8,22,14)
dmin = zeros(Int8,22,14)

for i in 1:22
        for j in 1:14
                dmax[i,j] = findmax(data[i][2][:,j])[1]
                dmin[i,j] = findmin(data[i][2][:,j])[1]
        end
end


function printtkztab(a)
        nl = 22
        nc = 14
        print("\\begin{tabular}{")
        for c in 1:nc print("|c") end
        println("|c|}")
        println("\\hline")
        for c in 1:nc print(" & R=",c) end
        println("\\\\\\hline")
        for l in 1:nl
                print(partoch[l]," & ")
                for c in 1:nc
                        print(a[l,c])
                        if c < nc print(" & ")
                        else println("\\\\\\hline") end
                end
        end
        println("\\end{tabular}\\\\")
end

function printtkztabline(a)
        nc = 14
        print("global & ")
        for c in 1:nc
                print(a[c])
                if c < nc print(" & ")
                else println("\\\\\\hline") end
        end
end

printtkztab(dmax)
dglobalmax = [findmax(dmax[:,r])[1] for r in 1:14]
printtkztabline(dglobalmax)



function study()
        R = 14
        val = 31
        res = zeros(Int8,14)
        for i in 1:22
                for k in 1:length(allperm)
                        if data[i][2][k,R] == val
                                res = hcat(res,data[i][2][k,:])
                        end end end
        return res
end

data31 = study()
max31 = [findmax(data31[r,:])[1] for r in 1:14]
printtkztabline(max31)

function study()
        R = 14
        val = 30
        res = zeros(Int8,14)
        for i in 1:22
                for k in 1:length(allperm)
                        if data[i][2][k,R] == val
                                res = hcat(res,data[i][2][k,:])
                        end end end
        return res
end

data30 = study()
max30 = [findmax(data30[r,:])[1] for r in 1:14]
printtkztabline(max30)


function study()
        R = 10
        val = 19
        res = zeros(Int8,14)
        for i in 1:22
                for k in 1:length(allperm)
                        if data[i][2][k,R] == val
                                res = hcat(res,data[i][2][k,:])
                        end end end
        return res
end

data19 = study()
max19 = [findmax(data19[r,:])[1] for r in 1:14]
printtkztabline(max19)


function proportions()
        compteur = zeros(Int,14)
        for i in 1:22
                for j in 1:14
                        compteur[j] += length(findall(x->x==dglobalmax[j],data[i][2][:,j]))
                end end
        return compteur
end

nbmax =  proportions()
printtkztabline(nbmax)



function computescore()
        compteur = zeros(Int,22,14,32)
        for i in 1:22
                for j in 1:14
                        for val in 1:32
                                compteur[i,j,val] += length(findall(x->x==val,data[i][2][:,j]))
                        end end end
        return compteur
end
score = computescore()

globalscore = reshape(sum(score,dims = 1),(14,32))



#=
JULIA_NUM_THREADS=128 ./julia
Threads.@Threads:static

cd Applications/Julia-1.5.app/Contents/Resources/julia/bin/ 
JULIA_NUM_THREADS=8 ./julia
=#




using Combinatorics

function difrec(b,dif,dif2,active,newdifs)
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end


#newdif[i] = min(difs[pred1]+x1,difs[pred2]+x2,difs[pred3]+x3,...)

function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end

function getdiff2(difs,p)
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end

function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,na,14)
        #io = open("res.txt", "w")
        Threads.@threads for jt in 0:Threads.nthreads()-1
                for ja in 1:charge
                        j = jt*charge+ja
                        if j<=na
                                pq = a[j]# [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                difs = ones(Int8,2^2k-1)
                                for r in 1:14
                                        difs = getdiff2(difs,pq)
                                        res[j,r] = findmin(difs)[1]-1
                                end
                        end
                end
        end
        #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        #close(io)
        return res
end

#8 coeurs 15 sec pour 300 taches

const k = 8
const partoch = collect(partitions(k))
const np = length(partoch)

path = "/Users/agontier/Desktop/crypto/Feistel/sols/"

include(string(path,"crit1.jl"))
include(string(path,"crit2R9m2.jl"))
include(string(path,"crit2R10m3.jl"))
include(string(path,"crit2R11m5.jl"))
include(string(path,"crit3R9m4.jl"))
include(string(path,"crit3R10m5.jl"))
include(string(path,"crit3R11m6.jl"))
include(string(path,"crit3R12m7.jl"))

getbars(a)


function getbars(a)
        pool = []
        for i in 1:22
                if a[i]!=[]
                        append!(pool,a[i])
                end
        end
        global a = pool
        global na = length(a)
        global charge = div(na,Threads.nthreads())+1 
        @time difs = paramain()
        global count = getcount(difs)
        for i in 10:2:14
                printbars(count[i,:],string("R",i))
                if i+1<=14
                        printbars(count[i+1,:],string("R",i+1))
                end
                println("\\\\~\\\\")
        end
end




pool = []
for i in 1:22
        if a[i]!=[]
                append!(pool,a[i])
        end
end
a = pool



global na = length(a)
global charge = div(na,Threads.nthreads())+1 
@time difs = paramain()




globalscore = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 887040 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 887040 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 560714 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 560714 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 560714 0 326326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 271869 288845 34204 203468 88654 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 271869 288845 34204 30700 172768 48984 39670 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 271869 14264 308785 0 96785 3049 157390 219 34679 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 248720 23149 4807 175042 143296 17041 26233 96260 91025 29489 23276 8477 173 52 0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 253402 18467 3667 115978 60515 41042 130264 16871 71712 49502 72513 17196 28939 5186 1738 48 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 253719 1651 20166 20489 105292 9638 104650 7002 121666 27144 69234 21647 74057 6209 35459 1367 7626 0 24 0 0 0 0 0 0; 0 0 0 0 0 0 0 246978 6741 1089 13267 61904 46529 19880 47887 42162 48959 53905 65698 41839 46080 41070 45552 24962 20981 9317 2108 84 48 0 0 0; 0 0 0 0 0 0 0 0 248024 5695 1000 9924 57619 25740 35180 19158 43307 27261 43617 26895 62102 44389 61860 30809 48059 34964 35112 19003 5942 1244 136 0]

function getscore(dif)
        return [(887040-sum(globalscore[i,dif[i]:32]))/887040 for i in 2:14]
end

prop = zeros(Float64,na,14)
for i in 1:na
        difs2 = getscore(difs[i,:])
        for r in 2:14
                prop[i,r] = difs2[r-1]
        end
end

function printstats()
        println("\\begin{tabular}{|c|c|c|c|c|c|}\n \\hline\n & R10 & R11 & R12 & R13 & R14\\\\\\hline")
        print("min & ")
        for i in 10:13
                print(findmin(prop[:,i])[1]," & ")
        end
        println(findmin(prop[:,14])[1],"\\\\\\hline")
        print("moy &")
        for i in 10:13
                print(sum(prop[:,i])/na," & ")
        end
        println(sum(prop[:,i])/na,"\\\\\\hline")
        print("max &")
        for i in 10:13
                print(findmax(prop[:,i])[1]," & ")
        end
        println(findmax(prop[:,14])[1],"\\\\\\hline")

        println("\\end{tabular}")
end
printstats()

function getcount(pool)
        compteur = zeros(Int,14,32)
        for j in 1:14
                for val in 1:32
                        compteur[j,val] += length(findall(x->x==val,pool[:,j]))
                end end
        return compteur
end

function printbars(count,title)
        nb = sum(count)
        f1 = true
        f2 = true
        valmin = 0
        valmax = 0
        for i in 1:31
                if f1 && count[i] > 0
                        valmin = i
                        f1 = false
                end
                if !f1 && count[i] > 0
                        valmax = i
                end
        end
        
        println("\\begin{tikzpicture}")
        println("\\node[whiteFill] (0) at (6,5)  {",title,"};")
        println("\\begin{axis} [ybar,xmin=",valmin-1,",xmax=",valmax+1,",ymin=0,ymax=",findmax([round(count[i]/sum(count)*100; digits=2) for i in 1:31])[1],"]")
        println("\\addplot coordinates {")
        for i in valmin:valmax
                println("(",i,",",round(count[i]/sum(count)*100; digits=2),")")
        end
        println("};\\end{axis}\\end{tikzpicture}")
end

for i in 4:2:14
        printbars(globalscore[i,:],string("R",i))
        if i+1<=14
                printbars(globalscore[i+1,:],string("R",i+1))
        end
        println("\\\\~\\\\")
end





aa = [[3,0,1,6,2,5,7,4,], [3,0,6,2,7,5,4,1,], [3,0,7,6,4,2,5,1,], [3,0,5,7,2,4,6,1,], [3,0,2,6,1,4,7,5,],
      [3,0,7,4,1,5,2,6,], [3,0,5,2,1,6,4,7,], [3,0,6,4,2,1,5,7,], 
      [3,0,6,2,4,1,7,5,], [3,0,6,2,5,1,4,7,], [3,0,2,7,5,1,4,6,], [3,0,7,2,5,1,6,4,],
      [3,0,1,6,5,2,4,7,], [3,0,1,4,7,2,6,5,], [3,0,5,1,4,2,7,6,], [3,0,1,6,4,2,7,5,],
      [3,0,6,1,5,4,2,7,], [3,0,1,7,4,6,2,5,], [3,0,2,1,7,6,5,4,]]


function makepepkm2(a)
        k = length(a)
        ep = vcat([1],[i for i in 3:k],[2])#decomp en e-sous-cycles de taille 1 et k-1
        p  = vcat([i+k for i in 1:k],[i+1 for i in a])
        return p,ep
end


function onlypcole(p,ep)
        k = length(ep)
        p = vcat([i for i in 1:2k]',p')
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#ordonne les p selon ep identite
                for l in 1:2
                        if p[l,i] > k
                                p[l,i] = ep[p[l,i]-k]+k
                        end
                end
        end
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#met les verts et rouges ensemble
                for l in 1:2
                        if p[l,i]>k #rouges sur les pairs 2:2k
                                p[l,i] = 2(p[l,i] - k)
                        else #verts sur les impairs 1:2k+1
                                p[l,i] = 2(p[l,i]) - 1 
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res
end

pool = []
for i in 1:length(aa)
        (p,ep) = makepepkm2(aa[i])
        push!(pool, onlypcole(p,ep))
end

function onetabdiff(p,ep,cmin,cmax,C)
        res = [0 for i in C]
        global k = length(p)÷2
        pp = onlypcole(p,ep)
        difs = ones(Int8,2^2k-1)
        difs = getdiff2(difs,cmin-2,pp)
        #difs = getdiff(difs,cmin-1,p,ep)        
        for k2 in C
                difs = getdiff2(difs,0,pp)
                res[k2-cmin+1] = findmin(difs)[1]-1
                print(findmin(difs)[1]-1)
                if k2 < cmax print(" & ")
                else println("\\\\\\hline") end
        end
        return res
end

for a in aa
        name = "["
        for i in a name = name * string(i) * " " end
        name = name * "]"
        print(name," & ")
        push!(names,name)
        (p,ep) = makepepkm2(a)
        res = hcat(onetabdiff(p,ep,cmin,cmax,C),res)
end


function transformpermut(p)
        k = length(p) ÷ 2
        p = p.+1
        p = vcat([i for i in 1:2k]',p)
        #show(stdout,"text/plain",p)
        #println()
        ep = [i for i in 1:k]
        for i in 1:2k
                for l in 1:2
                        if p[l,i]%2==0 # rouge ou vert ca depend du sens du schema TWINE est inverse
                                p[l,i] = p[l,i] ÷ 2
                        else
                                p[l,i] = p[l,i] ÷ 2 + k + 1
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res,ep
end


DSMITM = [[15,2,13,4,11,6,3,8,1,10,5,0,7,12,9,14], [7,2,13,4,15,6,1,8,5,10,3,0,11,12,9,14],
          [15,2,9,4,1,6,11,8,3,10,13,0,7,12,5,14], [7,2,11,4,9,6,1,8,15,10,13,0,3,12,5,14],
          [13,2,15,4,11,6,3,8,1,10,5,0,9,12,7,14], [7,2,11,4,9,6,1,8,13,10,15,0,5,12,3,14],
          [13,2,9,4,1,6,11,8,3,10,15,0,5,12,7,14], [7,2,15,4,13,6,1,8,5,10,3,0,9,12,11,14],
          [9,2,7,4,11,6,15,8,13,10,5,0,1,12,3,14], [5,2,9,4,13,6,15,8,3,10,7,0,1,12,11,14],
          [9,2,7,4,11,6,13,8,15,10,5,0,3,12,1,14], [5,2,9,4,15,6,13,8,3,10,7,0,11,12,1,14]]

pool = []

for a in DSMITM
        (p,ep) = transformpermut(a')
        push!(pool, onlypcole(p,ep))
end
