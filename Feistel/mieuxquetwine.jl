using SparseArrays
using Combinatorics
using Random

function nbways(x,p,fin,t)
        if t > 1
                x = p[x]#go to next red immediatly
                return nbways(x-1,p,fin,t-1) +
                        if p[x]>0 nbways(p[x],p,fin,t-2)
                        else 0 end
        else
                if t==1 x=p[x]-1 end
                return x==fin 
        end
end

function countsbox(x,p,fin,t,tab,R)
        if t > 1
                x = p[x]#go to next red
                if p[x]==0 
                        tab[x>>1,t] = true
                        return countsbox(x-1,p,fin,t-1,tab,R)
                else
                        a = countsbox(p[x],p,fin,t-2,copy(tab),R)
                        tab[x>>1,t] = true
                        return a .| countsbox(x-1,p,fin,t-1,tab,R)
                end
        else
                if t==1
                        tab[p[x]>>1,t] = true
                        x=p[x]-1
                end
                if x==fin return tab
                else return BitArray(zeros(Bool,k,R-2)) end #Hardcode
        end
end
function nbsbox(x,p,fin,t,R)
        return sum(countsbox(x,p,fin,t,BitArray(zeros(Bool,k,t)),R))
end
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways,pool,R,X)#make way for the king
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways,pool,R,X)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways,pool,R,X)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways,pool,R,X)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        #if nbsbox(deb,p,fin,R-2,R) < X
                        if nbways(deb,p,fin,R-2) < X
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways,pool,R,X)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways,pool,R,X)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function hasforrec(x,p,deb,fin,way,gways,R)#verify forbiden ways
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways,R) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways,R)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways,R)#verify forbiden ways
        return hasforrec(deb,p,deb,fin,0,gways,R)
end
function addtopool(p,pool)#add to pool without redundancy
        if p in pool pool[1]+=1
        else
                #=minch = 999999
                minsb = 999999
                for deb in 2k-1:-2:1 for fin in 2k-1:-2:1
                        #for deb in 1:2:2k for fin in 1:2:2k
                        minch = min(minch,nbways(deb,p,fin,RR-2))
                        minsb = min(minsb,nbsbox(deb,p,fin,RR-2,RR))
                        #println(deb," -> ",fin," : ",nbways(deb,p,fin,R-2))
                        #println(deb," -> ",fin," : ",nbsbox(deb,p,fin,RR-2,RR))
                        #println()
                end end
                
                if minsb>=twine[RR-7][2]
                        println("(",minch,",",minsb,")  VS  ",twine[RR-7],"    ",p)
                end
                if minsb>twine[RR-7][2]#HARDCODE=#
                        push!(pool,copy(p))
#                end
        end
end
function complete(p,pool)#complete p if needed then add to pool
        if 0 in p
                pool[2]+=1
                i = findfirst(isequal(0),p)
                for v in 1:length(p)
                        if ! (v in p)
                                p[i]=v
                                complete(copy(p),pool)
                        end
                end
        else
                addtopool(p,pool)
        end
end
function witchway(p,cp,gways,pool,R,X)
        #if length(pool)<5 #HARDCODE
                for deb in 1:2:2k for fin in 1:2:2k if hasforbidden(deb,p,fin,gways,R)
                                #on vas des verts aux verts parce qu'ils sont fusionnets avec les rouges a cause des ecycles
                                return;
                        end end end
                #for deb in 2k-1:-2:1 for fin in 2k-1:-2:1 if nbsbox(deb,p,fin,R-2,R) < X
                for deb in 2k-1:-2:1 for fin in 2k-1:-2:1 if nbways(deb,p,fin,R-2) < X 
                        makeway(deb,p,cp,deb,fin,0,zeros(Int8,2^(2R-4)),1,gways,pool,R,X)
                        return;
                end end end
                complete(p,pool)
        #end
end
function makecycle()#build all e-cycle decompositions
        tasks = []
        partoch = collect(partitions(k))
        for part in partoch if part[1] > 0
                pc = zeros(Int8,k)
                ii = 1
                for c in part
                        if c==1
                                pc[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        pc[l] = l+1
                                end
                                pc[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                p = zeros(Int8,2k)
                for l in 1:k
                        if pc[l]>0
                                p[2l-1] = 2pc[l]
                        end
                end
                pool = []
                push!(pool,0,0)
                push!(tasks,[p,pool,part])
        end end
        #println(length(tasks)," tasks")
        return tasks
end
function paramain()#search solutions until pool>=500
        tasks = shuffle(makecycle())
        pool = []
        push!(pool,0,0)
        first = false
        while length(pool)<5 #HARDCODE
                println("----------- R = ",RR," -----------")
                Threads.@threads for i in 1:np
                        R = RR
                        X = twine[RR-7][1]+1
                        t = tasks[i]
                        #pool = t[2]
                        gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
                        witchway(t[1],0,gways,pool,R,X)
                        dejavu = pool[1]
                        zeroin = pool[2]
                        nbsol = length(pool)-2
                        if !first && nbsol>0
                                first=true
                                print(RR)
                        end
                        if false #nbsol > 0 || true#&& false
                                println("\n",t[3],"               ",
                                        if nbsol>0 string(nbsol," sols     ") else " " end,
                                        if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                        if zeroin>0 string(zeroin," incomplete     ") else " " end)
                        end
                end
                global RR = RR+1
        end
        return pool
end


#p = [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1#TWINE
#p = [0,5,4,1,12,7,8,3,6,13,2,9,10,15,14,11].+1
#p = [31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10, 15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26].+1#warp

const k = Int8(8)
#const R = 9

function getmin(R,p)
        minch = minimum([nbways(i,p,j,R-2) for i in 1:2:2k for j in 1:2:2k])
        minsb = minimum([nbsbox(i,p,j,R-2,R) for i in 1:2:2k for j in 1:2:2k])
        #println("R",R,"   ",minch,"   ",minsb)
        println("(",minch,",",minsb,"),")
        return minch,minsb
end

#=
for R in 8:20
        println([nbways(i,p,j,R-2) for i in 1:2:2k for j in 1:2:2k])
        getmin(R,p)
end
=#
function checkp(p)
        global twine = [(1,2),(2,6),(3,8),(5,14),(9,22),(16,30),(26,38),(44,46),(71,54),(118,62),(191,70),(314,78),(509,86),(832,94),(1346,102),(2191,110),(3546,118),(5760,126),(9320,134),(15116,142),(24458,150),(39632,158),(64126,166)]
                #[(1,7),(2,18),(3,29),(5,40),(9,49),(16,58),(26,67),(44,75),(71,83),(118,91),(191,99),(314,107),(509,115),(832,123),(1346,131),(2191,139),(3546,147)]
        flag = true
        for R in 8:24
                a,b = getmin(R,p)
                flag &= a>twine[R-7][1]
                flag &= b>twine[R-7][2]
        end
        return flag
end
checkp(p)

global twine = [(1,2),(2,6),(3,8),(5,14),(9,22),(16,30),(26,38),(44,46),(71,54),(118,62),(191,70),(314,78),(509,86),(832,94),(1346,102),(2191,110),(3546,118),(5760,126),(9320,134),(15116,142),(24458,150),(39632,158),(64126,166)]
#global twine = [(1,7),(2,18),(3,29),(5,40),(9,49),(16,58),(26,67),(44,75),(71,83),(118,91),(191,99),(314,107),(509,115),(832,123),(1346,131),(2191,139),(3546,147)]


global XX=Int8(1)
global RR=Int8(8)

global partoch = collect(partitions(k))
global np = length(partoch)

function getmieux()
        global XX = Int8(2)
        global RR = Int(8)
        return paramain()
end
bigpool = getmieux()



p = bigpool[4]#[4, 9, 6, 13, 8, 15, 2, 11, 12, 5, 14, 3, 10, 7, 16, 1]
checkp(p)

function s1(p)
        for i in 1:length(p)
                if i<length(p)
                        print(string(i,'/',p[i],"/,"))
                else
                        print(string(i,'/',p[i],"/"))
                end
        end
        println()
end
function s2(p)
        pr = "\\node[noeudrou] ("
        pv = "\\node[noeudver] ("
        cords = [") at (6,14)  {",
                 ") at (8,14)  {",
                 ") at (11,13)  {",
                 ") at (13,11)  {",
                 ") at (14,8)  {",
                 ") at (14,6)  {",
                 ") at (13,3)  {",
                 ") at (11,1)  {",
                 ") at (8,0)  {",
                 ") at (6,0)  {",
                 ") at (3,1)  {",
                 ") at (1,3)  {",
                 ") at (0,6)  {",
                 ") at (0,8)  {",
                 ") at (1,11)  {",
                 ") at (3,13)  {"]
        post = "};"
        i = 1
        ic = 1
        tabou = []
        while length(tabou)<length(p)
                if i in tabou
                        i = i+1
                else
                        if i&1==0
                                println(string(pr,i,cords[ic],i-1,post))
                        else
                                println(string(pv,i,cords[ic],i-1,post))
                        end
                        push!(tabou,i)
                        i=p[i]
                        ic = ic+1
                end
        end
end
function grap(p)
        k = length(p)>>1
        println("\\begin{tikzpicture}[scale=0.5,auto=left]")
        s2(p)
        println("\\foreach \\from/\\to/\\arr in {")
        s1(p)
        println(string("}\\draw[tv] (\\from) -- (\\to)node[sloped,above,legende]{\\arr};
\\foreach \\from/\\to/\\arr in {"))
        for i in 1:k-1
                print(2i,'/',2i-1,"/,")
        end
        print(2k,'/',2k-1,"/")
        println(string("}\\draw[tb] (\\from) to[] (\\to)node[sloped,above,legende]{\\arr};
\\end{tikzpicture}"))
        println()
end

p = [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1#TWINE


function transformpermut(p)
        k = length(p) ÷ 2
        p = p.+1
        p = vcat([i for i in 1:2k]',p)
        #show(stdout,"text/plain",p)
        #println()
        ep = [i for i in 1:k]
        for i in 1:2k
                for l in 1:2
                        if p[l,i]%2==0 # rouge ou vert ca depend du sens du schema TWINE est inverse
                                p[l,i] = p[l,i] ÷ 2
                        else
                                p[l,i] = p[l,i] ÷ 2 + k + 1
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res,ep
end

function onlypcole(p,ep)
        k = length(ep)
        p = vcat([i for i in 1:2k]',p')
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#ordonne les p selon ep identite
                for l in 1:2
                        if p[l,i] > k
                                p[l,i] = ep[p[l,i]-k]+k
                        end
                end
        end
        #show(stdout,"text/plain",p)
        #println()
        for i in 1:2k#met les verts et rouges ensemble
                for l in 1:2
                        if p[l,i]>k #rouges sur les pairs 2:2k
                                p[l,i] = 2(p[l,i] - k)
                        else #verts sur les impairs 1:2k+1
                                p[l,i] = 2(p[l,i]) - 1 
                        end
                end
        end
        res = [0 for i in 1:2k]
        for i in 1:2k
                res[p[1,i]] = p[2,i]
        end
        #show(stdout,"text/plain",p)
        #println()
        return res
end

p = [5 0 1 4 7 12 3 8 13 6 9 2 15 10 11 14]
p,ep = transformpermut(p)

p = onlypcole(p,ep)

grap(p)
checkp(p)

p = [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1#TWINE
checkp(p)


pp = [#4-DR = 10
        [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        [4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1],
        [4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        [4, 13, 6, 1, 2, 11, 10, 15, 12, 7, 8, 5, 14, 9, 16, 3],
        [4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
        [4, 11, 6, 13, 2, 3, 10, 15, 12, 7, 8, 1, 14, 9, 16, 5],
        [4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        [4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        [4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        [4, 11, 6, 15, 2, 3, 10, 13, 12, 7, 8, 1, 14, 5, 16, 9],
        [4, 5, 6, 11, 2, 15, 10, 13, 12, 7, 8, 3, 14, 1, 16, 9],
        [4, 15, 6, 1, 2, 11, 10, 13, 12, 7, 8, 5, 14, 3, 16, 9],
        [4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        [4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        [4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11]]


for p in pp
        grap(p)
end

pp2 = [
        [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        [4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        [4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
        [4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        [4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        [4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        [4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        [4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        [4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11]]
for p in pp2
        println(p in pp)
end

range = 32
 res = zeros(Int8,length(tasks),range)

difs = ones(Int8,2^2k-1)


for r in 1:range
                               #difs = getdiff3(difs,p,adi,adt)
                               difs = getdiff2(difs,p)
                               res[i,r] += findmin(difs)[1]-1
                       end

pi = [0 1 2 3 4 6 8 11 14 19 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64]
tw = [0 1 2 3 4 6 8 11 14 18 22 24 27 30 32 35 36 39 41 44 45 48 50 53 54 57 59 62 63 66 68 71]

R = 9
nbsbox(1,p,3,R-2,R)


function test(a)
        a[2,2] = true
end
test(copy(a))




const k = Int8(8)

global partoch = collect(partitions(k))
global np = length(partoch)

const X = Int8(6)
const R = Int8(11)
function tmp()
        tasks = shuffle(makecycle())
        pool = []
        push!(pool,0,0)
        Threads.@threads for i in 1:np
                t = tasks[i]
                #pool = t[2]
                gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
                witchway(t[1],0,gways,pool,R,X)
                dejavu = pool[1]
                zeroin = pool[2]
                nbsol = length(pool)-2
                if false #nbsol > 0 || true#&& false
                        println("\n",t[3],"               ",
                                if nbsol>0 string(nbsol," sols     ") else " " end,
                                if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                if zeroin>0 string(zeroin," incomplete     ") else " " end)
                end
        end
        return pool
end
pool = tmp()



pp4 = [#4-DR = 10
       [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
       [4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1],
       [4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
       [4, 13, 6, 1, 2, 11, 10, 15, 12, 7, 8, 5, 14, 9, 16, 3],
       [4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
       [4, 11, 6, 13, 2, 3, 10, 15, 12, 7, 8, 1, 14, 9, 16, 5],
       [4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
       [4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
       [4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
       [4, 11, 6, 15, 2, 3, 10, 13, 12, 7, 8, 1, 14, 5, 16, 9],
       [4, 5, 6, 11, 2, 15, 10, 13, 12, 7, 8, 3, 14, 1, 16, 9],
       [4, 15, 6, 1, 2, 11, 10, 13, 12, 7, 8, 5, 14, 3, 16, 9],
       [4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
       [4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
       [4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11]]
pp6 = [#6-DR = 11
       [4, 7, 6, 13, 8, 15, 10, 5, 2, 11, 14, 9, 12, 3, 16, 1],
       [4, 13, 6, 7, 2, 15, 10, 3, 12, 9, 8, 5, 14, 11, 16, 1],
       [4, 13, 6, 9, 2, 15, 10, 5, 12, 3, 8, 11, 14, 7, 16, 1],
       [4, 15, 6, 13, 2, 7, 10, 5, 12, 9, 8, 1, 14, 11, 16, 3],
       [4, 15, 6, 13, 2, 9, 10, 1, 12, 5, 8, 11, 14, 7, 16, 3],
       [4, 7, 6, 1, 8, 13, 10, 15, 2, 11, 14, 9, 12, 5, 16, 3],
       [4, 7, 6, 15, 2, 13, 10, 1, 12, 9, 8, 3, 14, 11, 16, 5],
       [4, 9, 6, 15, 2, 13, 10, 3, 12, 1, 8, 11, 14, 7, 16, 5],
       [4, 9, 6, 3, 2, 11, 10, 13, 12, 1, 8, 15, 14, 5, 16, 7],
       [4, 11, 6, 9, 2, 5, 10, 13, 12, 3, 8, 15, 14, 1, 16, 7],
       [4, 7, 6, 1, 8, 11, 10, 15, 2, 13, 14, 5, 12, 9, 16, 3],
       [4, 1, 6, 11, 2, 9, 10, 13, 12, 5, 8, 15, 14, 3, 16, 7],
       [4, 11, 6, 9, 8, 3, 10, 13, 2, 15, 14, 1, 12, 7, 16, 5],
       [4, 11, 6, 3, 2, 7, 10, 15, 12, 13, 8, 1, 14, 5, 16, 9],
       [4, 7, 6, 11, 2, 5, 10, 15, 12, 13, 8, 3, 14, 1, 16, 9],
       [4, 1, 6, 7, 2, 11, 10, 15, 12, 13, 8, 5, 14, 3, 16, 9],
       [4, 5, 6, 9, 8, 1, 10, 15, 12, 13, 2, 7, 14, 11, 16, 3],
       [4, 9, 6, 11, 8, 1, 10, 13, 2, 15, 14, 3, 12, 7, 16, 5],
       [4, 7, 6, 3, 2, 9, 10, 1, 12, 15, 8, 13, 14, 5, 16, 11],
       [4, 9, 6, 7, 2, 5, 10, 3, 12, 15, 8, 13, 14, 1, 16, 11],
       [4, 1, 6, 9, 2, 7, 10, 5, 12, 15, 8, 13, 14, 3, 16, 11],
       [4, 13, 6, 9, 8, 3, 10, 11, 2, 15, 14, 7, 12, 1, 16, 5],
       [4, 9, 6, 7, 8, 11, 10, 3, 12, 15, 2, 13, 14, 1, 16, 5],
       [4, 15, 6, 11, 8, 1, 10, 5, 2, 13, 14, 3, 12, 9, 16, 7],
       [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
       [4, 9, 6, 11, 8, 13, 2, 15, 12, 7, 10, 3, 14, 5, 16, 1],
       [4, 15, 6, 1, 8, 11, 10, 3, 2, 13, 14, 5, 12, 9, 16, 7],
       [4, 11, 6, 9, 8, 13, 2, 15, 12, 3, 10, 7, 14, 5, 16, 1],
       [4, 15, 6, 13, 8, 1, 10, 5, 2, 11, 14, 9, 12, 3, 16, 7],
       [4, 9, 6, 13, 8, 11, 2, 15, 12, 7, 10, 5, 14, 3, 16, 1],
       [4, 11, 6, 13, 8, 9, 2, 15, 12, 5, 10, 7, 14, 3, 16, 1],
       [4, 13, 6, 15, 8, 11, 10, 3, 2, 7, 14, 5, 12, 1, 16, 9],
       [4, 13, 6, 15, 8, 3, 10, 11, 2, 5, 14, 7, 12, 1, 16, 9],
       [4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
       [4, 11, 6, 15, 8, 13, 10, 3, 2, 7, 14, 1, 12, 5, 16, 9],
       [4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
       [4, 7, 6, 3, 8, 13, 10, 15, 12, 9, 14, 5, 2, 11, 16, 1],
       [4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
       [4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
       [4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
       [4, 5, 6, 3, 8, 13, 10, 11, 12, 15, 2, 7, 14, 9, 16, 1],
       [4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
       [4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
       [4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11],
       [4, 13, 6, 9, 2, 7, 10, 5, 12, 15, 8, 11, 14, 3, 16, 1]]
for p in pp6
        #println(p in pp6)
        if p in pp4
                println(p)
        end
end

pp4inpp6 = [[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
            [4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
            [4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
            [4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
            [4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
            [4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
            [4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
            [4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
            [4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11]]

pp4alone = [[4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1],
            [4, 13, 6, 1, 2, 11, 10, 15, 12, 7, 8, 5, 14, 9, 16, 3],
            [4, 11, 6, 13, 2, 3, 10, 15, 12, 7, 8, 1, 14, 9, 16, 5],
            [4, 11, 6, 15, 2, 3, 10, 13, 12, 7, 8, 1, 14, 5, 16, 9],
            [4, 5, 6, 11, 2, 15, 10, 13, 12, 7, 8, 3, 14, 1, 16, 9],
            [4, 15, 6, 1, 2, 11, 10, 13, 12, 7, 8, 5, 14, 3, 16, 9]]


pp6alone = [[4, 7, 6, 13, 8, 15, 10, 5, 2, 11, 14, 9, 12, 3, 16, 1],
            [4, 13, 6, 7, 2, 15, 10, 3, 12, 9, 8, 5, 14, 11, 16, 1],
            [4, 13, 6, 9, 2, 15, 10, 5, 12, 3, 8, 11, 14, 7, 16, 1],
            [4, 15, 6, 13, 2, 7, 10, 5, 12, 9, 8, 1, 14, 11, 16, 3],
            [4, 15, 6, 13, 2, 9, 10, 1, 12, 5, 8, 11, 14, 7, 16, 3],
            [4, 7, 6, 1, 8, 13, 10, 15, 2, 11, 14, 9, 12, 5, 16, 3],
            [4, 7, 6, 15, 2, 13, 10, 1, 12, 9, 8, 3, 14, 11, 16, 5],
            [4, 9, 6, 15, 2, 13, 10, 3, 12, 1, 8, 11, 14, 7, 16, 5],
            [4, 9, 6, 3, 2, 11, 10, 13, 12, 1, 8, 15, 14, 5, 16, 7],
            [4, 11, 6, 9, 2, 5, 10, 13, 12, 3, 8, 15, 14, 1, 16, 7],
            [4, 7, 6, 1, 8, 11, 10, 15, 2, 13, 14, 5, 12, 9, 16, 3],
            [4, 1, 6, 11, 2, 9, 10, 13, 12, 5, 8, 15, 14, 3, 16, 7],
            [4, 11, 6, 9, 8, 3, 10, 13, 2, 15, 14, 1, 12, 7, 16, 5],
            [4, 11, 6, 3, 2, 7, 10, 15, 12, 13, 8, 1, 14, 5, 16, 9],
            [4, 7, 6, 11, 2, 5, 10, 15, 12, 13, 8, 3, 14, 1, 16, 9],
            [4, 1, 6, 7, 2, 11, 10, 15, 12, 13, 8, 5, 14, 3, 16, 9],
            [4, 5, 6, 9, 8, 1, 10, 15, 12, 13, 2, 7, 14, 11, 16, 3],
            [4, 9, 6, 11, 8, 1, 10, 13, 2, 15, 14, 3, 12, 7, 16, 5],
            [4, 7, 6, 3, 2, 9, 10, 1, 12, 15, 8, 13, 14, 5, 16, 11],
            [4, 9, 6, 7, 2, 5, 10, 3, 12, 15, 8, 13, 14, 1, 16, 11],
            [4, 1, 6, 9, 2, 7, 10, 5, 12, 15, 8, 13, 14, 3, 16, 11],
            [4, 13, 6, 9, 8, 3, 10, 11, 2, 15, 14, 7, 12, 1, 16, 5],
            [4, 9, 6, 7, 8, 11, 10, 3, 12, 15, 2, 13, 14, 1, 16, 5],
            [4, 15, 6, 11, 8, 1, 10, 5, 2, 13, 14, 3, 12, 9, 16, 7],
            [4, 9, 6, 11, 8, 13, 2, 15, 12, 7, 10, 3, 14, 5, 16, 1],
            [4, 15, 6, 1, 8, 11, 10, 3, 2, 13, 14, 5, 12, 9, 16, 7],
            [4, 11, 6, 9, 8, 13, 2, 15, 12, 3, 10, 7, 14, 5, 16, 1],
            [4, 15, 6, 13, 8, 1, 10, 5, 2, 11, 14, 9, 12, 3, 16, 7],
            [4, 9, 6, 13, 8, 11, 2, 15, 12, 7, 10, 5, 14, 3, 16, 1],
            [4, 11, 6, 13, 8, 9, 2, 15, 12, 5, 10, 7, 14, 3, 16, 1],
            [4, 13, 6, 15, 8, 11, 10, 3, 2, 7, 14, 5, 12, 1, 16, 9],
            [4, 13, 6, 15, 8, 3, 10, 11, 2, 5, 14, 7, 12, 1, 16, 9],
            [4, 11, 6, 15, 8, 13, 10, 3, 2, 7, 14, 1, 12, 5, 16, 9],
            [4, 7, 6, 3, 8, 13, 10, 15, 12, 9, 14, 5, 2, 11, 16, 1],
            [4, 5, 6, 3, 8, 13, 10, 11, 12, 15, 2, 7, 14, 9, 16, 1],
            [4, 13, 6, 9, 2, 7, 10, 5, 12, 15, 8, 11, 14, 3, 16, 1]]

using Combinatorics

function enumper(p,permuts,pool)# enumerate all symetries of p
        Threads.@threads for prism in permuts
                indices = Int8[i for i in 1:2k]
                ref = vcat(indices',p')
                newsol = zeros(Int8,2k)
                p2 = zeros(Int8,2,2k)
                paireind = impariteind = paireval = impariteval = Int8(0)
                #fill!(newsol,0)
                #fill!(p2,0)
                for i in 1:2k
                        paireind = (ref[1,i]+1)>>1
                        impariteind = ref[1,i]&1==1
                        paireval = (ref[2,i]+1)>>1
                        impariteval = ref[2,i]&1==1
                        
                        p2[1,i] = 2prism[paireind]-impariteind
                        p2[2,i] = 2prism[paireval]-impariteval
                end
                #show(stdout,"text/plain",p2)
                #println()
                for i in 1:2k
                        ind = findfirst(isequal(i),p2[1,:])
                        newsol[i] = p2[2,ind]
                end
                if newsol in pool
                        println(newsol,",")
                end
                #println(newsol)
        end
end

pairs = Int8[i for i in 1:k]
permuts = collect(permutations(pairs))

#const k = Int8(8)
#enumper([6, 15, 10, 11, 16, 5, 4, 1, 14, 7, 2, 3, 8, 13, 12, 9],permuts,[[2, 5, 6, 1, 14, 7, 10, 3, 8, 13, 4, 9, 12, 15, 16, 11]])# LBlock == TWINE ? NON

enumper(pp4inpp6[1],permuts,pp6)


sympi = [
        Int8[4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
        Int8[4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        Int8[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        Int8[4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11],
        Int8[4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        Int8[4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        Int8[4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        Int8[4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        Int8[4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        Int8[4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        Int8[4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        Int8[4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        Int8[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        Int8[4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11],
        Int8[4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        Int8[4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        Int8[4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        Int8[4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5]]
for p in pp4inpp6
        #println(p in pp6)
        if !(p in sympi)
                println(p)
        end
end
pi = [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1]# est bien la seule permut a combiner les deux

enumper(pp4alone[1],permuts,pp4)


sym4 = [Int8[4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1],
        Int8[4, 13, 6, 1, 2, 11, 10, 15, 12, 7, 8, 5, 14, 9, 16, 3],
        Int8[4, 15, 6, 1, 2, 11, 10, 13, 12, 7, 8, 5, 14, 3, 16, 9],
        Int8[4, 5, 6, 11, 2, 15, 10, 13, 12, 7, 8, 3, 14, 1, 16, 9],
        Int8[4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        Int8[4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11],
        Int8[4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        Int8[4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        Int8[4, 11, 6, 13, 2, 3, 10, 15, 12, 7, 8, 1, 14, 9, 16, 5],
        Int8[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        Int8[4, 15, 6, 1, 2, 9, 10, 11, 12, 5, 8, 13, 14, 3, 16, 7],
        Int8[4, 11, 6, 15, 2, 3, 10, 13, 12, 7, 8, 1, 14, 5, 16, 9],
        Int8[4, 15, 6, 1, 2, 7, 10, 5, 12, 13, 8, 9, 14, 3, 16, 11],
        Int8[4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        Int8[4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
        Int8[4, 7, 6, 15, 2, 3, 10, 1, 12, 13, 8, 9, 14, 5, 16, 11],
        Int8[4, 5, 6, 7, 2, 15, 10, 3, 12, 13, 8, 9, 14, 1, 16, 11],
        Int8[4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        Int8[4, 15, 6, 1, 2, 11, 10, 13, 12, 7, 8, 5, 14, 3, 16, 9],
        Int8[4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        Int8[4, 13, 6, 1, 2, 9, 10, 11, 12, 5, 8, 15, 14, 7, 16, 3],
        Int8[4, 9, 6, 15, 2, 3, 10, 11, 12, 1, 8, 13, 14, 5, 16, 7],
        Int8[4, 9, 6, 13, 2, 3, 10, 11, 12, 1, 8, 15, 14, 7, 16, 5],
        Int8[4, 13, 6, 1, 2, 11, 10, 15, 12, 7, 8, 5, 14, 9, 16, 3],
        Int8[4, 11, 6, 15, 2, 3, 10, 13, 12, 7, 8, 1, 14, 5, 16, 9],
        Int8[4, 5, 6, 9, 2, 15, 10, 11, 12, 3, 8, 13, 14, 1, 16, 7],
        Int8[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
        Int8[4, 11, 6, 13, 2, 3, 10, 15, 12, 7, 8, 1, 14, 9, 16, 5],
        Int8[4, 5, 6, 11, 2, 15, 10, 13, 12, 7, 8, 3, 14, 1, 16, 9],
        Int8[4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1]]

for p in pp4alone
        if !(p in sym4)
                println(p)
        end
end

pp4unique = [4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1]# seule permut pp4alone

enumper(pp6alone[1],permuts,pp6)

sym61 = [Int8[4, 7, 6, 13, 8, 15, 10, 5, 2, 11, 14, 9, 12, 3, 16, 1],
         Int8[4, 13, 6, 15, 8, 3, 10, 11, 2, 5, 14, 7, 12, 1, 16, 9],
         Int8[4, 9, 6, 11, 8, 1, 10, 13, 2, 15, 14, 3, 12, 7, 16, 5],
         Int8[4, 15, 6, 1, 8, 11, 10, 3, 2, 13, 14, 5, 12, 9, 16, 7]]

pp6alone2 = [p for p in pp6alone if !(p in sym61)]

enumper(pp6alone2[1],permuts,pp6alone2)

sym62 = [Int8[4, 13, 6, 7, 2, 15, 10, 3, 12, 9, 8, 5, 14, 11, 16, 1],
         Int8[4, 9, 6, 3, 2, 11, 10, 13, 12, 1, 8, 15, 14, 5, 16, 7],
         Int8[4, 7, 6, 3, 2, 9, 10, 1, 12, 15, 8, 13, 14, 5, 16, 11],
         Int8[4, 7, 6, 15, 2, 13, 10, 1, 12, 9, 8, 3, 14, 11, 16, 5],
         Int8[4, 9, 6, 7, 2, 5, 10, 3, 12, 15, 8, 13, 14, 1, 16, 11],
         Int8[4, 11, 6, 3, 2, 7, 10, 15, 12, 13, 8, 1, 14, 5, 16, 9],
         Int8[4, 1, 6, 9, 2, 7, 10, 5, 12, 15, 8, 13, 14, 3, 16, 11],
         Int8[4, 11, 6, 9, 2, 5, 10, 13, 12, 3, 8, 15, 14, 1, 16, 7],
         Int8[4, 9, 6, 15, 2, 13, 10, 3, 12, 1, 8, 11, 14, 7, 16, 5],
         Int8[4, 1, 6, 11, 2, 9, 10, 13, 12, 5, 8, 15, 14, 3, 16, 7],
         Int8[4, 15, 6, 13, 2, 7, 10, 5, 12, 9, 8, 1, 14, 11, 16, 3],
         Int8[4, 13, 6, 9, 2, 15, 10, 5, 12, 3, 8, 11, 14, 7, 16, 1],
         Int8[4, 7, 6, 11, 2, 5, 10, 15, 12, 13, 8, 3, 14, 1, 16, 9],
         Int8[4, 15, 6, 13, 2, 9, 10, 1, 12, 5, 8, 11, 14, 7, 16, 3],
         Int8[4, 1, 6, 7, 2, 11, 10, 15, 12, 13, 8, 5, 14, 3, 16, 9]]

pp6alone3 = [p for p in pp6alone2 if !(p in sym62)]

enumper(pp6alone3[1],permuts,pp6alone3)

sym63 = [Int8[4, 7, 6, 1, 8, 13, 10, 15, 2, 11, 14, 9, 12, 5, 16, 3],
        Int8[4, 13, 6, 15, 8, 11, 10, 3, 2, 7, 14, 5, 12, 1, 16, 9],
        Int8[4, 11, 6, 9, 8, 3, 10, 13, 2, 15, 14, 1, 12, 7, 16, 5],
        Int8[4, 15, 6, 11, 8, 1, 10, 5, 2, 13, 14, 3, 12, 9, 16, 7],
        Int8[4, 15, 6, 13, 8, 1, 10, 5, 2, 11, 14, 9, 12, 3, 16, 7],
        Int8[4, 13, 6, 9, 8, 3, 10, 11, 2, 15, 14, 7, 12, 1, 16, 5],
        Int8[4, 11, 6, 15, 8, 13, 10, 3, 2, 7, 14, 1, 12, 5, 16, 9],
        Int8[4, 7, 6, 1, 8, 11, 10, 15, 2, 13, 14, 5, 12, 9, 16, 3]]

pp6alone4 = [p for p in pp6alone3 if !(p in sym63)]

enumper(pp6alone4[1],permuts,pp6alone4)

sym64 = [Int8[4, 5, 6, 9, 8, 1, 10, 15, 12, 13, 2, 7, 14, 11, 16, 3],
         Int8[4, 9, 6, 7, 8, 11, 10, 3, 12, 15, 2, 13, 14, 1, 16, 5]]

pp6alone5 = [p for p in pp6alone4 if !(p in sym64)]

enumper(pp6alone5[1],permuts,pp6alone5)

sym65 = [Int8[4, 9, 6, 11, 8, 13, 2, 15, 12, 7, 10, 3, 14, 5, 16, 1],
         Int8[4, 11, 6, 9, 8, 13, 2, 15, 12, 3, 10, 7, 14, 5, 16, 1]]

pp6alone6 = [p for p in pp6alone5 if !(p in sym65)]

enumper(pp6alone6[1],permuts,pp6alone6)

sym66 = [Int8[4, 9, 6, 13, 8, 11, 2, 15, 12, 7, 10, 5, 14, 3, 16, 1],
         Int8[4, 11, 6, 13, 8, 9, 2, 15, 12, 5, 10, 7, 14, 3, 16, 1]]

pp6alone7 = [p for p in pp6alone6 if !(p in sym66)]

enumper(pp6alone7[1],permuts,pp6alone7)

pp6unique = [ [4, 7, 6, 3, 8, 13, 10, 15, 12, 9, 14, 5, 2, 11, 16, 1],
              [4, 5, 6, 3, 8, 13, 10, 11, 12, 15, 2, 7, 14, 9, 16, 1],
              [4, 13, 6, 9, 2, 7, 10, 5, 12, 15, 8, 11, 14, 3, 16, 1],
              [4, 9, 6, 13, 8, 11, 2, 15, 12, 7, 10, 5, 14, 3, 16, 1],
              [4, 9, 6, 11, 8, 13, 2, 15, 12, 7, 10, 3, 14, 5, 16, 1],
              [4, 5, 6, 9, 8, 1, 10, 15, 12, 13, 2, 7, 14, 11, 16, 3],
              [4, 7, 6, 1, 8, 13, 10, 15, 2, 11, 14, 9, 12, 5, 16, 3],
              [4, 13, 6, 7, 2, 15, 10, 3, 12, 9, 8, 5, 14, 11, 16, 1],
              [4, 7, 6, 13, 8, 15, 10, 5, 2, 11, 14, 9, 12, 3, 16, 1]]




ppunique = [[4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1],
            [4, 5, 6, 11, 2, 13, 10, 15, 12, 7, 8, 3, 14, 9, 16, 1],
            [4, 7, 6, 3, 8, 13, 10, 15, 12, 9, 14, 5, 2, 11, 16, 1],
            [4, 5, 6, 3, 8, 13, 10, 11, 12, 15, 2, 7, 14, 9, 16, 1],
            [4, 13, 6, 9, 2, 7, 10, 5, 12, 15, 8, 11, 14, 3, 16, 1],
            [4, 9, 6, 13, 8, 11, 2, 15, 12, 7, 10, 5, 14, 3, 16, 1],
            [4, 9, 6, 11, 8, 13, 2, 15, 12, 7, 10, 3, 14, 5, 16, 1],
            [4, 5, 6, 9, 8, 1, 10, 15, 12, 13, 2, 7, 14, 11, 16, 3],
            [4, 7, 6, 1, 8, 13, 10, 15, 2, 11, 14, 9, 12, 5, 16, 3],
            [4, 13, 6, 7, 2, 15, 10, 3, 12, 9, 8, 5, 14, 11, 16, 1],
            [4, 7, 6, 13, 8, 15, 10, 5, 2, 11, 14, 9, 12, 3, 16, 1]]
#=difs of uniques
 0  1  2  3  4  6  8  11  14  19  22  24  26  28  30  32  34  36  38  40  42  44  46  48  50  52  54  56  58  60  62  64
 0  1  2  3  4  6  8  11  14  19  22  24  26  28  30  32  34  36  38  40  42  44  46  48  50  52  54  56  58  60  62  64
 0  1  2  3  4  6  8  11  14  16  19  22  26  28  30  32  34  36  38  40  42  44  46  48  50  52  54  56  58  60  62  64
 0  1  2  3  4  6  8  10  12  14  16  18  20  22  24  28  30  34  36  38  40  42  44  46  48  52  54  56  58  60  62  64
 0  1  2  3  4  6  8  11  14  16  18  22  24  26  28  30  32  36  38  40  42  45  49  52  54  56  58  60  62  64  66  68
 0  1  2  3  4  6  7   9  12  14  16  18  20  22  26  30  34  36  38  40  43  45  46  48  50  53  56  59  61  63  66  68
 0  1  2  3  4  6  7   9  12  14  17  21  24  26  28  30  33  36  39  41  43  45  46  48  51  53  56  59  61  63  65  68
 0  1  2  3  4  6  8  11  14  16  18  22  24  27  30  31  32  34  36  39  42  43  45  48  49  51  54  55  57  60  61  63
 0  1  2  3  4  6  8  11  14  16  19  20  22  25  28  32  34  36  38  40  42  44  46  48  50  52  54  56  58  60  62  64
 0  1  2  3  4  6  8  10  12  14  16  20  24  28  30  31  32  34  36  39  42  46  49  53  55  57  58  59  60  62  64  67
 0  1  2  3  4  6  8  10  12  14  17  19  22  25  28  31  33  35  36  37  39  41  44  47  49  51  54  56  58  61  64  67
=#
