package feistel.models;

import org.chocosolver.solver.Solver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 10/11/2021
 */
public class FeistelSetTest {

    @DataProvider
    public Object[][] combinations() {
        return new Object[][]{
                {4, 4, 4, 7},
                {6, 5, 6, 54},
                {8, 6, 44, 550},
                {10, 7, 330, 9040},
                {12, 6, 1, 1},
                {12, 7, 1, 1},
//                {12, 8, 1, 1},

        };
    }

    @Test(dataProvider = "combinations")
    public void chekAll(int n, int r, int solutions, int nodes) {
        List<int[][]> cards = CardinalityGenerator.generate(n);
        long nbsol = 0;
        for (int[][] c : cards) {
            Solver solver = FeistelGraph.solve(n, r, c);
            solver.solve();
            nbsol += solver.getSolutionCount();
        }
        Assert.assertEquals(nbsol, solutions, "Wrong number of solutions");
    }

    @Test(dataProvider = "combinations")
    public void testAll(int n, int r, int solutions, int nodes) {
        Solver solver = FeistelSet.solve(n, r);
        solver.findAllSolutions();
        Assert.assertEquals(solver.getSolutionCount(), solutions, "Wrong number of solutions");
        Assert.assertEquals(solver.getNodeCount(), nodes, "Wrong number of nodes");
    }

    @Test(dataProvider = "combinations")
    public void testAll2(int n, int r, int solutions, int nodes) {
        List<int[][]> cards = CardinalityGenerator.generate(n);
        long nbsol = 0;
        for (int[][] c : cards) {
            Solver solver = FeistelGraph.solve(n, r, c);
            solver.findAllSolutions();
            nbsol += solver.getSolutionCount();
        }
        Assert.assertEquals(nbsol, solutions, "Wrong number of solutions");
    }

    @DataProvider
    public Object[][] cauchois() {
        return new Object[][]{
                {12, 6},
                {12, 8},
                {14, 8},
                {16,7},
                {16, 8},
                {18, 8}, //=> dure
                {20, 9},
//                {22, 8}, //=> dure
//                {24, 9}, //=> dure
//                {26, 9}, //=> dure
//                {32, 10},
//                {64, 11},
//                {128, 13},
        };
    }


    @Test(dataProvider = "cauchois")
    public void testOne(int n, int r) {
        FeistelSet.solve(n, r)
                .streamSolutions()
                .limit(1)
                .count();
    }

    @DataProvider
    public Object[][] permutations() {
        return new Object[][]{
                // 6x5
                {6, 5, new int[]{1, 2, 5, 0, 3, 4}},
                // 8x6
                {8, 6, new int[]{7, 0, 3, 4, 1, 6, 5, 2}},
                {8, 6, new int[]{3, 4, 1, 2, 7, 0, 5, 6}},
                {8, 6, new int[]{6, 0, 3, 5, 7, 1, 2, 4}},
                {8, 6, new int[]{7, 0, 1, 6, 5, 3, 4, 2}},
                {8, 6, new int[]{1, 2, 6, 0, 7, 3, 5, 4}},
                // 10x7
                {10, 7, new int[]{1, 2, 7, 8, 3, 4, 9, 0, 5, 6}},
                {10, 7, new int[]{9, 0, 5, 6, 3, 4, 1, 8, 7, 2}},
                {10, 7, new int[]{5, 6, 3, 4, 1, 2, 9, 0, 7, 8}},
                {10, 7, new int[]{8, 0, 3, 5, 7, 1, 9, 4, 2, 6}},
                {10, 7, new int[]{9, 4, 0, 2, 6, 8, 7, 1, 5, 3}},
                 // some does not work due to sym. breaking domain def.
        };
    }

//    @Test(dataProvider = "permutations")
//    public void testPerms(int n, int r, int[] p) {
//        Assert.assertEquals(FeistelSet.solve(n, r, p)
//                .streamSolutions()
//                .limit(1)
//                .count(), 1);
//    }


}