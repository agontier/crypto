/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.BoolVar;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 28/10/2021
 */
public class FeistelTable {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void main(String[] args) {
        FeistelTable.modelAndSolve(4, 4, "\\V0R0R1V1V0\\V0R0R1V0R0\\V0R0R1eV1V1V1\\V0R0R1eV1V0R0").limit(1).count();
    }

    public static Stream<Solution> modelAndSolve(int N, int R, String PATHS) {
        HashMap<Integer, HashMap<Integer, List<int[]>>> paths = parse(PATHS);

        Model model = new Model();
        int E = N * (N - 1) + N / 2;
        BoolVar[][][] C = new BoolVar[N][N][E];
        BoolVar[] Ck = model.boolVarArray("K", E);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                C[i][j] = model.boolVarArray(String.format("C[%d][%d]", i, j), E);
                Tuples t = new Tuples();
                for (int[] p : paths.get(i).get(i)) {
                    t.add(p);
                }
                model.table(C[i][j], t).post();
            }
        }
        for (int k = 0; k < E; k++) {
            BoolVar[] tmp = new BoolVar[N * N];
            for (int i = 0, l = 0; i < N; i++) {
                for (int j = 0; j < N; j++, l++) {
                    tmp[l] = C[i][j][k];
                }
            }
            model.sum(tmp, ">", 0).post();
        }
        model.sum(Ck, "=", N + N / 2).post();

        Solver solver = model.getSolver();
        return solver.streamSolutions();
    }

    /**
     * Like:
     * \V0R0R1V1V0\V0R0R1V0R0\V0R0R1eV1V1V1\V0R0R1eV1V0R0 ...
     * V : pair
     * R: impair
     * e : impair -> pair
     *
     * @param paths
     * @return
     */
    private static HashMap<Integer, HashMap<Integer, List<int[]>>> parse(String paths) {
        return null;
    }

}
