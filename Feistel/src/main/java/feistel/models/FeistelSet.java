/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;

import feistel.propagators.PropUnion;
import feistel.propagators.PropXor;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.reification.PropXltCReif;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.values.SetDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.InputOrder;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableRangeSet;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

/**
 * <br/>
 *
 * @author Charles Prud'homme, Arthur Gontier
 * @since 16/11/2021
 */
public class FeistelSet {

    public static Solver solve(int N, int R) {
        return solve(N, R, null);
    }

    public static Solver solve(int N, int R, int[][] cards) {
        Model m = new Model(String.format("Feistel [%d,%d]", N, R));
        SetVar[][] succs = new SetVar[R - 2][N];
        IntVar[] perm = new IntVar[N];
        int[] lb = new int[0];
        int[] nodes = IntStream.range(0, N).toArray();

        // declare successors
        if (cards != null) {
            TIntList successors = new TIntArrayList();
            for (int i = 0; i < N; i++) {
                successors.clear();
                for (int j = 0; j < N; j++)
                    if (i != j)
                        if (i % 2 == 0) {
                            if (cards[Math.floorMod(2, j)][j / 2] == 2)
                                successors.add(j);
                        } else {//i impair
                            if (cards[Math.floorMod(2, j)][j / 2] == 1)
                                successors.add(j);
                        }
                perm[i] = m.intVar("perm_" + i,
                        successors.toArray());
                if (i % 2 == 1)
                    successors.add(i);
                succs[0][i] = m.setVar("succ_0_" + i,
                        successors.toArray());
            }
        } else {
            TIntList successors = new TIntArrayList();
            for (int i = 0; i < N; i++) {
                successors.clear();
                int b = Math.min(N, 4 + 2 * i); // for static sym. break.
                for (int j = 0; j < b; j++)
                    if (i != j) {
                        successors.add(j);
                    }
                perm[i] = m.intVar("perm_" + i,
                        successors.toArray());
                if (i % 2 == 1)
                    successors.add(i);
                succs[0][i] = m.setVar("succ_0_" + i,
                        new int[0], successors.toArray());
            }
            //constrain non eo
            perm[0].eq(2).post();
            //Dynamic symetry break
            IntVar[] maxTill = m.intVarArray("maxTill", N - 1, 0, N);
            maxTill[0] = perm[0];
            for (int i = 1; i < N - 1; i++)
                m.max(maxTill[i], Arrays.stream(perm, 0, i + 1).toArray(IntVar[]::new)).post();
            BoolVar[][] bm = m.boolVarMatrix("bm", N - 1, N / 2);
            BoolVar[][] bm2 = m.boolVarMatrix("bm2", N - 1, N / 2);
            for (int i = 1; i < N; i++)
                for (int v = 0; v < N / 2; v++) {
                    m.reifyXinS(maxTill[i - 1], new IntIterableRangeSet(2 * v, 2 * v + 1), bm[i - 1][v]);
                    m.reifyXltC(perm[i], 2 * v + 4, bm2[i - 1][v]);
                    m.arithm(bm[i - 1][v], "<=", bm2[i - 1][v]).post();
                }
        }
        for (int v = 0; v < N / 2; v++)
            new Constraint("SXOR", new PropXor(succs[0][2 * v + 1], 2 * v, 2 * v + 1)).post();

        //Constrain Feistel doubling arcs
        for (int v = 0; v < N / 2; v++) {
            m.union(new IntVar[]{perm[2 * v]}, succs[0][2 * v]).post();
            m.union(new IntVar[]{perm[2 * v], perm[2 * v + 1]}, succs[0][2 * v + 1]).post();
        }

        //Permutation
        m.allDifferent(perm).post();//l'AC ne change pas le nombre de noeud pour un unsat

        //Diffusion avec l'Union
        for (int r = 1; r < R - 2; r++) {
            for (int i = 0; i < N; i++) {
                succs[r][i] = m.setVar("succ_" + r + "_" + i, lb, nodes);
                union(succs[r - 1][i], succs[0], succs[r][i]).post();
            }
        }

        //Dernière diffusion (chaque vert attein tous les rouges en R-1)
        SetVar[] lastGreen = new SetVar[N / 2];
        int[] rednodes = IntStream.range(0, N / 2).map(v -> 2 * v + 1).toArray();
        for (int v = 0; v < N / 2; v++) {
            lastGreen[v] = m.setVar("green_" + v, rednodes, nodes);
            union(succs[R - 3][2 * v], succs[0], lastGreen[v]).post();
        }

        // filtrage plus puissant
        for (int i = 0; i < N / 2 && R > 3; i++) {
            // uniquement pour les noeuds pairs
            BoolVar[] bl = m.setBoolsView(succs[0][2 * i], N, 0);
            for (int l = 0; l < N; l++) {
                for (int j = 0; j < R - 3; j++) {
                    bl[l].le(m.allEqual(succs[j][l], succs[j + 1][2 * i]).reify()).post();
                }
                bl[l].le(m.allEqual(succs[R - 3][l], lastGreen[i]).reify()).post();
            }
        }


        //Strategie: pairs d'abbord
        Solver s = m.getSolver();
        SetVar[] os = new SetVar[N];
        for (int v = 0; v < N / 2; v++) {
            os[v] = succs[0][2 * v];
            os[v + N / 2] = succs[0][2 * v + 1];
        }

        s.setSearch(
                //Search.lastConflict(
                Search.setVarSearch(
                        new InputOrder<>(m),
                        new SetDomainMin(),
                        true,
                        os));
        s.printShortFeatures();
        s.showShortStatistics();
        s.showShortStatisticsOnShutdown();
        return s;
    }

    public static Solver solveOld(int N, int R, int[]... pairs) {
        Model m = new Model(String.format("Feistel [%d,%d]", N, R));
        // Successor of successors...
        SetVar[][] succs = new SetVar[R - 2][N];
        IntVar[] cards = new IntVar[N];
        int[] lb = new int[0];
        int[] nodes = IntStream.range(0, N).toArray();
        SetVar allNodes = m.setVar("All", nodes);

        // declare successors
        HashMap<Integer, List<Integer>> ps = new HashMap<>();
        for (int i = 0; i < N; i++) {
            ps.put(i, new ArrayList<>());
        }
        for (int[] pair : pairs) {
            List<Integer> ns = ps.computeIfAbsent(pair[0], k -> new ArrayList<>());
            ns.add(pair[1]);
        }
        TIntList successors = new TIntArrayList();
        for (int i = 0; i < N; i++) {
            successors.clear();
            int b = Math.min(N, 4 + 2 * i); // for static sym. break.
            for (int j = 0; j < b; j++) {
                if (i != j) {
                    successors.add(j);
                }
            }
            if (i % 2 == 1) {
                successors.add(i);
            }
            succs[0][i] = m.setVar("succ_0_" + i,
                    ps.get(i).stream().mapToInt(k -> k).toArray(),
                    successors.toArray());
            cards[i] = succs[0][i].getCard();
        }

        //Dynamic symetry break
        IntVar[] max = m.intVarArray("max", N, 0, N);
        for (int i = 0; i < N; i++)
            m.max(succs[0][i], max[i], true).post();

        IntVar[] maxTill = m.intVarArray("maxTill", N - 1, 0, N);
        maxTill[0] = max[0];
        for (int i = 1; i < N - 1; i++)
            m.max(maxTill[i], Arrays.stream(max, 0, i + 1).toArray(IntVar[]::new)).post();

        BoolVar[][] bm = m.boolVarMatrix("bm", N - 1, N / 2);
        BoolVar[][] bm2 = m.boolVarMatrix("bm2", N - 1, N / 2);
        for (int i = 1; i < N; i++)
            for (int v = 0; v < N / 2; v++) {
                m.reifyXinS(maxTill[i - 1], new IntIterableRangeSet(2 * v, 2 * v + 1), bm[i - 1][v]);
                m.reifyXltC(max[i], 2 * v + 4, bm2[i - 1][v]);
                m.arithm(bm[i - 1][v], "<=", bm2[i - 1][v]).post();
            }

        //Constrain Feistel doubling arcs
        for (int v = 0; v < N / 2; v++) {
            m.subsetEq(succs[0][2 * v], succs[0][2 * v + 1]).post();
            // pas de 2cycle dans une paire.
            new Constraint("SXOR", new PropXor(succs[0][2 * v + 1], 2 * v, 2 * v + 1)).post();
            cards[2 * v].eq(1).post();
            cards[2 * v + 1].eq(2).post();
        }
        // Tous les noeuds pairs sont disjoints
        m.allDisjoint(IntStream.range(0, N / 2)
                .mapToObj(v -> succs[0][2 * v])
                .toArray(SetVar[]::new)).post();
        // Les noeuds impairs forment une partition de tous les noeuds => tous les successeurs impairs sont disjoints
        m.partition(IntStream.range(0, N / 2)
                .mapToObj(v -> succs[0][2 * v + 1])
                .toArray(SetVar[]::new), allNodes).post();

        //Diffusion avec l'Union
        for (int r = 1; r < R - 2; r++) {
            for (int i = 0; i < N; i++) {
                succs[r][i] = m.setVar("succ_" + r + "_" + i, lb, nodes);
                union(succs[r - 1][i], succs[0], succs[r][i]).post();
            }
        }

        //Dernière diffusion (chaque vert attein tous les rouges en R-1)
        SetVar[] lastGreen = new SetVar[N / 2];
        int[] rednodes = IntStream.range(0, N / 2).map(v -> 2 * v + 1).toArray();
        for (int v = 0; v < N / 2; v++) {
            lastGreen[v] = m.setVar("green_" + v, rednodes, nodes);
            union(succs[R - 3][2 * v], succs[0], lastGreen[v]).post();
        }

        //Strategie: impairs d'abbord
        Solver s = m.getSolver();
        SetVar[] os = new SetVar[N];
        for (int v = 0; v < N / 2; v++) {
            os[v] = succs[0][2 * v + 1];
            os[v + N / 2] = succs[0][2 * v];
        }

        s.setSearch(
                //Search.lastConflict(
                Search.setVarSearch(
                        new InputOrder<>(m),
                        new SetDomainMin(),
                        true,
                        os));
        s.printShortFeatures();
        s.showShortStatistics();
        s.showShortStatisticsOnShutdown();
        return s;
    }

    private static int fibo(int r) {
        if (r == 0) return 0;
        else if (r == 1) return 1;
        else return fibo(r - 1) + fibo(r - 2);
    }

    public static Constraint union(SetVar filter, SetVar[] sets, SetVar union) {
        return new Constraint("Union", new PropUnion(sets, filter, union));
    }
}