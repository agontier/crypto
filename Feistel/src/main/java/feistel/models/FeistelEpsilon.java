/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;
//import com.sun.org.apache.xpath.internal.operations.Bool;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gurobi.*;
import org.chocosolver.sat.MiniSat;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Settings;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.nary.cnf.ILogical;
import org.chocosolver.solver.constraints.nary.cnf.LogOp;
import org.chocosolver.solver.constraints.nary.cnf.LogicTreeToolBox;
import org.chocosolver.solver.constraints.nary.sat.PropSat;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.view.bool.BoolNotView;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class FeistelEpsilon {

    public boolean SAT = false;

    public Stream<Solution> solveEpsilon(int R, int N, int[] perm) throws GRBException {
        int n2 = N / 2;
        boolean[][] adjacency = new boolean[N][N];
        for (int i = 0; i < N; i++) {
            adjacency[i][perm[i]] = true;
        }
        HashMap<IntVar, GRBVar> map = new HashMap<>();
        Model cpModel = new Model(
                String.format("epsilon %d %d %s", R, N, Arrays.toString(perm))
                ,
                Settings.init()
                        .setEnableSAT(false)
                        .setSwapOnPassivate(true)
        );
        GRBEnv env = new GRBEnv();
        GRBModel plModel = new GRBModel(env);

        // Declares the epsilon variables
        BoolVar[][] cpE = cpModel.boolVarMatrix("E", n2, n2);
        GRBVar[][] plE = new GRBVar[n2][n2];
        for (int i = 0; i < n2; i++) {
            for (int j = 0; j < n2; j++) {
                plE[i][j] = plModel.addVar(0, 1, 1, GRB.BINARY, cpE[i][j].getName());
                map.put(cpE[i][j], plE[i][j]);
            }
        }
        plModel.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);
        // and the diffusion variables
        BoolVar[][][] cpC = new BoolVar[N][R + 1][N];
        GRBVar[][][] plC = new GRBVar[N][R + 1][N];
        // Declares the constraints
        // Exactly one 1 per rows and per columns
        for (int i = 0; i < n2; i++) {
            if (SAT) {
                LogOp cond = LogOp.or();
                for (int j = 0; j < n2; j++) {
                    LogOp and = LogOp.and(cpE[i][j]);
                    for (int k = 0; k < n2; k++) {
                        if (k != j) {
                            and = LogOp.and(and, cpE[i][k].not());
                        }
                    }
                    cond = LogOp.or(cond, and);
                }
                cpModel.addClauses(cond);
            } else {
                cpModel.sum(cpE[i], "=", 1).post();
                cpModel.sum(ArrayUtils.getColumn(cpE, i), "=", 1).post();
            }
            GRBLinExpr row = new GRBLinExpr();
            GRBLinExpr col = new GRBLinExpr();
            for (int j = 0; j < n2; ++j) {
                row.addTerm(1, plE[i][j]);
                col.addTerm(1, plE[j][i]);
            }
            plModel.addConstr(row, GRB.EQUAL, 1, "row_" + i);
            plModel.addConstr(col, GRB.EQUAL, 1, "col_" + i);

        }
        for (int v = 0; v < N; v++) {
            for (int i = 0; i < N; i++) {
                // first round
                cpC[v][0][i] = cpModel.boolVar(String.format("C_%d_%d_%d", v, 0, i), v == i);
                int bv = v == i ? 1 : 0;
                plC[i][0][i] = plModel.addVar(bv, bv, 0, GRB.BINARY, String.format("C_%d_%d_%d", v, 0, i));
                map.put(cpC[v][0][i], plC[v][0][i]);
                // intermediate rounds
                for (int r = 1; r < R; r++) {
                    cpC[v][r][i] = cpModel.boolVar(String.format("C_%d_%d_%d", v, r, i));
                    plC[v][r][i] = plModel.addVar(0, 1, 0, GRB.BINARY, String.format("C_%d_%d_%d", v, r, i));
                    map.put(cpC[v][r][i], plC[v][r][i]);
                }
                // last round
                cpC[v][R][i] = cpModel.boolVar(String.format("C_%d_%d_%d", v, R, i), true);
                plC[i][R][i] = plModel.addVar(1, 1, 0, GRB.BINARY, String.format("C_%d_%d_%d", v, R, i));
                map.put(cpC[v][R][i], plC[v][R][i]);
            }
        }
        for (int v = 0; v < N; v++) {
            for (int r = 1; r <= R; r++) {
                for (int i = 0; i < N; i++) {
                    //C[v,r,i] = exists(j in n)(
                    // if j mod 2 == 0 then
                    //  A[j,i] /\  C[v,r-1,j]
                    //  else
                    //  A[j,i] /\ (C[v,r-1,j] \/ exists(k in n2)(C[v,r-1,2*k] /\ E[k,j div 2])) endif)
                    //  );
                    List<LogOp> conditions = new ArrayList<>();
                    for (int j = 0; j < N; j++) {
                        if (adjacency[j][i]) {
                            LogOp cond = LogOp.or(cpC[v][r - 1][j]);
                            if (cpC[v][r - 1][j].isInstantiatedTo(1)) {
                                conditions.add(cond);
                                break;
                            }
                            for (int k = 0; j % 2 == 1 && k < n2; k++) {
                                cond = LogOp.or(cond, LogOp.and(cpC[v][r - 1][2 * k], cpE[k][j / 2]));
                            }
                            conditions.add(cond);
                        }
                    }
                    LogOp log = LogOp.reified(cpC[v][r][i], LogOp.or(conditions.toArray(LogOp[]::new)));
                    //cpModel.addClauses(log);
                    //cpModel.addClauses(LogOp.ifOnlyIf(C[v][r][i], log));
                    ILogical tree = LogicTreeToolBox.toCNF(log, cpModel);
                    if (cpModel.boolVar(true).equals(tree)) {
                        //ret = addClauseTrue(ref().boolVar(true));
                        throw new UnsupportedOperationException("todo");
                    } else if (cpModel.boolVar(false).equals(tree)) {
                        //ret = addClauseTrue(cpModel.boolVar(false));
                        throw new UnsupportedOperationException("todo");
                    } else {
                        ILogical[] clauses;
                        if (!tree.isLit() && ((LogOp) tree).is(LogOp.Operator.AND)) {
                            clauses = ((LogOp) tree).getChildren();
                        } else {
                            clauses = new ILogical[]{tree};
                        }
                        for (int l = 0; l < clauses.length; l++) {
                            ILogical clause = clauses[l];
                            if (clause.isLit()) {
                                BoolVar bv = (BoolVar) clause;
                                //ret &= addClauseTrue(bv);
                                throw new UnsupportedOperationException("todo");
                            } else {
                                LogOp n = (LogOp) clause;
                                BoolVar[] bvars = n.flattenBoolVar();
                                if (cpModel.getSettings().enableSAT()) {
                                    TIntList lits = new TIntArrayList(bvars.length);
                                    PropSat sat = cpModel.getMinisat().getPropSat();
                                    // init internal structures
                                    sat.beforeAddingClauses();
                                    for (int j = 0; j < bvars.length; j++) {
                                        lits.add(MiniSat.makeLiteral(sat.makeBool(bvars[j]), true));
                                    }
                                    // TODO: pass by satsolver directly
                                    sat.addClause(lits);
                                    sat.afterAddingClauses();
                                } else {
                                    if (Stream.of(bvars).mapToInt(IntVar::getLB).sum() == 0) {
                                        cpModel.sum(bvars, ">", 0).post();
                                    }
                                }
                                //cpModel.sum(bvars, ">", 0).post();
                                GRBLinExpr expr = new GRBLinExpr();
                                int rhs = 1;
                                for (int m = 0; m < bvars.length; m++) {
                                    IntVar cpV = bvars[m];
                                    if (cpV.isInstantiated()) {
                                        rhs -= cpV.getValue();
                                        continue;
                                    }
                                    int co = 1;
                                    GRBVar plV;
                                    if (cpV instanceof BoolNotView<?>) {
                                        co = -1;
                                        rhs -= 1;
                                        plV = map.get(((BoolNotView<?>) cpV).not());
                                    } else {
                                        plV = map.get(cpV);
                                    }
                                    expr.addTerm(co, plV);
                                }
                                plModel.addConstr(expr, GRB.GREATER_EQUAL, rhs, String.format("expr_%d_%d_%d_%d", v, r, i, l));
                            }
                        }
                    }
                }
            }
        }
        //plModel.write("model.lp");
        //plModel.optimize();
        //System.out.println("JSON solution:" + plModel.getJSONSolution());
        plModel.dispose();
        env.dispose();

        Solver solver = cpModel.getSolver();
        solver.printStatistics();
        solver.setSearch(
                /*Search.lastConflict*/(Search.inputOrderUBSearch(ArrayUtils.flatten(/*ArrayUtils.transpose*/(cpE)))));
        solver.showShortStatistics();
        return solver.streamSolutions();
    }

    public static void main(String[] args) throws GRBException {
        FeistelEpsilon runner = new FeistelEpsilon();
        runner.solveEpsilon(4, 4, new int[]{1, 2, 3, 0}).limit(10).toArray();
        //runner.solveEpsilon(5, 6, new int[]{1, 2, 5, 0, 3, 4}).toArray();
        //runner.solveEpsilon(8, 16, new int[]{5, 3, 7, 0, 6, 2, 9, 4, 13, 11, 15, 8, 14, 10, 1, 12}).limit(11).toArray();
        //runner.solveEpsilon(8, 18, new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,0}).limit(1).toArray();

    }
}