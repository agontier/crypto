/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;

import feistel.propagators.PropImply;
import feistel.propagators.PropUnion;
import feistel.propagators.PropXor;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.SetDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.InputOrder;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.DirectedGraphVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.objects.graphs.DirectedGraph;
import org.chocosolver.util.objects.setDataStructures.SetType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 28/10/2021
 */
public class FeistelGraph {

    public static final boolean PNG = false;

    public static Solver solve(int N, int R, int[] permutation) {
        int[][] pairs = new int[N][2];
        for (int i = 0; i < N; i++) {
            pairs[i] = new int[]{i, permutation[i]};
        }
        return solve(N, R, pairs, new int[0][0]);
    }


    public static Solver solve(int N, int R, int[][] l) {
        return solve(N, R, new int[0][0], l);
    }


    public static Solver solve(int N, int R) {
        return solve(N, R, new int[0][0], new int[0][0]);
    }

    public static Solver solve(int N, int R, int[][] pairs, int[][] cards) {
        Model m = new Model(String.format("Feistel [%d,%d]", N, R));

        DirectedGraph k, e;
        k = new DirectedGraph(m, N, SetType.BITSET, SetType.BITSET, true);
        e = new DirectedGraph(m, N, SetType.BITSET, SetType.BITSET, true);
        // add pairs
        for (int[] p : pairs) {
            k.addEdge(p[0], p[1]);
        }

        // add edges
        for (int i = 0; i < N; i++) {
            int b = N;//Math.min(N, 4 + 2 * i); // for sym. break.
            for (int j = 0; j < b; j++) {
                if (i != j) {
                    e.addEdge(i, j);
                }
            }
            if (i % 2 == 1) {
                e.addEdge(i, i);
            }
        }
        DirectedGraphVar g = m.digraphVar("F", k, e);
        IntVar[] inDeg = new IntVar[N];
        IntVar[] outDeg = new IntVar[N];
        for (int i = 0; i < N / 2; i++) {
            if (cards.length > 0) {
                inDeg[2 * i] = m.intVar("i" + i, cards[0][i]);
                inDeg[2 * i + 1] = m.intVar("i" + i, cards[1][i]);
            } else {
                inDeg[2 * i] = m.intVar("i" + i, 1, 2);
                inDeg[2 * i + 1] = m.intVar("i" + i, 1, 2);
            }

            outDeg[2 * i] = m.intVar("o1", 1);
            outDeg[2 * i + 1] = m.intVar("o2", 2);
        }

        m.inDegrees(g, inDeg).post();
        m.outDegrees(g, outDeg).post();
        // Successor of successors...
        SetVar[][] succs = new SetVar[R - 2][N];
        int[] lb = new int[0];
        int[] nodes = IntStream.range(0, N).toArray();
        SetVar allNodes = m.setVar("All", nodes);
        for (int i = 0; i < N; i++) {
            succs[0][i] = m.graphSuccessorsSetView(g, i);
//            succs[R - 1][i] = allNodes;
        }
        for (int v = 0; v < N / 2; v++) {
            m.subsetEq(succs[0][2 * v], succs[0][2 * v + 1]).post();
            // i impair, si i in succ(i) alors |pred(i)| = 2
            new Constraint("IMPL", new PropImply(succs[0][2 * v + 1], 2 * v + 1, inDeg[2 * v + 1])).post();
            // i impair, succ(i) contient i ou i+1  mais pas les 2 en même temps.
            new Constraint("SXOR", new PropXor(succs[0][2 * v + 1], 2 * v, 2 * v + 1)).post();
            /*
            // to strict
            for(int w = 0; w < N/2; w++) {
                new Constraint("SXOR", new PropXor(succs[0][2 * v + 1], 2 * w, 2 * w + 1)).post();
            }*/
        }
        m.partition(IntStream.range(0, N / 2)
                .mapToObj(v -> succs[0][2 * v + 1])
                .toArray(SetVar[]::new), allNodes).post();

        //Dynamic symetry break
        IntVar[] max = m.intVarArray("max", N, 0, N);
        for (int i = 0; i < N; i++)
            m.max(succs[0][i], max[i], true).post();

        IntVar[] maxTill = m.intVarArray("maxTill", N - 1, 0, N);
        maxTill[0] = max[0];
        for (int i = 1; i < N - 1; i++)
            m.max(maxTill[i], Arrays.stream(max, 0, i + 1).toArray(IntVar[]::new)).post();

        BoolVar[][] bm = m.boolVarMatrix("bm", N - 1, N / 2);
        BoolVar[][] bm2 = m.boolVarMatrix("bm2", N - 1, N / 2);
        for (int i = 1; i < N; i++)
            for (int v = 0; v < N / 2; v++) {
                //m.reifyXinS(maxTill[i - 1], new IntIterableRangeSet(2 * v, 2 * v + 1), bm[i - 1][v]);
                //m.reifyXltC(max[i], 2 * v + 4, bm2[i - 1][v]);
                //m.arithm(bm[i - 1][v], "<=", bm2[i - 1][v]).post();
            }

        //Diffusion avec l'Union
        for (int r = 1; r < R - 2; r++) {
            for (int i = 0; i < N; i++) {
                succs[r][i] = m.setVar("succ_" + r + "_" + i, lb, nodes);
                union(succs[r - 1][i], succs[0], succs[r][i]).post();
            }
        }

        //Dernière diffusion (chaque vert attein tous les rouges en R-1)
        SetVar[] lastGreen = new SetVar[N / 2];
        int[] rednodes = IntStream.range(0, N / 2).map(v -> 2 * v + 1).toArray();
        for (int v = 0; v < N / 2; v++) {
            lastGreen[v] = m.setVar("green_" + v, rednodes, nodes);
            union(succs[R - 3][2 * v], succs[0], lastGreen[v]).post();
        }

        // filtrage plus puissant
        for (int i = 0; i < N / 2 && R > 3; i++) {
            // uniquement pour les noeuds pairs
            BoolVar[] bl = m.setBoolsView(succs[0][2 * i], N, 0);
            for (int l = 0; l < N; l++) {
                for (int j = 0; j < R - 3; j++) {
                    bl[l].le(m.allEqual(succs[j][l], succs[j + 1][2 * i]).reify()).post();
                }
                bl[l].le(m.allEqual(succs[R - 3][l], lastGreen[i]).reify()).post();
            }
        }

        // useless?
        for (int i = 0; i < N; i++) {
            for (int r = 1; r < R; r++) {
                //succs[r - 1][i].getCard().le(succs[r][i].getCard()).post();
            }
        }
        // useless ?
//        for (int r = 1; r < R; r++) {
//            int fibo = fibo(r - 1);
//            for (int i = 0; i < N; i++) {
//                succs[r][i].getCard().ge(fibo).post();
//            }
//        }
        // proposition 2.3
        // CPRU too costly and brings nothing
        //SetVar red = m.setVar("red", IntStream.range(0, N / 2).map(v -> 2 * v + 1).toArray());
        //m.subsetEq(red, m.setUnionView(succs[R - 2])).post();
        // CPRU useless
        /*m.allEqual(
                ArrayUtils.append(new SetVar[]{allNodes},
                        Arrays.stream(succs[R - 1], 0, N).toArray(SetVar[]::new))
        ).post();*/


        // CPRU useless
//        int[][] ignored = new int[N / 2][N / 2];
//        for (int i = 0; i < N / 2; i++) {
//            ignored[i] = new int[]{2 * i + 1, 2 * i + 1};
//        }
//        DirectedGraphVar h = m.edgeInducedSubgraphView(g, ignored, true);
        // CPRU useless
        // m.nbStronglyConnectedComponents(g, m.intVar(1)).post();
        for (int i = 0; i < N; i++) {
            // Filters but very costly -> to remove
            //m.reachability(g, i).post();
        }


        Solver s = m.getSolver();
        s.plugMonitor((IMonitorSolution) () -> {
            for (int i = 0; i < N / 2; i++) {
                //System.out.printf("%d : %s %n", i, succs[0][i].getValue());
                //System.out.printf("%d::%d%n",
                //        inDeg[2 * i].getValue(), inDeg[2 * i + 1].getValue());
            }
            //System.out.printf("%s%n", g.getLB());
            /*for(int n : g.getPotentialNodes()){
                System.out.printf("%d -> ", n);
                for(int i : g.getPotentialSuccessorsOf(n)){
                    System.out.printf("%d,", i);
                }
                System.out.printf("%n");
            }*/
            /*for (int i = 0; i < N; i++) {
                System.out.printf("%d :%n", i);
                for (int r = 0; r < R; r++) {
                    System.out.printf("\t%s%n", succs[r][i].getLB());
                }
            }*/
            for (int i : g.getPotentialNodes()) {
                for (int j : g.getPotentialSuccessorsOf(i)) {
                    if (!(i % 2 == 1 && g.getPotentialSuccessorsOf(i - 1).contains(j))) {
                        System.out.printf("%d ", j);
                    }
                }
            }
            System.out.printf("%n");
            for (int i = 0; i < N / 2; i++) {
                System.out.printf("%d ", inDeg[2 * i].getValue());
            }
            System.out.printf("%n");
            for (int i = 0; i < N / 2; i++) {
                System.out.printf("%d", inDeg[2 * i + 1].getValue());
                if (inDeg[2 * i].getValue() > inDeg[2 * i + 1].getValue()) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.printf("%n");
            if (PNG) {
                export("sol", s.getSolutionCount(), graphVizExport(g));
            }
        });
        //return s.streamSolutions();
        //s.showDecisions(() -> g.toString() + "\n" + Arrays.toString(inDeg) + "\n" + Arrays.toString(outDeg));
        /*s.setSearch(Search.lastConflict(Search.graphVarSearch(
                new InputOrder<>(m),
                new GraphEdgesOnly(),
                new GraphLexNode(),
                new GraphRandomEdge(0),
                //new GraphLexEdge(),
                true, g)));*/
        SetVar[] os = new SetVar[N];
        for (int v = 0; v < N / 2; v++) {
            os[v] = succs[0][2 * v + 1];
            os[v + N / 2] = succs[0][2 * v];
        }

        s.setSearch(
                //Search.lastConflict(
                Search.setVarSearch(
                        new InputOrder<>(m),
//                                variables -> {
//                                    int small_dsize = Integer.MAX_VALUE;
//                                    SetVar nextVar = null;
//                                    int dsize;
//                                    for (SetVar v : variables) {
//                                        if (!v.isInstantiated()) {
//                                            dsize = v.getUB().size();
//                                            if (nextVar == null) {
//                                                nextVar = v;
//                                                small_dsize = dsize;
//                                            } else if (dsize > 1 && dsize < Integer.MAX_VALUE) {
//                                                if (dsize <= small_dsize) {
//                                                    small_dsize = dsize;
//                                                    nextVar = v;
//                                                }
//                                            }
//                                        }
//                                    }
//                                    return nextVar;
//                                },

                        new SetDomainMin(),
                                /*v -> {
                                    int mm = N + 1;
                                    int j = -1;
                                    for (int w : v.getUB()) {
                                        if (v.getLB().contains(w)) continue;
                                        int cnt = 0;
                                        for (SetVar s1 : os) {
                                            if (s1.isInstantiated()) continue;
                                            if (s1.getUB().contains(w)) {
                                                cnt++;
                                            }
                                        }
                                        if (cnt > 0 && cnt < mm) {
                                            mm = cnt;
                                            j = w;
                                        }
                                    }
                                    return j;
                                },*/
                                /*v -> {
                                    int j = -1;
                                    int t = -1;
                                    for (int w : v.getUB()) {
                                        if (v.getLB().contains(w)) continue;
                                        if (w % 2 == 0) {
                                            j = w;
                                            break;
                                        } else {
                                            t = w;
                                        }
                                    }
                                    return j > -1 ? j : t;
                                },*/
                        true,
                        os));
        s.printShortFeatures();
        s.showShortStatistics();
        s.showShortStatisticsOnShutdown();
        return s;
    }

    private static String graphVizExport(DirectedGraphVar g) {
        String arc = " -> ";
        int scale = 2;
        StringBuilder sb = new StringBuilder();
        sb.append("digraph ").append("G" + "{\n");
        for (int i : g.getPotentialNodes()) {
            sb.append(i).append("[pos=\"").append((i / 2) * scale).append(",").append((i % 2) * scale).append("!\"]\n;");
        }
        for (int i : g.getPotentialNodes()) {
            for (int j : g.getPotentialSuccessorsOf(i)) {
                sb.append(i).append(arc).append(j);
                if (i % 2 == 1) {
                    if (g.getPotentialSuccessorsOf(i - 1).contains(j)) {
                        sb.append("[style=dashed,splines=line]");
                    }
                }
                sb.append(" ;\n");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private static int fibo(int r) {
        if (r == 0) return 0;
        else if (r == 1) return 1;
        else return fibo(r - 1) + fibo(r - 2);
    }

    public static Constraint union(SetVar filter, SetVar[] sets, SetVar union) {
        return new Constraint("Union", new PropUnion(sets, filter, union));
    }

    private static void export(String prefix, long cnt, String txt) {
        try {
            Path path = Paths.get(prefix + "_" + cnt + ".gv");
            byte[] strToBytes = txt.toString().getBytes();
            Files.write(path, strToBytes);
            String homeDirectory = System.getProperty("user.home");
            Process process;
            boolean isWindows = System.getProperty("os.name")
                    .toLowerCase().startsWith("windows");
            if (isWindows) {
                process = Runtime.getRuntime()
                        .exec(String.format("cmd.exe /c dir %s", homeDirectory));
            } else {
                process = Runtime.getRuntime()
                        .exec(String.format("dot -Kfdp -Tpng %1$s_%2$d.gv -o %1$s_%2$d.png ", prefix, cnt));
            }
            int exitCode = process.waitFor();
            assert exitCode == 0;
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
