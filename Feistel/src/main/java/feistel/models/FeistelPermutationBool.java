/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;
//import com.sun.org.apache.xpath.internal.operations.Bool;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.HashMap;
import java.util.stream.IntStream;

public class FeistelPermutationBool {

    public static void simpleBoolModel() {
        boolean unsolved = true;
        int R = 7;
        int n = 10;

        while (unsolved) {
            Model model = new Model(String.format("F n%d R%d", n, R));
            BoolVar[][] A =
//                    new BoolVar[n][n];
                    model.boolVarMatrix("A:perm", n, n);// permutation matrix
//            symmetryfix(A, model);
//            symmetry4(A, model);
//            fixAp(A, model);
            for (int i = 0; i < n; i++) {
                model.sum(A[i], "=", 1).post();// line sum is 1
                int finalI = i;
                model.sum(IntStream.range(0, n).mapToObj(k -> A[k][finalI]).toArray(BoolVar[]::new)
                        , "=", 1).post();//col sum is 1
            }

            BoolVar[][] B = new BoolVar[n][n];// feistel encription
            for (int i = 0; i < Math.floorDiv(n, 2); i++)
                for (int j = 0; j < n; j++) {
                    B[2 * i][j] = A[2 * i][j];
                    B[2 * i + 1][j] = model.boolVar(String.format("B[%d][%d]", i, j));
                    model.addClausesBoolOrEqVar(A[2 * i][j], A[2 * i + 1][j], B[2 * i + 1][j]);
                }
            smartproduct(B, R, model);

            BoolVar[][] C = new BoolVar[n][n];// feistel decription
            for (int i = 0; i < Math.floorDiv(n, 2); i++)
                for (int j = 0; j < n; j++) {
                    C[2 * i][j] = A[j][2 * i];
                    C[2 * i + 1][j] = model.boolVar(String.format("C[%d][%d]", i, j));
                    model.addClausesBoolOrEqVar(A[j][2 * i], A[j][2 * i + 1], C[2 * i + 1][j]);
                }
            smartproduct(C, R, model);

            Solver solver = model.getSolver();
            solver.printShortFeatures();
//            solver.setLearningSignedClauses();
//            XParameters.PRINT_CLAUSE = true;
//            XParameters.PROOF = true;
//            XParameters.FINE_PROOF = true;
//            solver.showDashboard();
            solver.setSearch(Search.domOverWDegSearch(ArrayUtils.flatten(A)));
            if (solver.solve()) {
                print(A);
//                if (solver.solve())
//                    print(A);
            }
            print("R = " + R);
            solver.printShortStatistics();
            if (solver.getSolutionCount() > 0 || R == 10)
                unsolved = false;
            else
                R++;
        }
    }

    public static void smartproduct(BoolVar[][] A, int R, Model model) {
        HashMap<Integer, BoolVar[][]> mm = new HashMap<>();
        mm.put(1, A);
        powof2(R, mm, model, A.length, R);
    }

    public static void dumbproduct(BoolVar[][] A, int R, Model model) {
        int n = A.length;
        BoolVar[][][] Ci = new BoolVar[R][n][n];
        for (int i = 0; i < n; i++)//matrix product should be true for all
            for (int j = 0; j < n; j++)
                Ci[R - 1][i][j] = model.boolVar(true);
        BoolVar[][] Ck = A;
        for (int i = 0; i < R - 2; i++) {
            Ci[i] = model.boolVarMatrix(n, n);
            productBool(A, Ck, Ci[i]);
            Ck = Ci[i];
        }
        productBool(A, Ck);
    }

    private static BoolVar[][] powof2(int R, HashMap<Integer, BoolVar[][]> map, Model model, int n, int bigR) {
        if (map.containsKey(R)) {
            return map.get(R);
        }
        int mid = (int) Math.pow(2, Math.ceil(Math.log(R / 2.) / Math.log(2)));
        BoolVar[][] left = powof2(mid, map, model, n, bigR);
        BoolVar[][] right = powof2(R - mid, map, model, n, bigR);
        BoolVar[][] Ci;
        if (R == bigR) {
            productBool(left, right);
            return null;
        } else {
            Ci = model.boolVarMatrix("C" + R, n, n);
            productBool(left, right, Ci);
            map.put(R, Ci);
            return Ci;
        }
    }

    //Symmetry breaks
    public static void symmetryfix(BoolVar[][] A, Model model) {//si une somme sur un fin de ligne est 0 alors la sommes sur la ligne du dessous avec une paire en moins l'est aussi
        int n = A.length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (j >= 4 + i * 2)
                    A[i][j] = model.boolVar(String.format("A[%d][%d]", i, j), false);
                else if (i >= 4 + j * 2)
                    A[i][j] = model.boolVar(String.format("A[%d][%d]", i, j), false);
                else if (i == j)
                    A[i][j] = model.boolVar(String.format("A[%d][%d]", i, j));//TODO are loops forbiden ? NO!
                else
                    A[i][j] = model.boolVar(String.format("A[%d][%d]", i, j));
    }

    public static void symmetry4(BoolVar[][] A, Model model) {//toujours une seule nouvelle paire de libre pour la permut et sa transposée
        int n = A.length;
        //Top right symetries
        for (int j = 4; j < n; j++)
            model.sum(java.util.Arrays.stream(A[0], 2, n).toArray(BoolVar[]::new),
                    ">=", A[1][j]).post();
        for (int j = 6; j < n; j++)
            model.sum(java.util.Arrays.stream(A[0], 4, n).toArray(BoolVar[]::new),
                    ">=", A[1][j]).post();
        for (int i = 2; i < n - 3; i++)
            for (int j = 4 + 2 * Math.floorDiv(i, 2); j < n - 5; j = j + 2) {
                int finalj = j;
                for (int jj = j + 2; jj < n; jj++)
                    model.sum(ArrayUtils.flatten(IntStream.range(0, i).mapToObj(k -> java.util.Arrays.stream(
                            A[k], finalj, n).toArray(BoolVar[]::new)).toArray(BoolVar[][]::new)), ">=", A[i][jj]).post();
            }
        //bottom left symetries
        for (int i = 4; i < n; i++)
            model.sum(IntStream.range(2, n).mapToObj(k -> A[k][0]).toArray(BoolVar[]::new),
                    ">=", A[i][1]).post();
        for (int i = 6; i < n; i++)
            model.sum(IntStream.range(4, n).mapToObj(k -> A[k][0]).toArray(BoolVar[]::new),
                    ">=", A[i][1]).post();
        for (int j = 2; j < n - 3; j++) {
            int finalj = j;
            for (int i = 4 + 2 * Math.floorDiv(j, 2); i < n - 5; i = i + 2)
                for (int ii = i + 2; ii < n; ii++)
                    model.sum(ArrayUtils.flatten(IntStream.range(i, n).mapToObj(k -> java.util.Arrays.stream(
                            A[k], 0, finalj).toArray(BoolVar[]::new)).toArray(BoolVar[][]::new)), ">=", A[ii][j]).post();
        }
    }

    public static void fixA(BoolVar[][] A, Model model) {
        int[] p = new int[]{6, 3, 7, 1, 0, 2, 4, 5};
        int[] q = new int[]{3, 5, 1, 6, 4, 0, 2, 7};
        for (int i = 0; i < p.length; i++) {
            A[2 * i][2 * p[i] + 1] = model.boolVar(true);
            A[2 * i + 1][2 * q[i]] = model.boolVar(true);
        }
    }

    public static void fixAp(BoolVar[][] A, Model model) {
//        int[] p = new int[]{1, 3, 6, 2, 7, 8, 9, 11, 0, 14, 15, 4, 5, 12, 13, 10};
//        int[] p = new int[]{3, 0, 5, 6, 1, 8, 11, 12, 13, 2, 15, 4, 9, 10, 7, 14};
        int[] p = new int[]{3, 0, 7, 8, 1, 12, 15, 2, 11, 6, 9, 4, 5, 10, 13, 14};
        for (int i = 0; i < p.length; i++)
//            A[i][p[i]] = model.boolVar(true);
            model.arithm(A[i][p[i]], "=", 1).post();
    }

    public static void productBool(BoolVar[][] A, BoolVar[][] B, BoolVar[][] C) {
        assert A.length > 0 && B.length > 0 && C.length > 0;
        assert A[0].length > 0 && B[0].length > 0 && C[0].length > 0;
        assert A[0].length == B[0].length;
        assert A.length == C.length;
        assert B[0].length == C[0].length;
        int n = B.length;
        int m = C.length;
        int p = C[0].length;
        Model model = C[0][0].getModel();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                int finalI = i;
                int finalJ = j;
                model.addClausesBoolOrArrayEqVar(
                        IntStream.range(0, n)
                                .mapToObj(k -> A[finalI][k].and(B[k][finalJ]).boolVar())
                                .toArray(BoolVar[]::new)
                        , C[i][j]);
            }
        }
    }

    public static void productBool(BoolVar[][] A, BoolVar[][] B) {
        assert A.length > 0 && B.length > 0;
        assert A[0].length > 0 && B[0].length > 0;
        assert A[0].length == B[0].length;
        int n = B.length;
        int m = A.length;
        int p = A[0].length;
        Model model = A[0][0].getModel();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                int finalI = i;
                int finalJ = j;
                model.addClausesBoolOrArrayEqualTrue(
                        IntStream.range(0, n)
                                .mapToObj(k -> A[finalI][k].and(B[k][finalJ]).boolVar())
                                .toArray(BoolVar[]::new));
            }
        }
    }

//    public static Tuples productTuples(int n) { Bad idea
//        Tuples tuples = new Tuples(true);
//
//        return tuples;
//    }


    public static void main(String[] args) {
        simpleBoolModel();
    }

    public static void print(BoolVar[][] S) {
        System.out.print("    ");
        for (int i = 0; i < S.length; i++)
            if (i % 2 == 1)
                if (i + 1 < 10) System.out.print(i + 1 + "   ");
                else System.out.print(i + 1 - 10 + "   ");
        System.out.println();
        for (int i = 0; i < S.length; i++) {
            if (i % 2 == 1)
                if (i + 1 < 10) System.out.print(i + 1 + " ");
                else System.out.print(i + 1 - 10 + " ");
            else System.out.print("  ");
            for (BoolVar boolVar : S[i])
                if (boolVar.getValue() == 1) System.out.print("1 ");
                else System.out.print("0 ");//·
            System.out.println();
        }
        printperm(S);
    }

    public static void printperm(BoolVar[][] S) {
        System.out.println();
        for (int i = 0; i < S.length; i++)
            for (int j = 0; j < S.length; j++)
                if (i % 2 == 0) {
                    if (S[i][j].getValue() == 1 && S[i + 1][j].getValue() != 1)
                        System.out.print(j + " ");
                } else if (S[i][j].getValue() == 1)
                    System.out.print(j + " ");
        System.out.println();
        System.out.println();
    }

    public static void print(int[] S) {
        for (int s : S) System.out.print(s + " ");
        System.out.println();
    }

    public static void print(int S) {
        System.out.println(S);
    }

    public static void print(String S) {
        System.out.println(S);
    }

    public static int fibo(int r) {
        if (r == 0)
            return 0;
        else if (r == 1 || r == 2)
            return 1;
        else
            return fibo2(1, 1, r - 3);
    }

    public static int fibo2(int a, int b, int r) {
        if (r > 0)
            return fibo2(b, a + b, r - 1);
        else
            return a + b;
    }
}