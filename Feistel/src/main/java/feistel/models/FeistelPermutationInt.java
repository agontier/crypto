/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.stream.IntStream;

public class FeistelPermutationInt {

    public static void simpleModel() {
        boolean unsolved = true;
        int R = 3;
        int n = 10;
        while (unsolved) {
            Model model = new Model();
            IntVar[][] A = model.intVarMatrix(n, n, 0, 1);
            IntVar[][][] Ci = new IntVar[R - 2][n][n];
            IntVar[][] Ck = A;
            for (int i = 0; i < R - 2; i++) {
                Ci[i] = model.intVarMatrix(n, n, 0, 999);
                product(A, Ck, Ci[i]);
                Ck = Ci[i];
            }
            productGeq1(A, Ck);//matrix product should be >= 1 for all exept diag

            // start counting at 0 so...
            for (int i = 0; i < n; i++) {
                if (i % 2 == 1) { // Constraints on even nodes
                    model.sum(A[i], "=", 1).post();//one son
                    model.arithm(A[i][i], "=", 0).post();//no loop
                } else {// Constraints on odd nodes
                    model.sum(A[i], "=", 2).post();//two sons
                }
            }
            //at least same destination than the even node
            for (int i = 0; i < n; i = i + 2)
                for (int j = 0; j < n; j++)
                    model.arithm(A[i][j], ">=", A[i + 1][j]).post();

            // at most two pred (colsum <= 2)
            for (int i = 0; i < n; i++) {
                int finalI = i;
                model.sum(IntStream.range(0, n).mapToObj(k -> A[k][finalI]).toArray(IntVar[]::new)
                        , "<=", 2).post();
            }

            //Symmetry breaks
            for (int ii = 0; ii < n - 4; ii = ii + 2) {
                model.sum(java.util.Arrays.stream(A[ii], ii + 4, n).toArray(IntVar[]::new), "=", 0).post();
                model.sum(java.util.Arrays.stream(A[ii + 1], ii + 4, n).toArray(IntVar[]::new), "=", 0).post();
            }

            Solver solver = model.getSolver();
            solver.printShortFeatures();
//            solver.showDashboard();
            solver.setSearch(Search.domOverWDegSearch(ArrayUtils.flatten(A)));
            while (solver.solve())
                print(A);
            print("R = " + R);
            solver.printShortStatistics();
            if (solver.getSolutionCount() > 0 || R == 30)
                unsolved = false;
            else
                R++;
        }
    }


    static void product(IntVar[][] A, IntVar[][] B, IntVar[][] C) {
        assert A.length > 0 && B.length > 0 && C.length > 0;
        assert A[0].length > 0 && B[0].length > 0 && C[0].length > 0;
        assert A[0].length == B[0].length;
        assert A.length == C.length;
        assert B[0].length == C[0].length;
        int n = B.length;
        int m = C.length;
        int p = C[0].length;
        Model model = C[0][0].getModel();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                int finalI = i;
                int finalJ = j;
                model.sum(IntStream.range(0, n)
                                .mapToObj(k -> A[finalI][k].mul(B[k][finalJ]).intVar())
                                .toArray(IntVar[]::new),
                        "=", C[i][j]).post();
            }
        }
    }

    //constrain all product value to be > 1 exept diag
    static void productGeq1(IntVar[][] A, IntVar[][] B) {
        assert A.length > 0 && B.length > 0;
        assert A[0].length > 0 && B[0].length > 0;
        assert A[0].length == B[0].length;
        int n = B.length;
        int m = A.length;
        int p = A[0].length;
        Model model = A[0][0].getModel();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                if (i != j) {
                    int finalI = i;
                    int finalJ = j;
                    model.sum(IntStream.range(0, n)
                                    .mapToObj(k -> A[finalI][k].mul(B[k][finalJ]).intVar())
                                    .toArray(IntVar[]::new),
                            ">=", 1).post();
                }
            }
        }
    }

    public static void main(String[] args) {
        simpleModel();
    }

    public static void print(IntVar[][] S) {
        for (IntVar[] intVars : S) {
            for (IntVar intVar : intVars)
                System.out.print(intVar.getValue() + " ");
            System.out.println();
        }
        System.out.println();
    }

    public static void print(IntVar[] S) {
        for (IntVar s : S) System.out.print(s.getValue() + " ");
    }

    public static void print(int[] S) {
        for (int s : S) System.out.print(s + " ");
        System.out.println();
    }

    public static void print(int S) {
        System.out.println(S);
    }

    public static void print(IntVar S) {
        System.out.println(S.getValue());
    }

    public static void print(String S) {
        System.out.println(S);
    }
}