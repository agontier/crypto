/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.models;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 24/11/2021
 */
public class CardinalityGenerator {

    public static List<int[][]> generate(int N) {
        Model model = new Model("Card gen");
        IntVar[][] X = model.intVarMatrix(N / 2, 2, 1, 2);
        model.sum(ArrayUtils.flatten(X), "=", N * 3 / 2).post();
        model.lexChainLessEq(X).post();
        List<int[][]> solutions = new ArrayList<>();
        while (model.getSolver().solve()) {
            int[][] sol = new int[2][N / 2];
            for (int i = 0; i < N / 2; i++) {
                for (int j = 0; j < 2; j++) {
                    sol[j][i] = X[i][j].getValue();
                }
            }
            solutions.add(sol);
        }
        return solutions;
    }

    public static void main(String[] args) {
        List<int[][]> sol = CardinalityGenerator.generate(8);
        for (int[][] s : sol) {
            for (int i = 0; i < s.length / 2; i++) {
                for (int j = 0; j < s[0].length; j++) {
                    System.out.printf("%d ", s[i * 2][j]);
                }
                System.out.print("\n");
            }
            for (int i = 0; i < s.length / 2; i++) {
                for (int j = 0; j < s[0].length; j++) {
                    System.out.printf("%d ", s[i * 2 + 1][j]);
                }
                System.out.print("\n");
            }
            System.out.print("\n\n");
        }
    }
}
