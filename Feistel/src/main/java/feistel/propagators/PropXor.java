/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.ESat;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 10/11/2021
 */
public class PropXor extends Propagator<SetVar> {
    final int v1;
    final int v2;

    public PropXor(SetVar set, int v1, int v2) {
        super(set);
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (vars[0].getLB().contains(v1)) {
            vars[0].remove(v2, this);
            setPassive();
            return;
        } else if (!vars[0].getUB().contains(v1)) {
            setPassive();
            return;
        }
        if (vars[0].getLB().contains(v2)) {
            vars[0].remove(v1, this);
            setPassive();
        } else if (!vars[0].getUB().contains(v2)) {
            setPassive();
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }
}
