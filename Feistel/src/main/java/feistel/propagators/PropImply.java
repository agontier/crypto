/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.solver.variables.Variable;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.solver.variables.events.SetEventType;
import org.chocosolver.util.ESat;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 10/11/2021
 */
public class PropImply extends Propagator<Variable> {

    public final int i;

    public PropImply(SetVar s, int i, IntVar d) {
        super(s, d);
        this.i = i;
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        if (vIdx == 0) {
            return SetEventType.ALL_EVENTS;
        }
        return IntEventType.VOID.getMask();
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (vars[0].asSetVar().getLB().contains(i)) {
            vars[1].asIntVar().instantiateTo(2, this);
            setPassive();
        } else if (!vars[0].asSetVar().getUB().contains(i)) {
            setPassive();
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }
}
