/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.solver.variables.events.SetEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.ISetIterator;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.BitSet;

/**
 * @author Jean-Guillaume Fages
 * @author Charles Prud'homme
 * @since 14/01/13
 */
public class PropUnion extends Propagator<SetVar> {

    //***********************************************************************************
    // VARIABLES
    //***********************************************************************************

    private final int k;
    private final int[][] mates;
    private final BitSet iii = new BitSet();
    //***********************************************************************************
    // CONSTRUCTORS
    //***********************************************************************************

    /**
     * The union of sets is equal to union
     *
     * @param sets  set variables to unify
     * @param union resulting set variable
     */
    public PropUnion(SetVar[] sets, SetVar indices, SetVar union) {
        super(ArrayUtils.append(sets, new SetVar[]{union, indices}), PropagatorPriority.QUADRATIC, false);
        k = sets.length;
        mates = new int[union.getUB().size()][2];
        for (int i = 0; i < mates.length; i++) {
            mates[i] = new int[]{0, 1};
        }
    }

    //***********************************************************************************
    // METHODS
    //***********************************************************************************
    @Override
    public void propagate(int evtmask) throws ContradictionException {
        SetVar union = vars[k];
        SetVar indices = vars[k + 1];
        filter1(indices, union);
        filter2(indices, union);
        //filter5(indices, union);
        filter6(indices, union);
        //System.out.printf("%n");
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        if (vIdx == k) {
            return SetEventType.ADD_TO_KER.getMask();
        }
        return SetEventType.ALL_EVENTS;
    }

    private void filter1(SetVar indices, SetVar union) throws ContradictionException {
        ISetIterator it;
        for (int i : indices.getLB()) {
            it = vars[i].getLB().newIterator();
            while (it.hasNext()) {
                union.force(it.nextInt(), this);
            }
            it = vars[i].getUB().newIterator();
            while (it.hasNext()) {
                int v = it.nextInt();
                if (!union.getUB().contains(v)) {
                    vars[i].remove(v, this);
                }
            }
        }
    }

    private void filter2(SetVar indices, SetVar union) throws ContradictionException {
        ISetIterator it;
        for (int i : indices.getUB()) {
            boolean found = false;
            it = vars[i].getUB().newIterator();
            while (it.hasNext()) {
                int v = it.nextInt();
                if (union.getUB().contains(v)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                indices.remove(i, this);
            }
        }
    }

    private void filter5(SetVar indices, SetVar union) throws ContradictionException {
        ISetIterator it;
        for (int u : union.getUB()) {
            int mate = -1;
            it = indices.getUB().newIterator();
            while (it.hasNext()) {
                int i = it.nextInt();
                if (vars[i].getUB().contains(u)) {
                    if (mate == -1) {
                        mate = i;
                    } else {
                        mate = -2;
                        break;
                    }
                }
            }
            if (mate == -1) {
                union.remove(u, this);
            } else if (mate > -1 && union.getLB().contains(u)) {
                indices.force(mate, this);
                vars[mate].force(u, this);
            }
        }
    }

    private void filter6(SetVar indices, SetVar union) throws ContradictionException {
        iii.clear();
        ISetIterator it = indices.getUB().newIterator();
        while (it.hasNext()) {
            iii.set(it.nextInt());
        }
        for (int u : union.getUB()) {
            filter66(u, indices, union);
        }
    }

    private void filter66(int u, SetVar indices, SetVar union) throws ContradictionException {
        int[] ms = mates[u];
        boolean l0 = iii.get(ms[0]) && vars[ms[0]].getUB().contains(u);
        boolean l1 = iii.get(ms[1]) && vars[ms[1]].getUB().contains(u);
        if (l0 && !l1) {
            // swap
            int m = ms[0];
            ms[0] = ms[1];
            ms[1] = m;
            boolean b = l0;
            l0 = l1;
            l1 = b;
        }
        if (!l0) { // find another watcher
            int k = 0;
            //ISetIterator it = indices.getUB().newIterator();
            //while (it.hasNext()) {
            for (int i = iii.nextSetBit(0); i > -1; i = iii.nextSetBit(i + 1)) {
                if (i == ms[0] || i == ms[1]) continue;
                if (vars[i].getUB().contains(u)) {
                    if (k == 0) {
                        ms[k++] = i;
                        l0 = true;
                        if (l1) {
                            break;
                        }
                    } else {
                        ms[k] = i;
                        l1 = true;
                        break;
                    }
                }
            }
        }
        if (!l0 && !l1) {
            union.remove(u, this);
        } else if (l0 ^ l1 && union.getLB().contains(u)) {
            int mate = l0 ? ms[0] : ms[1];
            indices.force(mate, this);
            vars[mate].force(u, this);
        }
    }

    private final BitSet checker = new BitSet();

    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            checker.clear();
            for (int i : vars[k + 1].getLB().toArray()) {
                for (int j : vars[i].getLB().toArray()) {
                    if (!vars[k].getLB().contains(j)) {
                        return ESat.FALSE;
                    }
                    checker.set(j);
                }
            }
            return ESat.eval(checker.cardinality() == vars[k].getLB().size());
        }
        return ESat.UNDEFINED;
    }

}
