/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2021, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */
package feistel.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.SetVar;
import org.chocosolver.util.ESat;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 15/11/2021
 */
public class PropMaxLe extends Propagator<SetVar> {
    public PropMaxLe(SetVar setVar, SetVar setVar1) {
        super(setVar, setVar1);
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        // max(s0) +2 <= max(s1)
        int m1 = vars[1].getUB().max();
        for (int i = vars[0].getUB().max() + 2; i <= m1; i++) {
            vars[1].remove(i, this);
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }
}
