type noeud = R of int |V of int |A|E|Non
type arb = N of noeud*arb list |F of noeud list

(*Print*)
let ntostring n = match n with
  |R i->"R"^string_of_int i|V i->"V"^string_of_int i|A->""|E->"e"|Non->" Non"
let rec nltostring l= match l with []->"\\"|a::tl-> nltostring tl^ntostring a
let rec patprint c = match c with
    F nl -> nltostring nl
  | N (_,l) -> List.fold_left (fun acc elt -> acc ^ patprint elt) "" l
let rec nlltostring l = match l with
    []->""
  | a::tl -> (nltostring a)^(nlltostring tl)
let rec patlist c = match c with
    F nl -> [nltostring nl]
  | N (_,l) -> List.fold_left (fun acc elt -> acc @ patlist elt) [] l

(*Utils*)
let rec inl c l = match l with []->false|a::tl->if a=c then true else inl c tl
let inch c ch = inl c ch
let rec subl x l= match l with []->[]|a::tl-> if x=a then tl else a::subl x tl
let hd l = match l with []->E|a::tl->a
let rec last l= match l with []->E|a::[]->a|a::tl->last tl
let rec getA c ch = match ch with
  |a::A::b::tl-> if b=c then a else getA c (b::tl)
  |a::E::b::tl-> getA c (b::tl)
  |A::tl|E::tl-> getA c tl
  |a->Non(*failwith ("getA "^(nltostring a)^ntostring c)*)
let rec getE c ch = match ch with
  |a::A::b::tl-> getE c (b::tl)
  |a::E::b::tl-> if b=c then a else getE c (b::tl)
  |A::tl|E::tl-> getE c tl
  |a->Non(*failwith ("getE "^(nltostring a))*)
let geti c = match c with
  |R i|V i->i|_->failwith ("geti "^(ntostring c))
let rec okay ch = true

(*Build tree*)
let rec  make n r cur newr newv ch free epsifree =
  if r=0 then F (cur::ch) else if not (okay ch) then F [] else
    N(cur, if inch cur ch then
	match cur with
	  | V i-> make n (r-1) (getA cur (cur::ch)) newr newv (A::cur::ch) free epsifree::[]
	  | R i-> diverge n r cur newr newv (cur::ch) free epsifree
	  | a->failwith ("inch "^(ntostring a))
      else match cur with
	| R i->newa n r cur newr newv (cur::ch) free epsifree@
	       newe n r cur newr newv (cur::ch) free epsifree
	| V i->newa n r cur newr newv (cur::ch) free epsifree
	| a->failwith ("outch "^(ntostring a)^nltostring ch)
    )
and newa n r cur newr newv ch free epsifree =
  if geti newr<n/2||geti newv<n/2||free<>[] then
    (match newr with R i->(if i<n/2 then [
      make n (r-1) newr (R (i+1)) newv (A::ch) free epsifree
    ] else[])|_->[])
    @(match newv with V i->(if i<n/2 then [
      make n (r-1) newv newr (V (i+1)) (A::ch) free (newv::epsifree)
    ] else[])|_->[])
    @List.map (fun x->if x=cur then F [(* no 1loop*)] else
      make n (r-1) x newr newv (A::ch) (subl x free) epsifree
    ) free
  else if r<=1 then [F (ch)] else []
    
and newe n r cur newr newv ch free epsifree =
  if geti newv<n/2||epsifree<>[] then
    (match newv with V i->(if i<n/2 then [
      make n r newv newr (V (i+1)) (E::ch) (newv::free) epsifree
    ] else[])|_->[])
    @List.map (fun x->
      make n r x newr newv (E::ch) free (subl x epsifree)
    ) epsifree
  else if r<=1 then [F (ch)] else []
    
and diverge n r cur newr newv ch free epsifree = (*cur is red*)
  match getA cur ch, getE cur ch with
      Non,Non->failwith "nothing to get"
    | Non,e  ->
      make n r e   newr newv (E::ch) free epsifree::
      newa n r cur newr newv     ch  free epsifree
    | a  ,Non->
      make n (r-1) a newr newv (A::ch) free epsifree::
      newe n r cur newr newv     ch  free epsifree
    | a  ,e  ->
      make n r e   newr newv (E::ch) free epsifree::
      make n (r-1) a newr newv (A::ch) free epsifree::[]

(*tests*)
let r = 7
let n = 10
let v0 = make n (r) (V 0) (R 0) (V 1) [] [V 0] [V 0]
let r0 = make n (r) (R 0) (R 1) (V 0) [] [R 0] []
let vvr = make n (r-2-1) (R 0) (R 1) (V 2) [A;V 1;A;V 0] [V 0] [V 0;V 1]
let rrv = make n (r-2) (V 0) (R 2) (V 1) [A;R 1;A;R 0] [R 0] []
let vvvr = make n (r-3) (R 0) (R 1) (V 3) [A;V 2;A;V 1;A;V 0] [V 0] [V 0;V 1;V 2]

let rec countf c = match c with
    F l -> if l=[] then 0 else 1
  | N (_,l) -> List.fold_left (fun acc elt -> acc + countf elt) 0 l

let rec countn n r = if n>4 then countf (vvvr)::countn (n-2) r else []

let rec countrn n r= if r>6 then countn n r::countrn n (r-1) else []
(*let combien = countrn 32 9*)

let rec listlist c = match c with
    F nl -> if nl=[] then [] else [nl]
  | N (_,l) -> List.fold_left (fun acc elt -> acc @ listlist elt) [] l


let rec countlast c der = match c with
    F l -> if l=[] then 0 else if der = hd l then 1 else 0
  | N (_,l) -> List.fold_left (fun acc elt -> acc + countlast elt der) 0 l

let rec getlast c der = match c with
    F l -> if l=[] then [] else if der = hd l then l else []
  | N (_,l) -> List.fold_left (fun acc elt -> acc @ getlast elt der) [] l

let rec isempty l= match l with []->true |c::tl->(match c with
    F ll -> if ll=[] then isempty tl else false
  | N (_,_)-> false)
    
let rec simplify cond c = match c with
    F l -> if cond l then F l else F []
  | N (e,l)-> if isempty l then F [] else N (e,List.map (fun x->simplify cond x) l)

let svvr = simplify (fun x->hd x=R 0) vvr

let cmbvvr = countf vvr
let cmbsvvr = countf svvr

let llsvvr = listlist svvr

  
let a = nlltostring llsvvr
  
(*
    
let cmbv0 = countlast vvr (V 0)
let cmbv1 = countlast vvr (V 1)
let cmbr0 = countlast vvr (R 0)(*
let getr0 = getlast vvr (R 0)

    (*
let cmbv = countf vvr
let cmbr = countf rrv
    
;;
(*
let lv = listlist v0
let lr = listlist r0
(*
let filter ll = match 


    (*
    
let comparell l1 l2 = compare (ntostring (hd l1)) (ntostring (hd l2))

let sortlastv = nlltostring (List.sort comparell (listlist (v0)))
let sortlastr = nlltostring (List.sort comparell (listlist (r0)))



(*
let ff  aa = match aa with []->false|a::tl->inl a tl

let rec verif l = match l with []->false|a::tl->if inl a tl then true else verif tl
let fff = verif aa


  
(*let startv = N(V 0,[N(V 1,[N(V 2,[test])])])
(*type tra = SI|SP|I|P
type blo = {p:tra;i:tra}


let get e t = Array.get t e
  
let a = get 0 p

let rec inl a l = match l with []->false|aa::tl->if a=aa then true else inl a tl

let rec cir a l p =
  if inl a l then [List.rev (a::l)] else if a mod 2 = 0 then cir (get a p) (a::l) p@cir (get (a+1) p) (a::l) p else cir (get a p) (a::l) p

let id = [|0;1;2;3;4;5|]
let p1 = [|1;5;0;4;2;3|]
let res0 = cir 0 [] p

let p2 = [|1;5;0;4;3;2|]
let res1 = cir 0 [] p

let rec ulist l = match l with []->[]|a::tl-> if inl a tl then ulist tl else a::ulist tl

let rec dif a r p = if r=0 then [a] else if a mod 2 = 0 then dif (get a p) (r-1) p @ dif (get (a+1) p) (r-1) p else dif (get a p) (r-1) p

let fulldif a r p = List.length (ulist (dif a r p))

let a = ulist (dif 1 7 p1)
let res0 = fulldif 1 5 p1
let res1 = fulldif 1 5 p2

let rec repeatlast c ch = match ch with []|_::[]|_::_::[]->failwith "no cycle to reapeat"
  |a::E::b::tl-> if c=b then a::E::ch else repeatlast c (b::tl)
  |a::A::b::tl-> if c=b then a::A::ch else repeatlast c (b::tl)

let rec isA c ch = match ch with []|_::[]->false|a::A::tl-> if a=c then true else isA c tl
let rec isE c ch = match ch with []|_::[]->false|a::E::tl-> if a=c then true else isE c tl
let isAE c ch = isA c ch && isE c ch
*)
