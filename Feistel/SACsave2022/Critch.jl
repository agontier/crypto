using Combinatorics
using SparseArrays
using Random
function nbways(x,p,fin,t)
        if t > 1
                x = p[x]#go to next red
                return nbways(x-1,p,fin,t-1) +
                        if p[x]>0 nbways(p[x],p,fin,t-2)
                        else 0 end
        else
                if t==1 x=p[x]-1 end
                return x==fin 
        end
end
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways,pool)
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways,pool)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways,pool)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways,pool)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        if nbways(deb,p,fin,R-2) < X
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways,pool)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways,pool)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function countway(x,p,cp,fin,way)
        cw = 0
        if count_ones(way) < R-3
                x = p[x]
                cw += countway(x-1,p,cp,fin,way<<2|2)
                if p[x] > 0
                        cw += countway(p[x],p,cp,fin,way<<2|3)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                cw += countway(p[x],p,cp|1<<y,fin,way<<2|3)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                cw += x==fin
        end
        return cw
end
function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end
function witchway(p,cp,gways,pool)
        for deb in 1:2:2k for fin in 1:2:2k
                if hasforbidden(deb,p,fin,gways)
                        #println(deb," ",fin," ",p)
                        return;
                end end end
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:2:2k for fin in 1:2:2k if nbways(deb,p,fin,R-2) < X
                tmp = countway(deb,p,cp,fin,0)
                if minch > tmp
                        minch = tmp
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,0,zeros(Int8,2^(2R-4)),1,gways,pool)
        else
                if 0 in p pool[2]+=1 end
                if p in pool pool[1]+=1
                else
                        print(1)
                        push!(pool,copy(p))
                end
        end
end

function makecycle()
        tasks = []
        partoch = collect(partitions(k))
        for part in partoch if part[1] > 0
                pc = zeros(Int8,k)
                ii = 1
                for c in part
                        if c==1
                                pc[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        pc[l] = l+1
                                end
                                pc[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                p = zeros(Int8,2k)
                for l in 1:k
                        if pc[l]>0
                                p[2l-1] = 2pc[l]
                        end
                end
                pool = []
                push!(pool,0,0)
                push!(tasks,[p,pool,part])
        end end
        println(length(tasks)," tasks")
        return tasks
end
function paramain()
        tasks = shuffle(makecycle())
        Threads.@threads for i in 1:np
                t = tasks[i]
                pool = t[2]
                gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
                witchway(t[1],0,gways,pool)
                dejavu = pool[1]
                zeroin = pool[2]
                nbsol = length(pool)-2

                if nbsol > 0
                        println("\n",t[3],"               ",
                                if nbsol>0 string(nbsol," sols     ") else " " end,
                                if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                if zeroin>0 string(zeroin," incomplete     ") else " " end)
                end
                bigpool[i] = deepcopy(pool)
        end
end


const X=1
const R=9
const k=8

global partoch = collect(partitions(k))
global np = length(partoch)
global bigpool = [[] for i in 1:np]

@time paramain()


