using Combinatorics
using SparseArrays
using Random
function countsbox(x,p,fin,t,tab,R)
        if t > 1
                x = p[x]#go to next red
                if p[x]==0 
                        tab[x>>1,t] = true
                        return countsbox(x-1,p,fin,t-1,tab,R)
                else
                        a = countsbox(p[x],p,fin,t-2,tab,R)
                        tab[x>>1,t] = true
                        return a .| countsbox(x-1,p,fin,t-1,tab,R)
                end
        else
                if t==1
                        tab[p[x]>>1,t] = true
                        x=p[x]-1
                end
                if x==fin return tab
                else return BitArray(zeros(Bool,k,R-2)) end #Hardcode
        end
end
function nbsbox(x,p,fin,t,R)
        return sum(countsbox(x,p,fin,t,BitArray(zeros(Bool,k,t)),R))
end
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways,pool,R,X)#make way for the king
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways,pool,R,X)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways,pool,R,X)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways,pool,R,X)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        if nbsbox(deb,p,fin,R-2,R) < X
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways,pool,R,X)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways,pool,R,X)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function hasforrec(x,p,deb,fin,way,gways,R)#verify forbiden ways
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways,R) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways,R)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways,R)#verify forbiden ways
        return hasforrec(deb,p,deb,fin,0,gways,R)
end
function addtopool(p,pool)#add to pool without redundancy
        if p in pool pool[1]+=1
        else
                #print(1)#println
                push!(pool,copy(p))
        end
end
function complete(p,pool)#complete p if needed then add to pool
        if 0 in p
                pool[2]+=1
                i = findfirst(isequal(0),p)
                for v in 1:length(p)
                        if ! (v in p)
                                p[i]=v
                                complete(copy(p),pool)
                        end
                end
        else
                addtopool(p,pool)
        end
end
function witchway(p,cp,gways,pool,R,X)
        if length(pool)<502 #HARDCODE
                for deb in 1:2:2k for fin in 1:2:2k if hasforbidden(deb,p,fin,gways,R)
                                #on vas des verts aux verts parce qu'ils sont fusionnets avec les rouges a cause des ecycles
                                return;
                        end end end
                for deb in 2k-1:-2:1 for fin in 2k-1:-2:1 if nbsbox(deb,p,fin,R-2,R) < X
                        makeway(deb,p,cp,deb,fin,0,zeros(Int16,2^(2R-4)),1,gways,pool,R,X)
                        return;
                end end end
                complete(p,pool)
        end
end
function makecycle()#build all e-cycle decompositions
        tasks = []
        partoch = collect(partitions(k))
        for part in partoch if part[1] > 0
                pc = zeros(Int8,k)
                ii = 1
                for c in part
                        if c==1
                                pc[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        pc[l] = l+1
                                end
                                pc[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                p = zeros(Int8,2k)
                for l in 1:k
                        if pc[l]>0
                                p[2l-1] = 2pc[l]
                        end
                end
                pool = []
                push!(pool,0,0)
                push!(tasks,[p,pool,part])
        end end
        #println(length(tasks)," tasks")
        return tasks
end
function paramain()#search solutions until pool>=500
        tasks = shuffle(makecycle())
        pool = []
        push!(pool,0,0)
        first = false
        while length(pool)<502 #HARDCODE
                #println("----------- R = ",RR," -----------")
                Threads.@threads for i in 1:np
                        R = RR
                        X = XX
                        t = tasks[i]
                        #pool = t[2]
                        gways = [spzeros(Int16,2^(2R-4)) for i in 1:k, j in 1:k]
                        witchway(t[1],0,gways,pool,R,X)
                        dejavu = pool[1]
                        zeroin = pool[2]
                        nbsol = length(pool)-2
                        if !first && nbsol>0
                                first=true
                                print(RR)
                        end
                        if false#nbsol > 0 || true #&& false
                                println("\n",t[3],"               ",
                                        if nbsol>0 string(nbsol," sols     ") else " " end,
                                        if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                        if zeroin>0 string(zeroin," incomplete     ") else " " end)
                        end
                end
                global RR = RR+1
        end
        return pool
end
function difrec(b,dif,dif2,active,newdifs)#compode dif propagation
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end
function apply(p,dif)#apply perm p to a dif
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end
function getdiff2(difs,p)#propag all difs
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end
function paradifs(tasks)#parallel diferencial analysis
        #ADI,ADT = getAD()
        res = zeros(Int8,length(tasks),16)
        Threads.@threads for i in 1:length(tasks)
                p = tasks[i]
                #adi,adt = deepcopy(ADI),copy(ADT)
                difs = ones(Int8,2^2k-1)
                for r in 1:16
                        #difs = getdiff3(difs,p,adi,adt)
                        difs = getdiff2(difs,p)
                        res[i,r] += findmin(difs)[1]-1
                end
        end
        return res
end
function an(res)#comparison with best diferencials for k6,k7,k8
        if k==6
                kmax = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 16, 19, 22, 24, 26, 28, 29]
        elseif k==7
                kmax = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 19, 23, 26, 28, 30, 33, 35]
        else
                kmax = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 19, 22, 26, 29, 31, 34, 37]
        end
        max = [findmax(res[:,r])[1] for r in 1:16]
        nbmax = [count(x->x==max[r],res[:,r]) for r in 1:16]
        stat = zeros(Int,16)
        for i in 10:16
                if max[i]==kmax[i] #hardcode
                        #print(i,"  ")
                        stat[i] = nbmax[i]#nbmax[1] ÷ nbmax[i]
                else
                        stat[i] = 0#nbmax[1]
                end
        end
        stat[1] = nbmax[1]
        #println(max)
        #println(nbmax)
        return stat
end
function printtab(t)#print a latex tabular raw
        for i in 1:length(t)-1
                print(t[i]," & ")
        end
        println(t[end],"\\\\\\hline")
end



global XX=1
global RR=8
const k = Int8(7)

global partoch = collect(partitions(k))
global np = length(partoch)

function main()
        for X in 1:12
                global XX = X
                global RR = 8
                print(XX," SBoxes (R")
                pool = paramain()
                #res = paradifs(pool[3:end])
                #stat = an(res)
                print("-",RR-1,") & ")
                #printtab(stat[end-6:end])
                
        end
end

main()


p = [4, 5, 6, 9, 2, 13, 10, 11, 12, 3, 8, 15, 14, 7, 16, 1] #mieuxquetwine ?
#[0 1 2 3 4 6 8 11 14 19 22 24 26 28 30 32]

p = [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1#TWINE
#[0 1 2 3 4 6 8 11 14 18 22 24 27 30 32 35]
res = paradifs([p])
