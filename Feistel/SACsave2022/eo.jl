using Combinatorics
using Random
function haspath(x,p,fin,t)
        if t > 0
                return x&1==0 && haspath(x-1,p,fin,t) || p[x]>0 && haspath(p[x],p,fin,t-1)
        else
                return x==fin || x&1==0 && x-1==fin
        end
end
function checkinv(p)
        pm1 = zeros(Int8,2k)
        for i in 1:2k
                pm1[p[i]] = i
        end
        for deb in 2k-1:-2:1 for fin in 2k:-2:2 
                if !haspath(deb,pm1,fin,R-1)
                        return false
                end
        end end
        return true
end
function makeway(x,p,cp,deb,fin,way,pool)
        if count_ones(way) < R-3
                if x&1==0 makeway( x-1,p,cp,deb,fin,way<<1  ,pool) end
                if p[x]>0 makeway(p[x],p,cp,deb,fin,way<<1|1,pool)
                else
                        for y in 1:2:2k if cp>>y & 1==0
                                p[x] = y
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<1|1,pool)
                        end end
                        p[x] = 0
                end
        elseif x == fin || x&1==0 && x-1 == fin
                witchway(p,cp,pool)
        end
end
function witchway(p,cp,pool)
        for deb in 2k:-2:2 for fin in 2k-1:-2:1 
                if !haspath(deb,p,fin,R-3)
                        makeway(deb,p,cp,deb,fin,0,pool)
                        return;
                end
        end end
        if 0 in p pool[2]+=1
        elseif p in pool pool[1]+=1
        else
                if checkinv(p)
                        print(1)
                        push!(pool,copy(p))
                end
        end
end
function makecycle()
        tasks = []
        partoch = collect(partitions(k))
        for part in partoch if part[1] > 0
                used = 0
                pc = zeros(Int8,k)
                ii = 1
                for c in part
                        if c==1
                                pc[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        pc[l] = l+1
                                end
                                pc[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                p = zeros(Int8,2k)
                for l in 1:k
                        if pc[l]>0
                                p[2l-1] = 2pc[l]
                                used|=1<<2pc[l]
                        end
                end
                pool = []
                push!(pool,0,0)
                push!(tasks,[p,used,pool,part])
        end end
        println(length(tasks)," tasks")
        return tasks
end
function paracycle()
        tasks = shuffle(makecycle())
        Threads.@threads for t in tasks
                pool = t[3]
                witchway(t[1],t[2],pool)
                dejavu = pool[1]
                zeroin = pool[2]
                nbsol = length(pool)-2

                if nbsol > 0
                        println(t[4],"               ",
                                if nbsol>0 string(nbsol," sols     ") else " " end,
                                if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                if zeroin>0 string(zeroin," incomplete     ") else " " end)
                end
        end
end

const R = 8
const k = 11

@time paracycle()

#println(length(makechaines()))
