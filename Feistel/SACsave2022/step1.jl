using Combinatorics
using SparseArrays


function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end
function difrec3(b,dif,dif2,active,adi,adt)
        if b==k
                push!(adi,dif2)
                adt[dif] = active
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec3(b+1,dif,dif2,active,adi,adt)
                elseif c==1 #juste un vert
                        difrec3(b+1,dif,dif2 | 1 << 2b,active,adi,adt)
                elseif c==2 #juste un rouge
                        difrec3(b+1,dif,dif2 | 3 << 2b,active+1,adi,adt)
                elseif c==3 #doubling
                        difrec3(b+1,dif,dif2 | 2 << 2b,active+1,adi,adt)
                        difrec3(b+1,dif,dif2 | 3 << 2b,active+1,adi,adt)
                end
        end
end
function getAD()
        ADI = Vector{Vector{Int}}(undef,2^2k-1)
        ADT = Vector{Int8}(undef,2^2k-1)
        for dif in 1:2^2k-1
                adi = Vector{Int}(undef,0)
                ADI[dif] = adi
                difrec3(0,dif,0,0,adi,ADT)
        end
        return ADI,ADT
end
function getdiff3(difs,p,ADI,ADT)
        newdifs = [findmin(difs[ADI[dif]])[1]+ADT[dif] for dif in 1:2^2k-1]
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end


function paradifs(tasks)
        #ADI,ADT = getAD()
        res = zeros(Int8,length(tasks),16)
        Threads.@threads for i in 1:length(tasks)
                p = tasks[i]
                #adi,adt = deepcopy(ADI),copy(ADT)
                difs = ones(Int8,2^2k-1)
                for r in 1:16
                        #difs = getdiff3(difs,p,adi,adt)
                        difs = getdiff2(difs,p)
                        res[i,r] += findmin(difs)[1]-1
                end
        end
        return res
end
function complete(pool)        
        for p in pool
                for i in 1:length(p)
                        if p[i] == 0
                                for v in 1:length(p)
                                        if ! (v in p)
                                                p[i]=v
                                        end
                                end
                        end
                end
        end
end
function removedouble(pool)        
        p2 = []
        for p in pool
                if !(p in p2)
                        push!(p,p2)
                end
        end
end
function removedouble(pool)        
        p2 = []
        for p in pool
                if !(p in p2)
                        push!(p2,p)
                end
        end
        return p2
end
function getpfrombigpool()
        pool = []
        for i in 1:length(bigpool)
                if length(bigpool[i])!=2
                        append!(pool,bigpool[i][3:end])
                end
        end
        complete(pool)
        pool = removedouble(pool)
        return pool
end


function an(res)
        max = [findmax(res[:,r])[1] for r in 1:16]
        nbmax = [count(x->x==max[r],res[:,r]) for r in 1:16]
        stat = zeros(Int,16)
        for i in 11:16
                if max[i]==k8max[i] #hardcode
                        print(i,"  ")
                        stat[i] = nbmax[1] ÷ nbmax[i]
                else
                        stat[i] = nbmax[1]
                end
        end
        stat[1] = nbmax[1]
        println(max)
        println(nbmax)
        return stat
end


function an2(res)
        max = [findmax(res[:,:,r])[1] for r in 1:16]
        nbmax = [count(x->x==max[r],res[:,:,r]) for r in 1:16]
        println(max)
        println(nbmax)
end




@time res = paradifs(getpfrombigpool())
#@time res = paradifs(a)

k6max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 16, 19, 22, 24, 26, 28, 29]
k7max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 19, 23, 26, 28, 30, 33, 35]
k8max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 19, 22, 26, 29, 31, 34, 37]



stat = an(res)


function difrec(b,dif,dif2,active,newdifs)
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end
function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end
function getdiff2(difs,p)
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end
function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,np,na,16)
        res2 = [[] for i in 1:16]
        for i in 1:np
                println(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                @inbounds Threads.@threads for jt in 0:Threads.nthreads()-1
                        #adi,adt = deepcopy(ADI),copy(ADT)
                        for ja in 1:charge
                                j = jt*charge+ja
                                if j<=na
                                        q = allperm[j]
                                        pq = zeros(Int8,2k)
                                        for l in 1:k
                                                pq[2l] = 2p[l]-1
                                                pq[2l-1] = 2q[l]
                                        end
                                        #pq =  [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1

                                        difs = ones(Int8,2^2k-1)
                                        for r in 1:16
                                                difs = getdiff2(difs,pq)                                                
                                                res[i,j,r] += findmin(difs)[1]-1
                                                if r<6
                                                        if res[i,j,r] < k6max[r]
                                                                break;
                                                        end
                                                elseif r>8 && k6max[r] == res[i,j,r]
                                                        push!(res2[r],pq)
                                                end
                                        end
                                end
                        end
                end
                #println(partoch[i]," ",res[i,:,end])
                #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        end
        io = open(string("res",k,".txt"), "w")
        print(io,"const res",k," = ")
        println(io,res2)
        #write(io,string(res2))
        close(io)
        return res2
end
using Combinatorics

const k6max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 16, 19, 22, 24, 26, 28, 29]

const k = Int8(8)
const partoch = collect(partitions(k))
const np = length(partoch)
const allperm = collect(permutations([i for i in 1:k]))
const na = length(allperm)
const charge = div(na,Threads.nthreads())+1
#ADI,ADT = getAD()
#const adi = Tuple(ADI)
#const adt = Tuple(ADT)
@time res2 = paramain()

for i in 1:16
        println(length(res2[i]))
end

#=
23276
28939
35459
24961
34622
54189
32293

34679
23276
28939
35459
24962
34622
54189
32293


k6max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 16, 19, 22, 24, 26, 28, 29]
k7max = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 19, 23, 26, 28, 30, 33, 35]
#k6X1R7
k6a = Int8[0, 1, 2, 3, 4, 6, 8, 10, 12, 14, 17, 20, 22, 24, 26, 29]
nk6a =        [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 8, 16, 16, 16, 8]
k6b = Int8[0, 1, 2, 3, 4, 6, 8, 11, 14, 16, 19, 22, 24, 26, 28, 29]
nk6b =        [687, 687, 687, 687, 687, 687, 375, 154, 121, 106, 106, 44, 63, 63, 32, 72]
kk = k6max .- k6
findall(x->x==0,kk)
k6nbmax = [7920, 7920, 7920, 2915, 2915, 2915, 778, 217, 161, 118, 158, 44, 67, 67, 32, 109]
k7nbmax = [75600, 75600, 75600, 27810, 27810, 27810, 7426, 2575, 2103, 36, 36, 36, 36, 36, 48, 48]
k8nbmax = [887040, 887040, 887040, 326326, 326326, 326326, 88654, 39670, 34679, 52, 48, 24, 48, 136, 48, 240]
const k = 8
const partoch = collect(partitions(k))
const np = length(partoch)
const allperm = collect(permutations([i for i in 1:k]))
const na = length(allperm)
const charge = div(na,Threads.nthreads())+1
function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,np,na,16)
        ADI,ADT = getAD()
        #io = open("res.txt", "w")
        for i in 1:np
                println(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                Threads.@threads for jt in 0:Threads.nthreads()-1
                        adi,adt = deepcopy(ADI),copy(ADT)
                        for ja in 1:charge
                                j = jt*charge+ja
                                if j<=na
                                        q = allperm[j]
                                        pq = zeros(Int8,2k)
                                        for l in 1:k
                                                pq[2l] = 2p[l]-1
                                                pq[2l-1] = 2q[l]
                                        end
                                        #pq =  [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                        difs = ones(Int8,2^2k-1)
                                        for r in 1:16
                                                difs = getdiff3(difs,pq,adi,adt)
                                                res[i,j,r] += findmin(difs)[1]-1
                                        end
                                end
                        end
                end
                println(partoch[i]," ",res[i,:,end])
                #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        end
        #close(io)
        return res
end
@time res2 = paramain()
an2(res2)
=#

#=
function enum71()
        part = [k-1,1]
        pc = zeros(Int8,k)
        ii = 1
        for c in part
                if c==1
                        pc[ii] = ii
                        ii +=1
                else
                        for l in ii:ii+c-2
                                pc[l] = l+1
                        end
                        pc[ii+c-1] = ii
                        ii = ii+c
                end
        end
        pool = []
        push!(pool,0,0)
        for q in collect(permutations([i for i in 1:k]))
                pq = zeros(Int8,2k)
                for l in 1:k
                        pq[2l] = 2pc[l]-1
                        pq[2l-1] = 2q[l]
                end
                push!(pool,pq)
        end
        return pool
end
pool = enum71()
res = paradifs(pool[3:end])
sstat = an(res)

print(XX," paths (R",RR-1,") & ")
printtab(sstat[end-6:end])
=#
