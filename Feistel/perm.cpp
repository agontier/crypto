#include <iostream>
#include <map>
#include <algorithm>
#include <execution>
#include <mutex>

using namespace std;

class Perm {
public:
    map<int, int> link;
    map<int, int> elink;
    map<int, int> invlink;
    map<int, int> invelink;
};

unsigned nbNum(int first, int last, int k, int nextV, int nextR, Perm & P, int r) {//first: début du chemin qu'on veut relier, last: fin du chemin, k: nombre de noeuds n = 2k, nextV: indice du prochain noeud vert libre, nextR: prochain rouge libre, P: permutation en construction, r: nombre de transitions restantes pour finir le chemin
    if (r == 0) {//les cas de base ou on a plus de transitions possibles
        if (first == last) return 1;//chemin fini
        if (first >= 0 || last < 0) return 0;//si first est vert ou last est rouge, perdu car on peut pas rattraper avec un elink
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return 0;//si first est déja elink a qqchose qui n'est pas last perdu
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return 0;//si last a déja un invelink qui n'est pas first perdu
        return 1;//on peut relier first et last avec un elink
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1);//si first est vert et déja link on compte a partir de son succ
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1);// si last est rouge et a déja des pred, alors on compte a partir de son pred
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1) + nbNum(first, it->second, k, nextV, nextR, P, r);//si last est vert et a déja des pred et des epred alors on somme les deux
        }
    }
    unsigned res = 0;//compteur de chemins
    if (it_link_first != P.link.end()) res += nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1);//si first a déja un succ (et est implicitement rouge) alors on compte en plus a partir de son succ
    else {//si first n'a pas de link et quel que soit sa couleur
        auto it = P.invlink.begin();//iterateur sur link.
        for (int i = nextR+1; i < nextV; ++i) {//On itère sur tous les noeuds existants de la permut
            if (it != P.invlink.end() && it->first == i) ++it;// si notre arc arrive déja sur un noeud de la permut on prend le suivant
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                res += nbNum(i, last, k, nextV, nextR, P, r-1);// on ajoute le cas de l'arc qui retourne sur la permut par i
                P.link.erase(first);
                P.invlink.erase(i);
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            res += nbNum(nextV, last, k, nextV+1, nextR, P, r-1);// on compte le chemin qui continu avec un nouveau vert 
            P.link.erase(first);
            P.invlink.erase(nextV);
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            res += nbNum(nextR, last, k, nextV, nextR-1, P, r-1);// on compte le chemin qui continu avec un nouveau rouge 
            P.link.erase(first);
            P.invlink.erase(nextR);
        }
    }
    if (first < 0) {//si first est rouge 
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) res += nbNum(it_elink_first->second, last, k, nextV, nextR, P, r);// si first est déja elink, on ajoute dans res
        else {
            auto it = P.invelink.begin();//iteration sur les invelink
            for (int i = 0; i < nextV; ++i) {// on itère sur les verts
                if (it != P.invelink.end() && it->first == i) ++it;//si l'elink regardé par it fini par un truc déja dans la perm on prend le suivant
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    res += nbNum(i, last, k, nextV, nextR, P, r);//on ajoute le cas ou on retourne sur la permut par i
                    P.elink.erase(first);
                    P.invelink.erase(i);
                }
            }
            if (nextV < k) {//si on a le droit d'utiliser un nouveau vert
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                res += nbNum(nextV, last, k, nextV+1, nextR, P, r);//on vas sur ce nouveau vert avec un elink
                P.elink.erase(first);
                P.invelink.erase(nextV);
            }
        }
    }
    return res;
}

unsigned nbNum(int first, int last, int k, int nextV, int nextR, Perm & P, int r, unsigned bound) {//même chose sauf qu'on coupe la search de chemin si on dépasse un certain nombre de chemin bound
    if (r == 0) {
        if (first == last) return 1;
        if (first >= 0 || last < 0) return 0;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return 0;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return 0;
        return 1;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1, bound);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1, bound);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            auto tmp1 = nbNum(first, it_link_last->second, k, nextV, nextR, P, r-1, bound);
            if (tmp1 > bound) return tmp1;
            else return tmp1 + nbNum(first, it->second, k, nextV, nextR, P, r, bound-tmp1);
        }
    }
    unsigned res = 0;
    if (it_link_first != P.link.end()) {
        res += nbNum(it_link_first->second, last, k, nextV, nextR, P, r-1, bound);
        if (res > bound) return res;
    }
    else {
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                res += nbNum(i, last, k, nextV, nextR, P, r-1, bound - res);
                P.link.erase(first);
                P.invlink.erase(i);
                if (res > bound) return res;
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            res += nbNum(nextV, last, k, nextV+1, nextR, P, r-1, bound - res);
            P.link.erase(first);
            P.invlink.erase(nextV);
            if (res > bound) return res;
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            res += nbNum(nextR, last, k, nextV, nextR-1, P, r-1, bound - res);
            P.link.erase(first);
            P.invlink.erase(nextR);
            if (res > bound) return res;
        }
    }
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) res += nbNum(it_elink_first->second, last, k, nextV, nextR, P, r, bound - res);
        else {
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    res += nbNum(i, last, k, nextV, nextR, P, r, bound - res);
                    P.elink.erase(first);
                    P.invelink.erase(i);
                    if (res > bound) return res;
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                res += nbNum(nextV, last, k, nextV+1, nextR, P, r, bound - res);
                P.elink.erase(first);
                P.invelink.erase(nextV);
                if (res > bound) return res;
            }
        }
    }
    return res;
}

bool hasPath(int first, int last, Perm & P, int r) {//Sous partie de nbNum qui retourne true si un chemin est possible
    if (r == 0) {
        if (first == last) return true;
        if (first >= 0 || last < 0) return false;
        auto it = P.elink.find(first);
        return (it != P.elink.end() && it->second == last);
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return hasPath(it_link_first->second,last, P, r-1);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return hasPath(first, it_link_last->second, P, r-1);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            return hasPath(first, it_link_last->second, P, r-1) || hasPath(first, it->second, P, r);
        }
    }
    if (it_link_first != P.link.end() && hasPath(it_link_first->second, last, P, r-1)) return true;
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end() && hasPath(it_elink_first->second, last, P, r)) return true;
    }
    return false;
}

void allPerm(int first, int last, int k, int nextV, int nextR, Perm & P, int r, vector<Perm> & res) {
    if (r == 0) {
        if (first == last) {res.emplace_back(P); return;}
        if (first >= 0 || last < 0) return;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return;
        if (it != P.elink.end()) res.emplace_back(P);
        else {
            P.elink[first] = last;
            P.invelink[last] = first;
            res.emplace_back(P);
            P.elink.erase(first);
            P.invelink.erase(last);
        }
        return;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return allPerm(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return allPerm(first, it_link_last->second, k, nextV, nextR, P, r-1, res);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            allPerm(first, it_link_last->second, k, nextV, nextR, P, r-1, res);
            allPerm(first, it->second, k, nextV, nextR, P, r, res);
            return;
        }
    }
    if (it_link_first != P.link.end()) allPerm(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    else {
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                allPerm(i, last, k, nextV, nextR, P, r-1, res);
                P.link.erase(first);
                P.invlink.erase(i);
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            allPerm(nextV, last, k, nextV+1, nextR, P, r-1, res);
            P.link.erase(first);
            P.invlink.erase(nextV);
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            allPerm(nextR, last, k, nextV, nextR-1, P, r-1, res);
            P.link.erase(first);
            P.invlink.erase(nextR);
        }
    }
    if (first < 0) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) allPerm(it_elink_first->second, last, k, nextV, nextR, P, r, res);
        else {
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    allPerm(i, last, k, nextV, nextR, P, r, res);
                    P.elink.erase(first);
                    P.invelink.erase(i);
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                allPerm(nextV, last, k, nextV+1, nextR, P, r, res);
                P.elink.erase(first);
                P.invelink.erase(nextV);
            }
        }
    }
}

bool allPerm2(int first, int last, int k, int nextV, int nextR, Perm & P, int r, vector<Perm> & res) {
    if (r == 0) {
        if (first == last) {res.emplace_back(P); return true;}
        if (first >= 0 || last < 0) return false;
        auto it = P.elink.find(first);
        if (it != P.elink.end() && it->second != last) return false;
        auto it2 = P.invelink.find(last);
        if (it2 != P.invelink.end() && it2->second != first) return false;
        if (it != P.elink.end()) res.emplace_back(P);
        else {
            int nextV2 = (nextV == k) ? nextV : nextV + 1;
            auto iti = P.invelink.begin();
            for (int i = 0; i < nextV2; ++i) {
                if (iti != P.invelink.end() && iti->first == i) ++iti;
                else {
                    auto Q = P;
                    Q.elink[first] = i;
                    Q.invelink[i] = first;
                    res.emplace_back(move(Q));
                }
            }
        }
        return true;
    }
    auto it_link_first = P.link.find(first);
    if (it_link_first != P.link.end() && first >= 0) return allPerm2(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    auto it_link_last = P.invlink.find(last);
    if (it_link_last != P.invlink.end()) {
        if (last < 0) return allPerm2(first, it_link_last->second, k, nextV, nextR, P, r-1, res);
        auto it = P.invelink.find(last);
        if (it != P.invelink.end()) {
            if (allPerm2(first, it_link_last->second, k, nextV, nextR, P, r-1, res)) return true;
            return allPerm2(first, it->second, k, nextV, nextR, P, r, res);
        }
    }
    bool found = false;
    if (it_link_first != P.link.end()) found = allPerm2(it_link_first->second, last, k, nextV, nextR, P, r-1, res);
    else {
        vector<Perm> vtmp;
        auto it = P.invlink.begin();
        for (int i = nextR+1; i < nextV; ++i) {
            if (it != P.invlink.end() && it->first == i) ++it;
            else {
                P.link[first] = i;
                P.invlink[i] = first;
                if (found) vtmp.emplace_back(P);
                else {
                    found = allPerm2(i, last, k, nextV, nextR, P, r-1, res);
                    if (!found) vtmp.emplace_back(P);
                }
                P.link.erase(first);
                P.invlink.erase(i);
            }
        }
        if (nextV < k) {
            P.link[first] = nextV;
            P.invlink[nextV] = first;
            if (found) vtmp.emplace_back(P);
            else {
                found = allPerm2(nextV, last, k, nextV+1, nextR, P, r-1, res);
                if (!found) vtmp.emplace_back(P);
            }
            P.link.erase(first);
            P.invlink.erase(nextV);
        }
        if (nextR >= -k) {
            P.link[first] = nextR;
            P.invlink[nextR] = first;
            if (found) vtmp.emplace_back(P);
            else {
                found = allPerm2(nextR, last, k, nextV, nextR-1, P, r-1, res);
                if (!found) vtmp.emplace_back(P);
            }
            P.link.erase(first);
            P.invlink.erase(nextR);
        }
        if (found) {
            for (auto & Q : vtmp) res.emplace_back(move(Q));
            return true;
        }
    }
    if (first < 0 && !found) {
        auto it_elink_first = P.elink.find(first);
        if (it_elink_first != P.elink.end()) return allPerm2(it_elink_first->second, last, k, nextV, nextR, P, r, res);
        else {
            vector<Perm> vtmp;
            auto it = P.invelink.begin();
            for (int i = 0; i < nextV; ++i) {
                if (it != P.invelink.end() && it->first == i) ++it;
                else {
                    P.elink[first] = i;
                    P.invelink[i] = first;
                    if (found) vtmp.emplace_back(P);
                    else {
                        found = allPerm2(i, last, k, nextV, nextR, P, r, res);
                        if (!found) vtmp.emplace_back(P);
                    }
                    P.elink.erase(first);
                    P.invelink.erase(i);
                }
            }
            if (nextV < k) {
                P.elink[first] = nextV;
                P.invelink[nextV] = first;
                if (found) vtmp.emplace_back(P);
                else {
                    found = allPerm2(nextV, last, k, nextV+1, nextR, P, r, res);
                    if (!found) vtmp.emplace_back(P);
                }
                P.elink.erase(first);
                P.invelink.erase(nextV);
            }
            if (found) {
                for (auto & Q : vtmp) res.emplace_back(move(Q));
                return true;
            }
        }
    }
    return found;
}

void findPerm(Perm P, int r, int k) {
    static unsigned cptsol = 0;
    static mutex mt;
    unsigned res = ~0;
    auto nextV = max(P.link.rbegin()->first, P.invlink.rbegin()->first);
    if (!P.invelink.empty()) nextV = max(nextV, P.invelink.rbegin()->first);
    nextV += 1;
    auto nextR = min(P.link.begin()->first, P.invlink.begin()->first);
    if (!P.elink.empty()) nextR = min(nextR, P.elink.begin()->first);
    nextR -= 1;
    int p1 = -1;
    int p2 = 0;
    int rr = r-1;


    for (int i1 = 0; i1 < nextV; ++i1) {
        for (int i2 = -1; i2 > nextR; --i2) {
            if (!hasPath(i1, i2, P, r-1)) {
                auto tmp = nbNum(i1, i2, k, (i1 == nextV) ? nextV + 1 : nextV, (i2 == nextR) ? nextR - 1 : nextR, P, r-1, res);
                if (tmp < res) {
                    if (tmp == 0) return;
                    if (tmp == 1) {
                        vector<Perm> vPerm;
                        allPerm(i1, i2, k, (i1 == nextV) ? nextV + 1 : nextV, (i2 == nextR) ? nextR -1 : nextR, P, r-1, vPerm);
                        return findPerm(move(vPerm[0]), r, k);
                    }
                    res = tmp;
                    p1 = i1;
                    p2 = i2;
                }
            }
        }
    }
    bool flag = true;
    swap(P.link, P.invlink);
    for (int i1 = 0; i1 < nextV; ++i1) {
        for (int i2 = -1; i2 > nextR; --i2) {
            if (!hasPath(i1, i2, P, r-1)) {
                auto tmp = nbNum(i1, i2, k, (i1 == nextV) ? nextV + 1 : nextV, (i2 == nextR) ? nextR - 1 : nextR, P, r-1, res);
                if (tmp < res) {
                    if (tmp == 0) return;
                    if (tmp == 1) {
                        vector<Perm> vPerm;
                        allPerm(i1, i2, k, (i1 == nextV) ? nextV + 1 : nextV, (i2 == nextR) ? nextR -1 : nextR, P, r-1, vPerm);
                        return findPerm(move(vPerm[0]), r, k);
                    }
                    res = tmp;
                    p1 = i1;
                    p2 = i2;
                    flag = false;
                }
            }
        }
    }
    if (flag) swap(P.link, P.invlink);

    if (res == ~0 && nextV == k && nextR == -k-1) {
        mt.lock();
        cout << "\r" << "Yeah!! " << ++cptsol << flush;
        //getchar();
        mt.unlock();
    }
    else {
        if (res != ~0) {
            vector<Perm> vPerm;
            vPerm.reserve(res);
            allPerm2(p1, p2, k, (p1 == nextV) ? nextV + 1 : nextV, (p2 == nextR) ? nextR -1 : nextR, P, rr, vPerm);
            //cout << vPerm.size() << " (" << res << ") ";
            //getchar();
            for_each(execution::par, vPerm.begin(), vPerm.end(), [r,k](auto & Q){
                findPerm(move(Q), r, k);
            });
        }
        else {
            vector<Perm> vPerm;
            int nextR2 = (nextR == -k-1) ? nextR : nextR-1;
            int nextV2 = (nextV == k) ? nextV : nextV + 1;
            auto it = P.link.begin();
            for (int i1 = nextR + 1; i1 < nextV; ++i1) {
                if (it != P.link.end() && it->first == i1) ++it;
                else {
                    auto it2 = P.invlink.begin();
                    for (int i2 = nextR2 + 1; i2 < nextV2; ++i2) {
                        if (it2 != P.invlink.end() && it2->first == i2) ++it2;
                        else {
                            auto Q = P;
                            Q.link[i1] = i2;
                            Q.invlink[i2] = i1;
                            vPerm.emplace_back(move(Q));
                        }
                    }
                    return for_each(execution::par, vPerm.begin(), vPerm.end(), [r,k](auto & Q){
                        findPerm(move(Q), r, k);
                    });
                }
            }
            it = P.invelink.begin();
            for (int i1 = 0; i1 < nextV2; ++i1) {
                if (it != P.invelink.end() && it->first == i1) ++it;
                else {
                    auto it2 = P.elink.begin();
                    for (int i2 = nextR2 + 1; i2 < 0; ++i2) {
                        if (it2 != P.elink.end() && it2->first == i2) ++it2;
                        else {
                            auto Q = P;
                            Q.elink[i2] = i1;
                            Q.invelink[i1] = i2;
                            vPerm.emplace_back(move(Q));
                        }
                    }
                    return for_each(execution::par, vPerm.begin(), vPerm.end(), [r,k](auto & Q){
                        findPerm(move(Q), r, k);
                    });
                }
            }
        }
    }
}


int main(int argc, const char *argv[])
{
    int r = stoi(argv[1]);
    int k = stoi(argv[2]);
    {
        Perm P;

        P.link[0] = 1;
        P.invlink[1] = 0;

        P.link[-1] = -2;
        P.invlink[-2] = -1;

        P.link[-3] = 0;
        P.invlink[0] = -3;

        P.link[-2] = 2;
        P.invlink[2] = -2;

        findPerm(P, r, k);
    }

    {
        Perm P;

        P.link[0] = 1;
        P.invlink[1] = 0;

        P.link[-1] = -2;
        P.invlink[-2] = -1;

        P.link[-2] = 0;
        P.invlink[0] = -2;



        findPerm(P, r, k);
    }

        return 0;
        //g++ -O3 -std=c++17 -o perm perm.cpp -ltbb


