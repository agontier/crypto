n = 16
k = div(n,2)
R = 8
C = 11

p = [0 for i in 1:n]
pi = [0 for i in 1:n]
ep = [0 for i in 1:k]
epi = [0 for i in 1:k]

regex = [0 for i in 1:R]

function nbv()#number of green nodes
        nb = Int(0)
        for i in 1:k
                if p[i]!=0
                        nb = nb+1
                end
        end
        return nb
end

function nbr()#number of red nodes
        nb = Int(0)
        for i in 1:k
                if p[k+i]!=0
                        nb = nb+1
                end
        end
        return nb
end

function nbe()#number of epsi nodes
        nb = Int(0)
        for i in 1:k
                if ep[i]!=0
                        nb = nb+1
                end
        end
        return nb
end

function getregex(a,b,r)
        if a==b regex[R-r]=R-r end
        if r>0 
                if p[a]!=0
                        getregex(p[a],b,r-1)
                end
                if a>k && ep[a-k]!=0 #a est rouge
                        getregex(ep[a-k],b,r)
                end
        end
end

#call getregex on each pair of green red nodes
function getallregex()
        global p
        global ep
        global pi = getinvert(p)
        #global epi = getinvert(ep)
        allregex = []
        for i in 1:k
                for j in 1:k
                        if ((p[i]>0||pi[i]>0) && #le vert existe
                            (p[k+j]>0||pi[k+j]>0))#le rouge existe
                                global regex = [0 for i in 1:R]
                                getregex(i,k+j,R)
                                push!(allregex,(i,j,copy(regex)))
                                #println("            ",regex,' ',i," vers ",k+j)
                        end
                end
        end
        push!(regexpool[nbv()+nbr()],(nbv(),nbr(),nbe(),copy(p),copy(ep),allregex))
end

#enumerate all possible epsilon transitions and call allregex
function enumepsilon(nbv,nbr,nbe,v)
        if nbe<nbv && nbe<nbr
                global ep
                global epi
                #println("      ",ep,' ',nbv-nbe," in ",nbr-nbe," out")
                getallregex()
                for vv in v:nbv
                        for rr in 1:nbr
                                if ep[vv]==0 && epi[rr]==0
                                        ep[vv]=rr
                                        epi[rr]=vv
                                        enumepsilon(nbv,nbr,nbe+1,vv+1)
                                        ep[vv]=0
                                        epi[rr]=0
                                end
                        end
                end
        end
end

function getinvert(p)
        pi = [0 for i in 1:length(p)]
        for i in 1:length(p)
                if p[i]!=0
                        pi[p[i]]=i
                end
        end
        return pi
end

#order over a list of green red successive sets
function order(l,o,i)
        if i>length(l) return true else
                if l[o][1]==l[i][1]
                        if l[o][2]==l[i][2]
                                return order(l,o+1,i+1)
                        else return l[o][2]>l[i][2] end
                else return l[o][1]>l[i][1] end
        end
end

#build the pair of sets of successive green and red to check order
function respectorder()
        c = 1
        l = []
        cv = 1
        cr = 0
        while p[c]>1
                if p[c]>k #p[c] est rouge
                        cr = cr+1
                elseif c>k #c etais rouge donc une nouvelle paire commence
                        push!(l,(cv,cr))
                        cv = 1
                        cr = 0
                else
                        cv = cv+1
                end
                c = p[c]
        end
        push!(l,(cv,cr+1))
        for i in 1:length(l)
                if !order(l,1,i)
                        return false
                end
        end
        return true
end

#build all coloren cycle and call enumepsilon on all the orderred ones
function buildallcoloredcycle(nbv,nbr,v,r,c)
        if v<=nbv
                p[c]=v
                buildallcoloredcycle(nbv,nbr,v+1,r,v)
        end
        if r<=nbr+k
                p[c]=r
                buildallcoloredcycle(nbv,nbr,v,r+1,r)
        end
        if v==nbv+1 && r==nbr+k+1
                p[c]=k+1
                if respectorder()
                        println(p,' ',nbv," verts ",nbr," rouges")
                        global ep = [0 for i in 1:k]
                        global epi = [0 for i in 1:k]
                        enumepsilon(nbv,nbr,0,1)
                else
                        println(p,' ',nbv," verts ",nbr," rouges UNORDERED")
                end
        end
        p[c]=0
end

#initialise the building of all colored cycles for a given length
function buildcycle(taille)
        nbv = min(taille,k)
        nbr = taille-nbv
        nbmax = nbv
        while nbr<=nbmax
                println()
                global p = [0 for i in 1:n]
                global pi = [0 for i in 1:n]
                p[k+1]=1
                buildallcoloredcycle(nbv,nbr,2,k+2,1)
                nbv = nbv-1
                nbr = nbr+1
        end
end

function buildregexpool()
        global regexpool = [[] for i in 1:C]
        for i in 2:C
                buildcycle(i)
        end
end

#return true if there is a regex that dominate all regex of an other cycle (always false?)
function compareallregexdomall(allregex1,allregex2)
        flag = false
        for r1 in allregex1
                domall = true
                for r2 in allregex2
                        if !in(false,r1[3] .>= r2[3]) && r1[1] != r2[1]
                                #println(r1)
                                #println(r2)
                                #println()
                        else
                                domall = false
                        end
                end
                if domall
                        flag = true
                        println(r1)
                end
        end
        return flag
end
function lookallcycle()
        for cycle in regexpool
        end
end
function compareallcycle()
        for cycle in regexpool
                for cycle2 in regexpool
                        if cycle[1]==cycle2[1] &&
                                cycle[2]==cycle2[2] &&
                                cycle[3]==cycle2[3]
                                if compareallregexdomall(cycle[6],cycle2[6])
                                        println(cycle[4],cycle[5])
                                        println(cycle2[4],cycle2[5])
                                        println()
                                        println()
                                end
                        end
                end
        end
end


function printc(c)
        println(c[4:5])
        for r in c[6]
                println(r)
        end
        println()
end

function printcritical(c=6)
        chmin = [0 for i in 1:c]
        for cycle in regexpool[c]
                m = 1
                for r in cycle[6]
                        if sum(r[3])>0
                                m = max(m,findfirst(x->x>0,r[3]))
                        else
                                m = c
                                #println("unreachable")
                                #printc(cycle)
                        end
                end
                chmin[m]=chmin[m]+1
        end
        println(chmin)
        criticalmin = findfirst(x->x>0,chmin)
        
        for cycle in regexpool[c]
                m = 1
                for r in cycle[6]
                        if sum(r[3])>0
                                m = max(m,findfirst(x->x>0,r[3]))
                        end
                end
                if m ==criticalmin
                        printc(cycle)
                end
        end
        println(chmin)
end
buildregexpool()
printcritical(9)

#([2, 10, 12, 9, 0, 0, 0, 0, 1, 11, 3, 13, 4, 0, 0, 0], [2, 3, 0, 4, 0, 0, 0, 0])
