
using SparseArrays
using Combinatorics

function getactive(x,p,fin,t,active)
        #if sum(p[2:2:2k])> 0 println(x," ",p," ",fin," ",t) end
        if t > 1
                x = p[x]#go to next red
                return getactive(x-1,p,fin,t-1,active|1<<(x>>1)) |
                        if p[x]>0 getactive(p[x],p,fin,t-2,active)
                        else 0 end
        else
                if t==1
                        active = active | 1<<(p[x]>>1)
                        x=p[x]-1
                end
                if x==fin return active
                else return 0 end
        end
end
function nbactive(deb,p,fin,t)
        #println(digits(getactive(deb,p,fin,t,0), base = 2))
        return count_ones(getactive(deb,p,fin,t,0))
end
function makeway(x,p,cp,deb,fin,way,fways,lvl,gways)
        #println(x," ",p[x]," ",p,way," ",count_ones(way)<R-3)
        if count_ones(way) < R-3
                x = p[x]
                makeway(x-1,p,cp,deb,fin,way<<2|2,fways,lvl,gways)
                if p[x] > 0
                        makeway(p[x],p,cp,deb,fin,way<<2|3,fways,lvl,gways)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                makeway(p[x],p,cp|1<<y,deb,fin,way<<2|3,fways,lvl,gways)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3
                        x = p[x]-1
                        way = way<<2|2
                end
                if x == fin && (fways[way] == 0 || fways[way] >= lvl)
                        fways[way] = lvl
                        if nbactive(deb,p,fin,R-2) < mand
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                makeway(deb,p,cp,deb,fin,0,fways,lvl+1,gways)
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        else
                                gways[(deb+1)>>1,(fin+1)>>1][way] = -lvl
                                gways[(deb+1)>>1,(fin+1)>>1] .+= fways
                                witchway(p, cp, gways)
                                gways[(deb+1)>>1,(fin+1)>>1] .-= fways
                                gways[(deb+1)>>1,(fin+1)>>1][way] = 0
                        end
                end
        end
end
function countway(x,p,cp,fin,way)
        cw = 0
        if count_ones(way) < R-3
                x = p[x]
                cw += countway(x-1,p,cp,fin,way<<2|2)
                if p[x] > 0
                        cw += countway(p[x],p,cp,fin,way<<2|3)
                else
                        for y in 1:k if cp>>y & 1==0
                                p[x] = 2y-1
                                cw += countway(p[x],p,cp|1<<y,fin,way<<2|3)
                        end end
                        p[x] = 0
                end
        else
                if count_ones(way)==R-3 x=p[x]-1 end
                cw += x==fin
        end
        return cw
end
function hasforrec(x,p,deb,fin,way,gways)
        if count_ones(way) < R-3
                x = p[x]           
                return hasforrec(x-1,p,deb,fin,way<<2|2,gways) ||
                        if p[x] > 0 hasforrec(p[x],p,deb,fin,way<<2|3,gways)
                        else false end
        else
                if count_ones(way) == R-3
                        x=p[x]-1
                        way = way<<2|2
                end
                if x==fin return gways[(deb+1)>>1,(fin+1)>>1][way] > 0
                else return false end
        end
end
function hasforbidden(deb,p,fin,gways)
        return hasforrec(deb,p,deb,fin,0,gways)
end
function witchway(p,cp,gways)
        for deb in 1:2:2k for fin in 1:2:2k
                if hasforbidden(deb,p,fin,gways)
                        #println(deb," ",fin," ",p)
                        return;
                end
        end end
        newdeb = newfin = tmp = 0
        minch = 20482048
        for deb in 1:2:2k for fin in 1:2:2k if nbactive(deb,p,fin,R-2) < mand
                tmp = countway(deb,p,cp,fin,0)
                if minch > tmp
                        minch = tmp
                        newdeb = deb
                        newfin = fin
                end end end end
        if newdeb > 0
                makeway(newdeb,p,cp,newdeb,newfin,0,spzeros(Int8,2^(2R-4)),one(Int8),gways)
        else
                if 0 in p ;#global zeroin+=1#println("0 in sol ",p)
                elseif p in pool ;#global dejavu+=1#println("DEJA VU ",p)
                else
                        push!(pool,copy(p))
                end
        end
end

function crit3(p,pool)
        gways = [spzeros(Int8,2^(2R-4)) for i in 1:k, j in 1:k]
        witchway(p,0,gways,pool)
end

function chmain()
        global bigpool = [[] for i in 1:np]
        Threads.@threads for i in 1:np
                print(partoch[i])
                p = zeros(Int8,k)
                ii = 1
                for c in partoch[i]
                        if c==1
                                p[ii] = ii
                                ii +=1
                        else
                                for l in ii:ii+c-2
                                        p[l] = l+1
                                end
                                p[ii+c-1] = ii
                                ii = ii+c
                        end
                end
                pq = zeros(Int8,2k)
                for l in 1:k
                        pq[2l-1] = 2p[l]
                end
                pool = []
                #global zeroin = 0
                #global dejavu = 0
                crit3(pq,pool)
                println(partoch[i],"               ",
                        if length(pool)>0 string(length(pool)," sols     ") else " " end)#,
                        #if dejavu>0 string(dejavu," deja vu     ") else " " end,
                        #if zeroin>0 string(zeroin," incomplete     ") else " " end)
                bigpool[i] = deepcopy(pool)
        end
end

global k=9

global partoch = collect(partitions(k))
global np = length(partoch)
#global pool = []
global bigpool = []
global zeroin = 0
global dejavu = 0

global mand = 2
global R=8

@time chmain()

#=
JULIA_NUM_THREADS=128 ./julia
Threads.@Threads:static

cd /Applications/Julia-1.5.app/Contents/Resources/julia/bin/ 
JULIA_NUM_THREADS=8 ./julia
=#
function difrec(b,dif,dif2,active,newdifs)
        if b==k
                if newdifs[dif2] == 0
                        newdifs[dif2] = active
                else
                        newdifs[dif2] = min(newdifs[dif2],active)
                end
        else
                c = dif >> 2b & 0x3
                if c==0 #rien
                        difrec(b+1,dif,dif2,active,newdifs)
                elseif c==1 #juste un vert
                        difrec(b+1,dif,dif2 | 1 << 2b,active,newdifs)
                elseif c==2 #juste un rouge
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                elseif c==3 #doubling
                        difrec(b+1,dif,dif2 | 2 << 2b,active+1,newdifs)
                        difrec(b+1,dif,dif2 | 3 << 2b,active+1,newdifs)
                end
        end
end
function apply(p,dif)
        dif2 = 0
        for i in 0:length(p)-1
                if dif >> i & 0x1 == 1
                        dif2 = dif2 | 1 << (p[i+1]-1)
                end
        end
        return dif2
end
function getdiff2(difs,p)
        newdifs = zeros(Int8,2^2k-1)
        for dif in 1:2^2k-1#compute propag
                difrec(0,dif,0,difs[dif],newdifs)
        end
        for i in 1:2^2k-1#apply permutation
                difs[apply(p,i)] = newdifs[i]
        end
        return difs
end
function paramain()
        println("Charge par th: ",charge)
        res = zeros(Int8,na,16)
        #io = open("res.txt", "w")
        Threads.@threads for jt in 0:Threads.nthreads()-1
                for ja in 1:charge
                        j = jt*charge+ja
                        if j<=na
                                pq = a[j]# [5, 0, 1, 4, 7, 12, 3, 8, 13, 6, 9, 2, 15, 10, 11, 14].+1
                                difs = ones(Int8,2^2k-1)
                                for r in 1:16
                                        difs = getdiff2(difs,pq)
                                        res[j,r] = findmin(difs)[1]-1
                                end
                        end
                end
        end
        #write(io,string("(",partoch[i],", ",res[i,:,:],"),\n"))
        #close(io)
        return res
end
function getcount(pool)
        compteur = zeros(Int,16,40)
        for j in 1:16
                for val in 1:40
                        compteur[j,val] += length(findall(x->x==val,pool[:,j]))
                end end
        return compteur
end
function printbars(count,title)
        nb = sum(count)
        f1 = true
        f2 = true
        valmin = 0
        valmax = 0
        for i in 1:31
                if f1 && count[i] > 0
                        valmin = i
                        f1 = false
                end
                if !f1 && count[i] > 0
                        valmax = i
                end
        end
        println("\\begin{tikzpicture}")
        println("\\node[whiteFill] (0) at (6,5)  {",title,"};")
        println("\\begin{axis} [ybar,xmin=",valmin-1,",xmax=",valmax+1,",ymin=0,ymax=",findmax([round(count[i]/sum(count)*100; digits=2) for i in 1:31])[1],"]")
        println("\\addplot coordinates {")
        for i in valmin:valmax
                print("(",i,",",round(count[i]/sum(count)*100; digits=2),")")
        end
        println()
        println("};\\end{axis}\\end{tikzpicture}")
end

function printallbars(counts)
        nbsol = sum(counts[2,:])
        countsum = [sum(counts[10:16,i]) for i in 1:size(counts)[2]]
        nbval = 40
        f1 = true
        f2 = true
        valmin = 0
        valmax = 0
        for i in 1:nbval
                if f1 && countsum[i] > 0
                        valmin = i
                        f1 = false
                end
                if !f1 && countsum[i] > 0
                        valmax = i
                end
        end
        println("\\begin{tikzpicture}")
        println("\\begin{axis} [height=6cm,width=20cm,legend style={at={(1.057,0.9)},anchor=north east},ybar,xmin=",valmin-1,",xmax=",valmax+1,",ymin=0,ymax=",round(findmax(counts[10:16,:])[1]/nbsol*100; digits=2),",ymajorgrids=true,xtick={1,2,...,40}, bar width=2pt]")
        colors = [" "," "," "," "," "," "," "," "," ","cyan","green","lime","yellow","orange","red","purple"," "," "," "]
        for r in 10:16
                println("\\addplot[",colors[r],"!100,fill=",colors[r],"!20] coordinates {")
                for i in valmin:valmax
                        if counts[r,i] != 0
                                print("(",i,",",round(counts[r,i]/nbsol*100; digits=2),")")
                        end
                end
                println("};")
        end
        println("\\legend{R10,R11,R12,R13,R14,R15,R16};\\end{axis}\\end{tikzpicture}")
end



if true# && (false || charge < 30)
        pool = []
        for i in 1:np
                if bigpool[i]!=[]
                        append!(pool,bigpool[i])
                end
        end
        global a = pool
        global na = length(a)
        global charge = div(na,Threads.nthreads())+1

        @time difs = paramain()
        println("\\subsubsection{X = ",mand,"}")
        println("Nb Sols: ",na," (R",R,")\\\\")

        global counts = getcount(difs)
        printallbars(counts)
        
        #=for i in 9:2:16
        printbars(counts[i,:],string("R",i))
        if i+1<=16
        printbars(counts[i+1,:],string("R",i+1))
        end
        println("\\\\~\\\\")
        end=#
end
