using SparseArrays
using Combinatorics
using Random

function checkinv(p)
        pm1 = zeros(Int8,2k)
        for i in 1:2k
                pm1[p[i]] = i
        end
        for deb in 2k-1:-2:1 for fin in 2k:-2:2 
                if !haspath(deb,pm1,fin,R-1)
                        return false
                end
        end end
        return true
end
function addtopool(p,pool)#add to pool without redundancy
        if p in pool pool[1]+=1
        elseif checkinv(p)
                #print(1)#println
                push!(pool,copy(p))
        end
end
function complete(p,pool)#complete p if needed then add to pool
        if 0 in p
                pool[2]+=1
                i = findfirst(isequal(0),p)
                for v in 1:length(p)
                        if ! (v in p)
                                p[i]=v
                                complete(copy(p),pool)
                        end
                end
        else
                addtopool(p,pool)
        end
end
function breaksym(a,x,y,isfree,group,nodes,cycles,chaines,authorized)
        cl = 2sum(cycles)
        nbcy = length(cycles)
        nbch = length(chaines)
        #println(a," ",cycles,chaines,x," ",y," ",group[x]," ",group[y],"  cl:",cl)
        if a==1#c1!=c2 pour a==1 et a==2
                c1 = group[x]
                if x<=cl
                        for c2 in 1:nbcy if isfree[c2] && cycles[c1] == cycles[c2]
                                for x2 in nodes[c2]
                                        authorized[x2,y] = false
                                end end end
                else# je casse la symetrie de x vers y et de y vers x aussi ?
                        c1 -= nbcy
                        n1 = cl+2sum(chaines[1:c1])-2chaines[c1]+1
                        d1 = x-n1
                        for c2 in 1:nbch if isfree[nbcy+c2] && chaines[c1] == chaines[c2]
                                n2 = cl+2sum(chaines[1:c2])-2chaines[c2]+1
                                authorized[n2+d1,y] = false
                        end end
                end
        elseif a==2
                c2 = group[y]
                if y<=cl#OK
                        for c1 in 1:nbcy if isfree[c1] && cycles[c1] == cycles[c2]
                                for y2 in nodes[c1]
                                        authorized[x,y2] = false
                                end end end
                else#OK
                        c2 -= nbcy
                        n2 = cl+2sum(chaines[1:c2])-2chaines[c2]+1
                        d2 = y-n2
                        for c1 in 1:nbch if isfree[nbcy+c1] && chaines[c1] == chaines[c2]
                                n1 = cl+2sum(chaines[1:c1])-2chaines[c1]+1
                                authorized[x,n1+d2] = false
                        end end end
        else
                c1 = group[x]
                c2 = group[y]
                if x<=cl && y<=cl
                        if c1==c2#OK
                                d = 0
                                m = 2cycles[c1]
                                if x<=y d = y-x
                                else d = y-x+m end
                                for c3 in 1:nbcy if isfree[c3] && cycles[c1] == cycles[c3]
                                        for x2 in nodes[c3]
                                                if x2+d<=2sum(cycles[1:c3])
                                                        authorized[x2,x2+d] = false
                                                else
                                                        authorized[x2,x2+d-m] = false
                                                end end end end
                        else#OK
                                for c3 in 1:nbcy if isfree[c3] && cycles[c1] == cycles[c3]
                                        for c4 in 1:nbcy if isfree[c4] && cycles[c2] == cycles[c4] && c3!=c4
                                                for x2 in nodes[c3]
                                                        for y2 in nodes[c4]
                                                                authorized[x2,y2] = false
                                                        end end end end end end 
                        end
                elseif x<=cl#OK
                        c2 -= nbcy
                        n2 = cl+2sum(chaines[1:c2])-2chaines[c2]+1
                        d2 = y-n2
                        for c3 in 1:nbcy if isfree[c3] && cycles[c1] == cycles[c3]
                                for c4 in 1:nbch if isfree[c4] && chaines[c2] == chaines[c4]
                                        n4 = cl+2sum(chaines[1:c4])-2chaines[c4]+1
                                        for x2 in nodes[c3]
                                                authorized[x2,n4+d2] = false
                                        end end end end end
                elseif y<=cl#OK
                        c1 -= nbcy
                        n1 = cl+2sum(chaines[1:c1])-2chaines[c1]+1
                        d1 = x-n1
                        for c3 in 1:nbch if isfree[c3] && chaines[c1] == chaines[c3]
                                n3 = cl+2sum(chaines[1:c3])-2chaines[c3]+1
                                for c4 in 1:nbcy if isfree[c4] && cycles[c2] == cycles[c4]
                                        for y2 in nodes[c4]
                                                authorized[n3+d1,y2] = false
                                        end end end end end
                else
                        c2 -= nbcy
                        c1 -= nbcy
                        n1 = cl+2sum(chaines[1:c1])-2chaines[c1]+1
                        n2 = cl+2sum(chaines[1:c2])-2chaines[c2]+1
                        d1 = x-n1
                        d2 = y-n2
                        if c1==c2#OK
                                for c3 in 1:nbch if isfree[c3] && chaines[c1] == chaines[c3]
                                        n3 = cl+2sum(chaines[1:c3])-2chaines[c3]+1
                                        authorized[n3+d1,n3+d2] = false
                                end end
                        else#OK
                                for c3 in 1:nbch if isfree[c3] && chaines[c1] == chaines[c3]
                                        n3 = cl+2sum(chaines[1:c3])-2chaines[c3]+1
                                        for c4 in 1:nbch if isfree[c4] && chaines[c2] == chaines[c4] && c3!=c4
                                                n4 = cl+2sum(chaines[1:c4])-2chaines[c4]+1
                                                authorized[n3+d1,n4+d2] = false
                                        end end end end end end end
        #println(findall(!isodd,authorized))
end
function haspath(x,p,fin,t)
        if t > 0
                return x&1==0 && haspath(x-1,p,fin,t) || p[x]>0 && haspath(p[x],p,fin,t-1)
        else
                return x==fin
        end
end
function makeway(x,p,cp,deb,fin,vv,rr,way,pool,isfree,group,nodes,cycles,chaines,authorized)
        if count_ones(way) < R-1
                if x&1==0 makeway( x-1,p,cp,deb,fin,vv,rr,way<<1  ,pool,isfree,group,nodes,cycles,chaines,copy(authorized)) end
                if p[x]>0 makeway(p[x],p,cp,deb,fin,vv,rr,way<<1|1,pool,isfree,group,nodes,cycles,chaines,authorized)
                else
                        for y in 1:2:2k if cp>>y & 1==0
                                if authorized[x,y]
                                        p[x] = y
                                        free = isfree[group[x]] <<1| isfree[group[y]]
                                        isfree[group[x]] = isfree[group[y]] = false
                                        makeway(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool,isfree,group,nodes,cycles,chaines,copy(authorized))
                                        isfree[group[x]] = free>>1&1
                                        isfree[group[y]] = free&1
                                        if free>0
                                                pool[3]+=1
                                                breaksym(free,x,y,isfree,group,nodes,cycles,chaines,authorized)
                                        end
                                end
                        end end
                        if x&1==0 #x is red then it can go to rr too
                                for y in rr if cp>>y & 1==0
                                        if authorized[x,y]
                                                p[x] = y
                                                free = isfree[group[x]] <<1| isfree[group[y]]
                                                isfree[group[x]] = isfree[group[y]] = false
                                                makeway(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool,isfree,group,nodes,cycles,chaines,copy(authorized))  
                                                isfree[group[x]] = free>>1&1
                                                isfree[group[y]] = free&1
                                                if free>0
                                                        pool[3]+=1
                                                        breaksym(free,x,y,isfree,group,nodes,cycles,chaines,authorized)
                                                end
                                        end
                                end end
                        end
                        p[x] = 0
                end
        elseif x == fin
                witchway(p,cp,vv,rr,pool,isfree,group,nodes,cycles,chaines,authorized)
        end
end
function witchway(p,cp,vv,rr,pool,isfree,group,nodes,cycles,chaines,authorized)
        #trouver et linker les free en premier.
        for deb in 2k-1:-2:1 for fin in 2k:-2:2
                if isfree[group[deb]] || isfree[group[fin]]
                        if !haspath(deb,p,fin,R-1)
                                makeway(deb,p,cp,deb,fin,vv,rr,0,pool,isfree,group,nodes,cycles,chaines,authorized)
                                return;
                        end end end end
        witchwaysimple(p,cp,vv,rr,pool,authorized)
end
function makewaysimple(x,p,cp,deb,fin,vv,rr,way,pool,authorized)
        if count_ones(way) < R-1
                if x&1==0 makewaysimple( x-1,p,cp,deb,fin,vv,rr,way<<1  ,pool,authorized) end
                if p[x]>0 makewaysimple(p[x],p,cp,deb,fin,vv,rr,way<<1|1,pool,authorized)
                else
                        for y in 1:2:2k if cp>>y & 1==0 if authorized[x,y]
                                p[x] = y
                                makewaysimple(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool,authorized)
                        end end end
                        if x&1==0 #x is red then it can go to rr too
                                for y in rr if cp>>y & 1==0 if authorized[x,y]
                                        p[x] = y
                                        makewaysimple(p[x],p,cp|1<<y,deb,fin,vv,rr,way<<1|1,pool,authorized)  
                                end end end
                        end
                        p[x] = 0
                end
        elseif x == fin
                witchwaysimple(p,cp,vv,rr,pool,authorized)
        end
end
function witchwaysimple(p,cp,vv,rr,pool,authorized)
        for deb in 2k-1:-2:1 for fin in 2k:-2:2 
                if !haspath(deb,p,fin,R-1)
                        makewaysimple(deb,p,cp,deb,fin,vv,rr,0,pool,authorized)
                        return;
                end
        end end
        complete(p,pool)
end
function makechaines(k,cl,used,tofix,p,part,tasks)
        partoch2 = collect(partitions(k-cl))
        for a in 1:k
                for part2 in partoch2
                        if length(part2) == a
                                vv = Vector{Int8}(undef,0)
                                rr = Vector{Int8}(undef,0)
                                used2 = used
                                pa = zeros(Int8,k)
                                ii = tofix
                                for c in part2
                                        push!(vv,2ii-1)
                                        if c==1
                                                ii +=1
                                        else
                                                for l in ii:ii+c-2
                                                        pa[l] = l+1
                                                end
                                                ii = ii+c
                                        end
                                        push!(rr,2(ii-1))
                                end
                                pp = copy(p)
                                for l in 1:k
                                        if pa[l]>0
                                                pp[2l-1] = 2pa[l]
                                                used2|= 1<<2pa[l]
                                        end
                                end
                                cycles = convert(Vector{Int8},part)
                                chaines = convert(Vector{Int8},part2)
                                parts = vcat(cycles,chaines)
                                group = zeros(Int8,2k)
                                nodes = Vector{UnitRange{Int8}}(undef,length(cycles))
                                if cl>0
                                        nodes[1] = 1:2cycles[1]
                                        s = parts[1]
                                        for i in 2:length(cycles)
                                                nodes[i] = 2s+1:2s+2cycles[i]
                                                s+=cycles[i]
                                        end
                                end
                                c = 1
                                for i in 1:2k
                                        if i>2sum(parts[1:c])
                                                c += 1
                                        end
                                        group[i] = c
                                end
                                isfree = ones(Bool,length(parts))
                                authorized = ones(Bool,2k,2k)
                                #authorized = BitArray(undef,2k,2k) slower
                                #fill!(authorized,true)
                                pool = []
                                push!(pool,0,0,0)
                                push!(tasks,deepcopy([pp,used2,vv,rr,pool,isfree,group,nodes,cycles,chaines,authorized]))
                        end
                end
        end
end
function makecycles()
        tasks = []
        for cl in k-1:-1:1
                partoch = collect(partitions(cl))
                for part in partoch if part[1] > 0
                        used = 0
                        pc = zeros(Int8,k)
                        ii = 1
                        for c in part
                                if c==1
                                        pc[ii] = ii
                                        ii +=1
                                else
                                        for l in ii:ii+c-2
                                                pc[l] = l+1
                                        end
                                        pc[ii+c-1] = ii
                                        ii = ii+c
                                end
                        end
                        p = zeros(Int8,2k)
                        for l in 1:k
                                if pc[l]>0
                                        p[2l-1] = 2pc[l]
                                        used|=1<<2pc[l]
                                end
                        end
                        tofix = ii
                        partoch2 = collect(partitions(k-cl))
                        makechaines(k,cl,used,tofix,p,part,tasks)
                end end
        end
        makechaines(k,0,0,1,zeros(Int8,2k),[],tasks)
        println(length(tasks)," tasks")
        return tasks
end
function parachaines()
        tasks = shuffle(makecycles())
        #tasks = makecycles()
        @time Threads.@threads for t in tasks
                #for t in tasks
                pool = t[5]
                #println(t[9],t[10])
                witchway(t[1],t[2],t[3],t[4],pool,t[6],t[7],t[8],t[9],t[10],t[11])
                #println(pool[1])
                dejavu = pool[1]
                zeroin = pool[2]
                breaksym = pool[3]
                nbsol = length(pool)-3
                
                if nbsol > 0 #|| zeroin > 0
                        println(t[9],t[10],"               ",
                                if nbsol>0 string(nbsol," sols     ") else " " end,
                                if dejavu>0 string(dejavu," deja vu     ") else " " end,
                                if zeroin>0 string(zeroin," incomplete     ") else " " end,
                                if breaksym>0 string(breaksym," breaksym calls     ") else " " end)
                end
                
        end
end

const R = 8
const k = 6
#println(length(makechaines()))

parachaines()
