global a = 0
global k = 16
global R = 8
global nbr = [0 for i in 1:R+1]

function stephtree(couleur, x, t)
        global a
        global k
        if couleur == 'R' nbr[R-t+1] = nbr[R-t+1]+x
        end
        if t==0
                println(couleur,' ',x)
        else
                if x<10 print(couleur,' ',x,"  --- ")
                else print(couleur,' ',x," --- ")
                end
                nr = 0
                nv = 0
                if couleur == 'V'
                        nr = min(k-a,x)#pas forcement le pire
                        nv = x-nv
                else
                        nr = min(k,2*x,x+a)
                        nv = 2*x-nr
                end
                stephtree('R', nr, t-1)
                if nv>0 && t>1
                        [print("         ") for i in 0:R-t]
                        println('|')
                        [print("         ") for i in 0:R-t]
                        stephtree('V', nv, t-1)
                end  
        end
end

stephtree('R',15,R)

nbr


#=
julia> nbr
   0
   4
  12
  24
  44
  80
 152
 292
 556

   15
   16
   30
   60
  120
  238
  473
  942
 1874

   15
   15
   30
   60
  120
  240
  480
  960
 1920

les formules des max sont les suivantes

Si on a $xV$ (x noeuds verts) alors on a comme successeurs\\
$max(k-a,x) R$
 et 
$x-max(k-a,x) V$

Si on a $xR$ (x noeuds rouges) alors on a comme successeurs\\
$min(k,2x,x+a) R$
 et 
 $x-min(k,2x,x+a) V$
=#



#=
#=
a:start of the way,
b:end of the way, 
t:time left to build the way, 
nv:name of the next free green node, 
nr:name of the next free red node=#
function makeway(a, b, t, nv, nr)
        if t == 0 #no time left (no need to check epsilon transition because b is always red)
                global te
                if a == b && te>=mandatoryepsilon
                        tmp = te
                        te = 0
                        qwer = witchway(nv, nr)
                        #witchwayclassic(nv, nr)
                        te = tmp
                        return true
                else
                        return false
                end
        else
                #transitions not forbiden
                mcopy = copy(allowed)
                emcopy = copy(eallowed)
                possibleway = false
                if p[a] == 0 #way is free
                        if a > k #a is red
                                if nbrr > 0
                                        global nbrr = nbrr-1
                                        # go back
                                        for r in k+1:k+nr-1
                                                if allowed[a,r] && !(r in p)
                                                        p[a]=r
                                                        if nbrr > 0 || p[k+1]!=0 #k+1->? must be set
                                                                if makeway(r, b, t-1, nv, nr)
                                                                        possibleway = true
                                                                        allowed[a,r] = false
                                                                end
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nr <= k 
                                                p[a] = k+nr
                                                if makeway(k+nr, b, t-1, nv, nr+1)
                                                        possibleway = true
                                                end
                                                p[a] = 0
                                        end
                                        nbrr = nbrr+1
                                end
                                if nbrv > 0 && a != k+1 #k+1 has a red succ
                                        global nbrv = nbrv-1
                                        # go back
                                        for v in 1:nv-1
                                                if allowed[a,v] && v!=1 && !(v in p) #1 has a green pred
                                                        p[a]=v
                                                        if makeway(v, b, t-1, nv, nr)
                                                                allowed[a,v] = false
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nv <= k && a != k+1
                                                p[a] = nv
                                                if makeway(nv, b, t-1, nv+1, nr)
                                                        possibleway = true
                                                end
                                                p[a] = 0
                                        end
                                        nbrv = nbrv+1
                                end
                        else #a is green
                                if nbvr > 0
                                        global nbvr = nbvr-1
                                        # go back
                                        for r in k+1:k+nv-1
                                                if allowed[a,r] && !(r in p)
                                                        p[a]=r
                                                        if makeway(r, b, t-1, nv, nr)
                                                                possibleway = true
                                                                allowed[a,r] = false
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nr <= k
                                                p[a] = k+nr
                                                if makeway(k+nr, b, t-1, nv, nr+1)
                                                        possibleway = true
                                                end
                                                p[a] = 0
                                        end
                                        nbvr = nbvr+1
                                end
                                if nbvv > 0
                                        global nbvv = nbvv-1
                                        # go back
                                        for v in 1:nr-1
                                                if allowed[a,v] && !(v in p)
                                                        p[a]=v
                                                        if nbvv > 0 || (1 in p[1:k]) #?->1 must be set
                                                                if makeway(v, b, t-1, nv, nr)
                                                                        possibleway = true
                                                                        allowed[a,v] = false
                                                                end
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nv <= k
                                                p[a] = nv
                                                if makeway(nr, b, t-1, nv+1, nr)
                                                        possibleway = true
                                                end
                                                p[a] = 0
                                        end
                                        nbvv = nbvv+1
                                end
                        end
                else
                        if makeway(p[a], b, t-1, nv, nr)
                                possibleway = true
                        end
                end
                if a > k #epsilon cases
                        te = te+1
                        if ep[a-k] == 0
                                #go back
                                for v in 1:nv-1
                                        if !(v in ep)
                                                ep[a-k] = v
                                                if makeway(v, b, t, nv, nr)
                                                        possibleway = true
                                                end
                                                ep[a-k] = 0
                                        end
                                end
                                #go new
                                if nv <= k
                                        ep[a-k] = nv
                                        if makeway(nv, b, t, nv+1, nr)
                                                possibleway = true
                                        end
                                        ep[a-k] = 0
                                end
                        else
                                if makeway(ep[a-k], b, t, nv, nr)
                                        possibleway = true
                                end
                        end
                        te = te-1
                end
                if possibleway
                        global allowed = mcopy
                        global eallowed = emcopy
                end
                return possibleway
        end
end

function countway(a, b, t, nv, nr)
        if t == 0 #no time left
                if a == b
                        return 1
                else
                        return 0
                end
        else
                res = 0
                if p[a] == 0 #way is free
                        if a > k #a is red
                                if nbrr > 0
                                        global nbrr = nbrr-1
                                        # go back
                                        for r in k+1:k+nr-1
                                                if !(r in p)
                                                        p[a]=r
                                                        if nbrr > 0 || p[k+1]!=0 #k+1->? must be set
                                                                res = res + countway(r, b, t-1, nv, nr)
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nr <= k 
                                                p[a] = k+nr
                                                res = res + countway(k+nr, b, t-1, nv, nr+1)
                                                p[a] = 0
                                        end
                                        nbrr = nbrr+1
                                end
                                if nbrv > 0 && a != k+1 #k+1 has a red succ
                                        global nbrv = nbrv-1
                                        # go back
                                        for v in 1:nv-1
                                                if v!=1 && !(v in p) #1 has a green pred
                                                        p[a]=v
                                                        res = res + countway(v, b, t-1, nv, nr)
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nv <= k && a != k+1
                                                p[a] = nv
                                                res = res + countway(nv, b, t-1, nv+1, nr)
                                                p[a] = 0
                                        end
                                        nbrv = nbrv+1
                                end
                        else #a is green
                                if nbvr > 0
                                        global nbvr = nbvr-1
                                        # go back
                                        for r in k+1:k+nv-1
                                                if !(r in p)
                                                        p[a]=r
                                                        res = res + countway(r, b, t-1, nv, nr)
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nr <= k
                                                p[a] = k+nr
                                                res = res + countway(k+nr, b, t-1, nv, nr+1)
                                                p[a] = 0
                                        end
                                        nbvr = nbvr+1
                                end
                                if nbvv > 0
                                        global nbvv = nbvv-1
                                        # go back
                                        for v in 1:nr-1
                                                if !(v in p)
                                                        p[a]=v
                                                        if nbvv > 0 || (1 in p[1:k]) #?->1 must be set
                                                                res = res + countway(v, b, t-1, nv, nr)
                                                        end
                                                        p[a]=0
                                                end
                                        end
                                        # go new
                                        if nv <= k
                                                p[a] = nv
                                                res = res + countway(nr, b, t-1, nv+1, nr)
                                                p[a] = 0
                                        end
                                        nbvv = nbvv+1
                                end
                        end
                else
                        res = res + countway(p[a], b, t-1, nv, nr)
                end
                if a > k #epsilon cases
                        if ep[a-k] == 0
                                #go back
                                for v in 1:nv-1
                                        if !(v in ep)
                                                ep[a-k] = v
                                                res = res + countway(v, b, t, nv, nr)
                                                ep[a-k] = 0
                                        end
                                end
                                #go new
                                if nv <= k
                                        ep[a-k] = nv
                                        res = res + countway(nv, b, t, nv+1, nr)
                                        ep[a-k] = 0
                                end
                        else
                                res = res + countway(ep[a-k], b, t, nv, nr)
                        end
                end
                return res
        end
end

function haspath(a,b,t)
        if a == 0
                return false
        elseif t == 0
                return a == b
        elseif haspath(p[a],b,t-1)
                return true
        elseif a>k
                return haspath(ep[a-k],b,t)
        else return false
        end
end

function witchway(nv, nr)
        if !haspath(1,k+1,R-2)
                return makeway(1,k+1,R-2,nv,nr)
        elseif !haspath(1,k+1,R-1)
                return makeway(1,k+1,R-1,nv,nr)
        end
        newa = 0
        newb = 0
        minch = 0
        todoch = false
        tmp = 0
        for RR in R-3:R-1
                for a in 1:k if a in p[1:k]#vv
                        for b in k+1:n if p[b]>k && (a!=1 || b!=k+1)#rr
                                if !haspath(a,b,RR)
                                        if !todoch #first todoch is found
                                                todoch = true
                                                minch = countway(a,b,RR,nv,nr)
                                                newa = a
                                                newb = b
                                        else #min todoch is computed
                                                tmp = countway(a,b,RR,nv,nr)
                                                if tmp < minch
                                                        minch = tmp
                                                        newa = a
                                                        newb = b
                                                end end end end end end end
                if todoch return makeway(newa,newb,RR,nv,nr) end
        end
        for RR in R-2:R-1
                for a in 1:k if a in p[1:k]#vv
                        for b in k+1:n if p[b]<=k && (a!=1 || b!=k+1)#rv
                                if !haspath(a,b,RR)
                                        if !todoch #first todoch is found
                                                todoch = true
                                                minch = countway(a,b,RR,nv,nr)
                                                newa = a
                                                newb = b
                                        else #min todoch is computed
                                                tmp = countway(a,b,RR,nv,nr)
                                                if tmp < minch
                                                        minch = tmp
                                                        newa = a
                                                        newb = b
                                                end end end end end end end
                if todoch return makeway(newa,newb,RR,nv,nr) end
        end
        for RR in R-2:R-1
                for a in 1:k if !(a in p[1:k])#rv
                        for b in k+1:n if p[b]>k && (a!=1 || b!=k+1)#rr
                                if !haspath(a,b,RR)
                                        if !todoch #first todoch is found
                                                todoch = true
                                                minch = countway(a,b,RR,nv,nr)
                                                newa = a
                                                newb = b
                                        else #min todoch is computed
                                                tmp = countway(a,b,RR,nv,nr)
                                                if tmp < minch
                                                        minch = tmp
                                                        newa = a
                                                        newb = b
                                                end end end end end end end
                if todoch return makeway(newa,newb,RR,nv,nr) end
        end
        for a in 1:k if !(a in p[1:k])#rv
                for b in k+1:n if p[b]<=k && (a!=1 || b!=k+1)#rv
                        if !haspath(a,b,R-1)
                                if !todoch #first todoch is found
                                        todoch = true
                                        minch = countway(a,b,R-1,nv,nr)
                                        newa = a
                                        newb = b
                                else #min todoch is computed
                                        tmp = countway(a,b,R-1,nv,nr)
                                        if tmp < minch
                                                minch = tmp
                                                newa = a
                                                newb = b
                                        end end end end end end end
        if todoch return makeway(newa,newb,R-1,nv,nr) end   
        global nbsol = nbsol+1
        #println(p,ep)
        push!(pool,(copy(p),copy(ep)))
end

function witchwayclassic(nv, nr)
        for a in 1:k
                for b in k+1:n
                        if !haspath(a,b,R-1)
                                return makeway(a,b,R-1,nv,nr)
                        end
                end
        end
        push!(pool,(copy(p),copy(ep)))
end

function start(a)
        global p = [0 for i in 1:n]# permutation
        global ep = [0 for i in 1:k]# epsilon transitions
        global nbvv = a#number of v -> v transitions
        global nbvr = k-a#number of v -> r transitions
        global nbrv = k-a#...
        global nbrr = a

        global mandatoryepsilon = 0
        global te = 0
#=
a:start of the way,
b:end of the way, 
t:time left to build the way, 
nv:name of the next free green node, 
nr:name of the next free red node=#
        print(makeway(1,k+1,R-3,2,2))

end
n = 16
k = div(n,2)
R = 8

nbsol = 0

p = [0 for i in 1:n]
ep = [0 for i in 1:k]

allowed = ones(Bool,n,n)
eallowed = ones(Bool,k,k)

nbvv = 0
nbvr = 0
nbrv = 0
nbrr = 0
     
pool = Set([])
for a in div(k,2):-1:1 #pour tous les a en commencant par les plus grands
        println()
        println("a = ",a)
        @time start(div(k,2)-a+1)
end




#=
set   1.176323 seconds (4.74 M allocations: 196.405 MiB, 6.88% gc time)
arr   1.127777 seconds (4.35 M allocations: 185.383 MiB, 12.60% gc time)

pool   3.914176 seconds (13.67 M allocations: 627.989 MiB, 13.87% gc time)
nopool  2.633775 seconds (9.89 M allocations: 384.565 MiB, 4.14% gc time)

16 7
  5.711981 seconds (23.23 M allocations: 1.170 GiB, 2.92% gc time)
R-1
 12.616806 seconds (57.97 M allocations: 2.923 GiB, 3.44% gc time)
classic
 75.049448 seconds (79.68 M allocations: 2.774 GiB, 0.47% gc time)

total
  5.567430 seconds (23.44 M allocations: 1.181 GiB, 2.97% gc time)
 19.784732 seconds (60.53 M allocations: 3.004 GiB, 2.21% gc time)
 31.893082 seconds (74.35 M allocations: 3.874 GiB, 1.71% gc time)
 41.595937 seconds (65.29 M allocations: 3.522 GiB, 1.22% gc time)
countway
  7.307276 seconds (21.00 M allocations: 995.714 MiB, 17.44% gc time)
 12.924905 seconds (50.41 M allocations: 2.120 GiB, 2.47% gc time)
  8.780447 seconds (34.79 M allocations: 1.521 GiB, 2.40% gc time)
  3.525146 seconds (13.45 M allocations: 641.417 MiB, 1.49% gc time)


le premier vv est ?->1
le premier rr est k+1->?
trouver les vv tchequer les R-3-2-1 avec tous les rr


contraindre les arcs teste dans la suite
134/180 = 0.74
4682/7608 = 0.61
21914/63527 = 0.34
allowed on p go back
18093/38598 = 0.46




Tests sur l'obligation d'epsilon ocntraintes
Sans obligations 163 secondes pour finir a = 4 sur n16r8 et 177 sols
Avec une epsilon transition obligatoire a = 4 en 101 secondes et 541 sols

Avec 4 epsilon transitions obligatoires 22 secondes

=#
=#
