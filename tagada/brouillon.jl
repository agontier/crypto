
function getDDT(S)
        n = length(S)
        ddt = zeros(Int8,n,n)
        for i in 0:n-1, j in 0:n-1
                ddt[j+1, (S[i+1] ⊻ S[((i) ⊻ (j))+1])+1] += 1
        end
        return ddt
end

function mainDDT()
        #WARP Sbox
        S = [c, a, d, 3, e, b, f, 7, 8, 9, 1, 5, 0, 2, 4, 6]
        S = [12, 10, 13, 3, 14, 11, 15, 7, 8, 9, 1, 5, 0, 2, 4, 6]
        
        S = [12,6,9,0,1,10,2,11,3,8,5,13,4,14,7,15]

        #TWINE Sbox
        S = [12,0,15,10,2,11,9,5,8,3,13,7,1,14,6,4]


        #Lblock sboxes
        SS = [[14, 9, 15, 0, 13, 4, 10, 11, 1, 2, 8, 3, 7, 6, 12, 5],
              [4, 11, 14, 9, 15, 13, 0, 10, 7, 12, 5, 6, 2, 8, 1, 3],
              [1, 14, 7, 12, 15, 13, 0, 6, 11, 5, 9, 3, 2, 4, 8, 10],
              [7, 6, 8, 11, 0, 15, 3, 14, 9, 10, 12, 13, 5, 2, 4, 1],
              [14, 5, 15, 0, 7, 2, 12, 13, 1, 8, 4, 9, 11, 10, 6, 3],
              [2, 13, 11, 12, 15, 14, 0, 9, 7, 10, 6, 3, 1, 8, 4, 5],
              [11, 9, 4, 14, 0, 15, 10, 13, 6, 12, 5, 7, 3, 8, 1, 2],
              [13, 10, 15, 0, 14, 4, 9, 11, 2, 1, 8, 3, 7, 5, 12, 6],
              [8, 7, 14, 5, 15, 13, 0, 6, 11, 12, 9, 10, 2, 4, 1, 3],
              [11, 5, 15, 0, 7, 2, 9, 13, 4, 8, 1, 12, 14, 10, 3, 6]]

        for S in SS
                printmat(getDDT(S))
        end
end
function t(s,a) return a!=0 ? s-log2(a) : 0 end

S = [12, 10, 13, 3, 14, 11, 15, 7, 8, 9, 1, 5, 0, 2, 4, 6]
ddt = getDDT(S)

for i in 2:2:64
        println(t(8,i))
end
FF = @GaloisField! 𝔽₂ β^8 + β^4 + β^3 + β + 1

open(string(ch,"AES_128_3.json"),"r") do f
        loadgraph(f,LGFormat())
end


ch = "tagada-lib0/examples/dags/"

a = JSON.parsefile(string(ch,"AES_128_3.json"))
b = JSON.parsefile(string(ch,"AES_192_8.json"))

a["operators"][1]



