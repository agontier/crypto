/*
 * This file is part of choco-solver, http://choco-solver.org/
 *
 * Copyright (c) 2020, IMT Atlantique. All rights reserved.
 *
 * Licensed under the BSD 4-clause license.
 *
 * See LICENSE file in the project root for full license information.
 */

import org.chocosolver.solver.search.loop.monitors.IMonitorDownBranch;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.Variable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ZoomPanel extends JPanel implements IMonitorDownBranch {

    private BoolVar[][] X;
    JFrame frame;

    public ZoomPanel(BoolVar[][] x) {
        X = x;
        frame = new JFrame("Welecome to JavaTutorial.net");
        frame.getContentPane().add(this);
        frame.setSize(840*2, 288*2);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }

    @Override
    public void afterDownBranch(boolean left) {
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Variable dec = X[0][0].getModel().getSolver().getDecisionPath().getLastDecision().getDecisionVariable();
        setBackground(Color.WHITE);
        for (int i = 0; i < 288; i++) {
            for (int j = 0; j < 840; j++) {
                g.setColor(Color.WHITE);
                if (X[j][i].isInstantiatedTo(1)) {
                    g.setColor(Color.BLACK);
                } else if (X[j][i].isInstantiatedTo(0)) {
                    g.setColor(Color.LIGHT_GRAY);
                }
                if(dec == X[j][i]){
                    g.setColor(Color.RED);
                }
                g.drawRect(j*2, i*2, 1, 1);
            }
        }
    }

    public void save(int i) {
        this.repaint();
        Container contentPane = frame.getContentPane();
        BufferedImage image = new BufferedImage(contentPane.getWidth(), contentPane.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        contentPane.printAll(g2d);
        g2d.dispose();

        // replace this path to your image
        try {
            ImageIO.write(image, "jpeg", new File("Print_"+i+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}