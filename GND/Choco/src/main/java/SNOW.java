
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.IntVar;

import javax.swing.*;

public class SNOW {
    public static Model model = new Model("SNOW V");
    public static int n = 20;
    public static IntVar[][][] Z = new IntVar[n][4][4];
    public static IntVar[][][] R1 = new IntVar[n][4][4];
    public static IntVar[][][] R2 = new IntVar[n][4][4];
    public static IntVar[][][] R3 = new IntVar[n][4][4];
    public static IntVar[][][] B0 = new IntVar[n][4][4];
    public static IntVar[][][] B1 = new IntVar[n][4][4];
    public static IntVar[][][] A0 = new IntVar[n][4][4];
    public static IntVar[][][] A1 = new IntVar[n][4][4];

    public static IntVar[][][] initSNOW() {
        IntVar[][] R1 =
                model.intVarMatrix("R1", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] R2 =
                model.intVarMatrix("R2", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] R3 =
                model.intVarMatrix("R3", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] B0 =
                model.intVarMatrix("B0", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] B1 =
                model.intVarMatrix("B1", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] A0 =
                model.intVarMatrix("A0", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] A1 =
                model.intVarMatrix("A1", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        return new IntVar[][][]{R1, R2, R3, B0, B1, A0, A1};
    }

    public static void initZ() {
        for (int t = 0; t < n; t++)
            Z[t] = fillState(128, model);
    }

    public static void SNOW() {
        IntVar[][][] init = initSNOW();
        R1[0] = init[0];
        R2[0] = init[1];
        R3[0] = init[2];
        B0[0] = init[3];
        B1[0] = init[4];
        A0[0] = init[5];
        A1[0] = init[6];
        initZ();
        postZ(0);
        for (int t = 1; t < n; t++) {
            R1[t] = model.intVarMatrix(4, 4, 0, 255);
            R2[t] = model.intVarMatrix(4, 4, 0, 255);
            R3[t] = model.intVarMatrix(4, 4, 0, 255);
            B0[t] = model.intVarMatrix(4, 4, 0, 255);
            B1[t] = model.intVarMatrix(4, 4, 0, 255);
            A0[t] = model.intVarMatrix(4, 4, 0, 255);
            A1[t] = model.intVarMatrix(4, 4, 0, 255);
            postROUND(t);
        }


        Solver solver = model.getSolver();
//        solver.setSearch(Search.inputOrderLBSearch(model.retrieveIntVars(true)));
        solver.printShortFeatures();
        solver.showDashboard();

        while (solver.solve()) {
//            print(R1[0]);
//            print(R2[0]);
//            print(R3[0]);
//            print(B0[0]);
//            print(B1[0]);
//            print(A0[0]);
//            print(A1[0]);
//            solver.printShortStatistics();
        }
        solver.printShortStatistics();
    }

    public static void postROUND(int t) {
        IntVar[][] R3A0 = model.intVarMatrix("R3A0", 4, 4, 0, 255);
        postXOR(R3A0, R3[t - 1], A0[t - 1], model);
        IntVar[][] R3A0R2 = model.intVarMatrix("R3A0R2", 4, 4, 0, 255);
        postSUM32(R3A0, R2[t - 1], R3A0R2, model);
        postSigma(R3A0R2, R1[t], model);

        postAES(R1[t - 1], R2[t], model);
        postAES(R2[t - 1], R3[t], model);
        postEQ(B1[t - 1], B0[t], model);
        postEQ(A1[t - 1], A0[t], model);
        postEQ(B0[t - 1], A1[t], model);//Malpha needed
        postEQ(A0[t - 1], B1[t], model);//Mbeta

        postZ(t);
    }

    //      z(t) = (R1 􏰎+ B1) ⊕ R2
    public static void postZ(int t) {
        IntVar[][] R1B1 = model.intVarMatrix("R1pB1", 4, 4, 0, 255);
        postSUM32(R1[t], B1[t], R1B1, model);
        postXOR(R1B1, R2[t], Z[t], model);

    }

    public static void Guess() {
        Model model = new Model("SNOW V");
        IntVar Zm1[][] = new IntVar[][]{new IntVar[]{
                model.intVar(60), model.intVar(220), model.intVar(172), model.intVar(158)}, new IntVar[]{
                model.intVar(121), model.intVar(62), model.intVar(56), model.intVar(185)}, new IntVar[]{
                model.intVar(211), model.intVar(48), model.intVar(144), model.intVar(221)}, new IntVar[]{
                model.intVar(236), model.intVar(125), model.intVar(44), model.intVar(237)}};
        IntVar Z[][] = new IntVar[][]{new IntVar[]{
                model.intVar(93), model.intVar(215), model.intVar(241), model.intVar(12)}, new IntVar[]{
                model.intVar(60), model.intVar(7), model.intVar(221), model.intVar(125)}, new IntVar[]{
                model.intVar(152), model.intVar(229), model.intVar(208), model.intVar(197)}, new IntVar[]{
                model.intVar(227), model.intVar(36), model.intVar(97), model.intVar(90)}};
        IntVar Zp1[][] = new IntVar[][]{new IntVar[]{
                model.intVar(4), model.intVar(180), model.intVar(109), model.intVar(133)}, new IntVar[]{
                model.intVar(57), model.intVar(52), model.intVar(100), model.intVar(80)}, new IntVar[]{
                model.intVar(201), model.intVar(143), model.intVar(37), model.intVar(189)}, new IntVar[]{
                model.intVar(195), model.intVar(72), model.intVar(24), model.intVar(12)}};

//        IntVar[][] Zm1 =
//                model.intVarMatrix("R1", 4, 4, 0, 255);
////                randState(model);
////                fillState(0, model);
//        IntVar[][] Z =
//                model.intVarMatrix("R1", 4, 4, 0, 255);
////                randState(model);
////                fillState(0, model);
//        IntVar[][] Zp1 =
//                model.intVarMatrix("R1", 4, 4, 0, 255);
////                randState(model);
////                fillState(99, model);

        IntVar R1[][] = new IntVar[][]{new IntVar[]{
                model.intVar(9), model.intVar(124), model.intVar(186), model.intVar(79)}, new IntVar[]{
                model.intVar(138), model.intVar(77), model.intVar(234), model.intVar(128)}, new IntVar[]{
                model.intVar(10), model.intVar(243), model.intVar(60), model.intVar(153)}, new IntVar[]{
                model.intVar(167), model.intVar(155), model.intVar(202), model.intVar(37)}};
        IntVar R2[][] = new IntVar[][]{new IntVar[]{
                model.intVar(197), model.intVar(215), model.intVar(223), model.intVar(81)}, new IntVar[]{
                model.intVar(231), model.intVar(224), model.intVar(23), model.intVar(222)}, new IntVar[]{
                model.intVar(178), model.intVar(207), model.intVar(105), model.intVar(199)}, new IntVar[]{
                model.intVar(56), model.intVar(195), model.intVar(171), model.intVar(249)}};
        IntVar B0[][] = new IntVar[][]{new IntVar[]{
                model.intVar(229), model.intVar(85), model.intVar(88), model.intVar(94)}, new IntVar[]{
                model.intVar(122), model.intVar(53), model.intVar(226), model.intVar(74)}, new IntVar[]{
                model.intVar(68), model.intVar(205), model.intVar(237), model.intVar(217)}, new IntVar[]{
                model.intVar(139), model.intVar(42), model.intVar(168), model.intVar(8)}};

//        IntVar[][] R1 =
//                model.intVarMatrix("R1", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
//        IntVar[][] R2 =
//                model.intVarMatrix("R2", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] R3 =
                model.intVarMatrix("R3", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
//        IntVar[][] B0 =
//                model.intVarMatrix("B0", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] B1 =
                model.intVarMatrix("B1", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        IntVar[][] A0 =
                model.intVarMatrix("A0", 4, 4, 0, 255);
//                randState(model);
//                fillState(255, model);
        Determine(Zm1, Z, Zp1, R1, R2, R3, B0, B1, A0, model);
    }

    public static IntVar randVar(Model model) {
        return model.intVar((int) (Math.random() * 255));
    }

    public static IntVar[][] randState(Model model) {
        return new IntVar[][]{new IntVar[]{randVar(model), randVar(model), randVar(model), randVar(model)},
                new IntVar[]{randVar(model), randVar(model), randVar(model), randVar(model)},
                new IntVar[]{randVar(model), randVar(model), randVar(model), randVar(model)},
                new IntVar[]{randVar(model), randVar(model), randVar(model), randVar(model)}};
    }

    public static IntVar[][] fillState(int i, Model model) {
        return new IntVar[][]{new IntVar[]{model.intVar(i), model.intVar(i), model.intVar(i), model.intVar(i)},
                new IntVar[]{model.intVar(i), model.intVar(i), model.intVar(i), model.intVar(i)},
                new IntVar[]{model.intVar(i), model.intVar(i), model.intVar(i), model.intVar(i)},
                new IntVar[]{model.intVar(i), model.intVar(i), model.intVar(i), model.intVar(i)}};
    }

    public static void Determine(IntVar[][] Zm1, IntVar[][] Z, IntVar[][] Zp1,
                                 IntVar[][] R1, IntVar[][] R2, IntVar[][] R3,
                                 IntVar[][] B0, IntVar[][] B1, IntVar[][] A0, Model model) {
        IntVar[][] C = model.intVarMatrix("C", 4, 4, 0, 255);
        IntVar[][] D = model.intVarMatrix("D", 4, 4, 0, 255);

        IntVar[][] AESm1R2 = model.intVarMatrix("AESm1R2", 4, 4, 0, 255);
        IntVar[][] AESm1R3 = model.intVarMatrix("AESm1R3", 4, 4, 0, 255);
        IntVar[][] AESm1R2pB0 = model.intVarMatrix("AESm1R2pB0", 4, 4, 0, 255);
        IntVar[][] R1pB1 = model.intVarMatrix("R1pB1", 4, 4, 0, 255);
        IntVar[][] AESR1 = model.intVarMatrix("AESR1", 4, 4, 0, 255);

        IntVar[][] LB0 = model.intVarMatrix("LB0", 4, 4, 0, 255);
        IntVar[][] HB1 = model.intVarMatrix("HB1", 4, 4, 0, 255);

        IntVar[][] AC = model.intVarMatrix("AC", 4, 4, 0, 255);
        IntVar[][] R3A = model.intVarMatrix("R3A", 4, 4, 0, 255);
        IntVar[][] R2R3A = model.intVarMatrix("R2R3A", 4, 4, 0, 255);
        IntVar[][] sigmaR2R3A = model.intVarMatrix("sigmaR2R3A", 4, 4, 0, 255);

//      z(t−1) = (AES−1 (R2) 􏰎+ B0) ⊕ AES−1 (R3)
        postXOR(AESm1R2pB0, AESm1R3, Zm1, model);
        postAES(AESm1R3, R3, model);
        postSUM32(AESm1R2, B0, AESm1R2pB0, model);
        postAES(AESm1R2, R2, model);

//      z(t) = (R1 􏰎+ B1) ⊕ R2
        postXOR(R1pB1, R2, Z, model);
        postSUM32(R1, B1, R1pB1, model);

//      z(t + 1) = AES(R1) ⊕ D
        postXOR(AESR1, D, Zp1, model);
        postAES(R1, AESR1, model);

//      C = Lbeta(B0) ⊕ Hbeta(B1)
        postLbeta(B0, LB0, model);
        postHbeta(B1, HB1, model);
        postXOR(LB0, HB1, C, model);

//      D = sigma(R2 􏰎+ (R3 ⊕ A0))􏰎 + (A0 ⊕ C).
        postXOR(A0, C, AC, model);
        postXOR(R3, A0, R3A, model);
        postSUM32(R2, R3A, R2R3A, model);
        postSigma(R2R3A, sigmaR2R3A, model);
        postSUM32(sigmaR2R3A, AC, D, model);

//        C′ = lβ(B1) ⊕ hβ(A0 ⊕ lβ(B0) ⊕ hβ(B1))
//        z(t+2) ⊕ R2(t+2) = σ(R2(t+1) 􏰎+ (R3(t+1) ⊕ A1)) 􏰎+ (A1 ⊕ C′).

        Solver solver = model.getSolver();
//        solver.setSearch(Search.inputOrderLBSearch(model.retrieveIntVars(true)));
//        solver.setSearch(Search.conflictHistorySearch(putTogether(new IntVar[][][]{Zm1, Z, Zp1, R1, R2, B1, B0, R3, A0})));

        solver.printShortFeatures();
//        solver.showDashboard();
        if (solver.solve()) {
            print(Zm1);
            print(Z);
            print(Zp1);
            print(R1);
            print(R2);
            print(R3);
            print(B0);
            print(B1);
            print(A0);//49 149 77 228
            print(C);
            print(D);
            solver.printShortStatistics();
            while (solver.solve()) {
                print(Zm1);
                print(Z);
                print(Zp1);
                print(R1);
                print(R2);
                print(R3);
                print(B0);
                print(B1);
                print(A0);//49 149 77 229
                print(C);
                print(D);
                solver.printShortStatistics();
            }
        } else System.out.println("IMPOSSIBEEEEEULE");
        solver.printShortStatistics();
    }

    public static IntVar[] putTogether(IntVar[][][] L) {
        IntVar[] res = new IntVar[L.length * 4 * 4];
        for (int i = 0; i < L.length; i++)
            for (int ii = 0; ii < 4; ii++)
                for (int iii = 0; iii < 4; iii++)
                    res[i * 16 + ii * 4 + iii] = L[i][ii][iii];
        return res;
    }

    // T = sigma(S)
    public static void postSigma(IntVar[][] S, IntVar[][] T, Model model) {
        for (int i = 0; i < 16; i++)
            model.arithm(T[i / 4][i % 4], "=", S[sigma[i] / 4][sigma[i] % 4]).post();
    }

    public static void postLbeta(IntVar[][] B, IntVar[][] T, Model model) {
        postbetaXor(B, T, 0, 6, beta, model);
        postbetaXor(B, T, 2, 8, beta, model);
        postbetaXor(B, T, 4, 10, beta, model);
        postbetaXor(B, T, 6, 12, beta, model);
        postbetaXor(B, T, 8, 14, beta, model);
        int i = 10;
        model.table(new IntVar[]{B[i / 4][i % 4], B[(i + 1) / 4][(i + 1) % 4],
                T[i / 4][i % 4], T[(i + 1) / 4][(i + 1) % 4]}, beta).post();
        i = 12;
        model.table(new IntVar[]{B[i / 4][i % 4], B[(i + 1) / 4][(i + 1) % 4],
                T[i / 4][i % 4], T[(i + 1) / 4][(i + 1) % 4]}, beta).post();
        i = 14;
        model.table(new IntVar[]{B[i / 4][i % 4], B[(i + 1) / 4][(i + 1) % 4],
                T[i / 4][i % 4], T[(i + 1) / 4][(i + 1) % 4]}, beta).post();
    }

    public static void postHbeta(IntVar[][] B, IntVar[][] T, Model model) {
        for (int i = 0; i <= 8; i = i + 2)
            model.table(new IntVar[]{B[i / 4][i % 4], B[(i + 1) / 4][(i + 1) % 4],
                    T[i / 4][i % 4], T[(i + 1) / 4][(i + 1) % 4]}, betam1).post();
        postbetaXor(B, T, 10, 0, betam1, model);
        postbetaXor(B, T, 12, 2, betam1, model);
        postbetaXor(B, T, 14, 4, betam1, model);
    }

    //l(B0||1) = Mbeta* B0||1 ⊕ B6||7
    public static void postbetaXor(IntVar[][] B, IntVar[][] T, int i, int j, Tuples tuples, Model model) {
        IntVar tmp1 = model.intVar("tmpl1", 0, 255);
        IntVar tmp2 = model.intVar("tmpl2", 0, 255);
        model.table(new IntVar[]{B[i / 4][i % 4], B[(i + 1) / 4][(i + 1) % 4], tmp1, tmp2}, tuples).post();
        new Constraint("lxor", new PropXOR(tmp1, B[j / 4][j % 4], T[i / 4][i % 4])).post();
        new Constraint("lxor", new PropXOR(tmp2, B[(j + 1) / 4][(j + 1) % 4], T[(i + 1) / 4][(i + 1) % 4])).post();
    }

    public static void postXOR(IntVar[][] S, IntVar[][] S2, IntVar[][] T, Model model) {
        for (int j = 0; j < 4; j++)
            for (int i = 0; i < 4; i++)
                new Constraint("A", new PropXOR(S[j][i], S2[j][i], T[j][i])).post();
    }

    //4 parallel modular 2^32 sum over a state
    public static void postSUM32(IntVar[][] S, IntVar[][] S2, IntVar[][] T, Model model) {
        IntVar v256 = model.intVar("256", 256);
        for (int j = 0; j < S.length; j++) {
            IntVar tmp1 = model.intVar("tmp1SUM32", 0, 510);
            IntVar ret1 = model.intVar("ret1SUM32", 0, 1);
            model.sum(new IntVar[]{S[0][j], S2[0][j]}, "=", tmp1).post();
            model.mod(tmp1, 256, T[0][j]).post();
            model.div(tmp1, v256, ret1).post();
            IntVar tmp2 = model.intVar("tmp2SUM32", 0, 511);
            IntVar ret2 = model.intVar("ret2SUM32", 0, 1);
            model.sum(new IntVar[]{S[1][j], S2[1][j], ret1}, "=", tmp2).post();
            model.mod(tmp2, 256, T[1][j]).post();
            model.div(tmp2, v256, ret2).post();
            IntVar tmp3 = model.intVar("tmp3SUM32", 0, 511);
            IntVar ret3 = model.intVar("ret3SUM32", 0, 1);
            model.sum(new IntVar[]{S[2][j], S2[2][j], ret2}, "=", tmp3).post();
            model.mod(tmp3, 256, T[2][j]).post();
            model.div(tmp3, v256, ret3).post();
            IntVar tmp4 = model.intVar("tmp4SUM32", 0, 511);
            model.sum(new IntVar[]{S[3][j], S2[3][j], ret3}, "=", tmp4).post();
            model.mod(tmp2, 256, T[3][j]).post();
        }
    }

    public static void postSUM8(IntVar S, IntVar S2, IntVar T, Model model) {
        IntVar tmp = model.intVar("tmpSUM8", 0, 510);
        model.sum(new IntVar[]{S, S2}, "=", tmp).post();
        model.mod(tmp, 256, T).post();
    }

    public static void postEQ(IntVar[][] xIn, IntVar[][] xOut, Model model) {
        for (int j = 0; j < 4; j++)
            for (int i = 0; i < 4; i++)
                model.arithm(xIn[j][i], "=", xOut[j][i]).post();
    }

    public static void postAES(IntVar[][] S, IntVar[][] T, Model model) {
        IntVar[][] S2 = model.intVarMatrix("tmpMC", 4, 4, 0, 255);
        postSB(S, S2, model);
        IntVar[][] S3 = model.intVarMatrix("tmpMC", 4, 4, 0, 255);
        postSR(S2, S3, model);
        postMC(S3, T, model);
    }

    // Shift rows
    public static void postSR(IntVar[][] xIn, IntVar[][] xOut, Model model) {
        for (int j = 0; j < 4; j++)
            for (int i = 0; i < 4; i++)
                model.arithm(xIn[(j + i) % 4][i], "=", xOut[j][i]).post();
    }

    // SB relation
    public static void postSB(IntVar[][] xIn, IntVar[][] xOut, Model model) {
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                model.table(xIn[j][i], xOut[j][i], tupleSB(), "CT+").post();
    }

    //Mix column
    public static void postMC(IntVar[][] S, IntVar[][] T, Model model) {
        for (int j = 0; j < S.length; j++) {
            postMC1(S[j], T[j], model);
            postMCInv(T[j], S[j], model);
        }
    }

    // Enforces the mixcolumns constraint
    public static void postMC1(IntVar[] SR, IntVar[] Y, Model model) {
        IntVar v1 = model.intVar("v1", 0, 255);
        IntVar v2 = model.intVar("v2", 0, 255);
        IntVar v3 = model.intVar("v3", 0, 255);
        IntVar v4 = model.intVar("v4", 0, 255);
        IntVar v5 = model.intVar("v5", 0, 255);
        IntVar v6 = model.intVar("v6", 0, 255);
        IntVar v7 = model.intVar("v7", 0, 255);
        IntVar v8 = model.intVar("v8", 0, 255);
        model.table(new IntVar[]{v1, SR[0], SR[1]}, tupleMul2xorMul3, "CT+").post(); // v1 = MUL2*SR[0][i] xor MUL3*SR[1][i]
        new Constraint("A", new PropXOR(v1, SR[2], v2)).post(); // v2 = v1 xor SR[2][i]
        new Constraint("A", new PropXOR(v2, SR[3], Y[0])).post(); // Y[0][i] = v2 xor SR[3][i]
        model.table(new IntVar[]{v3, SR[1], SR[2]}, tupleMul2xorMul3, "CT+").post(); // v3 = MUL2*SR[1][i] xor MUL3*SR[2][i]
        new Constraint("A", new PropXOR(v3, SR[0], v4)).post(); // v4 = v3 xor SR[0][i]
        new Constraint("A", new PropXOR(v4, SR[3], Y[1])).post(); // Y[1][i] = v4 xor SR[3][i]
        model.table(new IntVar[]{v5, SR[2], SR[3]}, tupleMul2xorMul3, "CT+").post(); // v5 = MUL2*SR[2][i] xor MUL3*SR[3][i]
        new Constraint("A", new PropXOR(v5, SR[0], v6)).post(); // v6 = v5 xor SR[2][i]
        new Constraint("A", new PropXOR(v6, SR[1], Y[2])).post(); // Y[2][i] = v6 xor SR[1][i]
        model.table(new IntVar[]{v7, SR[3], SR[0]}, tupleMul2xorMul3, "CT+").post(); // v7 = MUL2*SR[3][i] xor MUL3*SR[0][i]
        new Constraint("A", new PropXOR(v7, SR[1], v8)).post(); // v8 = v7 xor SR[1][i]
        new Constraint("A", new PropXOR(v8, SR[2], Y[3])).post(); // Y[3][i] = v8 xor SR[2][i]
    }

    // The inverse mixcolumn operation
    public static void postMCInv(IntVar[] DX, IntVar[] SR, Model model) {
        IntVar[] tmp = new IntVar[]{DX[0], DX[1], DX[2], DX[3]};
        IntVar[] tmp2 = new IntVar[]{SR[0], SR[1], SR[2], SR[3]};
//            model.ifThen( ???
//                    model.sum(tmp,"=",0),
//                    model.sum(tmp2,"=",0));
        IntVar[] tmpMC = model.intVarArray("tmpMC", 24, 0, 255);
        String algo = "CT+";
        model.table(tmp[0], tmpMC[0], mul14, algo).post();
        model.table(tmp[1], tmpMC[1], mul11, algo).post();
        model.table(tmp[2], tmpMC[2], mul13, algo).post();
        model.table(tmp[3], tmpMC[3], mul9, algo).post();
        model.table(tmp[0], tmpMC[4], mul9, algo).post();
        model.table(tmp[1], tmpMC[5], mul14, algo).post();
        model.table(tmp[2], tmpMC[6], mul11, algo).post();
        model.table(tmp[3], tmpMC[7], mul13, algo).post();
        model.table(tmp[0], tmpMC[8], mul13, algo).post();
        model.table(tmp[1], tmpMC[9], mul9, algo).post();
        model.table(tmp[2], tmpMC[10], mul14, algo).post();
        model.table(tmp[3], tmpMC[11], mul11, algo).post();
        model.table(tmp[0], tmpMC[12], mul11, algo).post();
        model.table(tmp[1], tmpMC[13], mul13, algo).post();
        model.table(tmp[2], tmpMC[14], mul9, algo).post();
        model.table(tmp[3], tmpMC[15], mul14, algo).post();
        new Constraint("A", new PropXOR(tmpMC[0], tmpMC[1], tmpMC[16])).post();
        new Constraint("A", new PropXOR(tmpMC[2], tmpMC[3], tmpMC[17])).post();
        new Constraint("A", new PropXOR(tmpMC[16], tmpMC[17], tmp2[0])).post();
        new Constraint("A", new PropXOR(tmpMC[4], tmpMC[5], tmpMC[18])).post();
        new Constraint("A", new PropXOR(tmpMC[6], tmpMC[7], tmpMC[19])).post();
        new Constraint("A", new PropXOR(tmpMC[18], tmpMC[19], tmp2[1])).post();
        new Constraint("A", new PropXOR(tmpMC[8], tmpMC[9], tmpMC[20])).post();
        new Constraint("A", new PropXOR(tmpMC[10], tmpMC[11], tmpMC[21])).post();
        new Constraint("A", new PropXOR(tmpMC[20], tmpMC[21], tmp2[2])).post();
        new Constraint("A", new PropXOR(tmpMC[12], tmpMC[13], tmpMC[22])).post();
        new Constraint("A", new PropXOR(tmpMC[14], tmpMC[15], tmpMC[23])).post();
        new Constraint("A", new PropXOR(tmpMC[22], tmpMC[23], tmp2[3])).post();
    }

    // returns { (x,y,z) | x = Mul2*y xor Mul3*z}
    public static Tuples createRelationMul2xorMul3() {
        Tuples tuples = new Tuples(true);
        for (int i = 0; i < 256; i++) {
            int ii;
            if (i < 128) ii = 2 * i;
            else ii = (2 * i % 256) ^ 27;
            for (int j = 0; j < 256; j++) {
                int jj;
                if (j < 128) jj = (2 * j) ^ j;
                else jj = ((2 * j % 256) ^ 27) ^ j;
                tuples.add(ii ^ jj, i, j);
            }
        }
        return tuples;
    }
    //27 n'est pas le vrais polynome réducteur. c'est le polynome moins le msb (le msb est dans le modulo 256)
    //

    // returns x||y = M * i||j
    public static Tuples createRelationMatrixLFSR(int[][] M) {
        Tuples tuples = new Tuples(true);
        int x, y;
        for (int i = 0; i < 256; i++) {
            for (int j = 0; j < 256; j++) {
                int[] ti = getBitArray(i);
                int[] tj = getBitArray(j);
                int[] tx = new int[8];
                int[] ty = new int[8];
                for (int k = 0; k < 8; k++) {
                    for (int ki = 0; ki < 8; ki++) {
                        tx[k] ^= M[k][ki] * ti[ki];
                        ty[k] ^= M[k + 8][ki] * ti[ki];
                    }
                    for (int kj = 0; kj < 8; kj++) {
                        tx[k] ^= M[k][kj + 8] * tj[kj];
                        ty[k] ^= M[k + 8][kj + 8] * tj[kj];
                    }
                }
                x = getInt(tx);
                y = getInt(ty);
                tuples.add(i, j, x, y);
            }
        }
        return tuples;
    }

    public static Tuples tupleMul2xorMul3 = createRelationMul2xorMul3();

    public static Tuples tupleSB() {
        Tuples tuples = new Tuples(true);
        for (int i = 0; i < 256; i++)
            tuples.add(i, Sbox[i]);
        return tuples;
    }

    // The SBox of the AES
    public static int[] Sbox = new int[]{
            99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118,
            202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192,
            183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21,
            4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117,
            9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132,
            83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207,
            208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168,
            81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210,
            205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115,
            96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219,
            224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121,
            231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8,
            186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138,
            112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158,
            225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223,
            140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22};

    // Lookup table for multiplying by 9 in Rijndael's finite field
    public static Tuples mul9 = initMul9();

    public static Tuples initMul9() {
        Tuples ret = new Tuples(true);
        int tab[] = {0x00, 0x09, 0x12, 0x1b, 0x24, 0x2d, 0x36, 0x3f, 0x48, 0x41, 0x5a, 0x53, 0x6c, 0x65, 0x7e, 0x77,
                0x90, 0x99, 0x82, 0x8b, 0xb4, 0xbd, 0xa6, 0xaf, 0xd8, 0xd1, 0xca, 0xc3, 0xfc, 0xf5, 0xee, 0xe7,
                0x3b, 0x32, 0x29, 0x20, 0x1f, 0x16, 0x0d, 0x04, 0x73, 0x7a, 0x61, 0x68, 0x57, 0x5e, 0x45, 0x4c,
                0xab, 0xa2, 0xb9, 0xb0, 0x8f, 0x86, 0x9d, 0x94, 0xe3, 0xea, 0xf1, 0xf8, 0xc7, 0xce, 0xd5, 0xdc,
                0x76, 0x7f, 0x64, 0x6d, 0x52, 0x5b, 0x40, 0x49, 0x3e, 0x37, 0x2c, 0x25, 0x1a, 0x13, 0x08, 0x01,
                0xe6, 0xef, 0xf4, 0xfd, 0xc2, 0xcb, 0xd0, 0xd9, 0xae, 0xa7, 0xbc, 0xb5, 0x8a, 0x83, 0x98, 0x91,
                0x4d, 0x44, 0x5f, 0x56, 0x69, 0x60, 0x7b, 0x72, 0x05, 0x0c, 0x17, 0x1e, 0x21, 0x28, 0x33, 0x3a,
                0xdd, 0xd4, 0xcf, 0xc6, 0xf9, 0xf0, 0xeb, 0xe2, 0x95, 0x9c, 0x87, 0x8e, 0xb1, 0xb8, 0xa3, 0xaa,
                0xec, 0xe5, 0xfe, 0xf7, 0xc8, 0xc1, 0xda, 0xd3, 0xa4, 0xad, 0xb6, 0xbf, 0x80, 0x89, 0x92, 0x9b,
                0x7c, 0x75, 0x6e, 0x67, 0x58, 0x51, 0x4a, 0x43, 0x34, 0x3d, 0x26, 0x2f, 0x10, 0x19, 0x02, 0x0b,
                0xd7, 0xde, 0xc5, 0xcc, 0xf3, 0xfa, 0xe1, 0xe8, 0x9f, 0x96, 0x8d, 0x84, 0xbb, 0xb2, 0xa9, 0xa0,
                0x47, 0x4e, 0x55, 0x5c, 0x63, 0x6a, 0x71, 0x78, 0x0f, 0x06, 0x1d, 0x14, 0x2b, 0x22, 0x39, 0x30,
                0x9a, 0x93, 0x88, 0x81, 0xbe, 0xb7, 0xac, 0xa5, 0xd2, 0xdb, 0xc0, 0xc9, 0xf6, 0xff, 0xe4, 0xed,
                0x0a, 0x03, 0x18, 0x11, 0x2e, 0x27, 0x3c, 0x35, 0x42, 0x4b, 0x50, 0x59, 0x66, 0x6f, 0x74, 0x7d,
                0xa1, 0xa8, 0xb3, 0xba, 0x85, 0x8c, 0x97, 0x9e, 0xe9, 0xe0, 0xfb, 0xf2, 0xcd, 0xc4, 0xdf, 0xd6,
                0x31, 0x38, 0x23, 0x2a, 0x15, 0x1c, 0x07, 0x0e, 0x79, 0x70, 0x6b, 0x62, 0x5d, 0x54, 0x4f, 0x46};

        for (int i = 0; i < 256; i++) ret.add(i, tab[i]);
        return ret;
    }

    // Lookup table for multiplying by 11 in Rijndael's finite field
    public static Tuples mul11 = initMul11();

    public static Tuples initMul11() {
        Tuples ret = new Tuples(true);
        int tab[] = {0x00, 0x0b, 0x16, 0x1d, 0x2c, 0x27, 0x3a, 0x31, 0x58, 0x53, 0x4e, 0x45, 0x74, 0x7f, 0x62, 0x69,
                0xb0, 0xbb, 0xa6, 0xad, 0x9c, 0x97, 0x8a, 0x81, 0xe8, 0xe3, 0xfe, 0xf5, 0xc4, 0xcf, 0xd2, 0xd9,
                0x7b, 0x70, 0x6d, 0x66, 0x57, 0x5c, 0x41, 0x4a, 0x23, 0x28, 0x35, 0x3e, 0x0f, 0x04, 0x19, 0x12,
                0xcb, 0xc0, 0xdd, 0xd6, 0xe7, 0xec, 0xf1, 0xfa, 0x93, 0x98, 0x85, 0x8e, 0xbf, 0xb4, 0xa9, 0xa2,
                0xf6, 0xfd, 0xe0, 0xeb, 0xda, 0xd1, 0xcc, 0xc7, 0xae, 0xa5, 0xb8, 0xb3, 0x82, 0x89, 0x94, 0x9f,
                0x46, 0x4d, 0x50, 0x5b, 0x6a, 0x61, 0x7c, 0x77, 0x1e, 0x15, 0x08, 0x03, 0x32, 0x39, 0x24, 0x2f,
                0x8d, 0x86, 0x9b, 0x90, 0xa1, 0xaa, 0xb7, 0xbc, 0xd5, 0xde, 0xc3, 0xc8, 0xf9, 0xf2, 0xef, 0xe4,
                0x3d, 0x36, 0x2b, 0x20, 0x11, 0x1a, 0x07, 0x0c, 0x65, 0x6e, 0x73, 0x78, 0x49, 0x42, 0x5f, 0x54,
                0xf7, 0xfc, 0xe1, 0xea, 0xdb, 0xd0, 0xcd, 0xc6, 0xaf, 0xa4, 0xb9, 0xb2, 0x83, 0x88, 0x95, 0x9e,
                0x47, 0x4c, 0x51, 0x5a, 0x6b, 0x60, 0x7d, 0x76, 0x1f, 0x14, 0x09, 0x02, 0x33, 0x38, 0x25, 0x2e,
                0x8c, 0x87, 0x9a, 0x91, 0xa0, 0xab, 0xb6, 0xbd, 0xd4, 0xdf, 0xc2, 0xc9, 0xf8, 0xf3, 0xee, 0xe5,
                0x3c, 0x37, 0x2a, 0x21, 0x10, 0x1b, 0x06, 0x0d, 0x64, 0x6f, 0x72, 0x79, 0x48, 0x43, 0x5e, 0x55,
                0x01, 0x0a, 0x17, 0x1c, 0x2d, 0x26, 0x3b, 0x30, 0x59, 0x52, 0x4f, 0x44, 0x75, 0x7e, 0x63, 0x68,
                0xb1, 0xba, 0xa7, 0xac, 0x9d, 0x96, 0x8b, 0x80, 0xe9, 0xe2, 0xff, 0xf4, 0xc5, 0xce, 0xd3, 0xd8,
                0x7a, 0x71, 0x6c, 0x67, 0x56, 0x5d, 0x40, 0x4b, 0x22, 0x29, 0x34, 0x3f, 0x0e, 0x05, 0x18, 0x13,
                0xca, 0xc1, 0xdc, 0xd7, 0xe6, 0xed, 0xf0, 0xfb, 0x92, 0x99, 0x84, 0x8f, 0xbe, 0xb5, 0xa8, 0xa3};
        for (int i = 0; i < 256; i++) ret.add(i, tab[i]);
        return ret;
    }

    public static Tuples mul13 = initMul13();

    // Lookup table for multiplying by 13 in Rijndael's finite field
    public static Tuples initMul13() {
        Tuples ret = new Tuples(true);
        int tab[] = {0x00, 0x0d, 0x1a, 0x17, 0x34, 0x39, 0x2e, 0x23, 0x68, 0x65, 0x72, 0x7f, 0x5c, 0x51, 0x46, 0x4b,
                0xd0, 0xdd, 0xca, 0xc7, 0xe4, 0xe9, 0xfe, 0xf3, 0xb8, 0xb5, 0xa2, 0xaf, 0x8c, 0x81, 0x96, 0x9b,
                0xbb, 0xb6, 0xa1, 0xac, 0x8f, 0x82, 0x95, 0x98, 0xd3, 0xde, 0xc9, 0xc4, 0xe7, 0xea, 0xfd, 0xf0,
                0x6b, 0x66, 0x71, 0x7c, 0x5f, 0x52, 0x45, 0x48, 0x03, 0x0e, 0x19, 0x14, 0x37, 0x3a, 0x2d, 0x20,
                0x6d, 0x60, 0x77, 0x7a, 0x59, 0x54, 0x43, 0x4e, 0x05, 0x08, 0x1f, 0x12, 0x31, 0x3c, 0x2b, 0x26,
                0xbd, 0xb0, 0xa7, 0xaa, 0x89, 0x84, 0x93, 0x9e, 0xd5, 0xd8, 0xcf, 0xc2, 0xe1, 0xec, 0xfb, 0xf6,
                0xd6, 0xdb, 0xcc, 0xc1, 0xe2, 0xef, 0xf8, 0xf5, 0xbe, 0xb3, 0xa4, 0xa9, 0x8a, 0x87, 0x90, 0x9d,
                0x06, 0x0b, 0x1c, 0x11, 0x32, 0x3f, 0x28, 0x25, 0x6e, 0x63, 0x74, 0x79, 0x5a, 0x57, 0x40, 0x4d,
                0xda, 0xd7, 0xc0, 0xcd, 0xee, 0xe3, 0xf4, 0xf9, 0xb2, 0xbf, 0xa8, 0xa5, 0x86, 0x8b, 0x9c, 0x91,
                0x0a, 0x07, 0x10, 0x1d, 0x3e, 0x33, 0x24, 0x29, 0x62, 0x6f, 0x78, 0x75, 0x56, 0x5b, 0x4c, 0x41,
                0x61, 0x6c, 0x7b, 0x76, 0x55, 0x58, 0x4f, 0x42, 0x09, 0x04, 0x13, 0x1e, 0x3d, 0x30, 0x27, 0x2a,
                0xb1, 0xbc, 0xab, 0xa6, 0x85, 0x88, 0x9f, 0x92, 0xd9, 0xd4, 0xc3, 0xce, 0xed, 0xe0, 0xf7, 0xfa,
                0xb7, 0xba, 0xad, 0xa0, 0x83, 0x8e, 0x99, 0x94, 0xdf, 0xd2, 0xc5, 0xc8, 0xeb, 0xe6, 0xf1, 0xfc,
                0x67, 0x6a, 0x7d, 0x70, 0x53, 0x5e, 0x49, 0x44, 0x0f, 0x02, 0x15, 0x18, 0x3b, 0x36, 0x21, 0x2c,
                0x0c, 0x01, 0x16, 0x1b, 0x38, 0x35, 0x22, 0x2f, 0x64, 0x69, 0x7e, 0x73, 0x50, 0x5d, 0x4a, 0x47,
                0xdc, 0xd1, 0xc6, 0xcb, 0xe8, 0xe5, 0xf2, 0xff, 0xb4, 0xb9, 0xae, 0xa3, 0x80, 0x8d, 0x9a, 0x97};
        for (int i = 0; i < 256; i++) ret.add(i, tab[i]);
        return ret;
    }

    public static Tuples mul14 = initMul14();
    // Lookup table for multiplying by 14 in Rijndael's finite field

    public static Tuples initMul14() {
        Tuples ret = new Tuples(true);
        int tab[] = {0x00, 0x0e, 0x1c, 0x12, 0x38, 0x36, 0x24, 0x2a, 0x70, 0x7e, 0x6c, 0x62, 0x48, 0x46, 0x54, 0x5a,
                0xe0, 0xee, 0xfc, 0xf2, 0xd8, 0xd6, 0xc4, 0xca, 0x90, 0x9e, 0x8c, 0x82, 0xa8, 0xa6, 0xb4, 0xba,
                0xdb, 0xd5, 0xc7, 0xc9, 0xe3, 0xed, 0xff, 0xf1, 0xab, 0xa5, 0xb7, 0xb9, 0x93, 0x9d, 0x8f, 0x81,
                0x3b, 0x35, 0x27, 0x29, 0x03, 0x0d, 0x1f, 0x11, 0x4b, 0x45, 0x57, 0x59, 0x73, 0x7d, 0x6f, 0x61,
                0xad, 0xa3, 0xb1, 0xbf, 0x95, 0x9b, 0x89, 0x87, 0xdd, 0xd3, 0xc1, 0xcf, 0xe5, 0xeb, 0xf9, 0xf7,
                0x4d, 0x43, 0x51, 0x5f, 0x75, 0x7b, 0x69, 0x67, 0x3d, 0x33, 0x21, 0x2f, 0x05, 0x0b, 0x19, 0x17,
                0x76, 0x78, 0x6a, 0x64, 0x4e, 0x40, 0x52, 0x5c, 0x06, 0x08, 0x1a, 0x14, 0x3e, 0x30, 0x22, 0x2c,
                0x96, 0x98, 0x8a, 0x84, 0xae, 0xa0, 0xb2, 0xbc, 0xe6, 0xe8, 0xfa, 0xf4, 0xde, 0xd0, 0xc2, 0xcc,
                0x41, 0x4f, 0x5d, 0x53, 0x79, 0x77, 0x65, 0x6b, 0x31, 0x3f, 0x2d, 0x23, 0x09, 0x07, 0x15, 0x1b,
                0xa1, 0xaf, 0xbd, 0xb3, 0x99, 0x97, 0x85, 0x8b, 0xd1, 0xdf, 0xcd, 0xc3, 0xe9, 0xe7, 0xf5, 0xfb,
                0x9a, 0x94, 0x86, 0x88, 0xa2, 0xac, 0xbe, 0xb0, 0xea, 0xe4, 0xf6, 0xf8, 0xd2, 0xdc, 0xce, 0xc0,
                0x7a, 0x74, 0x66, 0x68, 0x42, 0x4c, 0x5e, 0x50, 0x0a, 0x04, 0x16, 0x18, 0x32, 0x3c, 0x2e, 0x20,
                0xec, 0xe2, 0xf0, 0xfe, 0xd4, 0xda, 0xc8, 0xc6, 0x9c, 0x92, 0x80, 0x8e, 0xa4, 0xaa, 0xb8, 0xb6,
                0x0c, 0x02, 0x10, 0x1e, 0x34, 0x3a, 0x28, 0x26, 0x7c, 0x72, 0x60, 0x6e, 0x44, 0x4a, 0x58, 0x56,
                0x37, 0x39, 0x2b, 0x25, 0x0f, 0x01, 0x13, 0x1d, 0x47, 0x49, 0x5b, 0x55, 0x7f, 0x71, 0x63, 0x6d,
                0xd7, 0xd9, 0xcb, 0xc5, 0xef, 0xe1, 0xf3, 0xfd, 0xa7, 0xa9, 0xbb, 0xb5, 0x9f, 0x91, 0x83, 0x8d};
        for (int i = 0; i < 256; i++) ret.add(i, tab[i]);
        return ret;
    }

    // The beta root transform for the lfsr
    public static int[][] Mbeta = new int[][]{
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1}};
    public static int[][] Mbetam1 = new int[][]{
            {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

    public static int[][] MI = new int[][]{
            {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};

    public static Tuples beta = createRelationMatrixLFSR(Mbeta);

    public static Tuples betam1 = createRelationMatrixLFSR(Mbetam1);

    public static int[] sigma = {0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};

    public static int[] getBitArray(int n, int k) {
        int[] res = new int[k];
        for (int i = 0; i < k; i++)
            res[k - 1 - i] = getBit(n, i);
        return res;
    }

    public static int[] getBitArray(int n) {
        return getBitArray(n, 8);
    }

    public static int getInt(int[] n) {
        int res = 0;
        for (int i = 0; i < n.length; i++)
            res += n[n.length - 1 - i] * Math.pow(2, i);
        return res;
    }

    public static int getBit(int n, int k) {
        return (n >> k) & 1;
    }


    public static void print(IntVar[][] S) {
        for (int i = 0; i < S.length; i++) {
            for (int j = 0; j < S[i].length; j++)
                System.out.print(S[i][j].getValue() + " ");
            System.out.println();
        }
    }

    public static void print(IntVar[] S) {
        for (IntVar s : S) System.out.print(s.getValue() + " ");
    }

    public static void print(int[] S) {
        for (int s : S) System.out.print(s + " ");
        System.out.println();
    }

    public static void print(int S) {
        System.out.println(S);
    }

    public static void print(IntVar S) {
        System.out.println(S.getValue());
    }

    public static void main(String[] args) {
//        testDeq();
//        testAESeq();
//        testLFSR();
//        for (int i = 0; i < 1; i++)
//            Guess();
        SNOW();
    }

    @Deprecated
    public static void testLFSR() {
        Model model = new Model("Deq");
        Solver solver = model.getSolver();
        IntVar Z[][] = new IntVar[][]{new IntVar[]{
                model.intVar("z", 1), model.intVar("z", 2),
                model.intVar("z", 3), model.intVar("z", 4)}, new IntVar[]{
                model.intVar("z", 5), model.intVar("z", 6),
                model.intVar("z", 7), model.intVar("z", 8)}, new IntVar[]{
                model.intVar("z", 9), model.intVar("z", 10),
                model.intVar("z", 11), model.intVar("z", 12)}, new IntVar[]{
                model.intVar("z", 13), model.intVar("z", 14),
                model.intVar("z", 15), model.intVar("z", 16)}};
//        IntVar R1 = model.intVar("z", 201);
//        IntVar R2 = model.intVar("z", 201);
//        IntVar B0 = model.intVar("z", 0, 255);
//        IntVar B1 = model.intVar("z", 0, 255);
//        model.table(new IntVar[]{R1, R2, B0, B1}, beta).post();
        IntVar[][] R1 = model.intVarMatrix("R1", 4, 4, 0, 255);
        IntVar[][] R2 = model.intVarMatrix("R1", 4, 4, 0, 255);

//        postLbeta(R1, Z, model);
        postHbeta(R2, Z, model);
        solver.setSearch(Search.inputOrderLBSearch(model.retrieveIntVars(true)));

        if (solver.solve()) {
            print(R1);
            print(R2);
        } else System.out.println("IMPOSSIBEEEEEULE");
        while (solver.solve()) ;
        solver.printShortStatistics();
    }

    @Deprecated
    public static void testDeq() {
        Model model = new Model("Deq");
        Solver solver = model.getSolver();

        IntVar A = model.intVar("A", 0, 255);
        IntVar D = model.intVar("D", 114);
        IntVar C = model.intVar("C", 0, 255);
        IntVar R2 = model.intVar("R2", 0, 255);
        IntVar R3 = model.intVar("R3", 42);

        // D = (R2 + (R3 xor A)) + (A xor C)

        //Xor parts
        IntVar R3A = model.intVar("tmpR3A", 0, 255);
        IntVar AC = model.intVar("tmpAC", 0, 255);
        new Constraint("R3 XOR A", new PropXOR(R3A, R3, A)).post();
        new Constraint("A XOR C", new PropXOR(AC, A, C)).post();

        //First modular addition
        IntVar R2R3A = model.intVar("tmpR2+R3A", 0, 255);
        IntVar TR2R3A = model.intVar("tmpR2R3A", 0, 510);
//        postSUM8(R2,R3A,R2R3A,model);
        model.arithm(TR2R3A, "=", R2, "+", R3A).post();
        model.mod(TR2R3A, 256, R2R3A).post();

        //second modular addition
        IntVar TR2R3AAC = model.intVar("tmpR2R3AAC", 0, 510);
        model.arithm(D, "=", R2R3A, "+", AC).post();
        model.mod(TR2R3AAC, 256, D).post();

        solver.setSearch(Search.inputOrderLBSearch(A, D, C, R2, R3, R3A, AC, R2R3A, TR2R3A, TR2R3AAC));
        if (solver.solve()) System.out.println(A);
        else System.out.println("IMPOSSIBEEEEEULE");
        while (solver.solve()) ;
        solver.printShortStatistics();
    }

    @Deprecated
    public static Tuples postMtable() {//générer tous les tuples est impossible marine avait une bonne raison de faire les deux ctr de mc et mc-1
        Tuples tuples = new Tuples(true);
        Model model = new Model("Deq");
        Solver solver = model.getSolver();


        IntVar v1 = model.intVar("1", 0, 255);
        IntVar v2 = model.intVar("2", 0, 255);
        IntVar v3 = model.intVar("3", 0, 255);
        IntVar v4 = model.intVar("4", 0, 255);
        IntVar S[][] = new IntVar[][]{{v1, v2, v3, v4}};

        IntVar v5 = model.intVar("5", 0, 255);
        IntVar v6 = model.intVar("6", 0, 255);
        IntVar v7 = model.intVar("7", 0, 255);
        IntVar v8 = model.intVar("8", 0, 255);
        IntVar T[][] = new IntVar[][]{{v5, v6, v7, v8}};

        postMC(T, S, model);
        System.out.println("ALO?");

        solver.setSearch(Search.inputOrderLBSearch(v1, v2, v3, v4));

        solver.showDashboard();
        while (solver.solve()) tuples.add(
                new int[]{v1.getValue(), v2.getValue(), v3.getValue(), v4.getValue()},
                new int[]{v5.getValue(), v6.getValue(), v7.getValue(), v8.getValue()});
//        for (int i = 0; i < 4; i++) System.out.println(S[0][i]);
        solver.printShortStatistics();

        System.out.println(tuples.nbTuples());
        return tuples;
    }

}
//        Constraint TR2R3A1 = model.arithm(R2R3A,"=",TR2R3A);
//        Constraint TR2R3A2 = model.arithm(R2R3A, "=", TR2R3A, "-", 256);
//        model.or(TR2R3A1.reify(),TR2R3A2.reify()).post();

//        Constraint TD1 = model.arithm(D,"=",TR2R3AAC);
//        Constraint TD2 = model.arithm(D, "=", TR2R3AAC, "-", 256);
//        model.or(TD1.reify(),TD2.reify()).post();

//    print(Zm1);
//    print(Z);
//    print(Zp1);
//    print(R1);
//    print(R2);
//    print(R3);
//    print(B0);
//    print(B1);
//    print(A0);
//    print(C);
//    print(D);
//        60 121 211 236
//        220 62 48 125
//        172 56 144 44
//        158 185 221 237

//        93 60 152 227
//        215 7 229 36
//        241 221 208 97
//        12 125 197 90

//        4 57 201 195
//        180 52 143 72
//        109 100 37 24
//        133 80 189 12

//        9 138 10 167
//        124 77 243 155
//        186 234 60 202
//        79 128 153 37

//        197 231 178 56
//        215 224 207 195
//        223 23 105 171
//        81 222 199 249

//        227 65 145 138
//        110 117 184 129
//        21 95 184 77
//        172 129 196 228

//        229 122 68 139
//        85 53 205 42
//        88 226 237 168
//        94 74 217 8

//        143 81 32 25
//        132 153 55 146
//        116 223 124 110
//        14 35 105 235

//        103 90 109 48
//        50 76 159 16
//        212 185 16 84
//        19 240 94 240

//        196 181 12 2
//        92 98 147 191
//        128 201 199 216
//        254 110 41 75

//        236 34 2 34
//        113 72 10 72
//        3 104 233 104
//        240 184 117 184

//        60 220 172 158
//        121 62 56 185
//        211 48 144 221
//        236 125 44 237
//        93 215 241 12
//        60 7 221 125
//        152 229 208 197
//        227 36 97 90
//        4 180 109 133
//        57 52 100 80
//        201 143 37 189
//        195 72 24 12
//        9 124 186 79
//        138 77 234 128
//        10 243 60 153
//        167 155 202 37
//        197 215 223 81
//        231 224 23 222
//        178 207 105 199
//        56 195 171 249
//        227 110 21 172
//        65 117 95 129
//        145 184 184 196
//        138 129 77 228
//        229 85 88 94
//        122 53 226 74
//        68 205 237 217
//        139 42 168 8
//        143 132 116 14
//        81 153 223 35
//        32 55 124 105
//        0 7 0 0
//        99 2 52 35
//        106 74 57 246
//        13 31 199 222
//        49 149 77 228
//        196 92 128 254
//        181 98 201 110
//        12 147 199 41
//        49 149 5 157
//        236 113 3 240
//        34 72 104 184
//        2 10 233 117
//        34 72 104 184
